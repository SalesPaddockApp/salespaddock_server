/**
 * Created by Niranjan on 27/01/2017.
 * function name: deletePost
 * request: postId
 * response: succuss response if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var async = require('async');
var ObjectId = require('mongodb').ObjectID;


exports.GetAroundYouPosts = function (req, res) {

    if (!req.body.userId) {
        res.send({ errCode: 1, errNum: 132, Message: "mandatory userId  is missing" });
        return false;
    }
    if (!req.body.lat) {
        res.send({ errCode: 1, errNum: 132, Message: "mandatory lat  is missing" });
        return false;
    }
    if (!req.body.long) {
        res.send({ errCode: 1, errNum: 132, Message: "mandatory long  is missing" });
        return false;
    }
    if (!req.body.redious) {
        res.send({ errCode: 1, errNum: 132, Message: "mandatory redious  is missing" });
        return false;
    }

    let redious = parseInt(req.body.redious);

    var pageNum = 0;
    if (req.body.pageNo)
        pageNum = req.body.pageNo;

    var limit = 100;
    var skip = pageNum * limit;
    console.log("redious ", redious)
    var lat = parseFloat(req.body.lat);
    var long = parseFloat(req.body.long);


    var condition = [
        {
            $geoNear: {
                near: { longitude: long, latitude: lat }, spherical: true,
                distanceMultiplier: 6378.1, distanceField: "dist", num: 100000
            }
        },
        { "$sort": { "dist": -1 } },
        { "$match": { "activeStatus": "1", "dist": { "$lte": redious } } },
        { "$lookup": { "from": "wishList", "localField": "_id", "foreignField": "postId", "as": "Data" } },
        {
            "$unwind": {
                "path": "$Data",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$project": {
                "userId": 1, "activeStatus": 1, "location": 1, "created": 1,
                "values": 1,
                "productType": 1, "modified": 1,
                "productId": 1, "dist": 1,
                "distType": {
                    "$cond": [
                        { "$eq": [{ "$mod": ["$dist", 2] }, 0] },
                        "even",
                        "odd"
                    ]
                },
                "isMyfavoritPost": {
                    "$cond": { "if": { "$eq": ["$Data.userId", req.body.userId] }, "then": true, "else": false }
                }
            }
        },
        {
            "$group": {
                "_id": "$_id",
                "userId": { "$first": "$userId" },
                "activeStatus": { "$first": "$activeStatus" },
                "location": { "$first": "$location" },
                "created": { "$first": "$created" },
                "values": { "$first": "$values" },
                "productType": { "$first": "$productType" },
                "modified": { "$first": "$modified" },
                "productId": { "$first": "$productId" },
                "dist": { "$first": "$dist" },
                "distType": { "$first": "$distType" },
                "isMyfavoritPost": { "$addToSet": "$isMyfavoritPost" },

            }
        },
        // { "$match": { "distType": "even" } },
        { "$skip": skip },
        { "$limit": limit }
    ];

    Utility.AggregateGroup('post', condition, function (err, result) {
        if (err) {
            res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
            return false;
        } else if (result && result[0]) {
            var dataTosend = [];
            for (index = 0; index < result.length; index++) {
                data = {};
                data["_id"] = result[index]["_id"];
                data["location"] = result[index]["location"];
                data["dist"] = result[index]["dist"];
                for (values = 0; values < result[index]["values"].length; values++) {

                    if (result[index]["values"][values]["pref_id"] == "57efa0aa00eba37a3dd379cd" &&
                        result[index]["values"][values]["options"][0]) {
                        data["name"] = result[index]["values"][values]["options"][0];
                    } else if (result[index]["values"][values]["pref_id"] == "57f79d6700eba39c7b2f80f1" &&
                        result[index]["values"][values]["options"][0]) {
                        data["image"] = result[index]["values"][values]["options"][0]["thumbnailUrl"] || result[index]["values"][values]["options"][0]["imageUrl"];
                    }
                    if (result[index].isMyfavoritPost && (result[index].isMyfavoritPost[0] || result[index].isMyfavoritPost[1])) {
                        data["isMyfavoritPost"] = true;
                    } else {
                        data["isMyfavoritPost"] = false;
                    }
                }
                dataTosend.push(data);

            }
            console.log("result 1 ", result.length);
            console.log("dataTosend 1 ", dataTosend.length);
            res.send({ errCode: 0, Message: "data found successfully.", response: dataTosend });
            return true;
        } else {
            res.send({ errCode: 0, Message: "No have data for this location.", response: [] });
            return true;
        }
    });
}