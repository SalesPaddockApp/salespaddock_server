/**
 * Created by Niranjan on 27/01/2017.
 * function name: deletePost
 * request: postId
 * response: succuss response if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;
var Distance = require('geo-distance');


exports.GetAroundYouShows = function (req, res) {

    if (!req.body.userId) {
        res.send({ errCode: 1, errNum: 132, Message: "mandatory userId  is missing" });
        return false;
    }
    if (!req.body.lat) {
        res.send({ errCode: 1, errNum: 132, Message: "mandatory lat  is missing" });
        return false;
    }
    if (!req.body.long) {
        res.send({ errCode: 1, errNum: 132, Message: "mandatory long  is missing" });
        return false;
    }
    if (!req.body.redious) {
        res.send({ errCode: 1, errNum: 132, Message: "mandatory redious  is missing" });
        return false;
    }

    let redious = parseInt(req.body.redious);
    // let pageNo = parseInt(req.body.pageNo) || 0;
    // let skip = ((pageNo) * 20);
    // let limit = skip * 2 || 20;

    var pageNum = 0;
    if (req.body.pageNo)
        pageNum = req.body.pageNo;

    var limit = 50;
    var skip = 50 * pageNum;


    var lat = parseFloat(req.body.lat);
    var long = parseFloat(req.body.long);

    var curDate = Date.now();
    var cond = [
        { $match: { "active": "1" } },
        {
            $project: {
                showName: 1, imgPath: 1, lat: 1, lng: 1, startDate: 1, endDate: 1,
                placeName: "$venue", "Description": 1
            }
        },
        {
            $match: {
                $or: [{ startDate: { $lte: curDate }, endDate: { $gte: curDate } },
                { startDate: { $gte: curDate } }]
            }
        },
        { "$skip": skip },
        { "$limit": limit }

    ];

    Utility.AggregateGroup('shows', cond, function (err, result) {
        if (err) {
            console.log("err", err);
            res.send({ errCode: 1, Message: "Unknown error occurred" });
            return false;
        }
        else if (result.length) {

            var currentLocation = {
                lon: long,
                lat: lat
            }
            var targetLocation = {};
            var dataToSend = [];

            for (index = 0; index < result.length; index++) {
                targetLocation = { lon: result[index]["lng"], lat: result[index]["lat"] };
                result[index]["distance"] = Distance.between(currentLocation, targetLocation);
                result[index]["dist"] = result[index]["distance"].human_readable();

                if (result[index]['startDate']) {
                    var mainDate1 = new Date(parseFloat(result[index]['startDate']));

                    var datee1 = mainDate1.getDate();
                    var month1 = mainDate1.getMonth() + 1;
                    var year1 = mainDate1.getFullYear();

                    if (datee1 < 10)
                        datee1 = "0" + datee1;

                    if (month1 < 10)
                        month1 = "0" + month1;

                    result[index]['startDate'] = month1 + "/" + datee1 + "/" + year1;

                }
                if (result[index]['endDate']) {
                    var mainDate = new Date(parseFloat(result[index]['endDate']));

                    var datee = mainDate.getDate();
                    var month = mainDate.getMonth() + 1;
                    var year = mainDate.getFullYear();

                    if (datee < 10)
                        datee = "0" + datee;

                    if (month < 10)
                        month = "0" + month;

                    result[index]['endDate'] = month + "/" + datee + "/" + year;

                }
                if (result[index]["distance"] < Distance(redious + ' km')) {
                    result[index]["distance"] = result[index]["distance"].human_readable();
                    dataToSend.push(result[index]);
                }


            }
            // dataToSend.sort(function (a, b) {
            //     return a["distance"]["distance"] - b["distance"]["distance"];
            // });
            console.log("redious : ", redious)
            console.log(" dataToSend.length : ", dataToSend.length)
            // for (let index = 0; index < dataToSend.length; index++) {
            //     console.log("km====>>>", dataToSend[index]["distance"]["distance"])
            // }

            res.send({ errCode: 0, Message: "data send successfully.", response: dataToSend });

        }
        else {
            res.send({ errCode: 0, Message: "No have data for this location.", response: [] });
            return false;
        }
    });
}