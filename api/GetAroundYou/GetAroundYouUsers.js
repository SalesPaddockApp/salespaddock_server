/**
 * Created by Niranjan on 27/01/2017.
 * function name: deletePost
 * request: postId
 * response: succuss response if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;


exports.GetAroundYouUsers = function (req, res) {

    if (!req.body.userId) {
        res.send({ errCode: 1, errNum: 133, Message: "mandatory userId  is missing" });
        return false;
    }
    if (!req.body.lat) {
        res.send({ errCode: 1, errNum: 133, Message: "mandatory lat  is missing" });
        return false;
    }
    if (!req.body.long) {
        res.send({ errCode: 1, errNum: 133, Message: "mandatory long  is missing" });
        return false;
    }

    if (!req.body.redious) {
        res.send({ errCode: 1, errNum: 133, Message: "mandatory redious  is missing" });
        return false;
    }

    let redious = parseInt(req.body.redious);
    // let pageNo = parseInt(req.body.pageNo) || 0;
    // let skip = ((pageNo) * 20);
    // let limit = skip * 2 || 20;

    var pageNum = 0;
    if (req.body.pageNo)
        pageNum = req.body.pageNo;

    var limit = 100;
    var skip = pageNum * limit;


    var lat = parseFloat(req.body.lat);
    var long = parseFloat(req.body.long);

    var condition = [
        {
            $geoNear: {
                near: { longitude: long, latitude: lat }, spherical: true,
                distanceMultiplier: 6378.1, distanceField: "dist", num: 100000
            }
        },
        { "$sort": { "dist": -1 } },
        { "$match": { "dist": { "$lte": redious } } },
        {
            $project: {
                "name": { $concat: ["$name.fName", " ", "$name.lName"] },
                "image": "$profilePic", "location": "$location1", "dist": 1, "placeName": "$location", "userId": 1
            }
        }, { "$lookup": { "from": "post", "localField": "userId", "foreignField": "userId", "as": "data" } },
        {
            "$unwind": {
                "path": "$data",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$match": { "data.activeStatus": "1" } },
        {
            "$group": {
                "_id": "$_id",
                "userId": { "$last": "$userId" },
                "name": { "$last": "$name" },
                "location": { "$last": "$location" },
                "image": { "$last": "$image" },
                "placeName": { "$last": "$placeName" },
                "totalActivePost": { "$sum": 1 },
                "dist": { "$last": "$dist" }
            }
        },
        {
            "$project": {
                "userId": 1, "name": 1, "location": 1, "totalActivePost": 1, "dist": 1,
                "placeName": { "$cond": { "if": { "$eq": [null, "$placeName"] }, "then": "", else: "$placeName" } },
                "image": {
                    "$cond": { "if": { "$eq": [null, "$image"] }, "then": "", else: "$image" },
                }
            }
        },
        { "$skip": skip },
        { "$limit": limit }
    ];

    Utility.AggregateGroup('user', condition, function (err, result) {
        if (err) {
            res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
            return false;
        }
        else if (result[0]) {

            res.send({ errCode: 0, Message: "data found successfully.", response: result });
            return true;
        }
        else {
            res.send({ errCode: 0, Message: "No have data for this location.", response: [] });
            return true;
        }
    });
}