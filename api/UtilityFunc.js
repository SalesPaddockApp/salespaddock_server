/**importing connection file. And creating object of connection to use it.**/

/*  Utility file to handle MongoDB Query Operations */
var Utility = module.exports = {};


var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
// Connection URL. This is where your mongodb server is running.
var url = 'mongodb://alison:fSJMep6GvR7m4755sd@35.163.250.170:27017/salespaddock?maxPoolSize=50';

var db;
MongoClient.connect(url, function (err, connDB) {
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    } else {
        //console.log('Connected to MongoDB');
        console.log("hello everytime created");
        db = connDB;
    }
});

/**Insert function is use to insert parameters to database.
 * It's a generic function, tablename is use for Tablename in the db.
 * data is array to insert documents in table.
 * if inserted then go callback function else error.
 * **/
Utility.Insert = function(tablename, data,callback)
{
    //conn.DbConnection(function(result)
    //{
//        var db = result;
    var collection = db.collection(tablename);

    collection.insert([data], function (err, result) {

        if (err) {
            console.log(err);
        } else {
            return callback(err,result);
        }

    });
    //});
};



/**Select function is use to fetch data from database.
 * It's a generic function, tablename is use for Tablename in the db.
 * data is array to provide where condition in table.
 * if success then go callback function else error.
 * Result is use to store all data resulted from select function.
 * **/
Utility.Select = function(tablename,data,callback){

//    conn.DbConnection(function(result){
//        var db = result;
        var collection = db.collection(tablename);

        collection.find(data).toArray(function (err, result){
           if(err){
               console.log(err);
           } else{
               return callback(err,result);
           }
        });
//    });
}



/**Select function is use to fetch data from database.
 * It's a generic function, tablename is use for Tablename in the db.
 * data is array to provide where condition in table.
 * if success then go callback function else error.
 * Result is use to store all data resulted from select function.
 * **/
Utility.SelectMatched = function(tablename,condition,data,callback){
//    conn.DbConnection(function(result){
//        var db = result;
        var collection = db.collection(tablename);

        collection.find(condition,data).toArray(function (err, result){
           if(err){
               console.log(err);
           } else{
               return callback(err,result);
           }
        });
//    });
}



/**Select function is use to fetch data from database.
 * It's a generic function, tablename is use for Tablename in the db.
 * data is array to provide where condition in table.
 * if success then go callback function else error.
 * Result is use to store all data resulted from select function.
 * **/
Utility.SelectWithSorting = function(tablename,condition,data,callback){
//    conn.DbConnection(function(result){
//        var db = result;
        var collection = db.collection(tablename);

        collection.find(condition).sort(data).toArray(function (err, result){
           if(err){
               console.log(err);
           } else{
               return callback(err,result);
           }
        });
//    });
}



/**Select function is use to fetch data from database.
 * It's a generic function, tablename is use for Tablename in the db.
 * data is array to provide where condition in table.
 * if success then go callback function else error.
 * Result is use to store all data resulted from select function.
 * **/
Utility.SelectWithPagination = function(tablename,data,projectData,skipData,limitData,callback){
//    conn.DbConnection(function(result){
//        var db = result;
        var collection = db.collection(tablename);

        collection.find(data,projectData).skip(skipData).limit(limitData).toArray(function (err, result){
            if(err){
                console.log(err);
            } else{
                return callback(err,result);
            }
        });
//    });
}



/**Select function is use to fetch data from database.
 * It's a generic function, tablename is use for Tablename in the db.
 * data is array to provide where condition in table.
 * if success then go callback function else error.
 * Result is use to store all data resulted from select function.
 * **/
Utility.SelectWithSoringPagination = function(tablename,data,projectData,sortData,skipData,limitData,callback){
//    conn.DbConnection(function(result){
//        var db = result;
        var collection = db.collection(tablename);

        collection.find(data,projectData).sort(sortData).skip(skipData).limit(limitData).toArray(function (err, result){
            if(err){
                console.log(err);
            } else{
                return callback(err,result);
            }
        });
//    });
}

/**SelectMatch function is use to fetch data from database but fetch only that array which matches to condition from table.
 * It's a generic function, tablename is use for Tablename in the db.
 * data is array to provide where condition in table.
 * if success then go callback function else error.
 * Result is use to store all data resulted from select function.
 * **/
Utility.SelectMatch = function(tablename,condition,data,callback){
//    conn.DbConnection(function(result){
//        var db = result;
        var collection = db.collection(tablename);

        collection.findOne(condition,data,(function (err, result){
            if(err){
                console.log(err);
            } else{
                return callback(err,result);
            }

        }));
//    });
}

/**AggregateMatch function is use to fetch data from database but fetch only that array which matches to condition from table.
 * It's a generic function, tablename is use for Tablename in the db.
 * data is array to provide where condition in table.
 * if success then go callback function else error.
 * Result is use to store all data resulted from select function.
 * **/
Utility.AggregateMatch = function(tablename,condition,data,callback){
//    conn.DbConnection(function(result){
//        var db = result;
        var collection = db.collection(tablename);

        collection.aggregate(condition,data,(function (err, result){
            if(err){
                console.log(err);
            } else{
                return callback(err,result);
            }

        }));
//    });
}

/**AggregateGroup function is use to fetch data from database but fetch only that array which matches to condition from table.
 * It's a generic function, tablename is use for Tablename in the db.
 * data is array to provide where condition in table.
 * if success then go callback function else error.
 * Result is use to store all data resulted from select function.
 * **/
Utility.AggregateGroup = function(tablename,condition,callback){
//    conn.DbConnection(function(result){
//        var db = result;
        var collection = db.collection(tablename);

        collection.aggregate(condition,(function (err, result){
            if(err){
                console.log(err);
            } else{
                return callback(err,result);
            }

        }));
//    });
}


/**AggregateSort function is use to fetch data from database but fetch only that array which matches to condition from table.
 * It's a generic function, tablename is use for Tablename in the db.
 * data is array to provide where condition in table.
 * if success then go callback function else error.
 * Result is use to store all data resulted from select function.
 * **/
Utility.AggregateSort = function(tablename,condition,data,sortData,callback){
//    conn.DbConnection(function(result){
//        var db = result;
        var collection = db.collection(tablename);

        collection.aggregate(condition,data,sortData,(function (err, result){
            if(err){
                console.log(err);
            } else{
                return callback(err,result);
            }

        })  );
//    });
}


/**count function is use to fetch count of total data from database.
 * It's a generic function, tablename is use for Tablename in the db.
 * data is array to provide where condition in table.
 * if success then go callback function else error.
 * Result is use to store all data resulted from select function.
 * **/
Utility.Count = function(tablename,data,callback){
//    conn.DbConnection(function(result){
//        var db = result;
        var collection = db.collection(tablename);

        collection.count(data,(function (err, result){
            if(err){
                console.log(err);
            } else{
                return callback(err,result);
            }
        }));
//    });
}


/**SelectOne function is use to fetch data from database but fetch only one row from table.
 * It's a generic function, tablename is use for Tablename in the db.
 * data is array to provide where condition in table.
 * if success then go callback function else error.
 * Result is use to store all data resulted from select function.
 * **/
Utility.SelectOne = function(tablename,data,callback){
//    conn.DbConnection(function(result){
//        var db = result;
        var collection = db.collection(tablename);

        collection.findOne(data,(function (err, result){
           if(err){
               console.log(err);
           } else{
               return callback(err,result);
           }

        }));
//    });
}


/**Update function is use to Update parameters to database.
 * It's a generic function, tablename is use for Tablename in tnullhe db.
 * data is array to Update documents in table.
 * if Updated then go callback function else error.
 * **/
Utility.Update = function(tablename,condition,data,callback){
//    conn.DbConnection(function(result){
//        var db = result;

        var collection = db.collection(tablename);
        collection.update(condition,{$set : data},{multi: true},(function (err, result){
           if(err){
               console.log(err);
           } else{
               return callback(err,result);
           }

        }));
//    });
}


/**UpdateMulti function is use to Update parameters to database.
 * It's a generic function, tablename is use for Tablename in tnullhe db.
 * data is array to Update documents in table.
 * if Updated then go callback function else error.
 * **/
Utility.UpdateMulti = function(tablename,condition,data,callback){
//    conn.DbConnection(function(result){
//        var db = result;
        var collection = db.collection(tablename);
        collection.update(condition,{$set : data},{multi:true},(function (err, result){
           if(err){
               console.log(err);
           } else{
               return callback(err,result);
           }

        }));
//    });
}


/**UpdateField function is use to delete parameters to database but selected one from array.
 * It's a generic function, tablename is use for Tablename in the db.
 * data is array to give the condition.
 * if deleted then go callback function else error.
 * **/
Utility.UpdateField = function(tablename,condition,data,callback){
//    conn.DbConnection(function(result){
//        var db = result;
        var collection = db.collection(tablename);
        collection.update(condition,{$pull : data},(function (err, result){
            if(err){
                console.log(err);
            } else{
                return callback(err,result);
            }

        }));
//    });

};


/**UpdateAsInsertSpecificField function is use to insert parameters to database but selected one to array.
 * It's a generic function, tablename is use for Tablename in the db.
 * data is array to give the condition.
 * if deleted then go callback function else error.
 * **/
Utility.UpdateAsInsertSpecificField = function(tablename,condition,data,callback){
//    conn.DbConnection(function(result){
//        var db = result;
        var collection = db.collection(tablename);
        collection.update(condition,{$push : data},(function (err, result){
            if(err){
                console.log(err);
            } else{
                return callback(err,result);
            }

        }));
//    });

};


/**AddIntoArray function is use to insert parameters to database but selected one to array.
 * It's a generic function, tablename is use for Tablename in the db.
 * data is array to give the condition.
 * if deleted then go callback function else error.
 * **/
Utility.AddIntoArray = function(tablename,condition,data,callback){
//    conn.DbConnection(function(result){
//        var db = result;
        var collection = db.collection(tablename);
        collection.update(condition,{$addToSet : data},(function (err, result){
            if(err){
                console.log(err);
            } else{
                return callback(err,result);
            }

        }));
//    });

};



/**UpdateMultilpe function is use to update multiple documents to database.
 * It's a generic function, tablename is use for Tablename in the db.
 * data is array to give the condition.
 * if deleted then go callback function else error.
 * **/
Utility.UpdateMultilpe = function(tablename,condition,data,callback){
//    conn.DbConnection(function(result){
//        var db = result;
        var collection = db.collection(tablename);
        collection.updateMany(condition,{$set : data},(function (err, result){
            if(err){
                console.log(err);
            } else{
                return callback(err,result);
            }

        }));
//    });

};


/**Delete function is use to delete parameters to database.
 * It's a generic function, tablename is use for Tablename in the db.
 * data is array to give the condition.
 * if deleted then go callback function else error.
 * **/
Utility.Delete = function(tablename,condition,callback){
//    conn.DbConnection(function(result){
//        var db = result;
        var collection = db.collection(tablename);
        collection.remove(condition, function(err, numberOfRemovedDocs) {
            if(err){
                console.log(err);
            } else{
                return callback(err,numberOfRemovedDocs);
            }
            //assert.equal(null, err);
            //assert.equal(1, numberOfRemovedDocs);
            //db.close();
        });
//    });
};

