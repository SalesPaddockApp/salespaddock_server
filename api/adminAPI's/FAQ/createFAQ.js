/**
 * Created by Niranjan on 04/01/17.
 * function name: createFAQ
 * request: question,answer
 * response: succuss response if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');

exports.createFAQ = function (req, res)
{

    if (!req.body.question)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory question is missing"});
        return false;
    }
    if (!req.body.answer)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory answer is missing"});
        return false;
    }
 

    var dataToInsert = {
        question: req.body.question,
        answer: req.body.answer
    };

    Utility.Insert('FAQ', dataToInsert, function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        } else if (result)
        {
            res.send({errCode: 0, Message: "you have created one Recored successfully.", response: {DataId: result['ops'][0]['_id']}});
            return true;
        } else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no data is there, please try after some time."});
            return true;
        }
    });
}






