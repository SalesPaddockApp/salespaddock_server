/**
 * Created by Niranjan on 04/01/17.
 * function name: createFAQ
 * request: question,answer
 * response: succuss response if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;

exports.editFAQ = function (req, res)
{

if (!req.body._id)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory _id is missing"});
        return false;
    }
    if (!req.body.question)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory question is missing"});
        return false;
    }
    if (!req.body.answer)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory answer is missing"});
        return false;
    }
 

    var dataToInsert = {
        question: req.body.question,
        answer: req.body.answer
    };
    var cond = {_id:ObjectId(req.body._id)};

    Utility.Update('FAQ',cond, dataToInsert, function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        } else if (result)
        {
            res.send({errCode: 0, Message: "you have update one Recored successfully."});
            return true;
        } else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no data is there, please try after some time."});
            return true;
        }
    });
}






