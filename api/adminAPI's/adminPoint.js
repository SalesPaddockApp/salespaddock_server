
    express = require("express"),
        bodyParser = require("body-parser"),
        morgan = require("morgan"),
        cors = require("cors");
    jwt = require("jsonwebtoken");
    app = express();
    app.use(bodyParser.json());
    app.use(cors({ credentials: true, origin: true }));


    console.log("You are connected with:  " + 2020);


    app.use(function(req, res, next) {
        /**
         * req.headers.horse this is used for side
         * req.headers.authorization this is for postman
         */
        if (!req.headers.horse) {
            console.log('basic auth: ' + JSON.stringify(req.headers))
            if (!req.headers.authorization) {
                res.status(401).json({ errFlag: '1', errMsg: 'error', data: 'Unauthorised Access' });
            } else {
                console.log(req.headers.authorization);
                var auth = 'Basic ' + new Buffer("horse:123456").toString('base64');
                if (req.headers.authorization.toString() == auth) {
                    console.log("working...");
                    next();
                } else
                    res.status(401).json({ errFlag: '1', errMsg: 'Unauthorised Access' });
            }
        } else {
            console.log('horse auth: ' + JSON.stringify(req.headers));
            var auth = req.headers.horse;
            if ('123456' === auth) {
                next();
            } else
                res.status(401).json({ errFlag: '1', errMsg: 'Unauthorised Access' });
        }
    });
    app.get('/', function(req, res) {
        console.log("get request");
        res.json({ Message: "you are connected." })
    });
    var validateEmail = require('./register/validateEmail');
    app.post('/validateEmail', validateEmail.validateEmail);
    var api = require('./login/login')(app, express);
    app.use('/api', api);
    /*
     *function name: Admin Login.
     * Desc: this function is use to login Admin.
     * req: EmailId,Password.
     * response: success response if no error else error.
     *  */
    var api = require('./login/adminLogin')(app, express);
    app.use('/api', api);
    var getShows = require('./shows/getShows');
    app.post('/getShows', getShows.getShows);
    var getShowDetails = require('./shows/getShowDetails');
    app.post('/getShowDetails', getShowDetails.getShowDetails);
    var getSummary = require('./shows/getSummary');
    app.post('/getSummary', getSummary.getSummary);
    api = require('./register/register')(app, express);
    app.use('/api', api);
    api = require('./profile/editProfile')(app, express);
    app.use('/api', api);
    api = require('./preference/getPreference')(app, express);
    app.use('/api', api);
    var productType = require('./productType/productType');
    app.post('/prodType', productType.productType);
    /*
     *function name: adminProductType.
     * Desc: this function is use to get All ProductType.
     * req: none.
     * response: success response if no error else error.
     *  */
    var productType = require('./productType/getProducttypes');
    app.post('/getProducttypes', productType.getProducttypes);
    /*
     *function name: getProductTypeById.
     * Desc: this function is use to get All ProductType.
     * req: _id.
     * response: success response if no error else error.
     *  */
    var productTypeById = require('./productType/getProductTypeById');
    app.post('/getProductTypeById', productTypeById.getProductTypeById);
    /*
     *function name: searchActivePost.
     * Desc: this function is use to update the details of a show,(eg: horses details).
     * req: SearchP.
     * response: success response if no error else error.
     *  */
    var searchActivePost = require('./posts/searchActivePost');
    app.post('/searchActivePost', searchActivePost.searchActivePost);
    /*
     *function name: searchInActivePost.
     * Desc: this function is use to update the details of a show,(eg: horses details).
     * req: SearchP.
     * response: success response if no error else error.
     *  */
    var searchInActivePost = require('./posts/searchInActivePost');
    app.post('/searchInActivePost', searchInActivePost.searchInActivePost);
    /*
     *function name: createProducttype.
     * Desc: this function is use to insert ProductType.
     * req: product Name.
     * response: success response if no error else error.
     *  */
    var productType = require('./productType/createProducttype');
    app.post('/createProducttype', productType.createProducttype);
    /*
     *function name: deleteProducttype.
     * Desc: this function is use to Delete ProductType.
     * req: product Name.
     * response: success response if no error else error.
     *  */
    var productType = require('./productType/deleteProducttype');
    app.post('/deleteProducttype', productType.deleteProducttype);
    /*
     *function name: editProducttype.
     * Desc: this function is use to update ProductType.
     * req: product Name,_id.
     * response: success response if no error else error.
     *  */
    var productType = require('./productType/editProducttype');
    app.post('/editProducttype', productType.editProducttype);
    /*
     *function name: searchProducttypes.
     * Desc: this function is use to Search ProductType.
     * req: productName.
     * response: success response if no error else error.
     *  */
    var productType = require('./productType/searchProducttypes');
    app.post('/searchProducttypes', productType.searchProducttypes);
    /*
     *function name: getPrefSettings.
     * Desc: this function is use to get preference.
     * req: .
     * response: success response if no error else error.
     *  */
    var preference = require('./preference/getPrefsettings');
    app.post('/getPrefsettings', preference.getPrefsettings);
    /*
     *function name: getPrefToCreatePost.
     * Desc: this function is use to get preference with Product name(defualt pref).
     * req: .
     * response: success response if no error else error.
     *  */
    var preference = require('./preference/getPrefToCreatePost');
    app.post('/getPrefToCreatePost', preference.getPrefToCreatePost);
    /*
     *function name: createPrefSetting.
     * Desc: this function is use to create preference.
     * req: preferenceTitle,onCreateScreen,typeOfPreference,productType,
     mandatory,prirority,Unit,optionsValue[].
     * response: success response if no error else error.
     *  */
    var preference = require('./preference/createPrefSetting');
    app.post('/createPrefSetting', preference.createPrefSetting);
    /*
     *function name: editPrefSettng.
     * Desc: this function is use to create preference.
     * req: _id,optional filed (preferenceTitle,onCreateScreen,productType,mandatory,Unit,optionsValue).
     * response: success response if no error else error.
     *  */
    var preference = require('./preference/editPrefSettng');
    app.post('/editPrefSettng', preference.editPrefSettng);
    /*
     *function name: deletePrefSetting.
     * Desc: this function is use to Delete preference.
     * req: _id.
     * response: success response if no error else error.
     *  */
    var preference = require('./preference/deletePrefSetting');
    app.post('/deletePrefSetting', preference.deletePrefSetting);
    /*
     *function name: searchPrefSettings.
     * Desc: this function is use to Search preferenceTitle.
     * req: _id.
     * response: success response if no error else error.
     *  */
    var preference = require('./preference/searchPrefSettings');
    app.post('/searchPrefSettings', preference.searchPrefSettings);
    /*
     *function name: incPriority.
     * Desc: this function is use to incerece  priority.
     * req: _id.
     * response: success response if no error else error.
     *  */
    var preference = require('./preference/incPriority');
    app.post('/incPriority', preference.incPriority);
    /*
     *function name: decPriority.
     * Desc: this function is use to decerece priority.
     * req: _id.
     * response: success response if no error else error.
     *  */
    var preference = require('./preference/decPriority');
    app.post('/decPriority', preference.decPriority);
    /*
     *function name: getPrefSettingById.
     * Desc: this function is use get prefrancedata by id.
     * req: _id.
     * response: success response if no error else error.
     *  */
    var preference = require('./preference/getPrefSettingById');
    app.post('/getPrefSettingById', preference.getPrefSettingById);

    /*
     *function name: getDeviceLog.
     * Desc: this function is use get Device Log.
     * req: _id.
     * response: success response if no error else error.
     *  */
    var getDeviceLog = require('./users/getDeviceLog');
    app.post('/getDeviceLog', getDeviceLog.getDeviceLog);
    /*
     *function name: getUser.
     * Desc: this function is use get All user.
     * req: .
     * response: success response if no error else error.
     *  */
    var users = require('./users/getUser');
    app.post('/getUser', users.getUser);
    /*
     *function name: getUserById.
     * Desc: this function is use get All user.
     * req: _id.
     * response: success response if no error else error.
     *  */
    var users = require('./users/getUserById');
    app.post('/getUserById', users.getUserById);
    /*
     *function name: editUser.
     * Desc: this function is use to editUser.
     * req: all data with id.
     * response: success response if no error else error.
     *  */
    var users = require('./users/editUser');
    app.post('/editUser', users.editUser);
    /*
     *function name: makeUserActive.
     * Desc: this function is use to mack Active user,user active=1,inactive=0.
     * req: _id.
     * response: success response if no error else error.
     *  */
    var userActive = require('./users/makeUserActive');
    app.post('/makeUserActive', userActive.makeUserActive);
    /*
     *function name: makeUserInactive.
     * Desc: this function is use to mack Active user,user active=1,inactive=0.
     * req: _id.
     * response: success response if no error else error.
     *  */
    var userInactive = require('./users/makeUserInactive');
    app.post('/makeUserInactive', userInactive.makeUserInactive);
    /*
     *function name: getActiveUsers.
     * Desc: this function is use to get Active user,user active=1,inactive=0.
     * req: .
     * response: success response if no error else error.
     *  */
    var getuserActive = require('./users/getActiveUsers');
    app.post('/getActiveUsers', getuserActive.getActiveUsers);
    /*
     *function name: getInactiveUsers.
     * Desc: this function is use to get Inactive user,user active=1,inactive=0.
     * req: .
     * response: success response if no error else error.
     *  */
    var getuserInactive = require('./users/getInactiveUsers');
    app.post('/getInactiveUsers', getuserInactive.getInactiveUsers);
    /*
     *function name: getUserBy_Id.
     * Desc: this function is use to get  user,user active=1,inactive=0.
     * req: _id.
     * response: success response if no error else error.
     *  */
    var getUserBy_Id = require('./users/getUserBy_Id');
    app.post('/getUserBy_Id', getUserBy_Id.getUserBy_Id);
    /*
     *function name: deleteUser.
     * Desc: this function is use to deleteUser.
     * req: _id.
     * response: success response if no error else error.
     *  */
    var users = require('./users/deleteUser');
    app.post('/deleteUser', users.deleteUser);
    /*
     *function name: searchUser.
     * Desc: this function is use to searchUser.
     * req: searchP.
     * response: success response if no error else error.
     *  */
    var users = require('./users/searchUser');
    app.post('/searchUser', users.searchUser);
    var viewProfile = require('./profile/viewProfile');
    app.post('/viewProf', viewProfile.viewProfile);
    var addWish = require('./wishList/addWish');
    app.post('/addWish', addWish.addWish);
    var removeWish = require('./wishList/removeWish');
    app.post('/removeWish', removeWish.removeWish)

    var getShowForUsers = require('./shows/getShowForUsers');
    app.post('/getShowForUsers', getShowForUsers.getShowForUsers);
    var uploadImage = require('./uploadImage/uploadImage');
    app.post('/uploadImage', uploadImage.uploadImage);
    var logout = require('./logout/logout');
    app.post('/logout', logout.logout);
    /*
     *function name: createPost.
     * Desc: this function is use to create the posts(eg: horses) of a shows..
     * req: token,userId,postId,post data.
     * response: success response if no error else error.
     *  */
    var createPost = require('./posts/createPost');
    app.post('/createPost', createPost.createPost);
    /*
     *function name: updatePost.
     * Desc: this function is use to update the details of a show,(eg: horses details).
     * req: token,userId,postId,data which have to update.
     * response: success response if no error else error.
     *  */
    var updatePost = require('./posts/updatePost');
    app.post('/updatePost', updatePost.updatePost);
    /*
     *function name: getpost.
     * Desc: this function is use to get Post.
     * req: .
     * response: success response if no error else error.
     *  */
    var getPostData = require('./posts/getPosts');
    app.post('/getPosts', getPostData.getPosts);
    /*
     *function name: getAdminpostById.
     * Desc: this function is use to get Post.
     * req: _id.//here horsename used static key
     * response: success response if no error else error.
     *  */
    var getPostData = require('./posts/getAdminpostById');
    app.post('/getAdminpostById', getPostData.getAdminpostById);
    /*
     *function name: getAdminpostByIdToView.
     * Desc: this function is use to get Post for View.
     * req: _id.
     * response: success response if no error else error.
     *  */
    var getPostData = require('./posts/getAdminpostByIdToView');
    app.post('/getAdminpostByIdToView', getPostData.getAdminpostByIdToView);
    /*
     *function name: deletePost.
     * Desc: this function is use to delete post.
     * req: _id.
     * response: success response if no error else error.
     *  */
    var getPostData = require('./posts/deletePost');
    app.post('/deletePost', getPostData.deletePost);
    /*
     *function name: searchPosts.
     * Desc: this function is use to search Posts.
     * req: searchP.//here horsename used static key
     * response: success response if no error else error.
     *  */
    var getPostData = require('./posts/searchPosts');
    app.post('/searchPosts', getPostData.searchPosts);
    /*
     *function name: editPosts.
     * Desc: this function is use to get Post.
     * req: .//here horsename used static key
     * response: success response if no error else error.
     *  */
    var getPostData = require('./posts/editPosts');
    app.post('/editPosts', getPostData.editPosts);
    /*
     *function name: getPostByProductId.
     * Desc: this function is use to get Post.
     * req: productId
     * response: success response if no error else error.
     *  */
    var getPostByProId = require('./posts/getPostByProductId');
    app.post('/getPostByProductId', getPostByProId.getPostByProductId);

    var getPostByUser = require('./posts/getPostByUser');
    app.post('/getPostByUser', getPostByUser.getPostByUser);

    var getPostByUsers = require('./posts/getPostByUsers');
    app.post('/getPostByUsers', getPostByUsers.getPostByUsers);
    /*
     *function name: addToShow.
     * Desc: this function is use to add your post(eg: horses) to the available shows.
     * req: token,userId,postId,showId.
     * response: success response if no error else error.
     *  */
    var addToShow = require('./shows/addToShow');
    app.post('/addToShow', addToShow.addToShow);

    /*
     *function name: addToRecentList.
     * Desc: this function is use to add your post(eg: horses) to the available shows.
     * req: token,userId,postId,showId.
     * response: success response if no error else error.
     *  */
    var addToRecentList = require('./posts/addToRecentList');
    app.post('/addToRecentList', addToRecentList.addToRecentList);
    /*
     *function name: getUniqueViewByPostId.
     * Desc: this function is use get Unique view.
     * req: postId.
     * response: success response if no error else error.
     *  */
    var getUniqueViewByPostId = require('./wishList/getUniqueViewByPostId');
    app.post('/getUniqueViewByPostId', getUniqueViewByPostId.getUniqueViewByPostId);
    /*
     *function name: getTotalViewByPostId.
     * Desc: this function is use get total view user .
     * req: postId.
     * response: success response if no error else error.
     *  */
    var getTotalViewByPostId = require('./wishList/getTotalViewByPostId');
    app.post('/getTotalViewByPostId', getTotalViewByPostId.getTotalViewByPostId);
    /*
     *function name: getWishListByPostId.
     * Desc: this function is use get user in whishlist.
     * req: postId.
     * response: success response if no error else error.
     *  */
    var getWishListByPostId = require('./wishList/getWishListByPostId');
    app.post('/getWishListByPostId', getWishListByPostId.getWishListByPostId);
    /*
     *function name: getWishList.
     * Desc: this function is use to add your post(eg: horses) to the available shows.
     * req: token,userId,postId,showId.
     * response: success response if no error else error.
     *  */
    var getWishList = require('./wishList/getWishList');
    app.post('/getWishList', getWishList.getWishList);
    /*
     *function name: wishListDetail.
     * Desc: this function is use to add your post(eg: horses) to the available shows.
     * req: token,postIds.
     * response: success response if no error else error.
     *  */
    var wishListDetail = require('./wishList/wishListDetail');
    app.post('/wishListDetail', wishListDetail.wishListDetail);
    /*
     *function name: getPostDetails.
     * Desc: this function is use to add your post(eg: horses) to the available shows.
     * req: token,userId,postId,showId.
     * response: success response if no error else error.
     *  */
    var getPostDetails = require('./posts/getPostDetails');
    app.post('/getPostDetails', getPostDetails.getPostDetails);
    /*
     *function name: getAllActivePosts.
     * Desc: this function is use to get all ActivePosts.
     * req: token.
     * response: success response if no error else error.
     *  */
    var getPostDeta = require('./posts/getAllActivePosts');
    app.post('/getAllActivePosts', getPostDeta.getAllActivePosts);


    /*
     *function name: getAllInactivePosts.
     * Desc: this function is use to get all ActivePosts.
     * req: token.
     * response: success response if no error else error.
     *  */
    var getPostDeta = require('./posts/getAllInactivePosts');
    app.post('/getAllInactivePosts', getPostDeta.getAllInactivePosts);


    /*
     *function name: getAllIncompletePost.
     * Desc: this function is use to get all incompleted post.
     * req: token.
     * response: success response if no error else error.
     *  */
    var getAllIncompletePost = require('./posts/getIncompletePost');
    app.post('/getAllIncompletePost', getAllIncompletePost.getAllIncompletePost);
    /*
     *function name: makeActivePost.
     * Desc: this function is use to set post as Active.
     * req: token,_id.
     * response: success response if no error else error.
     *  */
    var makePost = require('./posts/makeActivePost');
    app.post('/makeActivePost', makePost.makeActivePost);
    /*
     *function name: makeInactivePost.
     * Desc: this function is use to set post as Inactive.
     * req: token,_id.
     * response: success response if no error else error.
     *  */
    var makePost = require('./posts/makeInactivePost');
    app.post('/makeInactivePost', makePost.makeInactivePost);
    /*
     *function name: deleteShow.
     * Desc: this function is use to delete shows.
     * req: token,_id.
     * response: success response if no error else error.
     *  */
    var deleteShow = require('./shows/deleteShow');
    app.post('/deleteShow', deleteShow.deleteShow);
    /*
     *function name: searchActiveShows.
     * Desc: this function is use to serch shows,flage=(active=1=active).
     * req: token,searchP.
     * response: success response if no error else error.
     *  */
    var searchActiveShows = require('./shows/searchActiveShows');
    app.post('/searchActiveShows', searchActiveShows.searchActiveShows);
    /*
     *function name: searchInactiveShows.
     * Desc: this function is use to serch shows,flage=(active=0=Inactive).
     * req: token,searchP.
     * response: success response if no error else error.
     *  */
    var searchInactiveShows = require('./shows/searchInactiveShows');
    app.post('/searchinactiveShows', searchInactiveShows.searchInactiveShows);
    /*
     *function name: searchCompletedShows.
     * Desc: this function is use to serch shows,flage=(active=2=Complted).
     * req: token,searchP.
     * response: success response if no error else error.
     *  */
    var searchShow = require('./shows/searchCompletedShows');
    app.post('/searchCompletedShows', searchShow.searchCompletedShows);
    /*
     *function name: makeShowActive.
     * Desc: this function is use to serch shows,flage=(active=1=active).
     * req: token,_id.
     * response: success response if no error else error.
     *  */
    var makeShowActive = require('./shows/makeShowActive');
    app.post('/makeShowActive', makeShowActive.makeShowActive);

    /*
     *function name: makeShowInactive.
     * Desc: this function is use to serch shows,flage=(active=0=Inactive).
     * req: token,_id.
     * response: success response if no error else error.
     *  */
    var makeShowInactive = require('./shows/makeShowInactive');
    app.post('/makeShowInactive', makeShowInactive.makeShowInactive);
    /*
     *function name: getAllCompletedShows.
     * Desc: this function is use to serch shows,flage=(active=2=completed).
     * req: token.
     * response: success response if no error else error.
     *  */
    var getAllCompletedShows = require('./shows/getAllCompletedShows');
    app.post('/getAllCompletedShows', getAllCompletedShows.getAllCompletedShows);

    /*
     *function name: getAllInactiveShows.
     * Desc: this function is use to serch shows,flage=(active=0=Inactive).
     * req: token.
     * response: success response if no error else error.
     *  */
    var getAllInactiveShows = require('./shows/getAllInactiveShows');
    app.post('/getAllInactiveShows', getAllInactiveShows.getAllInactiveShows);

    /*
     *function name: getAllActiveShows.
     * Desc: this function is use to serch shows,flage=(active=1=Inactive).
     * req: token.
     * response: success response if no error else error.
     *  */
    var getAllActiveShows = require('./shows/getAllActiveShows');
    app.post('/getAllActiveShows', getAllActiveShows.getAllActiveShows);

    /*
     *function name: archiveShow.
     * Desc: this function is use to add your post(eg: horses) to the available shows.
     * req: token,userId,postId,showId.
     * response: success response if no error else error.
     *  */
    var archiveShow = require('./shows/archiveShow');
    app.post('/archiveShow', archiveShow.archiveShow);
    /*
     *function name: getShowsList.
     * Desc: this function is use to get shows.
     * req: token.
     * response: success response if no error else error.
     *  */
    var showList = require('./shows/getShowsList');
    app.post('/getShowsList', showList.getShowsList);

    /*
     *function name: getShowsListById.
     * Desc: this function is use to get show.
     * req: token,_id.
     * response: success response if no error else error.
     *  */
    var showList = require('./shows/getShowsListById');
    app.post('/getShowsListById', showList.getShowsListById);
    /*
     *function name: editShow.
     * Desc: this function is use to edit Show.
     * req: token,_id,feilds to update.
     * response: success response if no error else error.
     *  */
    var showList = require('./shows/editShow');
    app.post('/editShow', showList.editShow);
    /*
     *function name: createShow.
     * Desc: this function is use to create Show.
     * req: token,Allfeilds.
     * response: success response if no error else error.
     *  */
    var showList = require('./shows/createShow');
    app.post('/createShow', showList.createShow);
    /*
     *function name: getParticipantById.
     * Desc: this function is use to create Show.
     * req: _id.
     * response: success response if no error else error.
     *  */
    var getParticipantsById = require('./shows/getParticipantsById');
    app.post('/getParticipantsById', getParticipantsById.getParticipantsById);
    /*
     *function name: getAllPostsHost.
     * Desc: this function is use to add your post(eg: horses) to the available shows.
     * req: token,userId,postId,showId.
     * response: success response if no error else error.
     *  */
    var getAllPostsHost = require('./posts/getAllPostsHost');
    app.post('/getAllPostsHost', getAllPostsHost.getAllPostsHost);
    /*
     *function name: getCurrency.
     * Desc: this function is use to get All currency
     * req: .
     * response: success response if no error else error.
     *  */
    var getCurrency = require('./currency/getCurrency');
    app.post('/getCurrency', getCurrency.getCurrency);
    /*
     *function name: createCurrency.
     * Desc: this function is use to add new currancy
     * req: currancy,country.
     * response: success response if no error else error.
     *  */
    var createCurrency = require('./currency/createCurrency');
    app.post('/createCurrency', createCurrency.createCurrency);
    /*
     *function name: editCurrency.
     * Desc: this function is use to edit currancy data.
     * req: _id,(other update fileds).
     * response: success response if no error else error.
     *  */
    var editCurrency = require('./currency/editCurrency');
    app.post('/editCurrency', editCurrency.editCurrency);
    /*
     *function name: deleteCurrency.
     * Desc: this function is use to Delete Currency.
     * req: _id.
     * response: success response if no error else error.
     *  */
    var deleteCurrency = require('./currency/deleteCurrency');
    app.post('/deleteCurrency', deleteCurrency.deleteCurrency);
    /*
     *function name: createFAQ.
     * Desc: this function is use to createFAQ.
     * req: question,answer.
     * response: success response if no error else error.
     *  */
    var createFAQ = require('./FAQ/createFAQ');
    app.post('/createFAQ', createFAQ.createFAQ);
    /*
     *function name: editFAQ.
     * Desc: this function is use to editFAQ.
     * req: question,answer,_id.
     * response: success response if no error else error.
     *  */
    var editFAQ = require('./FAQ/editFAQ');
    app.post('/editFAQ', editFAQ.editFAQ);
    /*
     *function name: getFAQ.
     * Desc: this function is use to get FAQ.
     * req: .
     * response: success response if no error else error.
     *  */
    var getFAQ = require('./FAQ/getFAQ');
    app.post('/getFAQ', getFAQ.getFAQ);
    /*
     *function name: deleteFAQ.
     * Desc: this function is use to deleteFAQ.
     * req: _id.
     * response: success response if no error else error.
     *  */
    var deleteFAQ = require('./FAQ/deleteFAQ');
    app.post('/deleteFAQ', deleteFAQ.deleteFAQ);
    /*
     *function name: getFAQById.
     * Desc: this function is use to getFAQById.
     * req: _id.
     * response: success response if no error else error.
     *  */
    var getFAQById = require('./FAQ/getFAQById');
    app.post('/getFAQById', getFAQById.getFAQById);
    /*
     *function name: getLanguage.
     * Desc: this function is use to get Language.
     * req: .
     * response: success response if no error else error.
     *  */
    var getLanguage = require('./language/getLanguage');
    app.post('/getLanguage', getLanguage.getLanguage);
    /*
     *function name: createLanguage.
     * Desc: this function is use to create Language.
     * req: language.
     * response: success response if no error else error.
     *  */
    var createLanguage = require('./language/createLanguage');
    app.post('/createLanguage', createLanguage.createLanguage);
    /*
     *function name: deleteLanguage.
     * Desc: this function is use to create Language.
     * req: _id.
     * response: success response if no error else error.
     *  */
    var deleteLanguage = require('./language/deleteLanguage');
    app.post('/deleteLanguage', deleteLanguage.deleteLanguage);
    /*
     *function name: editLanguage.
     * Desc: this function is use to edit Language.
     * req: _id.
     * response: success response if no error else error.
     *  */
    var editLanguage = require('./language/editLanguage');
    app.post('/editLanguage', editLanguage.editLanguage);
    /*
     *function name: searchLanguage.
     * Desc: this function is use to search Language.
     * req: searchP.
     * response: success response if no error else error.
     *  */
    var searchLanguage = require('./language/searchLanguage');
    app.post('/searchLanguage', searchLanguage.searchLanguage);

    /*
     *function name: getUpcomingShows.
     * Desc: this function is use to search Language.
     * req: .
     * response: success response if no error else error.
     *  */
    var getUpcomingShows = require('./shows/getUpcomingShows');
    app.post('/getUpcomingShows', getUpcomingShows.getUpcomingShows);
    /*
     *function name: getAllImages.
     * Desc: this function is use to getAllImages.
     * req: .
     * response: success response if no error else error.
     *  */
    var getAllImages = require('./FAQ/getAllImages');
    app.post('/getAllImages', getAllImages.getAllImages);
    /*
     *function name: sendAlerts.
     * Desc: this function is use to send notifications.
     * req: userId,postId,showId,prouctiontype.
     * response: success response if no error else error.
     *  */
    var alerts = require('./alerts/sendAlerts');
    app.post('/sendAlerts', alerts.sendAlerts);


    /*
     *function name: sendAlerts.
     * Desc: this function is use to send notifications.
     * req: userId,postId,showId,prouctiontype.
     * response: success response if no error else error.
     *  */
    var sendNotificationToSelectedUsers = require('./alerts/sendNotificationToSelectedUsers');
    app.post('/sendNotificationToSelectedUsers', sendNotificationToSelectedUsers.sendNotificationToSelectedUsers);

    var getDashBoardData = require('./dashboard/getDashBoardData');
    app.post('/getDashBoardData', getDashBoardData.getDashBoardData);

    var testPush = require('./alerts/testPush');
    app.post('/testPush', testPush.testPush);
    var testmail = require('./alerts/testPush');
    app.post('/testmail', testmail.testmail);
    app.listen(2020);
