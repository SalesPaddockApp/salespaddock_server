/**
 * Created by Niranjan on 8/10/16.
 */
var exports = module.exports = {};
var notification = require('../notification/sendNotification');
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;

exports.sendAlerts1 = function(req,res)
{
    console.log("sendAlerts1: " +JSON.stringify(req.body));
    if(!req.body.userId)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory user id is missing"});
        return false;
    }

    if(!req.body.postId)
    {
        res.send({errCode: 1, errNum: 145, Message: "mandatory post id is missing"});
        return false;
    }

    if(!req.body.showId)
    {
        res.send({errCode: 1, errNum: 145, Message: "mandatory show id is missing"});
        return false;
    }

    if(!req.body.mess)
    {
        res.send({errCode: 1, errNum: 145, Message: "mandatory message is missing"});
        return false;
    }

    var cond= {postId: req.body.postId,showId: req.body.showId};

    console.log("cond: " +JSON.stringify(cond));
    Utility.Select('recentVisitors',cond,function(err,result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result.length > 0)
        {
            var toGetUserToken = [];
            for (var i = 0; i < result.length; i++)
            {
                toGetUserToken.push({userId: result[i]['userId']});
            }

            var cond1 = {$or : toGetUserToken};

            console.log("cond1:  " +JSON.stringify(cond1));

            Utility.Select('user', cond1, function (err, result1)
            {
                if (err)
                {
                    res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
                    return false;
                }
                else if (result1.length > 0)
                {
                    var pushTokens = [];
                    for(var j=0; j<result1.length; j++)
                    {
                        if(result1[j]['pushToken'])
                            pushTokens.push(result1[j]['pushToken']);
                    }

                    var message= "hiii";
                    var data = {pushTokens: pushTokens,alert: req.body.mess,production: true,isArr :1,payload:{data: "not required now"}};

                    notification.sendNotification(data,(function (err, result)
                    {
                    }));

                    res.send({errCode: 0, Message: "Alert sent successfully."});
                    return true;

                }
                else
                {
                    console.dir("error else part");
                    res.send({errCode: 0, Message: "Alert sent successfully."});
                    return true;

                }
            })
        }
        else
        {
            res.send({errCode: 0, Message: "Alert sent successfully."});
            return true;
        }
    })
};



exports.sendAlerts = function(req,res)
{
    console.log("sendAlerts: " +JSON.stringify(req.body));
    if(!req.body.userId)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory user id is missing"});
        return false;
    }

    if(!req.body.postId)
    {
        res.send({errCode: 1, errNum: 145, Message: "mandatory post id is missing"});
        return false;
    }

    if(!req.body.showId)
    {
        res.send({errCode: 1, errNum: 145, Message: "mandatory show id is missing"});
        return false;
    }

    if(!req.body.mess)
    {
        res.send({errCode: 1, errNum: 145, Message: "mandatory message is missing"});
        return false;
    }

    var cond= {userId: {$ne: req.body.userId}};

    Utility.Select('user', cond, function (err, result1)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if (result1.length > 0)
        {
            var pushTokens = [];
            for(var j=0; j<result1.length; j++)
            {
                if(result1[j]['pushToken'])
                    pushTokens.push(result1[j]['pushToken']);
            }

            require("uniq")(pushTokens);

            //console.dir("pushTokens: " +JSON.stringify(pushTokens));
            var data = {pushTokens: pushTokens,alert: req.body.mess,production: true,isArr :1,payload:{data: "not required now"}};

            notification.sendNotification(data,(function (err, result)
            {
            }));

            res.send({errCode: 0, Message: "Alert sent successfully."});
            return true;

        }
        else
        {
            console.dir("error else part");
            res.send({errCode: 0, Message: "Alert sent successfully."});
            return true;

        }
    })
};
