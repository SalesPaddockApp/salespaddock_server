/**
 * Created by Niranjan on 9/1/17.
 */

var exports = module.exports = {};
var notification = require('../notification/sendNotification');
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;

exports.sendNotificationToSelectedUsers = function(req,res)
{
    console.log("sendAlerts2: " +JSON.stringify(req.body));
    if(!req.body.userId)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory user id is missing"});
        return false;
    }

    if(!req.body.mess)
    {
        res.send({errCode: 1, errNum: 145, Message: "mandatory message is missing"});
        return false;
    }

    if(req.body.userId.length < 1)
    {
        res.send({errCode: 1, errNum: 145, Message: "please select the mandatory users to send the push notifications."});
        return false;
    }


    var toGetUserToken = [];
    for (var i = 0; i < req.body.userId.length; i++)
    {
        toGetUserToken.push({userId: req.body.userId[i]});
    }

    var cond1 = {$or : toGetUserToken};

    console.log("cond1:  " +JSON.stringify(cond1));

    Utility.Select('user', cond1, function (err, result1)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if (result1.length > 0)
        {
            var pushTokens = [];
            //for(var j=0; j<result1.length; j++)
            //{
            //    if(result1[j]['pushToken'])
            //        pushTokens.push(result1[j]['pushToken']);
            //}

            var userIds = [];
            for(var j=0; j<result1.length; j++)
            {
                if(result1[j]['pushToken'])
                {
                    userIds.push({userId: result1[j]['userId']});
                    pushTokens.push(result1[j]['pushToken']);
                }
            }

            var message= "hiii";
            var data = {pushTokens: pushTokens,alert: req.body.mess,production: true,isArr :1,payload:{type : "admin alerts"}};

	    console.log("data:  " + JSON.stringify(data));

            notification.sendNotification(data,(function (err, result)
            {
            }));

            var dataToinsert = {message: req.body.mess,recievers: userIds,sender: "admin",postId : ObjectId("589d8c8bc486dd4be78ab335")};
            Utility.Insert('notification',dataToinsert,function(err,isInserted)
            {
                if(err)
                {
                    console.log("some error occurred:  " + JSON.stringify(err));
                }
                else
                {
                    console.log("data inserted successfully.")
                }
            });

            res.send({errCode: 0, Message: "Alert sent successfully."});
            return true;

        }
        else
        {
            console.dir("error else part");
            res.send({errCode: 0, Message: "Alert sent successfully."});
            return true;

        }
    })
};
