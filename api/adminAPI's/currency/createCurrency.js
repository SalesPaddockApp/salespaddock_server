/**
 * Created by Niranjan on 19/9/16.
 * function name: createPost
 * request: token,userId,wishId
 * response: succuss response if no error else error accordingly
 * active = 0 = inactive
 * active = 1 = active
 * active = 2 = completed
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');

exports.createCurrency = function (req, res)
{

    if (!req.body.country)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory country is missing"});
        return false;
    }
    if (!req.body.currency)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory currency is missing"});
        return false;
    }
    if (!req.body.symbol)
    {
        res.send({errCode: 1, errNum: 132, Message: "symbols symbol is missing"});
        return false;
    }


   
    var dataToInsert = {
        country: req.body.country,
        currency: req.body.currency,
        symbol: req.body.symbol
    };

    Utility.Insert('currency', dataToInsert, function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        } else if (result)
        {
            res.send({errCode: 0, Message: "you have created one currency successfully.", response: {ShowId: result['ops'][0]['_id']}});
            return true;
        } else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no data is there, please try after some time."});
            return true;
        }
    });
}






