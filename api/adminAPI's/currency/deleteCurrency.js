/**
 * Created by Dipen on 22/11/2016.
 * function name: deleteProducttype
 * request: _id
 * response: succuss response if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;


exports.deleteCurrency = function (req, res)
{
    if (!req.body._id)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory feild _id is missing"});
        return false;
    }
  

    Utility.Delete('currency', {"_id": ObjectId(req.body._id)}, function (err, result)
    {
       
       
            
            if (err)
            {
                res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
                return false;
            } else if (result)
            {

                res.send({errCode: 0, Message: "your currency Deleted successfully.", response: {}});
                return true;


            } else
            {
                res.send({errCode: 1, errNum: 135, Message: "Currently no currency is there, please try after some time."});
                return true;
            }
        




    });


}

