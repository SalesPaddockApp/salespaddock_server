/**
 * Created by Dipen on 22/11/2016.
 * function name: editProducttype
 * request: _id,productName
 * response: succuss response if no error else error accordingly
 *
 * var timestamp=new Date('02/10/2016').getTime();
 var todate=new Date(timestamp).getDate();
 var tomonth=new Date(timestamp).getMonth()+1;
 var toyear=new Date(timestamp).getFullYear();
 var original_date=tomonth+'/'+todate+'/'+toyear;
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;


exports.editCurrency = function (req, res)
{
    if (!req.body._id)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory feild _id is missing"});
        return false;
    }
    var data = [];
    var dataToUpdate = {}, cond = {};

    var i = 0;
    var _id = "_id", token = "token";
    for (key in  req.body) {

        data.push(key);

        if (data[i] == '_id') {
            cond[key] = ObjectId(req.body[data[i]]);//condition to updatedate
        } else if (data[i] == 'token') {

//            extra feild which is not for update
        } else {
            //data for update
            if(data[i]=='startDate'){
                req.body.startDate=parseInt(new Date(req.body.startDate).getTime());
            }else if(data[i]=='endDate'){
                req.body.endDate=parseInt(new Date(req.body.endDate).getTime());
            }else if(data[i]=='lat'){
                req.body.lat= parseFloat(req.body.lat);
            }else if(data[i]=='lng'){
                req.body.lng= parseFloat(req.body.lng);
            }

                dataToUpdate[key] = req.body[data[i]];


        }
        i++;
    }


//if no data for update then
    if (Object.keys(dataToUpdate).length <= 0) {
        res.send({errCode: 1, errNum: 132,
            Message: "Please define any one filed for update"});
        return false;
    }
  

    Utility.Update('currency', cond, dataToUpdate, function (err, result)
    {

        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        } else if (result)
        {

            res.send({errCode: 0, Message: "currency updated successfully.", response: {}});
            return true;


        } else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no currency is there, please try after some time."});
            return true;
        }
    });


}

