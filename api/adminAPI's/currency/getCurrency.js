/**
 * Created by Niranjan on 23/9/16.
 * function name:  getShows
 * request: token,userId
 * response: all upcoming shows, recelntly viewed shows, and popular shows.
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');

var ObjectId = require('mongodb').ObjectID;

exports.getCurrency = function(req, res)
{
 

    Utility.Select('currency',{},function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result.length > 0)
        {
           res.send({errCode: 0,  Message: "Data  send successfully.",totalFound:result.length,response:{data:result}});
        }
        else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no Data is there, please try after some time."});
            return true;
        }
    });
}

