var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;

exports.getDashBoardData = function(req, res)
{
    var newDate = new Date();
    var currentDate = newDate.getDate();
    var currentMonth = newDate.getMonth();
    var currentYear = newDate.getFullYear();
    var lastYear = newDate.getTime() - (1000*60*60*24*365);
    console.log(lastYear);

    Utility.Select('user',{}, function(err, result) {
        console.log("started");
        if(err){
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if (result.length > 0) {
            var totalUsers = result.length;
            var usersToday = 0;
            var usersThisMonth = 0;
            var usersThisYear = 0;
            var todaysList = [];
            for(var i=0; i<result.length; i++){
                if(new Date(result[i]['memberSince']).getDate() == currentDate && new Date(result[i]['memberSince']).getMonth() == currentMonth && new Date(result[i]['memberSince']).getFullYear() == currentYear){
                    usersToday++;
                    todaysList.push(result[i]['userId']);
                }
                else if(new Date(result[i]['memberSince']).getMonth() == currentMonth && new Date(result[i]['memberSince']).getFullYear() == currentYear){
                    usersThisMonth++
                }
                else if(lastYear >= new Date(result[i]['memberSince']) <= newDate){
                    usersThisYear++
                }
            }
            res.send({errCode: 0, Message: "sent successfully.",
             response: {usersToday: usersToday, usersThisMonth: usersThisMonth,
                  usersThisYear: usersThisYear,totalUsers: totalUsers, todaysList : todaysList}});
            return true;
        }
        else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no data is there, please try after some time."});
            return true;
        }
    })
}
