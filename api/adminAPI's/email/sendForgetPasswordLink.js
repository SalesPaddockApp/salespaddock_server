/**
 * Created by Niranjan on 12/10/16.
 */

var conf = require('../conf');
var domainName = conf.mailGunDomain;
var apiKey = conf.mailGunAPiKey;

var Mailgun = require('mailgun-js');
var mailgun = new Mailgun({apiKey: apiKey, domain: domainName});

exports.sendForgetPasswordLink = function (args, callback)
{
    console.log("apiKey: " + apiKey);
    console.log("domainName: " + domainName);
    console.log("args: " + JSON.stringify(args));

    var data = {
        from: "noreply@salespaddock.org",
        to: args.to,
        subject: "Reset your password",
        html:
'<html lang="en">'+
'<head>'+
'<meta name="viewport" content="width=device-width, initial-scale=1">'+
'<style> '+
'html,body,h1,h2,h3,h4,h5,h6 {font-family: "Roboto", sans-serif}'+
'.button { background-color: #0AB49D; border: none; width:100%;  color: white; padding: 15px 32px; text-align: center; text-decoration: none; display: inline-block;'+
    'font-size: 16px; margin: 4px 2px; cursor: pointer;}'+
    '.div2 {  width:40%; height: 30%; padding: 50px; border: 1px solid; border-color: #0AB49D;}'+
    'p { font-family: Arial, "Helvetica Neue", Helvetica, sans-serif; font-size: 14px;  font-style: normal; font-variant: normal; font-weight: 400; line-height: 20px; }'+
'</style>'+
'</head>'+
'<body>'+
'<div class="div2"><center><img src="http://45.55.233.55/api/email/SalesPaddock.png"></center><p>Hi '+args.userName+'! <br><br> We’ve received a request to reset your password. If you didn’t make the request, just ignore this email. Otherwise, you can reset your password using link :<br>'+
'<a target="_blank" href="http://45.55.233.55:5006/forgetPassword/'+args.resetLink+'" ><button class="button">Click here to reset your password </button></a> <br> Thanks,<br>The SalesPaddock Team<br><br>'+
'</p><center><img src="http://45.55.233.55/api/email/social/facebook.png" height="30" width="30" >'+
'<img src="http://45.55.233.55/api/email/social/twitter.png" height="30" width="30" >'+
'<img src="http://45.55.233.55/api/email/social/instagram.png" height="30" width="30" ></center><center>Sent with Like from SalesPaddock</center></div>'+

'</body></html>'

    };

    mailgun.messages().send(data, function (err, success)
    {
        if (err)
        {
            console.log('Could not send mail error!' + JSON.stringify(err));
            return callback(err, {message: "some error occurredd", code: 198});
        }
        console.log(JSON.stringify({message: 'Success! Mail send' + JSON.stringify(success), code: 200}));
        return callback(err, {message: 'Success! Mail send' + JSON.stringify(success), code: 200});


    });
};