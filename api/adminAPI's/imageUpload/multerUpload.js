var express = require('express');

var bodyParser = require('body-parser');
cors = require('cors');

var multer = require('multer');
var thumbler = require('video-thumb');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
    }
})
var imageStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'images/')
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
    }
})

var upload = multer({ storage: storage });
var imageUpload = multer({storage: imageStorage});

    
var app = express();
app.use("/uploads", express.static(__dirname + "/uploads"));
app.use("/images", express.static(__dirname + "/images"))

app.use(cors({credentials: true, origin: true}));


app.post('/upload', upload.single('photo'), function (req, res, next) {
    if (req)
        console.log("req.file: " + JSON.stringify(req.file));

    if (res)
    {	
	var orgpath = __dirname +"/" +req.file.path
	console.log("orgpath "+orgpath)
	thumbler.extract(orgpath, orgpath+".png", '00:00:22', '240x160', function(){

	    console.log('snapshot saved to snapshot.png (200x125) with a frame at 00:00:22');

	});
        console.log("res.file: " + req.file.path);
        return res.json({ code: 200,
            url:  req.file.path});
    }
        
});

app.post('/imageUpload', imageUpload.single('photo'), function (req, res, next) {
    if (req)
        console.log("req.file: " + JSON.stringify(req.file));

    if (res)
    {
        console.log("res.file: " + req.file.path);
        return res.json({ code: 200,
            url:  req.file.path});
    }

});



app.listen(8009, function (err) {
    if (err) {
        console.log(err);
    } else {
        console.log('App listening on port: ' + 8009);
    }
});
