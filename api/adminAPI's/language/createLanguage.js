/**
 * Created by Dipen on 04/01/17.
 * function name: createLanguage
 * request: token,userId,wishId
 * response: succuss response if no error else error accordingly
 * active = 0 = inactive
 * active = 1 = active
 * active = 2 = completed
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');

exports.createLanguage = function (req, res)
{
    if (!req.body.language)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory language is missing"});
        return false;
    }
    if (!req.body.country)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory country is missing"});
        return false;
    }



   
    var dataToInsert = {
        language: req.body.language,
        country: req.body.country    
    };

    Utility.Insert('language', dataToInsert, function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        } else if (result)
        {
            res.send({errCode: 0, Message: "you have created one recored successfully.", response: {ID: result['ops'][0]['_id']}});
            return true;
        } else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no data is there, please try after some time."});
            return true;
        }
    });
}






