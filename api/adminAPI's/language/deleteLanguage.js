/**
 * Created by Dipen on 04/01/2017.
 * function name: deleteLanguage
 * request: _id
 * response: succuss response if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;


exports.deleteLanguage = function (req, res)
{
    if (!req.body._id)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory feild _id is missing"});
        return false;
    }
  

    Utility.Delete('language', {"_id": ObjectId(req.body._id)}, function (err, result)
    {
       
       
            
            if (err)
            {
                res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
                return false;
            } else if (result)
            {

                res.send({errCode: 0, Message: "your data Deleted successfully.", response: {}});
                return true;


            } else
            {
                res.send({errCode: 1, errNum: 135, Message: "Currently no data is there, please try after some time."});
                return true;
            }
        




    });


}

