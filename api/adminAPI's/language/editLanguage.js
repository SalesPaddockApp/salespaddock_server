/**
 * Created by Dipen on 04/01/2017.
 * function name: editLanguage
 * request: _id
 * response: succuss response if no error else error accordingly

 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;


exports.editLanguage = function (req, res)
{
    if (!req.body._id)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory feild _id is missing"});
        return false;
    }
     if (!req.body.language)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory language is missing"});
        return false;
    }
   if (!req.body.country)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory country is missing"});
        return false;
    }
  
  var data = {language:req.body.language,country:req.body.country};

    Utility.Update('language', {_id:ObjectId(req.body._id)},data, function (err, result)
    {

        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        } else if (result)
        {
            res.send({errCode: 0, Message: "Data updated successfully.", response: {}});
            return true;

        } else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no data is there, please try after some time."});
            return true;
        }
    });


}

