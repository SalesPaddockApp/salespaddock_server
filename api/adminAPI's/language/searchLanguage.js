/**
 * Created by Dipen on 04/01/17.
 * function name: searchLanguage
 * request: language
 * response: all product type if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');


exports.searchLanguage = function (req, res)
{
    if (!req.body.searchP)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory searchP is missing"});
        return false;
    }


    var cond = [
      
        {$match: { $or: [
                    {"language": new RegExp("^" + req.body.searchP, "gi")},
                    {"country": new RegExp("^" + req.body.searchP, "gi")}]}}
    ];

    Utility.AggregateGroup('language', cond, function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        } else if (result.length > 0)
        {
            console.log("dataTosend for Post:  " + JSON.stringify(result.length));

            res.send({errCode: 0, Message: "data send succcessfully.", totalFound: result.length, response: {data: result}});
            return true;
        } else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no Posts there, please try after some time."});
            return true;
        }
    });
}


