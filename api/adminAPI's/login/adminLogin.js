module.exports = function(app,express){
    var router = express.Router();

var Utility = require('../UtilityFunc');
var jsonwebtoken = require('jsonwebtoken');

var conf = require('../conf');

var secretKey = conf.secretKey;


router.post('/adminLogin',function(req,res)
{

    console.log("adminLogin: " + JSON.stringify(req.body));

    if(!req.body.email)
    {
        res.send({errCode: 1, errNum: 108, Message: "mandatory email is missing"});
        return false;
    }

    if(!req.body.password)
    {
        res.send({errCode: 1, errNum: 109, Message: "mandatory password is missing"});
        return false;
    }

    var cond = {"userName" : req.body.email};
    
    console.log("cond:  " + JSON.stringify(cond));

    Utility.SelectOne('adminLogin',cond, (function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result)
        {
            if(req.body.password == result.password)
            {
                 user = {email: req.body.email}
                var token = createToken(user);
                res.send({errCode: 0, Message: "Login successfully",response:{token:token}});
                return true;
            }   
            else
            {
                res.send({errCode: 1, errNum: 112, Message: "password does not match."});
                return false;                  
            }        
        }
        else
        {
            res.send({errCode: 1, errNum: 112, Message: "User does not exists"});
            return false;            
        }
    }));
});


function createToken(user) {
    var token = jsonwebtoken.sign({email: user.email}, secretKey, {expiresIn: '60 days'});
    return token;
}
   return router;
};