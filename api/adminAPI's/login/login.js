module.exports = function(app,express){
    var router = express.Router();

var Utility = require('../UtilityFunc');
var jsonwebtoken = require('jsonwebtoken');

var conf = require('../conf');

var secretKey = conf.secretKey;


router.post('/login',function(req,res)
{

    console.log("login: " + JSON.stringify(req.body));

    if(!req.body.email)
    {
        res.send({errCode: 1, errNum: 108, Message: "mandatory email is missing"});
        return false;
    }

    if(!req.body.password)
    {
        res.send({errCode: 1, errNum: 109, Message: "mandatory password is missing"});
        return false;
    }

    var cond = {"email.primaryEmail" : req.body.email.toUpperCase()};

    Utility.SelectOne('user',cond, (function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result)
        {
            if(req.body.password == result.password)
            {
                var pushToken = req.body.pushToken ? req.body.pushToken :'';
                var data1 = {pushToken: pushToken};
                Utility.Update('user',cond,data1,function(err,isUpdated)
                {});

                var lName = "";
                if(result.name.lName)
                    lName = result.name.lName;

                user = {userId:result.userId,name:result.name.fName,email: result.email}
                var token = createToken(user);

                res.send({errCode: 0, Message: "Login successfully",response:{
                    userId:result.userId,fname:result.name.fName,
                    lname:lName,token:token}});
                return true;
            }   
            else
            {
                res.send({errCode: 1, errNum: 112, Message: "password does not match."});
                return false;                  
            }        
        }
        else
        {
            res.send({errCode: 1, errNum: 112, Message: "User does not exists"});
            return false;            
        }
    }));
});


function createToken(user) {
    var token = jsonwebtoken.sign({id: user._id,name: user.name,email: user.email}, secretKey, {expiresIn: '60 days'});
    return token;
}
   return router;
};