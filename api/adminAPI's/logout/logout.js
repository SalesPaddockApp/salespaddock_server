/**
 * Created by Niranjan on 8/10/16.
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;

exports.logout = function(req,res)
{
    console.log("logout: " +JSON.stringify(req.body));
    if(!req.body.userId)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory user id is missing"});
        return false;
    }

    var cond= {userId: req.body.userId};
    var data= {pushToken: ""};

    console.log("cond: " +JSON.stringify(cond));
    Utility.Update('user',cond,data,function(err,result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result)
        {
            res.send({errCode: 0, Message: "Logged out successfully."});
        }
        else
        {
            res.send({errCode: 1, errNum: 160,Message: "user does not exists successfully."});
            return true;
        }
    })
};
