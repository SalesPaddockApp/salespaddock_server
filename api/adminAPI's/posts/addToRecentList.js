/**
 * Created by Niranjan on 26/9/16.
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');

exports.addToRecentList = function(req, res)
{
    console.dir("addToRecentList: " + JSON.stringify(req.body));
    if(!req.body.userId)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory user id is missing"});
        return false;
    }

    if(!req.body.postId)
    {
        res.send({errCode: 1, errNum: 138, Message: "mandatory post id is missing"});
        return false;
    }

    if(!req.body.showId)
    {
        res.send({errCode: 1, errNum: 153, Message: "mandatory show id is missing"});
        return false;
    }

    if(!req.body.hostUserId)
    {
        res.send({errCode: 1, errNum: 154, Message: "mandatory host user id is missing"});
        return false;
    }

    var dataToInsert = {userId: req.body.userId,viewedTime: Date.now(),postId: req.body.postId,
        showId: req.body.showId,hostUserId: req.body.hostUserId};
    Utility.Insert('recentVisitors',dataToInsert,function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result)
        {
            console.log("result:  " + JSON.stringify(result));
            res.send({errCode: 0,  Message: "you are just viewed one post.",response: {}});
            return true;
        }
        else
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
    });
}


