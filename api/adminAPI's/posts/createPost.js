/**
 * Created by Niranjan on 19/9/16.
 * function name: createPost
 * request: token,userId,wishId
 * response: succuss response if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;
var async = require("async");

// exports.createPost = function (req, res) {
//     console.log("data:  " + JSON.stringify(req.body));

//     if (!req.body.userId) {
//         res.send({ errCode: 1, errNum: 132, Message: "mandatory user id is missing" });
//         return false;
//     }

//     if (!req.body.productId) {
//         res.send({ errCode: 1, errNum: 138, Message: "mandatory productId is missing" });
//         return false;
//     }
//     if (!req.body.productType) {
//         res.send({ errCode: 1, errNum: 138, Message: "mandatory productType is missing" });
//         return false;
//     }

//     var values = req.body.values;



//     console.log("data Coming======================================>>>>>>>>> : ", req.body);
//     for (var i = 0; i < values.length; i++) {
//         values[i].pref_id = ObjectId(values[i].pref_id)
//     }





//     var dataToInsert = {
//         userId: req.body.userId, "activeStatus": "0",
//         created: Date.now(), values: values, productId: ObjectId(req.body.productId),
//         productType: req.body.productType
//     };



//     Utility.UpdateAsInsertSpecificField("user", { "userId": req.body.userId }, { "farmDetail": req.body.farmDetails }, function (err, result) {
//         if (err) {
//             res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
//             return false;
//         }

//     })

//     Utility.Insert('post', dataToInsert, function (err, result) {

//         if (err) {
//             res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
//             return false;
//         }
//         else if (result) {
//             console.log("result:  inserted");
//             res.send({ errCode: 0, Message: "you have created one post successfully.", response: { postId: result['ops'][0]['_id'] } });
//             return true;
//         }
//         else {
//             res.send({ errCode: 1, errNum: 135, Message: "Currently no product is there, please try after some time." });
//             return false;
//         }
//     });
// }



exports.createPost = function (req, res) {
    console.log("createPost API :data:  " + JSON.stringify(req.body));

    if (!req.body.userId) {
        res.send({ errCode: 1, errNum: 132, Message: "mandatory user id is missing" });
        return false;
    }

    if (!req.body.productId) {
        res.send({ errCode: 1, errNum: 138, Message: "mandatory productId is missing" });
        return false;
    }
    if (!req.body.productType) {
        res.send({ errCode: 1, errNum: 138, Message: "mandatory productType is missing" });
        return false;
    }

    var values = req.body.values;



    console.log("data Coming: ", JSON.stringify(req.body));
    for (var i = 0; i < values.length; i++) {
        values[i].pref_id = ObjectId(values[i].pref_id)
    }





    var dataToInsert = {
        userId: req.body.userId, "activeStatus": "0",
        location: {
            "longitude": parseFloat(req.body.farmDetails.long) || 0,
            "latitude": parseFloat(req.body.farmDetails.lat) || 0
        },
        created: Date.now(), values: values,
        productId: ObjectId(req.body.productId),
        productType: req.body.productType

    };

    async.series([

        function (callback) {
            Utility.SelectOne("user", { userId: req.body.userId, "farmDetail.farmID": req.body.farmDetails.farmID }, function (err, result) {

                if (err) {
                    res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
                    return false;
                } else if (result) {

                    var condition = { "userId": req.body.userId, "farmDetail": { "$elemMatch": { "farmID": req.body.farmDetails.farmID } } }
                    var dataToUpdate = {

                        "farmDetail.$.lat": req.body.farmDetails.lat,
                        "farmDetail.$.long": req.body.farmDetails.long,
                        "farmDetail.$.street": req.body.farmDetails.street,
                        "farmDetail.$.apt": req.body.farmDetails.apt,
                        "farmDetail.$.city": req.body.farmDetails.city,
                        "farmDetail.$.state": req.body.farmDetails.state,
                        "farmDetail.$.zipCode": req.body.farmDetails.zipCode,
                        "farmDetail.$.country": req.body.farmDetails.country,
                        "farmDetail.$.farmName": req.body.farmDetails.farmName


                    }


                    Utility.Update("user", condition, dataToUpdate, function (err, result) {
                        if (err) {
                            res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
                            return false;
                        }

                    })
                    callback(err, result);

                } else {
                    Utility.UpdateAsInsertSpecificField("user", { "userId": req.body.userId }, { "farmDetail": req.body.farmDetails }, function (err, result) {
                        if (err) {
                            res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
                            return false;
                        }

                    });
                    callback(err, result);
                }
            });
        },
        function (callback) {
            Utility.Insert('post', dataToInsert, function (err, result) {

                if (err) {
                    res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
                    return false;
                }
                else if (result) {
                    console.log("result:  inserted");
                    res.send({ errCode: 0, Message: "you have created one post successfully.", response: { postId: result['ops'][0]['_id'] } });
                    return true;
                }
                else {
                    res.send({ errCode: 1, errNum: 135, Message: "Currently no product is there, please try after some time." });
                    return false;
                }
            });
        },
    ]);
}