/**
 * Created by Dipen on 22/11/2016.
 * function name: editProducttype
 * request: _id,productName
 * response: succuss response if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;
var async = require("async");


exports.editPosts = function (req, res) {
    console.log("in edit post API")
    if (!req.body._id) {
        res.send({ errCode: 1, errNum: 132, Message: "mandatory feild _id is missing" });
        return false;
    }
    var data = [];
    var dataToUpdate = {}, cond = {};
    var i = 0;
    var _id = "_id", token = "token";



    var values = req.body.values;

    for (var j = 0; j < values.length; j++) {
        values[j].pref_id = ObjectId(values[j].pref_id);
    }

    //console.log("Values : "+JSON.stringify(values));
    req.body.values = values;

    for (key in req.body) {

        data.push(key);

        if (data[i] == '_id') {
            cond[key] = ObjectId(req.body[data[i]]);//condition to updatedate
        } else if (data[i] == 'token') {

            //            extra feild which is not for update
        } else {
            dataToUpdate[key] = req.body[data[i]];
            //data for update
        }
        i++;
    }


    //if no data for update then
    if (Object.keys(dataToUpdate).length <= 0) {
        res.send({
            errCode: 1, errNum: 132,
            Message: "Please define any one filed for update"
        });
        return false;
    }





    async.series([

        function (callback) {
            Utility.SelectOne("user", { userId: req.body.userId, "farmDetail.farmID": req.body.farmDetails.farmID }, function (err, result) {

                if (err) {
                    res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
                    return false;
                } else if (result) {

                    var condition = { "userId": req.body.userId, "farmDetail": { "$elemMatch": { "farmID": req.body.farmDetails.farmID } } }
                    var dataToUpdate = {
                        "farmDetail.$.lat": req.body.farmDetails.lat,
                        "farmDetail.$.long": req.body.farmDetails.long,
                        "farmDetail.$.street": req.body.farmDetails.street,
                        "farmDetail.$.apt": req.body.farmDetails.apt,
                        "farmDetail.$.city": req.body.farmDetails.city,
                        "farmDetail.$.state": req.body.farmDetails.state,
                        "farmDetail.$.zipCode": req.body.farmDetails.zipCode,
                        "farmDetail.$.country": req.body.farmDetails.country,
                        "farmDetail.$.farmName": req.body.farmDetails.farmName
                    }


                    Utility.Update("user", condition, dataToUpdate, function (err, result) {
                        if (err) {
                            res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
                            return false;
                        }

                    })
                    callback(err, result);

                } else {
                    Utility.UpdateAsInsertSpecificField("user", { "userId": req.body.userId }, { "farmDetail": req.body.farmDetails }, function (err, result) {
                        if (err) {
                            res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
                            return false;
                        }

                    });
                    callback(err, result);
                }
            });
        },
        function (callback) {
            // delete req.body.farmDetails;

            var location = {
                "longitude": req.body.farmDetails.long,
                "latitude": req.body.farmDetails.lat
            };

            var dataToUpdateInPost = { "location": location };
            console.log("\n\n\n Condition to update : ", cond)
            console.log("\n\n\n dataToUpdateInPost to update : ", dataToUpdateInPost)
            Utility.Update('post', cond, dataToUpdateInPost, function (err, result) { });

            delete dataToUpdate.farmDetails

            Utility.Update('post', cond, dataToUpdate, function (err, result) {

                if (err) {
                    res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
                    return false;
                } else if (result) {

                    res.send({ errCode: 0, Message: "Post updated successfully.", response: {} });
                    return true;


                } else {
                    res.send({ errCode: 1, errNum: 135, Message: "Currently no Post is there, please try after some time." });
                    return true;
                }
            });
        },
    ]);


}

