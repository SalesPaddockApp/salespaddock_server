/**
 * Created by Niranjan on 19/9/16.
 * function name: prodType
 * request: userId,token
 * response: all product type if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;
var limitNumber = 10;

exports.getAdminpostById = function (req, res)
{
if (!req.body._id)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory _id is missing"});
        return false;
    }
    
    var cond =  {"_id": ObjectId(req.body._id)};
            Utility.Select('post', cond, function (err, result)
            {
                if (err)
                {
                    res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
                    return false;
                } else if (result.length > 0)
                {
                    console.log("dataTosend for Post:  " + JSON.stringify(result.length));

                    res.send({errCode: 0, Message: "data send succcessfully.", totalFound: result.length, response: {data: result[0]}});
                    return true;
                } else
                {
                    res.send({errCode: 1, errNum: 135, Message: "Currently no Posts there, please try after some time."});
                    return true;
                }
            });
}


