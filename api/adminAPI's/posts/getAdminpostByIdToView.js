/**
 * Created by Niranjan on 19/9/16.
 * function name: prodType
 * request: userId,token
 * response: all product type if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;
var limitNumber = 10;

exports.getAdminpostByIdToView = function (req, res) {
    console.log("get adminpostby id to view APi")
    if (!req.body._id) {
        res.send({ errCode: 1, errNum: 132, Message: "mandatory _id is missing" });
        return false;
    }


    var cond = [
        { $unwind: "$values" },
        { $match: { "_id": ObjectId(req.body._id), "values.options": { $ne: "" } } },
        { $lookup: { from: "Preferences", "localField": "values.pref_id", foreignField: "_id", as: "mydata" } },
        { $unwind: "$mydata" }, {
            $project: {
                "_id": 1, userId: 1, activeStatus: 1, created: 1, productType: 1, productId: 1,
                values: {
                    "options": "$values.options", "pref_id": "$values.pref_id",
                    "preferenceTitle": "$mydata.preferenceTitle",
                    "onCreateScreen": "$mydata.onCreateScreen",
                    "typeOfPreference": "$mydata.typeOfPreference",
                    "productType": "$mydata.productType",
                    "mandatory": "$mydata.mandatory",
                    "prirority": "$mydata.prirority",
                    "Unit": "$mydata.Unit",
                    "optionsValue": "$mydata.optionsValue"
                }
            }
        },
        {
            $group: {
                "_id": "$_id", "values": { '$push': "$values" },
                "productType": { '$first': "$productType" }, "productId": { '$first': "$productId" }, "userId": { '$first': "$userId" },
                "activeStatus": { '$first': "$activeStatus" }, "created": { '$first': "$created" }
            }
        }
    ];
    Utility.AggregateGroup('post', cond, function (err, result) {
        if (err) {
            res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
            return false;
        } else if (result.length > 0) {
            // console.log("dataTosend for Post:  " + JSON.stringify(result[0]['values'].length));

            var pref_id_not_equls = [];


            for (var i = 0; i < result[0]['values'].length; i++) {
                var datas = [];
                datas.push("Contact for Price")
                pref_id_not_equls.push(
                    { _id: { "$ne": ObjectId(result[0]['values'][i].pref_id) } }
                );

                if (result[0]['values'][i]['preferenceTitle'] == "Price Details") {
                    for (var j = 0; j < result[0]['values'][i]['optionsValue'].length; j++) {
                        datas.push(result[0]['values'][i]['optionsValue'][j]);
                    }
                    result[0]['values'][i]['optionsValue'] = datas;
                }
            }
            var condition = { "$and": pref_id_not_equls,"productType" : "Horse" };
            console.log("condition : ", JSON.stringify(condition));
            Utility.Select("Preferences", condition, function (err, Preferences) {
                if (Preferences[0]) {
                    for (var index = 0; index < Preferences.length; index++) {
                        result[0]['values'].push(Preferences[index])
                    }

                }




                Utility.SelectOne("user", { userId: result[0].userId }, function (err, useData) {
                    if (err) {
                        res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
                        return false;
                    }
                    else if (useData) {
                        farmDetails = useData.farmDetail || [];
                    } else {
                        farmDetails = [];
                    }


                    // console.log("data to send ", result[0]);
                    res.send({
                        errCode: 0, Message: "data send succcessfully.", totalFound: result.length,
                        response: { data: result[0], farmDetails: farmDetails }
                    });
                    return true;
                });
            });
            // console.log("dataTosend for Post:  " + JSON.stringify(result[0]['values']));            


        } else {
            res.send({ errCode: 1, errNum: 135, Message: "Currently no Posts there, please try after some time." });
            return true;
        }
    });
}


