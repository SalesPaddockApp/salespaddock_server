/**
 * Created by Niranjan on 19/9/16.
 * function name: prodType
 * request: userId,token
 * response: all product type if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;
var limitNumber = 10;


exports.getAllActivePosts = function(req, res) {

    var pageNumber = 0;
    if (!req.body.pageNum)
        pageNumber = 0;
    else
        pageNumber = req.body.pageNum;

    var skipNumber = pageNumber * limitNumber;


    // var cond = [
    //     { $unwind: "$values" }, { $unwind: "$values.options" },
    //     { $match: { "values.pref_id": ObjectId("57efa0aa00eba37a3dd379cd"), "activeStatus": "1" } },
    //     { $lookup: { from: 'user', localField: 'userId', foreignField: 'userId', as: 'user' } },
    //     { $lookup: { from: "recentVisitors", "localField": "_id", foreignField: "postId", as: "UserDataforUnique" } },
    //     { $unwind: "$UserDataforUnique" },
    //     {
    //         $group: {
    //             _id: "$_id",
    //             itemsforUnique: { $addToSet: '$UserDataforUnique.userId' },
    //             "userId": { $first: "$userId" },
    //             "activeStatus": { $first: "$activeStatus" },
    //             "created": { $first: "$created" },
    //             "productType": { $first: "$productType" },
    //             "userName": { $first: "$user.name.fName" },
    //             "email": { $first: "$user.email.primaryEmail" },
    //             "productName": { $first: "$values.options" }
    //         }
    //     },
    //     { $lookup: { from: "recentVisitors", "localField": "_id", foreignField: "postId", as: "postData" } },
    //     { $lookup: { from: "wishList", "localField": "_id", "foreignField": "postId", as: "whishData" } },
    //     {
    //         $project: {
    //             _id: 1,
    //             userId: 1,
    //             activeStatus: 1,
    //             created: 1,
    //             productType: 1,
    //             userName: 1,
    //             email: 1,
    //             productName: 1,
    //             totalUniqueView: { $size: "$itemsforUnique" },
    //             totalView: { $size: "$postData" },
    //             countWish: { $size: "$whishData" }
    //         }
    //     },
    // ];

    var cond = [
        { $unwind: "$values" }, { $unwind: "$values.options" },
        { $match: { "values.pref_id": ObjectId("57efa0aa00eba37a3dd379cd"), "activeStatus": "1" } },
        { $lookup: { from: 'user', localField: 'userId', foreignField: 'userId', as: 'user' } },
        { $lookup: { from: "recentVisitors", "localField": "_id", foreignField: "postId", as: "totalView" } },
        { $lookup: { from: "wishList", "localField": "_id", "foreignField": "postId", as: "whishData" } },
        { $unwind: "$user" },
        {
            $project: {
                _id: 1,
                userId: 1,
                activeStatus: 1,
                created: 1,
                productType: 1,
                userName: "$user.name.fName",
                email: "$user.email.primaryEmail",
                productName: "$values.options",
                whishData: 1,
                totalView: 1

            }
        }, {$sort : {_id : -1}},{ $skip: skipNumber }, { $limit: limitNumber }
    ]

    console.log("cond: " +JSON.stringify(cond));

    Utility.AggregateGroup('post', cond, function(err, result) {
        if (err) {
            res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
            return false;
        } else if (result.length > 0) {
            console.log("Data Coming");
            var dataToSend = [];
            var uniqueView = [];

            var _id, userId, activeStatus, created, productType, userName, email, productName, totalUniqueView, totalView, countWish;

            for (var i = 0; i < result.length; i++) {

                _id = result[i]._id;
                userId = result[i].userId;
                activeStatus = result[i].activeStatus;
                created = result[i].created;
                productType = result[i].productType;
                userName = result[i].userName;
                email = result[i].email;
                productName = result[i].productName;

                totalView = result[i].totalView.length;
                countWish = result[i].whishData.length;
                uniqueView = [];

                for (j = 0; j < result[i].totalView.length; j++) {

                    if (uniqueView.length) {
                        match = 1;
                        for (k = 0; k < uniqueView.length; k++) {
                            if (uniqueView[k] == result[i].totalView[j].userId) {
                                match = 0;
                            }
                        }
                        if (match) {
                            uniqueView.push(result[i].totalView[j].userId);
                        }

                    } else {
                        uniqueView.push(result[i].totalView[j].userId);
                    }

                }
                totalUniqueView = uniqueView.length;


                dataToSend.push({
                    _id: _id,
                    userId: userId,
                    activeStatus: activeStatus,
                    created: created,
                    productType: productType,
                    userName: userName,
                    email: email,
                    productName: productName,
                    totalView: totalView,
                    countWish: countWish,
                    totalUniqueView: totalUniqueView
                });

            }







            res.send({ errCode: 0, Message: "data send succcessfully.", totalFound: dataToSend.length, response: { data: dataToSend } });
            return true;
        } else {
            res.send({ errCode: 1, errNum: 135, Message: "Currently no Posts there, please try after some time." });
            return true;
        }
    });
}