/**
 * Created by Niranjan on 27/12/16.
 * function name: getAllIncompletePost
 * request: token
 * response: all incompleted post if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;


exports.getAllIncompletePost = function (req, res)
{
    var condToGetPreferences = {"productType" : "Horse"};
    Utility.Select('Preferences', condToGetPreferences, function (err, result1)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if (result1.length > 0)
        {
            var cond = [{$project: { count: { $size:"$values" },userId : 1,created:1,productType:1,values:1}},
                {$match:{"productType" : "Horse"}},
                {$match : {count :{$lt : result1.length}}},{$lookup:{from:'user',localField:'userId',foreignField:'userId',as:'user'}},
                {$unwind:'$user'},{$project:{userName:'$user.name.fName',userId:1,created:1,values:1,email:'$user.email.primaryEmail',productType:1}}];
            Utility.AggregateGroup('post', cond, function (err, result)
            {
                if (err)
                {
                    res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
                    return false;
                }
                else if (result.length > 0)
                {
                    var incompleteResultToSend = [];
                    for(var resultCount=0; resultCount<result.length; resultCount++)
                    {
                        var productName = '';
                        for(var countOfPostPrefs=0; countOfPostPrefs<result[resultCount]['values'].length; countOfPostPrefs++)
                        {

                            if( result[resultCount]['values'][countOfPostPrefs]['pref_id'].toString() == '57efa0aa00eba37a3dd379cd')
                            {
                                productName = result[resultCount]['values'][countOfPostPrefs]['options'][0];
                                break;
                            }
                        }
                        incompleteResultToSend.push({userName: result[resultCount]['userName'], userId: result[resultCount]['userId'],
                            email: result[resultCount]['email'], productType: result[resultCount]['productType'],
                            created: result[resultCount]['created'],
                             productName: productName,
                            _id:result[resultCount]['_id']});

                    }

                    res.send({errCode: 0, Message: "data send succcessfully.", totalFound: result.length, response: {data: incompleteResultToSend}});
                    return true;
                } else
                {
                    res.send({errCode: 1, errNum: 135, Message: "Currently no Posts there, please try after some time."});
                    return true;
                }
            });
        }
        else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no Posts there, please try after some time."});
            return true;
        }
    });
}

