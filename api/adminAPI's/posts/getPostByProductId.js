/**
 * Created by Niranjan on 19/9/16.
 * function name: prodType
 * request: userId,token
 * response: all product type if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;
var limitNumber = 10;

exports.getPostByProductId = function (req, res)
{
    console.log("get post by product id API")
if (!req.body.productId)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory productId is missing"});
        return false;
    }

    var cond =   [
        {$match:{"productId" : ObjectId(req.body.productId), "activeStatus": "1"}},
        {$unwind:"$values"},{$match:{"values.pref_id":ObjectId("57efa0aa00eba37a3dd379cd")}},
        {$project:{"productName": "$values.options","productId":1, "productType":1, "created":1, "activeStatus":1, "userId":1}},
        {$unwind: "$productName"}

        ];
            Utility.AggregateGroup('post', cond, function (err, result)
            {
                if (err)
                {
                    res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
                    return false;
                } else if (result.length > 0)
                {
                    console.log("dataTosend for Post:  " + JSON.stringify(result.length));

                    res.send({errCode: 0, Message: "data send succcessfully.", totalFound: result.length, response: {data: result}});
                    return true;
                } else
                {
                    res.send({errCode: 1, errNum: 135, Message: "Currently no Posts there, please try after some time."});
                    return true;
                }
            });
}
