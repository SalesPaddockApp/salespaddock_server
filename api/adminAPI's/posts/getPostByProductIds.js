var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;
var limitNumber = 10;

exports.getPostByProductIds = function (req, res)
{
    console.log("get postByproductId API")
    if (!(req.body.postIds).length > 0)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory postId is missing"});
        return false;
    }

    console.log("user:  " + JSON.stringify(req.query));
    var pageNumber = 0;
    if (!req.body.pageNum)
        pageNumber = 0;
    else
        pageNumber = req.body.pageNum;

    var postIds = req.body.postIds;
    console.log(postIds);
    var searchData = [];
    var skipNumber = pageNumber * limitNumber;
    for (var i = 0; i < postIds.length; i++) {
        console.log("user id "+postIds[i]);
        searchData.push({"_id": ObjectId(postIds[i])})
    }
    console.log(searchData);
    var condition =
    [
        {$match:{$or:searchData}},
        {$unwind:"$values"},{$match:{"values.pref_id":ObjectId("57efa0aa00eba37a3dd379cd")}},
        {$project:{"postName": "$values.options","productId":1, "productType":1, "created":1, "activeStatus":1, "userId":1}},
        {$unwind: "$postName"}, { "$skip": skipNumber },
         { "$limit": limitNumber }

    ]

    console.log("Condition is : "+JSON.stringify(condition));

    Utility.AggregateGroup('post', condition, function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        } else if (result.length > 0)
        {

            console.log("dataTosend:  " + JSON.stringify(result));

            res.send({errCode: 0, Message: "data send succcessfully.", response: {data: result}});
            return true;
        } else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no post is there, please try after some time."});
            return true;
        }
    });
}
