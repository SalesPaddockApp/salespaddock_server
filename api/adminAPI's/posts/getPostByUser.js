var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;
var limitNumber = 10;

exports.getPostByUser = function (req, res)
{
    if (!req.body.userId)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory userId is missing"});
        return false;
    }

    console.log("user:  " + JSON.stringify(req.query));
    var pageNumber = 0;
    if (!req.body.pageNum)
        pageNumber = 0;
    else
        pageNumber = req.body.pageNum;

    var skipNumber = pageNumber * limitNumber;

    var condition = [
        {$match: {"userId": req.body.userId}},{$unwind: "$values"}, {$unwind: "$values.options"},
    {$match: {"values.pref_id": ObjectId("57efa0aa00eba37a3dd379cd")}},
    {$lookup: {from: "Preferences", "localField": "values.pref_id", foreignField: "_id", as: "mydata"}},
    {$sort:{"modified":-1}},
    {$project: {"_id": 1, "productType": 1, "userId": 1,created:1,activeStatus:1, productName: "$values.options"}}, 
    { "$skip": skipNumber },
         { "$limit": limitNumber }]

    Utility.AggregateGroup('post', condition, function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        } else if (result.length > 0)
        {

            console.log("dataTosend:  " + JSON.stringify(result));

            res.send({errCode: 0, Message: "data send succcessfully.", response: {data: result}});
            return true;
        } else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no post is there, please try after some time."});
            return true;
        }
    });
}
