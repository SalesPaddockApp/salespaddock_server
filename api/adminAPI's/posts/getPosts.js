/**
 * Created by Niranjan on 19/9/16.
 * function name: prodType
 * request: userId,token
 * response: all product type if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var limitNumber = 10;
var ObjectId = require('mongodb').ObjectID;

exports.getPosts = function (req, res)
{
         var pageNumber = 0;
    if(!req.body.pageNum)
        pageNumber = 0;
    else
        pageNumber = req.body.pageNum;

    var skipNumber = pageNumber*limitNumber;



    var cond = [
        {$unwind: "$values"}, {$unwind: "$values.options"},
        {$match: {"values.pref_id": ObjectId("57efa0aa00eba37a3dd379cd")}},
        {$lookup: {from: "Preferences", "localField": "values.pref_id", foreignField: "_id", as: "mydata"}},
        {$project: {"_id": 1, "productType": 1, "userId": 1,created:1, HorseName: "$values.options"}},
          { "$skip": skipNumber },
         { "$limit": limitNumber }
    ];

    Utility.AggregateGroup('post', cond, function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        } else if (result.length > 0)
        {
            console.log("dataTosend for Post:  " + JSON.stringify(result.length));

            res.send({errCode: 0, Message: "data send succcessfully.", totalFound: result.length, response: {data: result}});
            return true;
        } else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no Posts there, please try after some time."});
            return true;
        }
    });
}


