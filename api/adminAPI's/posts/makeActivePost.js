/**
 * Created by Niranjan on 19/9/16.
 * function name: updatePost
 * request: token,userId,wishId
 * response: succuss response if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;

exports.makeActivePost = function(req, res)
{
    console.log("updatePost: " + JSON.stringify(req.body));
    if(!req.body._ids)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory _ids is missing"});
        return false;
    }
    
    var ids=[];
    for(i=0;i<req.body._ids.length;i++)
    {
        ids[i]=ObjectId(req.body._ids[i]);
    }
    
    var dataToInsert = {"activeStatus":"1",dateOfActivation : Date.now()};
    var cond = {_id:{$in:ids}};

    console.log("cond",JSON.stringify(cond));
    
    Utility.Update('post',cond,dataToInsert,function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result)
        {
            console.log("result",result);
            res.send({errCode: 0,  Message: "you have updated your post successfully.",response: {}});
            return true;
        }
        else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no post is there, please try after some time."});
            return true;
        }
    });
}






