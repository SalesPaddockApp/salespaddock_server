/**
 * Created by Dipen
 * function name: searchInActivePost
 * request: 
 * response: all product type if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;
var limitNumber = 10;


exports.searchInActivePost = function(req, res) {

    var pageNumber = 0;
    if (!req.body.pageNum)
        pageNumber = 0;
    else
        pageNumber = req.body.pageNum;

    var skipNumber = pageNumber * limitNumber;



    Utility.Select('Preferences', { "productType": "Horse" }, function(err, result) {


        var cond = [
            { $match: { values: { $size: result.length }, "activeStatus": "0" } },
            { $unwind: "$values" }, { $unwind: "$values.options" },
            {
                $match: {
                    "values.pref_id": ObjectId("57efa0aa00eba37a3dd379cd"),
                    "activeStatus": "0",
                    $or: [
                        { "values.options": new RegExp("^" + req.body.searchP, "gi") },
                        { "productType": new RegExp("^" + req.body.searchP, "gi") },
                        { "userId": new RegExp("^" + req.body.searchP, "gi") }
                    ]
                }
            },
            { $lookup: { from: "Preferences", "localField": "values.pref_id", foreignField: "_id", as: "mydata" } },
            { $sort: { "modified": -1 } }, { $lookup: { from: 'user', localField: 'userId', foreignField: 'userId', as: 'user' } },
            { $unwind: '$user' },
            {
                $project: {
                    "_id": 1,
                    "productType": 1,
                    "userId": 1,
                    created: 1,
                    activeStatus: 1,
                    productName: "$values.options",
                    userName: '$user.name.fName',
                    email: '$user.email.primaryEmail'
                }
            },
            { "$skip": skipNumber },
            { "$limit": limitNumber }
        ];

        Utility.AggregateGroup('post', cond, function(err, result) {
            if (err) {
                res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
                return false;
            } else if (result.length > 0) {
                console.log("dataTosend for Post:  " + JSON.stringify(result.length));

                res.send({ errCode: 0, Message: "data send succcessfully.", totalFound: result.length, response: { data: result } });
                return true;
            } else {
                res.send({ errCode: 1, errNum: 135, Message: "Currently no Posts there, please try after some time." });
                return true;
            }
        });

    });









}