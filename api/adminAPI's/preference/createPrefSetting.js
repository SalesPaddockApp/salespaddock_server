/**
 * Created by Dipen on 22/11/2016.
 * function name: addWish
 * request: token,userId,wishId
 * response: succuss response if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;

exports.createPrefSetting = function (req, res)
{

    if (!req.body.preferenceTitle)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory preferenceTitle is missing"});
        return false;
    }
    if (req.body.onCreateScreen.toString()=="")
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory onCreateScreen is missing"});
        return false;
    }
    if (!req.body.typeOfPreference)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory typeOfPreference is missing"});
        return false;
    }
    if (!req.body.productType)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory productType is missing"});
        return false;
    }
    if (!req.body.productId)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory productId is missing"});
        return false;
    }
    if (req.body.mandatory.toString()=="")
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory mandatory is missing"});
        return false;
    }

    if (!req.body.Unit)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory Unit is missing"});
        return false;
    }
    if (!req.body.optionsValue)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory optionsValue is missing"});
        return false;
    }

    
 var prirority = 1;
    Utility.AggregateGroup('Preferences', [{$match:{productType:req.body.productType}},{$sort: {'prirority': -1}}, {$limit: 1}], function (err1, result1) {
       
        prirority = 1;
        console.log("befor if : prirority "+prirority);
        console.log("befor if : "+JSON.stringify(result1));
        console.log("result1.lenght : "+result1.length);
        if (result1.length>0) {
            console.log("result1[0].prirority : "+result1[0].prirority);
            prirority = result1[0].prirority + 1;
            console.log("in if : "+prirority);
            
        }
        console.log("after if : "+prirority);


        var data = {preferenceTitle: req.body.preferenceTitle,
            onCreateScreen: req.body.onCreateScreen,
            typeOfPreference: req.body.typeOfPreference,
            productType: req.body.productType,
            mandatory: req.body.mandatory,
            prirority: prirority,
            Unit: req.body.Unit,
            productId:ObjectId(req.body.productId),
            optionsValue: req.body.optionsValue};
        Utility.Insert('Preferences', data, function (err, result)
        {
            if (err)
            {
                res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
                return false;
            } else if (result)
            {

                res.send({errCode: 0, Message: "your Preferences saved successfully.", response: {}});
                return true;
            } else
            {
                res.send({errCode: 1, errNum: 135, Message: "Currently no Preferences is there, please try after some time."});
                return true;
            }
        });

    });




}

