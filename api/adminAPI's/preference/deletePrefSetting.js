/**
 * Created by Dipen on 22/11/2016.
 * function name: deleteProducttype
 * request: _id
 * response: succuss response if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;


exports.deletePrefSetting = function (req, res)
{
    if (!req.body._id)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory feild _id is missing"});
        return false;
    }
    if (!req.body.prirority)
    {
        res.send({errCode: 1, errNum: 133, Message: "mandatory feild prirority is missing"});
        return false;
    }
    if (!req.body.productType)
    {
        res.send({errCode: 1, errNum: 134, Message: "mandatory feild productType is missing"});
        return false;
    }

    Utility.Delete('Preferences', {$and: [{"_id": ObjectId(req.body._id), productType: req.body.productType,
                prirority: parseInt(req.body.prirority)}]}, function (err, result)
    {
        if (err) {
            res.send({errCode: 1, errNum: 103, Message: "data not deleted"});
            return false;
        } else if (result) {



            Utility.Select('Preferences', {prirority: {$gt: parseInt(req.body.prirority)}}, function (err1, result1) {

                for (var i = 0; i < result1.length; i++) {
                    Utility.Update('Preferences', {"_id": ObjectId(result1[i]._id), productType: req.body.productType},
                            {prirority: result1[i].prirority - 1}, function (err2, result2) {});
                }


                if (err1)
                {
                    res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
                    return false;
                } else if (result1)
                {

                    res.send({errCode: 0, Message: "your Preferences Deleted successfully.", response: {}});
                    return true;


                } else
                {
                    res.send({errCode: 1, errNum: 135, Message: "Currently no Preferences is there, please try after some time."});
                    return true;
                }
            });






        } else {
            res.send({errCode: 1, errNum: 105, Message: "some error"});
            return false;
        }



    });


}

