/**
 * Created by Dipen on 22/11/2016.
 * function name: editProducttype
 * request: _id,productName
 * response: succuss response if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;


exports.editPrefSettng = function (req, res)
{
    if (!req.body._id)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory feild _id is missing"});
        return false;
    }
    
    var dataToUpdate = {};
    if (req.body.preferenceTitle)
    {
        dataToUpdate['preferenceTitle'] = req.body.preferenceTitle;
    }
    
    if (JSON.stringify(req.body.onCreateScreen))
    {
        dataToUpdate['onCreateScreen'] = req.body.onCreateScreen;
    }
    
    if(req.body.productType)
    {
       dataToUpdate['productType'] = req.body.productType; 
    }
    
    if(JSON.stringify(req.body.mandatory))
    {
       dataToUpdate['mandatory'] = req.body.mandatory; 
    }
    
    if(req.body.Unit)
    {
       dataToUpdate['Unit'] = req.body.Unit; 
    }
    
    if(req.body.optionsValue)
    {
       dataToUpdate['optionsValue'] = req.body.optionsValue; 
    }
    if(Object.keys(dataToUpdate).length<=0){
          res.send({errCode: 1, errNum: 132,
Message: "Please define any one optional filed (preferenceTitle,onCreateScreen,productType,mandatory,Unit,optionsValue) is missing"});
        return false;
    }
    console.log("Data To Update  : "+JSON.stringify(req.body));
    var cond = {_id: ObjectId(req.body._id)};
    Utility.Update('Preferences', cond, dataToUpdate, function (err, result)
    {
        
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        } else if (result)
        {

            res.send({errCode: 0, Message: "your Preferences updated successfully.", response: {}});
            return true;


        } else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no Preferences is there, please try after some time."});
            return true;
        }
    });


}

