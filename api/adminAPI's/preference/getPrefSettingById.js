/**
 * Created by Niranjan on 19/9/16.
 * function name: prodType
 * request: userId,token
 * response: all product type if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;
 limitNumber = 10;

exports.getPrefSettingById = function (req, res)
{
    if (!req.body._id)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory _id is missing"});
        return false;
    }

    console.log("Preferences:  " + JSON.stringify(req.query));
    var pageNumber = 0;
    if (!req.body.pageNum)
        pageNumber = 0;
    else
        pageNumber = req.body.pageNum;

    var skipNumber = pageNumber * limitNumber;

    var searchData = {_id:ObjectId(req.body._id)};

    Utility.SelectWithPagination('Preferences', searchData, {}, skipNumber, limitNumber, function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        } else if (result.length > 0)
        {
           var dataTosend = [];
            for(var i=0; i<result.length; i++)
            {
                var _id,preferenceTitle,onCreateScreen,typeOfPreference,productType,
                mandatory,prirority,Unit,optionsValue;
                
                if(result[i]['preferenceTitle']){
                    _id = result[i]['_id'];
                    preferenceTitle = result[i]['preferenceTitle'];
                    onCreateScreen = result[i]['onCreateScreen'];
                    typeOfPreference = result[i]['typeOfPreference'];
                    productType = result[i]['productType'];
                    mandatory = result[i]['mandatory'];
                    prirority = result[i]['prirority'];
                    Unit = result[i]['Unit'];
                    optionsValue = result[i]['optionsValue'];
                    
                }
           

                dataTosend.push({_id:_id,preferenceTitle:preferenceTitle,onCreateScreen:onCreateScreen,
                    typeOfPreference:typeOfPreference,productType:productType,
                mandatory:mandatory,prirority:prirority,Unit:Unit,optionsValue:optionsValue});
            }

            console.log("dataTosend:  " + JSON.stringify(dataTosend));

            res.send({errCode: 0, Message: "data send succcessfully.", response: {data: dataTosend[0]}});
            return true;
        } else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no Preferences is there, please try after some time."});
            return true;
        }
    });
}


