/**
 * Created by Niranjan on 19/9/16.
 * function name: prodType
 * request: userId,token
 * response: all product type if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;


exports.getPrefToCreatePost = function(req, res)
{

    console.log("Preferences:  " + JSON.stringify(req.query));
     if(!req.body.productId)
        {
            res.send({errCode: 1, errNum: 142, Message: "Mandatory productId is missing."});
            return false;
        }
 
    Utility.AggregateGroup('Preferences',[{$match:{$or:[{productId:ObjectId(req.body.productId)},
                    {"_id":ObjectId("584bd48a6696a8166bd9cb65")}]}},{$sort: {'prirority': 1}}],function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result.length > 0)
        {
            var dataTosend = [];
            for(var i=0; i<result.length; i++)
            {
                var _id,preferenceTitle,onCreateScreen,typeOfPreference,productType,
                mandatory,prirority,Unit,optionsValue;
                
                if(result[i]['preferenceTitle']){
                    if(result[i]['preferenceTitle'] == "Price Details")
                    {
                         var values = [];
                        values.push("Contact for price");

                        var values1 =  result[i]['optionsValue'];
                        //values1.sort()
                        for(var k=0; k<values1.length; k++)
                        {
                            values.push(values1[k]);
                        }
                         _id = result[i]['_id'];
                        preferenceTitle = result[i]['preferenceTitle'];
                        onCreateScreen = result[i]['onCreateScreen'];
                        typeOfPreference = result[i]['typeOfPreference'];
                        productType = result[i]['productType'];
                        mandatory = result[i]['mandatory'];
                        prirority = result[i]['prirority'];
                        Unit = result[i]['Unit'];
                        optionsValue = values;

                        console.log("values1",values);
                    }
                    else
                    {
                          _id = result[i]['_id'];
                        preferenceTitle = result[i]['preferenceTitle'];
                        onCreateScreen = result[i]['onCreateScreen'];
                        typeOfPreference = result[i]['typeOfPreference'];
                        productType = result[i]['productType'];
                        mandatory = result[i]['mandatory'];
                        prirority = result[i]['prirority'];
                        Unit = result[i]['Unit'];
                        optionsValue = result[i]['optionsValue'];
                    }
                }
           

                dataTosend.push({_id:_id,preferenceTitle:preferenceTitle,onCreateScreen:onCreateScreen,
                    typeOfPreference:typeOfPreference,productType:productType,
                mandatory:mandatory,prirority:prirority,Unit:Unit,optionsValue:optionsValue});
            }

            console.log("dataTosend:  " + JSON.stringify(dataTosend));

            res.send({errCode: 0,  Message: "data send succcessfully.",TotalFound:result.length,response:{data : dataTosend}});
            return true;
        }
        else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no Preferences there, please try after some time."});
            return true;
        }
    });
}


