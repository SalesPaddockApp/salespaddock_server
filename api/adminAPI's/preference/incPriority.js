/**
 * Created by Dipen on 22/11/2016.
 * function name: editProducttype
 * request: _id,productName
 * response: succuss response if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;


exports.incPriority = function (req, res)
{
    if (!req.body._id)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory feild _id is missing"});
        return false;
    }
    if (!req.body.priority)
    {
        res.send({errCode: 1, errNum: 133, Message: "mandatory feild priority is missing"});
        return false;
    } if (!req.body.productType)
    {
        res.send({errCode: 1, errNum: 134, Message: "mandatory feild productType is missing"});
        return false;
    }
    var currentPriority = parseInt(req.body.priority);
    var conddition = {_id: ObjectId(req.body._id)};
    var forpdate = {prirority: parseInt(parseInt(currentPriority) - 1)};



    Utility.Update('Preferences', conddition, forpdate, function (err, result)
    {


        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        } else if (result)

        {
            var conddition1 = {_id: {$ne: ObjectId(req.body._id)}, productType:req.body.productType,
                    prirority: parseInt(currentPriority - 1)};
            var forpdate1 = {prirority: parseInt(currentPriority)};
            Utility.Update('Preferences', conddition1, forpdate1, function (err1, result1)
            {
                if (result1) {
                    res.send({errCode: 0, Message: "your prirority updated successfully.", response: {}});
                    return true;
                } else if (err1)
                {
                    res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
                    return false;
                }

            });

        } else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no Preferences is there, please try after some time."});
            return true;
        }
    });
}



