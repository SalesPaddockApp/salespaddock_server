/**
 * Created by Dipen on 22/11/2016.
 * function name: addWish
 * request: token,userId,wishId
 * response: succuss response if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');


exports.createProducttype = function(req, res)
{
    if(!req.body.productName)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory productName is missing"});
        return false;
    }
    if(!req.body.imgPath)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory imgPath is missing"});
        return false;
    }

 
            Utility.Insert('procuctType',{name:req.body.productName,imgPath:req.body.imgPath},function (err, result)
            {
                if (err)
                {
                    res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
                    return false;
                }
                else if(result)
                {

                    res.send({errCode: 0,  Message: "your procuctType saved successfully.",response: {}});
                    return true;
                }
                else
                {
                    res.send({errCode: 1, errNum: 135, Message: "Currently no product is there, please try after some time."});
                    return true;
                }
            });
    

}

