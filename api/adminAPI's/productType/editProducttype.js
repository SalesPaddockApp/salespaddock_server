/**
 * Created by Dipen on 22/11/2016.
 * function name: editProducttype
 * request: _id,productName
 * response: succuss response if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;


exports.editProducttype = function (req, res)
{
    if (!req.body._id)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory product _id is missing"});
        return false;
    }
    if (!req.body.productName)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory productName is missing"});
        return false;
    }
    if (!req.body.imgPath)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory imgPath is missing"});
        return false;
    }

    var cond ={_id : ObjectId(req.body._id)};
    var updateData = { name : req.body.productName,imgPath:req.body.imgPath};
    
    Utility.Update('procuctType',cond,updateData, function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        } else if (result)
        {

                res.send({errCode: 0, Message: "your procuctType updated successfully.", response: {}});
                return true;
          

        } else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no product is there, please try after some time."});
            return true;
        }
    });


}

