/**
 * Created by Niranjan on 19/9/16.
 * function name: prodType
 * request: userId,token
 * response: all product type if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
const limitNumber = 10;

exports.productType = function(req, res)
{

    console.log("product type:  " + JSON.stringify(req.query));
    var pageNumber = 0;
    if(!req.body.pageNum)
        pageNumber = 0;
    else
        pageNumber = req.body.pageNum;

    var skipNumber = pageNumber*limitNumber;


    Utility.SelectWithPagination('procuctType',{},{},skipNumber,limitNumber,function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result.length > 0)
        {
            var dataTosend = [];
            for(var i=0; i<result.length; i++)
            {
                var name = "";
                if(result[i]['name'])
                    name = result[i]['name'];

                var image = "";
                if(result[i]['image'])
                    image = result[i]['image'];

                dataTosend.push({name: name,image: image});
            }

            console.log("dataTosend:  " + JSON.stringify(dataTosend));

            res.send({errCode: 0,  Message: "data send succcessfully.",response:{data : dataTosend}});
            return true;
        }
        else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no product is there, please try after some time."});
            return true;
        }
    });
}


