/**
 * Created by Niranjan on 19/9/16.
 * function name: prodType
 * request: userId,token
 * response: all product type if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
 limitNumber = 10;

exports.searchProducttypes = function(req, res)
{
  if (!req.body.productName)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory productName is missing"});
        return false;
    }
   
    console.log("product type:  " + JSON.stringify(req.query));
    var pageNumber = 0;
    if(!req.body.pageNum)
        pageNumber = 0;
    else
        pageNumber = req.body.pageNum;

    var skipNumber = pageNumber*limitNumber;
    
            var searchData = {name : new RegExp("^"+req.body.productName,"gi")};
//             new RegExp(req.body.productName);

    Utility.SelectWithPagination('procuctType',searchData,{},skipNumber,limitNumber,function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result.length > 0)
        {
            var dataTosend = [];
            for(var i=0; i<result.length; i++)
            {
                var name = "";
                var id="";
                var imgPath="";
                if(result[i]['name']){
                    name = result[i]['name'];
                    id = result[i]['_id'];
                    imgPath = result[i]['imgPath'];
                }
           

                dataTosend.push({productName: name,_id:id,imgPath:imgPath});
            }

            console.log("dataTosend:  " + JSON.stringify(dataTosend));

            res.send({errCode: 0,  Message: "data send succcessfully.",response:{data : dataTosend}});
            return true;
        }
        else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no product is there, please try after some time."});
            return true;
        }
    });
}


