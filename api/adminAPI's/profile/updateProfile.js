exports.updateData = function(cond,dataToUpdate,userId,callback) 
{

    var Utility = require('../UtilityFunc');
    
        Utility.Update('user',cond,dataToUpdate,function(err,result)
        {
            if(err)
            {
                console.log(err);
                return callback(err,{errCode: 1, errNum: 104, Message: "unknown error occurred."});
            }
            else if(result)
            {
                return callback(null,{errCode: 0, Message: "profile updated successfull.", response:{userId: userId}});
            }
            else
            {
                return callback(null,{errCode: 1, errNum: 134, Message: "Wrong user id, please check."});
            }
            
        })
}