/**
 * Created by Niranjan on 19/9/16.
 * function name: viewProfile
 * request: token,userId
 * response: all data of specific user but only related data if  no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
const limitNumber = 10;

exports.viewProfile = function(req, res)
{
    if(!req.body.userId)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory email is missing"});
        return false;
    }

    var cond = {userId: req.body.userId};

    console.log("cond:  " +JSON.stringify(cond));

    Utility.SelectOne('user',cond,function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result)
        {
            var lName = '';
            if(result.name.lName)
                lName = result.name.lName;

            var dob = '';
            if(result.dob)
            {
                var mainDate = new Date(result.dob);

                var datee = mainDate.getDate();
                var month = mainDate.getMonth() + 1;
                var year = mainDate.getYear();

                if(datee < 10)
                    datee = "0" + datee;

                if(month < 10)
                    month = "0" + month;

                dob = month + "/" + datee + "/" + year;

            }

            var profilePic = '';
            if(result.profilePic)
                profilePic = result.profilePic;

            var about = '';
            if(result.about)
                about = result.about;

            var address = '';
            if(result.address)
                address = result.address;

            var gender = '';
            if(result.gender)
                gender = result.gender.toString();

            var dataToSend = {fName: result.name.fName,lName: lName,
                dob: dob, email: result.email.primaryEmail,profilePic: profilePic,phone: result.phone,
                gender: gender,about : about,address : address,location : result.location,
                work : result.work,company : result.company,school : result.school,wEmail : result.email.workEmail};

            res.send({errCode: 0,  Message: "data send succcessfully.",response: dataToSend});
            return true;
        }
        else
        {
            res.send({errCode: 1, errNum: 135, Message: "Unknown error occurred."});
            return true;
        }
    });
}



