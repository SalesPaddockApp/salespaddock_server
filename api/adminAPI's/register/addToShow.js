/**
 * Created by Niranjan on 26/9/16.
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');

var ObjectId = require('mongodb').ObjectID;

exports.addToShow = function(req, res)
{
    if(!req.body.userId)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory user id is missing"});
        return false;
    }

    if(!req.body.postId)
    {
        res.send({errCode: 1, errNum: 145, Message: "mandatory post id is missing"});
        return false;
    }

    var cond = {};
    var dataToInsert= {posts: args.postId};

    Utility.AddIntoArray('shows',cond,dataToInsert,function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result)
        {
            res.send({errCode: 0,  Message: "You have added your post to the show successfully.",response: {}});

        }
        else
        {
            res.send({errCode: 1, errNum: 144, Message: "You have selected a wrong show."});
            return true;
        }
    });
}


