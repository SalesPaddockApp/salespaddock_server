module.exports = function(app,express)
{
var router = express.Router();

var Utility = require('../UtilityFunc');
var jsonwebtoken = require('jsonwebtoken');



var sendMail = require('../email/sendMail');

var conf = require('../conf');

var secretKey = conf.secretKey;

router.post('/register',function(req,res)
{
    if(!req.body.fName)
    {
        res.send({errCode: 1, errNum: 107, Message: "mandatory name is missing"});
        return false;
    }

    if(!req.body.email)
    {
        res.send({errCode: 1, errNum: 108, Message: "mandatory email is missing"});
        return false;
    }

    if(!req.body.password)
    {
        res.send({errCode: 1, errNum: 109, Message: "mandatory password is missing"});
        return false;
    }

    //if(!req.body.deviceId)
    //{
    //    res.send({errCode: 1, errNum: 109, Message: "mandatory password is missing"});
    //    return false;
    //}
    var cond = {"email.primaryEmail" : req.body.email.toUpperCase()};

    Utility.SelectOne('user',cond, (function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result)
        {
            res.send({errCode: 1, errNum: 112, Message: "Email already exists"});
            return false;
        }
        else
        {
            var dataForMail = {to: req.body.email,subject: 'email verification'};
            sendMail.sendMail(dataForMail,function(err,ress)
            {
                if(err)
                    console.dir("send mail: " +JSON.stringify(err))
            });

            // var dob = new Date(req.body.dob).getTime();
            var userId = 'UId' + (Math.floor(Math.random()*9000000)+1000000);
            var data = {name:{lName: req.body.lName,fName: req.body.fName},
                password: req.body.password,devices: [{deviceId: req.body.deviceId}],
                email: {primaryEmail: req.body.email.toUpperCase(),profilePic: req.body.profilePic,
                isPrimarVerified:0},userId : userId,pushToken: req.body.pushToken};

            Utility.Insert('user',data, function (err, isInserted)
            {
                if(err)
                {
                    console.log(err);
                    res.send({errCode: 1, errNum: 104, Message: "unknown error occurred."});
                }
                else if(isInserted)
                {
                    var user = {userId:userId,name:req.body.fName,email: req.body.email.toUpperCase()}
                    var token = createToken(user);
                    res.send({errCode: 0, Message: "Sign up successfull.", response:{userId: userId,token:token}});
                }
                else
                {
                    res.send({errCode: 1, errNum: 105, Message: "Unknown error occurred"});
                    return false;
                }

            });
        }
    }));
});


function createToken(user) {
    var token = jsonwebtoken.sign({id: user.userId,name: user.name,email: user.email}, secretKey, {expiresIn: '60 days'});
    return token;
}
   return router;
};