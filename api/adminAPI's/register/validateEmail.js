/**
 * Created by Niranjan on 8/10/16.
 */
var exports = module.exports = {};
var notification = require('../notification/sendNotification');
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;

exports.validateEmail = function(req,res)
{
    console.log("sendAlerts: " +JSON.stringify(req.body));
    if(!req.body.email)
    {
        res.send({errCode: 1, errNum: 108, Message: "mandatory email is missing"});
        return false;
    }

    var cond= {"email.primaryEmail" : req.body.email.toUpperCase()};

    console.log("cond: " +JSON.stringify(cond));
    Utility.Select('user',cond,function(err,result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result.length > 0)
        {
            res.send({errCode: 1, errNum: 155, Message: "Email already exists."});
            return true;
        }
        else
        {
            res.send({errCode: 0, Message: "Email is available."});
            return true;
        }
    })
};