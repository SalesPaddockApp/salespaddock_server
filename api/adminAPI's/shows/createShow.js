/**
 * Created by Niranjan on 19/9/16.
 * function name: createPost
 * request: token,userId,wishId
 * response: succuss response if no error else error accordingly
 * active = 0 = inactive
 * active = 1 = active
 * active = 2 = completed
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;

exports.createShow = function (req, res)
{

    if (!req.body.showName)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory showName is missing"});
        return false;
    }
    if (!req.body.startDate)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory startDate is missing"});
        return false;
    }
    if (!req.body.endDate)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory endDate is missing"});
        return false;
    }
    if (!req.body.venue)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory venue is missing"});
        return false;
    }
    if (!req.body.productType)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory productType is missing"});
        return false;
    }
    if (!req.body.lat)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory lat is missing"});
        return false;
    }
    if (!req.body.lng)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory lng is missing"});
        return false;
    }
    if (!req.body.imgPath)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory imgPath is missing"});
        return false;
    }
    if (!req.body.Description)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory Description is missing"});
        return false;
    }
    if (!req.body.posts)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory posts is missing"});
        return false;
    }
    if (!req.body.productId)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory productId is missing"});
        return false;
    }
    var posts = req.body.posts;

     for(var i=0;i<posts.length;i++){
        posts[i].postId = ObjectId(posts[i].postId)
    }

   
    var dataToInsert = {
        showName: req.body.showName,
        startDate:parseInt(new Date(req.body.startDate).getTime()),
        endDate: parseInt(new Date(req.body.endDate).getTime()),
        venue: req.body.venue,
        productType: req.body.productType,
        lat: parseFloat(req.body.lat),
        lng:  parseFloat(req.body.lng),
        imgPath: req.body.imgPath,
        Description: req.body.Description,
        active:"0",
        posts: posts,
        productId: req.body.productId
    };
    Utility.Insert('shows', dataToInsert, function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        } else if (result)
        {
            console.log("result:  insertedddd");
            res.send({errCode: 0, Message: "you have created one show successfully.", response: {ShowId: result['ops'][0]['_id']}});
            return true;
        } else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no show is there, please try after some time."});
            return true;
        }
    });
}






