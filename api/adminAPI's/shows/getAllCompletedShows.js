/**
 * Created by Niranjan on 23/9/16.
 * function name:  getShows
 * request: token,userId
 * response: all upcoming shows, recelntly viewed shows, and popular shows.
 * active = 0 = inactive
 * active = 1 = active
 * active = 2 = completed
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var moment = require("moment");


exports.getAllCompletedShows = function(req, res)
{

  var dateTo = moment().valueOf();
    var cond = {active: "1",endDate:{$lte:dateTo}};

    console.log(cond);

    Utility.Select('shows',cond,function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result.length > 0)
        {

            res.send({errCode: 0,  Message: "shows send successfully.",totalFound:result.length, response:{data :result}});


        }
        else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no show is there, please try after some time."});
            return true;
        }
    });
}
