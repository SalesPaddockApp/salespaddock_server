/**
 * Created by Niranjan on 23/9/16.
 * function name:  getShows
 * request: token,userId
 * response: all upcoming shows, recelntly viewed shows, and popular shows.
 * active = 0 = inactive
 * active = 1 = active
 * active = 2 = completed
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');


exports.getAllInactiveShows = function(req, res)
{
    
    var cond = {"active":"0"};

    //Utility.SelectWithPagination('shows',cond,projection,skip,limit,function (err, result)
    Utility.Select('shows',cond,function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result.length > 0)
        {
            var d,formattedDate;

            for(var i=0;i<result.length;i++){

                d = new Date(result[i].startDate);
                formattedDate =  d.getFullYear()+ "-" + (d.getMonth() + 1) + "-" +d.getDate();
                result[i].startDate=formattedDate;

                d = new Date(result[i].endDate);
                formattedDate =  d.getFullYear()+ "-" + (d.getMonth() + 1) + "-" +d.getDate();
                result[i].endDate=formattedDate;


            }
           res.send({errCode: 0,  Message: "shows send successfully.",totalFound:result.length,  response:{data :result}});

        }
        else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no product is there, please try after some time."});
            return true;
        }
    });
}

