/**
 * Created by Dipen on /12/16.
 * function name: prodType
 * request: userId,token
 * response: all product type if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;
 

exports.getParticipantsById = function (req, res)
{
    if (!req.body._id)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory _id is missing"});
        return false;
    }

condition=[
{$match:{"_id" : ObjectId(req.body._id)}},
{$unwind:"$posts"},
{$lookup:{from:"post",localField:"posts.postId",foreignField:"_id", as :"PostData"}},
{$project:{"productType":1,"PostData":1}},
{$unwind:"$PostData"},{$unwind:"$PostData.values"},{$unwind:"$PostData.values.options"},
{$match: {"PostData.values.pref_id": ObjectId("57efa0aa00eba37a3dd379cd")}},
{$project:{productType:1,PostData:1,postName:"$PostData.values.options"}},
{$lookup:{from:"user",localField:"PostData.userId",foreignField:"userId",as:"UserData"}},
{$unwind:"$UserData"},
{$project:{productType:1,postDate:"$PostData.created",postName:1,userId:"$PostData.userId",
    "fName":"$UserData.name.fName","lName":"$UserData.name.lName"}}   
];

    Utility.AggregateGroup('shows', condition, function (err, result)
     {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        } else if (result.length > 0)
        {
            console.log("dataTosend:  " + JSON.stringify(result));
            res.send({errCode: 0, Message: "data send succcessfully.", response: {data: result}});
            return true;
        } else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no data is there, please try after some time."});
            return true;
        }
    });
}


