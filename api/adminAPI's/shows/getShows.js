/**
 * Created by Niranjan on 23/9/16.
 * function name:  getShows
 * request: token,userId
 * response: all upcoming shows, recelntly viewed shows, and popular shows.
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');

var ObjectId = require('mongodb').ObjectID;

exports.getShows = function(req, res)
{
    var cond = {active: "1"};

    Utility.Select('shows',cond,function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result.length > 0)
        {
            var curDate = Date.now();
            var onGoingShows = [];//active shows

            console.log("result:  " + JSON.stringify(result));

            for(var i=0; i<result.length; i++)
            {
                var sdate = '';
                if(result[i]['startDate'])
                {
                    var mainDate1 = new Date(parseFloat(result[i]['startDate']));

                    var datee1 = mainDate1.getDate();
                    var month1 = mainDate1.getMonth() + 1;
                    var year1 = mainDate1.getFullYear();

                    if(datee1 < 10)
                        datee1 = "0" + datee1;

                    if(month1 < 10)
                        month1 = "0" + month1;

                    sdate = month1 + "/" + datee1 + "/" + year1;

                }

                var edate = '';
                if(result[i]['endDate'])
                {
                    var mainDate = new Date(parseFloat(result[i]['endDate']));

                    var datee = mainDate.getDate();
                    var month = mainDate.getMonth() + 1;
                    var year = mainDate.getFullYear();

                    if(datee < 10)
                        datee = "0" + datee;

                    if(month < 10)
                        month = "0" + month;

                    edate = month + "/" + datee + "/" + year;

                }

                console.log("result[i]['startDate']:  " + result[i]['startDate']);
                console.log("curDate:  " + curDate);
                console.log("result[i]['endDate']:  " + result[i]['endDate']);


                if(!(parseFloat(result[i]['endDate']) < parseFloat(curDate))){
                    onGoingShows.push(result[i])
                }
            }

            var dataToSend = [];
            if(onGoingShows.length > 0)
            {
                dataToSend = onGoingShows;
            }


            res.send({errCode: 0,  Message: "All shows send successfully.",response:dataToSend});

        }
        else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no show is there, please try after some time."});
            return true;
        }
    });
}

