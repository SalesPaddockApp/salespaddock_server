/**
 * Created by Niranjan on 23/9/16.
 * function name:  getShows
 * request: token,userId
 * response: all upcoming shows, recelntly viewed shows, and popular shows.
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');

var ObjectId = require('mongodb').ObjectID;

exports.getShowsList = function(req, res)
{
 

    var cond = [{$project:{_id:1,showName:1,startDate:1,endDate:1,venue:1,productType:1,imgPath:1,Description:1,postsSize:{$size:"$posts"}}}];

    //Utility.SelectWithPagination('shows',cond,projection,skip,limit,function (err, result)
    Utility.AggregateGroup('shows',cond,function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result.length > 0)
        {
            var d,formattedDate;

            for(var i=0;i<result.length;i++){

                 d = new Date(result[i].startDate);
                 formattedDate =  d.getFullYear()+ "-" + ("0" + (d.getMonth() + 1)).slice(-2) + "-" +("0" + d.getDate()).slice(-2);
                 result[i].startDate=formattedDate;

                d = new Date(result[i].endDate);
                formattedDate =  d.getFullYear()+ "-" + ("0" + (d.getMonth() + 1)).slice(-2) + "-" +("0" + d.getDate()).slice(-2);
                result[i].endDate=formattedDate;


            }

           res.send({errCode: 0,  Message: "All shows send successfully.",totalFound:result.length,response:result});

        }
        else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no product is there, please try after some time."});
            return true;
        }
    });
}

