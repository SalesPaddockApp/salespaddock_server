/**
 * Created by Dipen on 05/01/17.
 * function name:  getUpcomingShows
 * request: 
 * response: all upcoming shows, recelntly viewed shows, and popular shows.
 * 
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var moment = require("moment");

exports.getUpcomingShows = function(req, res)
{



var dateTo = moment().add(7,'d').valueOf();
    var cond = {active: "1",startDate:{$gte:dateTo}};

    console.log(cond);

    Utility.Select('shows',cond,function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result.length > 0)
        {
                    
            res.send({errCode: 0,  Message: "shows send successfully.",totalFound:result.length, response:{data :result}});


        }
        else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no show is there, please try after some time."});
            return true;
        }
    });
}

