/**
 * Created by Niranjan on 19/9/16.
 * function name: updatePost
 * request: token,userId,wishId
 * response: succuss response if no error else error accordingly
 * active = 0 = inactive
 * active = 1 = active
 * active = 2 = completed
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;

exports.makeShowInactive = function(req, res)
{
    console.log("update : " + JSON.stringify(req.body));
     if(!req.body._ids)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory _ids is missing"});
        return false;
    }
    
    var ids=[];
    for(i=0;i<req.body._ids.length;i++){
    ids[i]=ObjectId(req.body._ids[i]);
    
    }
    
    var dataToInsert = {"active": "0"};
    var cond = {_id:{$in:ids}};
    Utility.Update('shows',cond,dataToInsert,function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result)
        {
            res.send({errCode: 0,  Message: "you have updated your data successfully.",response: {}});
            return true;
        }
        else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no data is there, please try after some time."});
            return true;
        }
    });
}






