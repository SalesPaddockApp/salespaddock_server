/**
 * Created by Niranjan on 19/9/16.
 * function name: prodType
 * request: userId,token
 * response: all product type if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');


exports.searchShows = function (req, res)
{
    if (!req.body.searchP)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory searchP is missing"});
        return false;
    }

    console.log("Shows :  " + JSON.stringify(req.query));


    var cond = [{$match: {  $or: [
                    {"showName": new RegExp("^" + req.body.searchP, "gi")},
                    {"venue": new RegExp("^" + req.body.searchP, "gi")},
                    {"productType": new RegExp("^" + req.body.searchP, "gi")}]}},
        {$project:{_id:1,showName:1,startDate:1,endDate:1,venue:1,productType:1,post:1}}];

    Utility.AggregateGroup('shows', cond, function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        } else if (result.length > 0)
        {
            console.log("dataTosend for shows :  " + JSON.stringify(result.length));
            var d,formattedDate;

            for(var i=0;i<result.length;i++){

                d = new Date(result[i].startDate);
                formattedDate =  d.getFullYear()+ "-" + ("0" + (d.getMonth() + 1)).slice(-2) + "-" +("0" + d.getDate()).slice(-2);
                result[i].startDate=formattedDate;

                d = new Date(result[i].endDate);
                formattedDate =  d.getFullYear()+ "-" + ("0" + (d.getMonth() + 1)).slice(-2) + "-" +("0" + d.getDate()).slice(-2);
                result[i].endDate=formattedDate;


            }

            res.send({errCode: 0, Message: "data send succcessfully.", totalFound: result.length, response: {data: result}});
            return true;
        } else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no shows there, please try after some time."});
            return true;
        }
    });
}


