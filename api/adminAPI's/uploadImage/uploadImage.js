/**
 * Created by Niranjan on 5/10/16.
 */

var exports = module.exports = {};
var Utility = require('../UtilityFunc');

var ObjectId = require('mongodb').ObjectID;

exports.uploadImage = function(req, res)
{
    if(!req.body.data)
    {
        res.send({errCode: 1, errNum: 149, Message: "mandatory image data is missing"});
        return false;
    }



    //get number and the code for verication
    var fileData = base64_decode(req.body.data);

        var ImageName = Date.now() + ".png";

        // set where the file should actually exists - in this case it is in the "images" directory
        var target_path = '/var/www/html/images/' + ImageName;

        fs.appendFile(target_path, fileData, 'binary', function (err,res)
        {
            if (err)
            {
                console.log("getting error: " + err);
                res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            }
            else
            {
                res.json({'errCode': 0, 'Message': 'Uploaded', response:{'Url': 'http://45.55.233.55/images/' + ImageName}});
            }
        });

}

function base64_decode(data) {


    var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
    var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
        ac = 0,
        dec = '',
        tmp_arr = [];
    if (!data) {
        return data;
    }

    data += '';
    do { // unpack four hexets into three octets using index points in b64
        h1 = b64.indexOf(data.charAt(i++));
        h2 = b64.indexOf(data.charAt(i++));
        h3 = b64.indexOf(data.charAt(i++));
        h4 = b64.indexOf(data.charAt(i++));
        bits = h1 << 18 | h2 << 12 | h3 << 6 | h4;
        o1 = bits >> 16 & 0xff;
        o2 = bits >> 8 & 0xff;
        o3 = bits & 0xff;
        if (h3 == 64) {
            tmp_arr[ac++] = String.fromCharCode(o1);
        } else if (h4 == 64) {
            tmp_arr[ac++] = String.fromCharCode(o1, o2);
        } else {
            tmp_arr[ac++] = String.fromCharCode(o1, o2, o3);
        }
    } while (i < data.length);
    dec = tmp_arr.join('');
    return dec.replace(/\0+$/, '');
}