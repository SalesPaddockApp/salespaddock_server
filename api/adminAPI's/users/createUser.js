/**
 * Created by Dipen on 22/11/2016.
 * function name: addWish
 * request: token,userId,wishId
 * response: succuss response if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');


exports.createUser = function (req, res)
{

    if (!req.body.preferenceTitle)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory preferenceTitle is missing"});
        return false;
    }
    if (!req.body.onCreateScreen)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory onCreateScreen is missing"});
        return false;
    }
    if (!req.body.typeOfPreference)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory typeOfPreference is missing"});
        return false;
    }
    if (!req.body.productType)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory productType is missing"});
        return false;
    }
    if (!req.body.mandatory)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory mandatory is missing"});
        return false;
    }
    if (!req.body.prirority)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory prirority is missing"});
        return false;
    }
    if (!req.body.Unit)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory Unit is missing"});
        return false;
    }
    if (!req.body.optionsValue)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory optionsValue is missing"});
        return false;
    }


    var data = {preferenceTitle: req.body.preferenceTitle,
        onCreateScreen: req.body.onCreateScreen,
        typeOfPreference: req.body.typeOfPreference,
        productType: req.body.productType,
        mandatory: req.body.mandatory,
        prirority: req.body.prirority,
        Unit: req.body.Unit,
        optionsValue: req.body.optionsValue};
    Utility.Insert('Preferences', data, function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        } else if (result)
        {

            res.send({errCode: 0, Message: "your Preferences saved successfully.", response: {}});
            return true;
        } else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no Preferences is there, please try after some time."});
            return true;
        }
    });


}

