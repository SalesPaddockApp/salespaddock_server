/**
 * Created by Dipen on 22/11/2016.
 * function name: deleteProducttype
 * request: _id
 * response: succuss response if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;
var async = require('async');


exports.deleteUser = function (req, res)
{
   if(!req.body.userId)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory userId is missing"});
        return false;
    }
    
    var cond = {userId: req.body.userId};
  
    Utility.Delete('user', cond, function (userErr, userRes)
    {
       
        var msg = "",errMsg="";

        if(userRes)
        {
            msg = "Data Successfully Delete from : User";
        }
        else if(userErr){

             errMsg = "Data Unsuccessfully Delete from : User";
        }

console.log("delete form users");

async.parallel([

///.........................1 start
function(){
    console.log("delete START form shows"+JSON.stringify({"userId":req.body.userId}));

 Utility.UpdateField('shows',{},{"posts":{"userId":req.body.userId}}, function (showsErr, showsRes)
            {
                    if(showsRes){

                              msg += ", shows";
                    }else if(showsErr){
                         errMsg += ", shows";
                    }
                    console.log("delete end form shows");

            })
       },

//...........................1 End

//............................2 Start
function(){
    console.log("delete s form post");
   Utility.Delete('post',{"userId":req.body.userId.toString()}, function (postErr, postRes)
            {

                    if(postRes){

                     msg += ", post";
                    }else if(postErr){
                     errMsg += ", post";
                    }
                    console.log("delete e form post");
            })},

//............................2 End
//............................3 Start
function(){
    console.log("delete s form wishList");
 Utility.Delete('wishList', {"userId":req.body.userId}, function (wishListErr, wishListRes)
            {

                    if(wishListRes){

                     msg += ", wishList";
                    }else if(wishListErr){
                     errMsg += ", wishList";
                    }
                    console.log("delete e form wishList");
            })},

//............................3 End
//............................4 Start
function(){
    console.log("delete r form RV");
Utility.Delete('recentVisitors', {"userId":req.body.userId}, function (RVErr, RVRes)
            {

                    if(RVRes){

                     msg += ", recentVisitors";
                    }else if(RVErr){
                     errMsg += ", recentVisitors";
                    }
                    console.log("delete e form RV");
            })},
//............................4 End
//............................5 Start
function(){
    console.log("delete s form msg");
 Utility.Delete('Messages', {"userId":req.body.userId}, function (msgErr, msgRes)
            {

                    if(msgRes){

                     msg += ", Messages";
                    }else if(msgErr){
                     errMsg += ", Messages";
                    }
                    console.log("delete e form msg");
            })
}
// ............................5 End



    ]


);

console.log("delete form 001 ");
            if (errMsg != "")
            {
                res.send({errCode: 1, errNum: 104, Message: errMsg});
                return false;
            } else
            {
                res.send({errCode: 0, Message: msg, response: {}});
                return true;
            }



           

    
});

}

