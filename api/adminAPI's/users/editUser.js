/**
 * Created by Dipen on 22/11/2016.
 * function name: editProducttype
 * request: _id,productName
 * response: succuss response if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;


exports.editUser = function (req, res)
{
    if (!req.body._id)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory feild _id is missing"});
        return false;
    }
    var data = [];
    var dataToUpdate = {}, cond = {};

    var i = 0;
    var _id = "_id", token = "token";
    for (key in  req.body) {

        data.push(key);

        if (data[i] == '_id') {
            cond[key] = ObjectId(req.body[data[i]]);//condition to updatedate
        } else if (data[i] == 'token') {

//            extra feild which is not for update
        } else {
            dataToUpdate[key] = req.body[data[i]];
            //data for update
        }
        i++;
    }


//if no data for update then
    if (Object.keys(dataToUpdate).length <= 0) {
        res.send({errCode: 1, errNum: 132,
            Message: "Please define any one filed for update"});
        return false;
    }
    console.log("========================cond>>>>>>>>>>>>>>>" + JSON.stringify(cond));
    console.log("========================datatopush>>>>>>>>>>>>>>>" + JSON.stringify(dataToUpdate));


    Utility.Update('user', cond, dataToUpdate, function (err, result)
    {

        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        } else if (result)
        {

            res.send({errCode: 0, Message: "User updated successfully.", response: {}});
            return true;


        } else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no User is there, please try after some time."});
            return true;
        }
    });


}

