/**
 * Created by Niranjan on 19/9/16.
 * function name: prodType
 * request: userId,token
 * response: all product type if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var limitNumber = 10;
var ObjectId = require('mongodb').ObjectID;

exports.getDeviceLog = function(req, res)
{
    if (!req.body.userId)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory user id is missing"});
        return false;
    }


    Utility.SelectOne('user',{userId:req.body.userId},function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result)
        {

            console.log("dataTosend:  " + JSON.stringify(result));

            res.send({errCode: 0,  Message: "data send succcessfully.",response:{data : result.devices}});
            return true;
        }
        else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no User there, please try after some time."});
            return true;
        }
    });
}


