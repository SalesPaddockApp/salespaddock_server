/**
 * Created by Niranjan on 19/9/16.
 * function name: prodType
 * request: userId,token
 * response: all product type if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var limitNumber = 10;

exports.getInactiveUsers = function(req, res)
{

    console.log("User:  " + JSON.stringify(req.query));
    var pageNumber = 0;
    if(!req.body.pageNum)
        pageNumber = 0;
    else
        pageNumber = req.body.pageNum;

    var skipNumber = pageNumber*limitNumber;


    Utility.SelectWithPaginationSorting('user',{"active":"0"},{_id : -1},{},skipNumber,limitNumber,function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result.length > 0)
        {
           

            //console.log("dataTosend:  " + JSON.stringify(result));

            res.send({errCode: 0,  Message: "data send succcessfully.",response:{data : result}});
            return true;
        }
        else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no User there, please try after some time."});
            return true;
        }
    });
}


