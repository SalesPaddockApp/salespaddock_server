/**
 * Created by Niranjan on 19/9/16.
 * function name: prodType
 * request: userId,token
 * response: all product type if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;
 limitNumber = 10;

exports.getUserBy_Id = function (req, res)
{
    console.log("getUserBy_Id", JSON.stringify(req.body));
    if (!req.body._id)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory _id is missing"});
        return false;
    }

    console.log("user:  " + JSON.stringify(req.query));
    var pageNumber = 0;
    if (!req.body.pageNum)
        pageNumber = 0;
    else
        pageNumber = req.body.pageNum;

    var skipNumber = pageNumber * limitNumber;

    var searchData = {userId : req.body._id};

    Utility.SelectWithPagination('user', searchData, {}, skipNumber, limitNumber, function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        } else if (result.length > 0)
        {
           
          

            console.log("dataTosend:  " + JSON.stringify(result));

            res.send({errCode: 0, Message: "data send succcessfully.", response: {data: result[0]}});
            return true;
        } else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no user is there, please try after some time."});
            return true;
        }
    });
}


