/**
 * Created by Dipen on 22/11/2016.
 * function name: editProducttype
 * request: _id,productName
 * response: succuss response if no error else error accordingly
 user active=1,inactive=0
 **/
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;


exports.makeUserActive = function (req, res)
{

 if(!req.body._ids)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory _ids is missing"});
        return false;
    }
    
    var ids=[];
    for(i=0;i<req.body._ids.length;i++){
    ids[i]=ObjectId(req.body._ids[i]);

    }
    
    var dataToUpdate = {"active": "1"};
    var cond = {_id:{$in:ids}};

  

    Utility.Update('user', cond, dataToUpdate, function (err, result)
    {

        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        } else if (result)
        {

            res.send({errCode: 0, Message: "User updated successfully.", response: {}});
            return true;


        } else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no data is there, please try after some time."});
            return true;
        }
    });


}

