/**
 * Created by Niranjan on 19/9/16.
 * function name: prodType
 * request: userId,token
 * response: all product type if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
limitNumber = 10;

exports.searchUser = function (req, res)
{
    if (!req.body.searchP)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory searchP is missing"});
        return false;
    }

    console.log("user:  " + JSON.stringify(req.query));
    var pageNumber = 0;
    if (!req.body.pageNum)
        pageNumber = 0;
    else
        pageNumber = req.body.pageNum;

    var skipNumber = pageNumber * limitNumber;

    var searchData = {$or: [{'name.fName': new RegExp("^" + req.body.searchP, "gi")},
        {'email.primaryEmail': new RegExp("^" + req.body.searchP, "gi")},
            {"name.lName": new RegExp("^" + req.body.searchP, "gi")},
            {"userId": new RegExp("^" + req.body.searchP, "gi")}]};

    Utility.SelectWithPagination('user', searchData, {}, skipNumber, limitNumber, function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        } else if (result.length > 0)
        {

        res.send({errCode: 0, Message: "data send succcessfully.",totalFounf:result.length ,response: {data: result}});
        return true;
    } else
    {
        res.send({errCode: 1, errNum: 135, Message: "Currently no Preferences is there, please try after some time."});
        return true;
        }
    }
    );
}


