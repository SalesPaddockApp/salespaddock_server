/**
 * Created by Dipen on 03/01/17.
 * function name: prodType
 * request: userId,token
 * response: all product type if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;


exports.getUniqueViewByPostId = function (req, res)
{

  if(!req.body.postId)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory postId is missing"});
        return false;
    }

   var cond = [
{$match:{"postId" : ObjectId(req.body.postId)}},
{$group : {_id : "$userId",total : { $sum : 1 }}},
{$lookup:{from:"user","localField":"_id",foreignField:"userId",as:"UserData"}},
{$unwind:"$UserData"},
{$project:{total:1,_id:0,
    fName:"$UserData.name.fName",
    lName:"$UserData.name.lName",
    email:"$UserData.email.primaryEmail",
    timestamp:"$UserData.timestamp",
    userId:"$UserData.userId"
    }}   
        
    ];

    Utility.AggregateGroup('recentVisitors', cond, function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        } else if (result.length > 0)
        {
            console.log("dataTosend for Post:  " + JSON.stringify(result.length));

            res.send({errCode: 0, Message: "data send succcessfully.", totalFound: result.length, response: {data: result}});
            return true;
        } else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no Posts there, please try after some time."});
            return true;
        }
    });
}


