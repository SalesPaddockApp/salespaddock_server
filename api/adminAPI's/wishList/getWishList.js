/**
 * Created by Niranjan on 30/9/16.
 */

var exports = module.exports = {};
var Utility = require('../UtilityFunc');

var ObjectId = require('mongodb').ObjectID;

/*Including async to make function asynchronous*/
var async=require('async');

exports.getWishList = function(req, res)
{
    if(!req.body.userId)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory user id is missing"});
        return false;
    }

    var skip =0;
    var limit = 10;


    var cond = [{$match: {"userId" : req.body.userId}},{$group: { _id: '$showId', count: {$sum: 1}, postIds: {$addToSet: '$postId'}}},
        { $lookup: { from: 'shows', localField: '_id', foreignField: '_id', as: 'show'}},{ $unwind: '$show'},
        {$project: {showName: '$show.showName',imgPath: '$show.imgPath',count: 1,postIds: 1}}];

    Utility.AggregateGroup('wishList',cond,function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result.length > 0)
        {
            var dataToSend = [];
            for(var i=0; i<result.length; i++)
            {
                //var sdate = '';
                //if(result[0]['shows'][i][0]['startDate'])
                //{
                //    var mainDate1 = new Date(result[0]['shows'][i][0]['startDate']);
                //
                //    var datee1 = mainDate1.getDate();
                //    var month1 = mainDate1.getMonth() + 1;
                //    var year1 = mainDate1.getFullYear();
                //
                //    if(datee1 < 10)
                //        datee1 = "0" + datee1;
                //
                //    if(month1 < 10)
                //        month1 = "0" + month1;
                //
                //    sdate = month1 + "/" + datee1 + "/" + year1;
                //
                //}
                //
                //var edate = '';
                //if(result[0]['shows'][i][0]['endDate'])
                //{
                //    var mainDate = new Date(result[0]['shows'][i][0]['endDate']);
                //
                //    var datee = mainDate.getDate();
                //    var month = mainDate.getMonth() + 1;
                //    var year = mainDate.getFullYear();
                //
                //    if(datee < 10)
                //        datee = "0" + datee;
                //
                //    if(month < 10)
                //        month = "0" + month;
                //
                //    edate = month + "/" + datee + "/" + year;
                //
                //}

                dataToSend.push({showName: result[i]['showName'],image: result[i]['imgPath'],
                    count: result[i]['count'],postIds: result[i]['postIds'],
                    showId: result[i]['_id']})
            }
            res.send({errCode: 0,  Message: "All shows send successfully.",response: {result: dataToSend}});
        }
        else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no product is there, please try after some time."});
            return true;
        }
    });
}
