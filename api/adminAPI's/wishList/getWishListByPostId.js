/**
 * Created by Dipen on 03/01/17.
 * function name: prodType
 * request: userId,token
 * response: all product type if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;


exports.getWishListByPostId = function (req, res)
{

  if(!req.body.postId)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory postId is missing"});
        return false;
    }

   var cond = [
{$match:{"postId" : ObjectId(req.body.postId)}},
{$lookup:{from:"user","localField":"userId",foreignField:"userId",as:"UserData"}},
{$unwind:"$UserData"},
{$project:{postId:1,timestamp:1,userId:1,
    fName:"$UserData.name.fName",
    lName:"$UserData.name.lName",
    email:"$UserData.email.primaryEmail"    
    }}
];

    Utility.AggregateGroup('wishList', cond, function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        } else if (result.length > 0)
        {
            console.log("dataTosend for Post:  " + JSON.stringify(result.length));

            res.send({errCode: 0, Message: "data send succcessfully.", totalFound: result.length, response: {data: result}});
            return true;
        } else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no Posts there, please try after some time."});
            return true;
        }
    });
}


