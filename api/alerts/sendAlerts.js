/**
 * Created by Niranjan on 8/10/16.
 */
var exports = module.exports = {};
var notification = require('../notification/sendNotification');
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;

exports.sendAlerts = function (req, res) {
    console.log("sendAlerts: " + JSON.stringify(req.body));
    if (!req.body.userId) {
        res.send({ errCode: 1, errNum: 132, Message: "mandatory user id is missing" });
        return false;
    }

    if (!req.body.postId) {
        res.send({ errCode: 1, errNum: 145, Message: "mandatory post id is missing" });
        return false;
    }

    //if(!req.body.showId)
    //{
    //    res.send({errCode: 1, errNum: 145, Message: "mandatory show id is missing"});
    //    return false;
    //}

    if (!req.body.mess) {
        res.send({ errCode: 1, errNum: 145, Message: "mandatory message is missing" });
        return false;
    }

    //var cond= {postId: req.body.postId,showId: req.body.showId};
    var cond = [{ $match: { postId: ObjectId(req.body.postId) } },
    { $lookup: { from: 'user', localField: 'userId', foreignField: 'userId', as: 'user' } },
    { $unwind: '$user' }, { $project: { pushToken: '$user.pushToken', userId: '$user.userId' } }];

    //console.log("cond: " +JSON.stringify(cond));
    Utility.AggregateGroup('wishList', cond, function (err, result) {
        if (err) {
            res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
            return false;
        }
        else if (result.length > 0) {
            console.log("users are there.");
            var pushTokens = [];
            var userIds = [];
            var userIdss = [];
            for (var j = 0; j < result.length; j++) {
                console.log("result[j]['pushToken']", result[j]['pushToken']);
                if (result[j]['pushToken']) {
                    userIds.push({ userId: result[j]['userId'] });
                    userIdss.push(result[j]['userId']);
                    pushTokens.push(result[j]['pushToken']);
                }
            }

            updateBadgeCount(userIdss);
            var isSaveFirstTime = true;

            for (var i = 0; i < pushTokens.length; i++) {
                console.log("====>>>>", pushTokens[i]);

                var data = { pushTokens: [pushTokens[i]], alert: req.body.mess, payload: { userId: req.body.userId, type: "rideAlert" } };

                notification.sendNotification(data, (function (err, result) {
                    if (err) {
                        console.log("some error occurred send alert:  " + JSON.stringify(err));
                    }
                    else {
                        if (isSaveFirstTime) {
                            isSaveFirstTime = false;
                            var dataToinsert = {
                                message: req.body.mess, recievers: userIds, sender: req.body.userId,
                                showId: ObjectId(req.body.showId), postId: ObjectId(req.body.postId)
                            };
                            Utility.Insert('notification', dataToinsert, function (err, isInserted) {
                                if (err) {
                                    console.log("some error occurred:  " + JSON.stringify(err));
                                }
                                else {
                                    console.log("data inserted successfully. send alert")
                                }
                            });
                        }

                    }
                }));
            }


            res.send({ errCode: 0, Message: "Alert sent successfully." });
            return true;
        }
        else {
            console.log("no users.");
            res.send({ errCode: 0, Message: "Alert sent successfully." });
            return true;
        }
    })
};

function updateBadgeCount(userIds) {
    console.log("coming update Badge Count");

    var cond = { userId: { $in: userIds } };

    Utility.Select('user', cond, function (err, respo) {
        if (err) {
            console.log("some error occurred if:  " + JSON.stringify(err));
        }
        else if (respo.length > 0) {
            for (var i = 0; i < respo.length; i++) {
                var alertCount = 0;
                var totalCount = 0;
                if (respo[i]['alertCount'])
                    alertCount = parseInt(respo[i]['alertCount']);

                if (respo[i]['totalCount'])
                    totalCount = parseInt(respo[i]['totalCount']);

                alertCount = alertCount + 1;
                totalCount = totalCount + 1;

                var datoUpdate = { alertCount: alertCount, totalCount: totalCount };

                var condToUpdate = { userId: respo[i]['userId'] };
                updateBadge(condToUpdate, datoUpdate);
            }

            console.log("result.length:  " + respo.length)
        }
        else {
            console.log("some error occurred else.")
        }
    })
}

function updateBadge(cond, dataToUpdate) {
    Utility.Update('user', cond, dataToUpdate, function (err, respon) {
        if (err) {
            console.log("some error occurred if:  " + JSON.stringify(err));
        }
        else if (respon) {
            console.log("result.length:  " + respon);
        }
        else {
            console.log("some error occurred else.")
        }
    })
}



exports.sendAlerts1 = function (req, res) {
    console.log("sendAlerts1: " + JSON.stringify(req.body));
    if (!req.body.userId) {
        res.send({ errCode: 1, errNum: 132, Message: "mandatory user id is missing" });
        return false;
    }

    if (!req.body.postId) {
        res.send({ errCode: 1, errNum: 145, Message: "mandatory post id is missing" });
        return false;
    }

    if (!req.body.showId) {
        res.send({ errCode: 1, errNum: 145, Message: "mandatory show id is missing" });
        return false;
    }

    if (!req.body.mess) {
        res.send({ errCode: 1, errNum: 145, Message: "mandatory message is missing" });
        return false;
    }

    var cond = { userId: { $ne: req.body.userId } };

    Utility.Select('user', cond, function (err, result1) {
        if (err) {
            res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
            return false;
        }
        else if (result1.length > 0) {
            var pushTokens = [];
            for (var j = 0; j < result1.length; j++) {
                if (result1[j]['pushToken'])
                    pushTokens.push(result1[j]['pushToken']);
            }

            require("uniq")(pushTokens);

            //console.dir("pushTokens: " +JSON.stringify(pushTokens));
            var data = { pushTokens: pushTokens, alert: req.body.mess, production: true, isArr: 1, payload: { data: "not required now" } };

            notification.sendNotification(data, (function (err, result) {
            }));

            res.send({ errCode: 0, Message: "Alert sent successfully." });
            return true;

        }
        else {
            console.dir("error else part");
            res.send({ errCode: 0, Message: "Alert sent successfully." });
            return true;

        }
    })
};