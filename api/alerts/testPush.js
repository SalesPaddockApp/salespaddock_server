/**
 * Created by Niranjan on 11/10/16.
 */

var apn = require('apn');
var path = require("path");

exports.testPush = function(args, callback)
{
    var apn = require('apn');
    var path = require("path");
    var options = {
        "cert": path.join(__dirname, "/prodCert.pem"),
        "key" : path.join(__dirname, "/prodKey.pem"),
        "passphrase": 'DanielRulz',
        "production" : true
    };

    var apnConnection = new apn.Connection(options);

    var myDevice = new apn.Device("B496596B3A2F6A1FA8CC6D7124E11C29FC054A728FCC2EE7CA7CA8BDB8B94B84");

    var note = new apn.Notification();
    note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
    note.badge = 1;
    note.sound = "sms-received.wav";
    note.alert = "getting push??",
        note.payload = {data: "1111"};

    apnConnection.on('connected', function() {
        console.log("Connected");
    });

    apnConnection.on('transmitted', function(notification, device) {
        console.log("Notification transmitted to:" + device.token.toString('hex'));
    });

    apnConnection.on('transmissionError', function(errCode, notification, device) {
        console.error("Notification caused error: " + errCode + " for device ", device, notification);
    });

    apnConnection.on('timeout', function () {
        console.log("Connection Timeout");
    });

    apnConnection.on('disconnected', function() {
        console.log("Disconnected from APNS");
    });

    apnConnection.on('error', function(error) {
        console.log(error);
    });

    apnConnection.on('socketError', console.error);
    try {
        apnConnection.pushNotification(note, myDevice);
        callback.send({errCode: 0,  Message: "notification sent.",response: {}});
        //apnConnection.pushNotification(note, userTokens);
    } catch (e) {
        callback.send({errCode: 0,  Message: e,response: {}});
        return res(null, e)
    }

};

var Mailgun = require('mailgun-js');
var mailgun = new Mailgun({ apiKey: "key-78789ed5b923a06be436c9de5212abb2", domain: "www.salespaddock.com"});


exports.testmail = function(args, callback)
{
    var data = {
        from: "noreply@salespaddock.org",
        to: 'poddarniranjan2@gmail.com',
        subject: "testing",
        html: 'hiiiiiii',
    };

    mailgun.messages().send(data, function(err, success)
    {
        if (err)
        {
            console.log('Could not send mail error!' + JSON.stringify(err));
            callback.send({message :  "some error occurredd", code : 198});
        }
        console.log(JSON.stringify({message : 'Success! Mail send' + JSON.stringify(success), code : 200}));
        callback.send({message : 'Success! Mail send' + JSON.stringify(success), code : 200});
    });

};