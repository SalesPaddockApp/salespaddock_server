/**
 * Created by Niranjan on 21/12/16.
 * function name: viewProfile
 * request: token,userId
 * response: all data of specific user but only related data if  no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
const limitNumber = 10;

exports.getCurrency = function(req, res)
{
    var cond = {};

    Utility.Select('currency',cond,function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result.length > 0)
        {
            res.send({errCode: 0,  Message: "data send succcessfully.",response: result});
            return true;
        }
        else
        {
            res.send({errCode: 1, errNum: 135, Message: "currently no data found."});
            return true;
        }
    });
}