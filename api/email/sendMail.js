/**
 * Created by Niranjan on 12/10/16.
 */

var conf = require('../conf');
var domainName = conf.mailGunDomain;
var apiKey = conf.mailGunAPiKey;

var Mailgun = require('mailgun-js');
var mailgun = new Mailgun({ apiKey: apiKey, domain: domainName});

exports.sendMail = function(args, callback)
{
    console.log("apiKey: " + apiKey);
    console.log("domainName: " +domainName);
    console.log("args: " +JSON.stringify(args));

    var data = {
        from: "noreply@salespaddock.org",
        to: args.to  ,
        subject: args.subject,
        html: '<p>Thank you for creating an account with Sales Paddock! Whether you are here to sell, buy, or both...we hope you enjoy your experience.</p>' +
         '<p>We know there can be a learning curve with new technology so we have made several videos to help you navigate the Sales Paddock App.</p>'+
        '<p>Please visit our website, www.salespaddock.com or on Facebook, www.facebook.com/salespaddock to get more information.</p>'+
        '<p>If you are still having problems, please call, text, email or message us through the Facebook page.</p>'+
        '<p>We hope to make Sales Paddock the most valued tool for buying and selling horses! We will continue to add new features that will enhance your experience. Feedback is always welcome, so please let us know if there is anything we can do to improve.</p><br><br><br>'+
        'Sincerely,<br>'+
        '<b>The Sales Paddock Team</b><br>'+
        '615-933-9131<br>'+
        'salespaddock@gmail.com<br>'+
        'www.salespaddock.com<br>'+
        'www.facebook.com/salespaddock'
    };

    mailgun.messages().send(data, function(err, success)
    {
        if (err)
        {
            console.log('Could not send mail error!' + JSON.stringify(err));
            return callback(err, {message :  "some error occurredd", code : 198});
        }
        console.log(JSON.stringify({message : 'Success! Mail send' + JSON.stringify(success), code : 200}));
        return callback(err, {message : 'Success! Mail send' + JSON.stringify(success), code : 200});
    });
};