/**
 * Created by Niranjan on 27/1/17.
 * function name: sendMailForPostCreation
 * request: ''
 * response: sending mail to alsion, for approving the listings.
 */

var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var conf = require('../conf');
var domainName = conf.mailGunDomain;
var apiKey = conf.mailGunAPiKey;

var Mailgun = require('mailgun-js');
var mailgun = new Mailgun({apiKey: apiKey, domain: domainName});


exports.sendMailForPostCreation = function (req, res)
{
    var data = {
        from: "noreply@salespaddock.org",
        to: "salespaddock@gmail.com",
        //to: "subir@mobifyi.com",
        subject: "Listings pending for approval.",
        html: '<html><body>Kindly check the DASHBOARD, few listings are pending for approval.</body></html>'
    };

    mailgun.messages().send(data, function (err, success)
    {
        if (err)
        {
            console.log('Could not send mail error!' + JSON.stringify(err));
            //return callback(err, {message: "some error occurredd", code: 198});
        }
        console.log(JSON.stringify({message: 'Success! Mail send' + JSON.stringify(success), code: 200}));
        //(err, {message: 'Success! Mail send' + JSON.stringify(success), code: 200});


    });
}


