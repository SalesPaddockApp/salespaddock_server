// Include the cluster module
var cluster = require('cluster');


// Code to run if we're in the master process
if (cluster.isMaster) {
    // Count the machine's CPUs
    var cpuCount = require('os').cpus().length;

    // Create a worker for each CPU
    for (var i = 0; i < cpuCount; i += 1) {
        cluster.fork();
    }

    console.log("i am in if cluster");

    // Code to run if we're in a worker process
}
else {

    const express = require("express"),
        bodyParser = require("body-parser"),
        morgan = require("morgan"),
        cors = require("cors"),
        jwt = require("jsonwebtoken");

    const app = express();

    app.use(bodyParser.json());

    app.use(cors({ credentials: true, origin: true }));

    //using morgan to check performance of API
    app.use(morgan('dev'));

    app.use(function (req, res, next) {
        /**
         * req.headers.horse this is used for side
         * req.headers.authorization this is for postman
         */
        if (!req.headers.horse) {
            //console.log('basic auth: ' + JSON.stringify(req.headers))
            if (!req.headers.authorization) {
                res.status(401).json({ errFlag: '1', errMsg: 'error', data: 'Unauthorised Access' });
            }
            else {
                console.log(req.headers.authorization);
                var auth = 'Basic ' + new Buffer("horse:123456").toString('base64');

                if (req.headers.authorization.toString() === auth) {
                    next();
                }
                else
                    res.status(401).json({ errFlag: '1', errMsg: 'Unauthorised Access' });
            }
        }
        else {
            //console.log('horse auth: ' + JSON.stringify(req.headers));
            var auth = req.headers.horse;
            if ('123456' === auth) {
                next();
            }
            else
                res.status(401).json({ errFlag: '1', errMsg: 'Unauthorised Access' });
        }
    });



    app.get('/', function (req, res) {
        res.json({ Message: "you are connected." })
    });


    /*
     *function name: getAppliedDescipline.
     * Desc: this function is use to get all descipline which user can apply.
     * req: token, productType  .
     * response: success response if no error else error.
     *  */
    var getAppliedDescipline = require('./filters/getAppliedDescipline');
    app.post('/getAppliedDescipline', getAppliedDescipline.getAppliedDescipline);


    var validateEmail = require('./register/validateEmail');
    app.post('/validateEmail', validateEmail.validateEmail);


    var forgetPassword1 = require('./register/forgetPassword');
    app.post('/forgetPassword', forgetPassword1.forgetPassword);

    var api = require('./login/login')(app, express);
    app.use('/api', api);

    api = require('./preference/getTest')(app, express);
    app.use('/api', api);

    api = require('./register/register')(app, express);
    app.use('/api', api);

    api = require('./profile/editProfile')(app, express);
    app.use('/api', api);

    api = require('./preference/getPreference')(app, express);
    app.use('/api', api);

    var viewProfile = require('./profile/viewProfile');
    app.post('/viewProf', viewProfile.viewProfile);

    var viewProfileForChat = require('./profile/viewProfileForChat');
    app.post('/viewProfileForChat', viewProfileForChat.viewProfileForChat);

    var getShows = require('./shows/getShows');
    app.post('/getShows', getShows.getShows);

    var searchHorse = require('./posts/searchHorse');
    app.post('/searchPosts', searchHorse.searchHorse);

    var sendOTP = require('./profile/sendOTP');
    app.post('/sendOTP', sendOTP.sendOTP);

    var getSummary = require('./shows/getSummary');
    app.post('/getSummary', getSummary.getSummary);

    //var getShowForUsers = require('./shows/getShowForUsers');
    //app.post('/getShowForUsers1',getShowForUsers.getShowForUsers);

    var getShowForUsers1 = require('./shows/getShowForUsers1');
    app.post('/getShowForUsers', getShowForUsers1.getShowForUsers1);

    var getAroundYou = require('./shows/getAroundYou');
    app.post('/getAroundYou', getAroundYou.getAroundYou);

    var activeShows = require('./shows/activeShows');
    app.post('/activeShows', activeShows.activeShows);

    var upcomingShows = require('./shows/upcomingShows');
    app.post('/upcomingShows', upcomingShows.upcomingShows);

    var getSellers = require('./shows/getSellers');
    app.post('/getSellers', getSellers.getSellers);

    var getCurrency = require('./currenncy/getCurrency');
    app.post('/getCurrency', getCurrency.getCurrency);

    var getShowDetails = require('./shows/getShowDetails');
    app.post('/getShowDetails', getShowDetails.getShowDetails);

    var getMyNotifications = require('./notification/getMyNotifications');
    app.post('/getMyNotifications', getMyNotifications.getMyNotifications);

    var productType = require('./productType/productType');
    app.post('/prodType', productType.productType);

    var addWish = require('./wishList/addWish');
    app.post('/addWish', addWish.addWish);

    var removeWish = require('./wishList/removeWish');
    app.post('/removeWish', removeWish.removeWish);

    var uploadImage = require('./uploadImage/uploadImage');
    app.post('/uploadImage', uploadImage.uploadImage);

    var logout = require('./logout/logout');
    app.post('/logout', logout.logout);

    /*
     *function name: createPost.
     * Desc: this function is use to create the posts(eg: horses) of a shows..
     * req: token,userId,postId,post data.
     * response: success response if no error else error.
     *  */
    var createPost = require('./posts/createPost');
    app.post('/createPost', createPost.createPost);


    /*
     *function name: updatePost.
     * Desc: this function is use to update the details of a show,(eg: horses details).
     * req: token,userId,postId,data which have to update.
     * response: success response if no error else error.
     *  */
    var updatePost = require('./posts/updatePost');
    app.post('/updatePost', updatePost.updatePost);


    /*
    *function name: addToShow.
    * Desc: this function is use to add your post(eg: horses) to the available shows.
    * req: token,userId,postId,showId.
    * response: success response if no error else error.
     *  */
    var addToShow = require('./shows/addToShow');
    app.post('/addToShow', addToShow.addToShow);


    /*
    *function name: deletePostFromShow.
    * Desc: this function is use to add your post(eg: horses) to the available shows.
    * req: token,userId,postId,showId.
    * response: success response if no error else error.
     *  */
    var deletePostFromShow = require('./shows/deletePostFromShow');
    app.post('/deletePostFromShow', deletePostFromShow.deletePostFromShow);


    /*
    *function name: addToRecentList.
    * Desc: this function is use to add your post(eg: horses) to the available shows.
    * req: token,userId,postId,showId.
    * response: success response if no error else error.
     *  */
    var addToRecentList = require('./posts/addToRecentList');
    app.post('/addToRecentList', addToRecentList.addToRecentList);


    /*
    *function name: getWishList.
    * Desc: this function is use to add your post(eg: horses) to the available shows.
    * req: token,userId,postId,showId.
    * response: success response if no error else error.
     *  */
    var getWishList = require('./wishList/getWishList');
    app.post('/getWishList', getWishList.getWishList);


    /*
    *function name: wishListDetail.
    * Desc: this function is use to add your post(eg: horses) to the available shows.
    * req: token,postIds.
    * response: success response if no error else error.
     *  */
    var wishListDetail = require('./wishList/wishListDetail');
    app.post('/wishListDetail', wishListDetail.wishListDetail);


    /*
    *function name: getPostDetails.
    * Desc: this function is use to add your post(eg: horses) to the available shows.
    * req: token,userId,postId,showId.
    * response: success response if no error else error.
     *  */
    var getPostDetails = require('./posts/getPostDetails');
    app.post('/getPostDetails', getPostDetails.getPostDetails);


    /*
    *function name: archiveShow.
    * Desc: this function is use to add your post(eg: horses) to the available shows.
    * req: token,userId,postId,showId.
    * response: success response if no error else error.
     *  */
    var archiveShow = require('./shows/archiveShow');
    app.post('/archiveShow', archiveShow.archiveShow);


    /*
    *function name: getAllPostsHost.
    * Desc: this function is use to add your post(eg: horses) to the available shows.
    * req: token,userId,postId,showId.
    * response: success response if no error else error.
     *  */
    var getAllPostsHost = require('./posts/getAllPostsHost');
    app.post('/getAllPostsHost', getAllPostsHost.getAllPostsHost);


    /*
    *function name: getAllPostsHost.
    * Desc: this function is use to add your post(eg: horses) to the available shows.
    * req: token,userId,postId,showId.
    * response: success response if no error else error.
     *  */
    var getAllMyListedPost = require('./posts/getAllMyListedPost');
    app.post('/getAllMyListedPost', getAllMyListedPost.getAllMyListedPost);


    /*
    *function name: validateNumber.
    * Desc: this function is use to confirm  your phone number.
    * req: token,userId.
    * response: success response if no error else error.
     *  */
    var validateNumber = require('./profile/validateNumber');
    app.post('/validateNumber', validateNumber.validateNumber);


    /*
    *function name: getFaq.
    * Desc: this function is use to get all faq.
    * req: token.
    * response: success response if no error else error.
     *  */
    var getFaq = require('./faq/getFaq');
    app.post('/getFaq', getFaq.getFaq);



    /*
    *function name: sendAlerts.
    * Desc: this function is use to send notifications.
    * req: userId,postId,showId,prouctiontype.
    * response: success response if no error else error.
     *  */
    var alerts = require('./alerts/sendAlerts');
    app.post('/sendAlerts', alerts.sendAlerts);

    var testPush = require('./alerts/testPush');
    app.post('/testPush', testPush.testPush);

    var testmail = require('./alerts/testPush');
    app.post('/testmail', testmail.testmail);

    /*
     *function name: getFilters.
     * Desc: this function is use to get all filters which user can apply.
     * req: token.
     * response: success response if no error else error.
     *  */
    var getFilters = require('./filters/getFilters');
    app.post('/getFilters', getFilters.getFilters);


    /*
     *function name: deletePost.
     * Desc: this function is use to get all filters which user can apply.
     * req: token.
     * response: success response if no error else error.
     *  */
    var deletePostFunc = require('./posts/deletePost');
    app.post('/deletePost', deletePostFunc.deletePost);



    /*
     *function name: deletePost.
     * Desc: this function is use to get all filters which user can apply.
     * req: token.
     * response: success response if no error else error.
     *  */
    var notifyAllUsersFunc = require('./notification/notifyAllUsers');
    app.post('/notifyAllUsers', notifyAllUsersFunc.notifyAllUsers);



    /*
     *function name: deletePost.
     * Desc: this function is use to get all filters which user can apply.
     * req: token.
     * response: success response if no error else error.
     *  */
    var applyDescipline = require('./filters/applyDescipline');
    app.post('/applyDescipline', applyDescipline.applyDescipline);



    /*
     *function name: getAllFarms.
     * Desc: this function is use to get all user farms.
     * req: token,userId.
     * response: success response if no error else error.
     *  */
    var getAllFarms = require('./profile/getAllFarms');
    app.post('/getAllFarms', getAllFarms.getAllFarms);



    /*
     *function name: addFarm.
     * Desc: this function is use to add farms.
     * req: token,userId, farm data.
     * response: success response if no error else error.
     *  */
    var addFarm = require('./profile/addFarm');
    app.post('/addOrUpdateFarm', addFarm.addOrUpdateFarm);



    /*
     *function name: deleteFarm.
     * Desc: this function is use to delete farms.
     * req: token,userId, farm id.
     * response: success response if no error else error.
     *  */
    var deleteFarm = require('./profile/deleteFarm');
    app.post('/deleteFarm', deleteFarm.deleteFarm);

    var createCollection = require('./userPostCollection/createCollection');
    app.post('/createCollection', createCollection.createCollection);

    var getCollections = require('./userPostCollection/getCollections');
    app.post('/getCollections', getCollections.getCollections);

    var getCollectionDetails = require('./userPostCollection/getCollectionDetails');
    app.post('/getCollectionDetails', getCollectionDetails.getCollectionDetails);

    var editCollection = require('./userPostCollection/editCollection');
    app.post('/editCollection', editCollection.editCollection);

    var deleteCollection = require('./userPostCollection/deleteCollection');
    app.post('/deleteCollection', deleteCollection.deleteCollection);

    var addPosts_Collection = require('./userPostCollection/addPosts');
    app.post('/addPostsToCollection', addPosts_Collection.addPosts);

    var removePostFromCollection = require('./userPostCollection/removePost');
    app.post('/removePostFromCollection', removePostFromCollection.removePostFromCollection);

    var getPostwithOutcollecction = require('./userPostCollection/getPostwithOutcollecction');
    app.post('/getPostwithOutcollecction', getPostwithOutcollecction.getPostwithOutcollecction);


    var GetAroundYouPosts = require('./GetAroundYou/GetAroundYouPosts');
    app.post('/GetAroundYouPosts', GetAroundYouPosts.GetAroundYouPosts);

    var GetAroundYouShows = require('./GetAroundYou/GetAroundYouShows');
    app.post('/GetAroundYouShows', GetAroundYouShows.GetAroundYouShows);

    var GetAroundYouUsers = require('./GetAroundYou/GetAroundYouUsers');
    app.post('/GetAroundYouUsers', GetAroundYouUsers.GetAroundYouUsers);



    /*
     * function name: updateLocaton.
     * Desc: this function is use to update current location of user.
     * req: token,userId, token, userId, location.
     * response: success response if no error else error.
     *  */
    var updateLocation = require('./profile/updateLocation');
    app.post('/updateLocation', updateLocation.updateLocation);

    console.log("its started now:  " + 5005);
    app.listen(5005)

}

