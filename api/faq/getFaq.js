/**
 * Created by embed on 6/1/17.
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;

exports.getFaq = function(req,res)
{
    //console.log("getFaq: " + JSON.stringify(req.body));
    var condToGetFilters = {};
    var projectDataForFilters = {question : 1, answer : 1};

    Utility.SelectMatched('FAQ',condToGetFilters,projectDataForFilters,function(err,result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result)
        {
            var dataToSend = [];
            for(var countOfFaqs=0; countOfFaqs<result.length; countOfFaqs++)
            {
                dataToSend.push({question : result[countOfFaqs]['question'], answer : '<html><body>'+result[countOfFaqs]['answer']+'</body></html>'});
            }
            res.send({errCode: 0, Message: "Data sent successfully.",data : dataToSend});
            return true;

        }
        else
        {
            res.send({errCode: 1, errNum: 155, Message: "currently no filters are available."});
            return true;
        }
    })
};
