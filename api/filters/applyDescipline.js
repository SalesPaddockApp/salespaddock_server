/**
 * Created by Niranjan on 16/2/17.
 * function name:  getShows
 * request: token,userId
 * response: all upcoming shows, recelntly viewed shows, and popular shows.
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');

exports.applyDescipline = function(req, res)
{
    if(!req.body.userId)
    {
        res.send({errCode: 1, errNum: 107, Message: "mandatory user Id is missing."});
        return false;
    }
    var cond = {userId : req.body.userId};

    var desciplineToUpdate = {descipline : req.body.descipline};

    //Utility.SelectWithPagination('shows',cond,projection,skip,limit,function (err, result)
    Utility.Update('user',cond,desciplineToUpdate,function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result)
        {
            res.send({errCode: 0,  Message: "Descipline applied successfully.",response:{}});
        }
        else
        {
            res.send({errCode: 1,  errNum: 135, Message: "wrong user Id provided."});
        }
    });
}