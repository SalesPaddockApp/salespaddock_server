/**
 * Created by Niranjan on 14/2/17.
 */

var Utility = require('../UtilityFunc');
exports.getAppliedDescipline = function(params,response)
{
    //console.log(JSON.stringify(params.body));

    //if(!params.body.name)
    //{
    //    response.send({errCode: 1, errNum: 142, Message: "Mandatory product type is missing."});
    //    return false;
    //}

    var cond = {productType: "Horse",preferenceTitle : "Discipline"};

    Utility.SelectOne('Preferences',cond,function(err,result)
    {
        if(err)
        {
            console.log(err);
            response.send({errCode: 1, errNum: 104, Message: "unknown error occurred."});
        }
        else if(result)
        {
            var dataToSend  = {title: result['preferenceTitle'],type: result['typeOfPreference'],
                values: result['optionsValue'],id: result['_id']};


            response.send({errCode: 0, Message: "Data sent successfully.", response:dataToSend});
        }
        else
        {
            response.send({errCode: 1, errNum: 105, Message: "Unknown error occurred"});
            return false;
        }
    })

}
