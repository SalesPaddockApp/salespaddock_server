/**
 * Created by embed on 6/1/17.getFilters
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;

exports.getFilters = function(req,res)
{

    console.log("getFilters: ");

    var condToGetFilters = {"productType" : "Horse", "isFilterable" : 1};
    var projectDataForFilters = {preferenceTitle : 1, typeOfPreference : 1,optionsValue : 1,Unit : 1};
    var sortingData = {prirority : 1};

    Utility.SelectWithSoringPagination('Preferences',condToGetFilters,projectDataForFilters,sortingData,0,100,function(err,result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result)
        {
            var filtersToSend = [];
            for(var countOfFilters=0; countOfFilters<result.length; countOfFilters++)
            {
                var unit = 'N/A';

                switch (result[countOfFilters]['Unit'])
                {
                    case 0:
                        unit = 'N/A';
                        break;
                    case '1':
                        unit = 'Km';
                        break;
                    case '2':
                        unit = 'Miles';
                        break;
                    case '3':
                        unit = 'Years';
                        break;
                    case '4':
                        unit = 'Kg';
                        break;
                    case '5'    :
                        unit = 'Dollar';
                        break;
                    default:
                        unit = 'N/A';

                }

                if(result[countOfFilters]['preferenceTitle'] == "Price Details")
                {
                    var values = [];
                    values.push("Contact for Price");

                    var values1 = result[countOfFilters]['optionsValue'];
                    //values1.sort()
                    for(var k=0; k<values1.length; k++)
                    {
                        values.push(values1[k]);
                    }
                    filtersToSend.push({preferenceTitle: result[countOfFilters]['preferenceTitle'],
                        typeOfPreference: result[countOfFilters]['typeOfPreference'],
                        unit: unit,optionsValue: values,_id: result[countOfFilters]['_id']});
                }

                else if(result[countOfFilters]['preferenceTitle'] == "Breed")
                {
                    var values8 = [];

                    var values9 = result[countOfFilters]['optionsValue'];
                    values9.sort()
                    for(var k=0; k<values9.length; k++)
                    {
                        values8.push(values9[k]);
                    }
                    filtersToSend.push({preferenceTitle: result[countOfFilters]['preferenceTitle'],
                        typeOfPreference: result[countOfFilters]['typeOfPreference'],
                        unit: unit,optionsValue: values8,_id: result[countOfFilters]['_id']});
                }

                else if(result[countOfFilters]['preferenceTitle'] == "Discipline")
                {
                    var values11 = [];

                    var values12 = result[countOfFilters]['optionsValue'];
                    values12.sort()
                    for(var k=0; k<values12.length; k++)
                    {
                        values11.push(values12[k]);
                    }


                    console.log("values11: " + JSON.stringify(values11));

                    filtersToSend.push({preferenceTitle: result[countOfFilters]['preferenceTitle'],
                        typeOfPreference: result[countOfFilters]['typeOfPreference'],
                        unit: unit,optionsValue: values11,_id: result[countOfFilters]['_id']});
                }
                else if(result[countOfFilters]['preferenceTitle'] == "Secondary Breed")
                {
                    var values2 = [];
                    values2.push("Not applicable");

                    var values3 = result[countOfFilters]['optionsValue'];
                    values3.sort()
                    for(var k=0; k<values3.length; k++)
                    {
                        if(values3[k] == "Not applicable")
                            continue;

                        values2.push(values3[k]);
                    }

                        var typeOfPreference = result[countOfFilters]['typeOfPreference'];
                        filtersToSend.push({preferenceTitle: result[countOfFilters]['preferenceTitle'],
                            typeOfPreference: typeOfPreference,
                        unit: unit,optionsValue: values2,_id: result[countOfFilters]['_id']});
                }
                else if(result[countOfFilters]['preferenceTitle'] == "Height")
                {
                    var values20 = [];

                    var values9 = result[countOfFilters]['optionsValue'];
                    values9.sort()
                    //for(var k=values9.length; k>=0; k--)
                    //{
                        values20.push(values9[0]);
                        values20.push(values9[values9.length-1]);
                    //}
                    filtersToSend.push({preferenceTitle: result[countOfFilters]['preferenceTitle'],
                        typeOfPreference: 3,
                        unit: 'hands',optionsValue: [],_id: result[countOfFilters]['_id']});
                }
                else
                {
                    var typeOfPreference = result[countOfFilters]['typeOfPreference'];

                    //if(result[countOfFilters]['preferenceTitle'] == 'Height')
                    //    typeOfPreference = '3';

                    filtersToSend.push({_id : result[countOfFilters]['_id'],
                        preferenceTitle : result[countOfFilters]['preferenceTitle'],
                        typeOfPreference : typeOfPreference,
                        optionsValue : result[countOfFilters]['optionsValue'],Unit : unit});
                }

            }
            res.send({errCode: 0, Message: "Data sent successfully.",data : filtersToSend});
            return true;

        }
        else
        {
            res.send({errCode: 1, errNum: 155, Message: "currently no filters are available."});
            return true;
        }
    })
};
