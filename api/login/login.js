module.exports = function(app,express){
    var router = express.Router();

var Utility = require('../UtilityFunc');
var jsonwebtoken = require('jsonwebtoken');

var conf = require('../conf');

var secretKey = conf.secretKey;


router.post('/login',function(req,res)
{

    console.log("login: " + JSON.stringify(req.body));

    if(!req.body.email)
    {
        res.send({errCode: 1, errNum: 108, Message: "mandatory email is missing"});
        return false;
    }

    if(!req.body.password)
    {
        res.send({errCode: 1, errNum: 109, Message: "mandatory password is missing"});
        return false;
    }

    var cond = {"email.primaryEmail" : req.body.email.toUpperCase()};

    console.log("cond:  " + JSON.stringify(cond));

    Utility.SelectOne('user',cond, (function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result)
        {
            if(req.body.password == result.password)
            {

                addCurrentDevice(req.body.email,req.body.deviceId);

                deviceInfo(req.body.email,req.body.deviceId,req.body.appVersion,req.body.os,req.body.manufacture,req.body.pushToken);

                var pushToken = req.body.pushToken ? req.body.pushToken :'';
                var data1 = {pushToken: pushToken};
                Utility.Update('user',cond,data1,function(err,isUpdated)
                {});

                var lName = "";
                if(result.name.lName)
                    lName = result.name.lName;

                var profilePic = "";
                if(result.profilePic)
                    profilePic = result.profilePic;

                user = {userId:result.userId,name:result.name.fName,email: result.email, deviceId : req.body.deviceId}
                var token = createToken(user);

                res.send({errCode: 0, Message: "Login successfully",response:{
                    userId:result.userId,fname:result.name.fName,profilePic: profilePic,
                    lname:lName,token:token}});
                return true;
            }   
            else
            {
                res.send({errCode: 1, errNum: 112, Message: "password does not match."});
                return false;                  
            }        
        }
        else
        {
            res.send({errCode: 1, errNum: 112, Message: "User does not exists"});
            return false;            
        }
    }));
});


function createToken(user)
{
    var token = jsonwebtoken.sign({id: user._id,name: user.name,email: user.email}, secretKey, {expiresIn: '60 days'});
    return token;
}



function deviceInfo(email,deviceId,appVersion,os,manufacture,pushToken)
{
    var condition = {$and : [{"email.primaryEmail" : email.toUpperCase()},{devices: {$elemMatch:{deviceId: deviceId}}}]};
    var dataToProject = {devices : 1};
    Utility.SelectMatch('user',condition,dataToProject,function(err,result)
    {
        if (err)
        {
            console.log(JSON.stringify({errCode: 1, errNum: 104, Message: "Unknown error occurred " + err}));
        }
        else if(result)
        {
            /*
            * here we donot need to update device id as device is same, we donot even require manufacturur.
            * */
            var dataToUpdate = {'devices.$.os': os,'devices.$.appVersion': appVersion,'devices.$.pushToken': pushToken};

            Utility.Update('user', condition, dataToUpdate, function (err,result1)
            {
                if (err)
                {
                    console.log(JSON.stringify({errCode: 1, errNum: 104, Message: "Unknown error occurred " + err}));
                }
                else
                {
                    console.log("device data successfully.")
                }
            })
        }
        else
        {
            /*
             * here we donot need to update device id as device is same, we donot even require manufacturur.
             * */
            var cond = {"email.primaryEmail" : email.toUpperCase()};
            var dataToUpdate1 = {'devices':{deviceId : deviceId, appVerion : deviceId,os: os,manufacture: manufacture,pushToken: pushToken}};

            Utility.UpdateAsInsertSpecificField('user', cond, dataToUpdate1, function (err,result1)
            {
                if (err)
                {
                    console.log(JSON.stringify({errCode: 1, errNum: 104, Message: "Unknown error occurred " + err}));
                }
                else
                {
                    console.log("device data successfully.")
                }
            })
        }
    })
}

function addCurrentDevice(email,deviceId)
{
    var cond = {"email.primaryEmail" : email.toUpperCase()};
    var data = {deviceId : deviceId};

    console.log(cond,data);
    Utility.Update('user',cond,data,function(err,res)
    {})
}


   return router;
};


