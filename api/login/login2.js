var exports = module.exports = {};

var Utility = require('../UtilityFunc');

exports.login = function(req, res)
{
    if(!req.body.email)
    {
        res.send({errCode: 1, errNum: 108, Message: "mandatory email is missing"});
        return false;
    }

    if(!req.body.password)
    {
        res.send({errCode: 1, errNum: 109, Message: "mandatory password is missing"});
        return false;
    }

    var cond = {email : req.body.email.toUpperCase()};

    Utility.SelectOne('user',cond, (function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result)
        {
            if(req.body.password == result.password)
            {
                res.send({errCode: 0, Message: "Login successfully",response:{}});
                return false;
            }   
            else
            {
                res.send({errCode: 1, errNum: 112, Message: "password does not match."});
                return false;                  
            }        
        }
        else
        {
            res.send({errCode: 1, errNum: 112, Message: "User does not exists"});
            return false;            
        }
    }));
};