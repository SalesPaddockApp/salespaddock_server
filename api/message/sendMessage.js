/**
 * Created by Niranjan on 5/1/17.
 */
exports.sendMessage = function(phoneNumber,message,userId,otp,callback)
{
    var twilio = require('twilio');
    var Utility = require('../UtilityFunc');
    var conf = require('../conf.json');
    var TWILIO_ACCOUNT_SID = conf.ACCOUNT_SID;
    var TWILIO_AUTH_TOKEN = conf.AUTH_TOKEN;
    var number = conf.number;

    // var client = new twilio.RestClient(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN);


    // var twilio = require('twilio');
    var client = new twilio(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN);

    client.messages.create({
        body: message,
        to: phoneNumber,  // Text this number
        from: number // From a valid Twilio number
    })
    .then((message) => console.log(message.sid));


    // var messageObject = {
    //     to: phoneNumber,
    //     from:number,
    //     body: message
    // };
    // client.sms.messages.create(messageObject, function(error, message)
    // {
    //     if (!error)
    //     {
    //         console.log('Message sent on:  ' + JSON.stringify(message));
    //         console.log(message.dateCreated);
    //     }
    //     else
    //     {
    //         console.log('Oops! There was an error.' + JSON.stringify(error));
    //     }
    // });


    var condToAddOtp = {userId : userId};
    var dataToAddOtp = {otp : otp};

    Utility.Update('user',condToAddOtp,dataToAddOtp,function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else if(result)
        {
            console.log("profile updated successfull.");
        }
        else
        {
            console.log("Wrong user id, please check.");
        }

    })

}