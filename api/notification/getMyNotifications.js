/**
 * Created by Niranjan on 23/12/16.
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
const limitNumber = 10;
var ObjectId = require('mongodb').ObjectID;
var async = require('async');

exports.getMyNotifications = function (req, res) {
    console.log("getMyNotifications:  " + JSON.stringify(req.body));
    if (!req.body.userId) {
        res.send({ errCode: 1, errNum: 132, Message: "mandatory user id is missing" });
        return false;
    }

    resetNotification(req.body.userId);


    async.parallel(
        {
            counts: function (callback) {
                var condToGetAllMyNotifications = [{ $sort: { _id: -1 } }, { $unwind: '$recievers' }, { $match: { "recievers.userId": req.body.userId } },
                { $group: { _id: '$sender', count: { $sum: 1 } } }];

                console.log("condToGetAllMyNotifications1:  " + JSON.stringify(condToGetAllMyNotifications));
                Utility.AggregateGroup('notification', condToGetAllMyNotifications, function (err, result) {
                    if (err) {
                        callback(err, err);
                    }
                    else if (result.length > 0) {
                        callback(null, result);
                    }
                    else {
                        callback(null, []);
                    }
                });
            },
            userAndPostData: function (callback) {
                var condToGetAllMyNotifications = [{ $sort: { _id: -1 } }, { $unwind: '$recievers' }, { $match: { "recievers.userId": req.body.userId } },
                { $lookup: { from: 'user', localField: 'sender', foreignField: 'userId', as: 'user' } }, { $unwind: '$user' },
                { $project: { postId: 1, message: 1, userId: '$recievers', sender: 1, userName: '$user.name.fName', userPic: '$user.profilePic' } },
                { $lookup: { from: 'post', localField: 'postId', foreignField: '_id', as: 'post' } },
                // {$unwind : '$post'},
                {
                    "$unwind": {
                        "path": "$post",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $project: {
                        postId: 1, message: 1, userId: 1, sender: 1, userName: 1, userPic: 1,
                        values: '$post.values'
                    }
                }];

                console.log("condToGetAllMyNotifications2:  " + JSON.stringify(condToGetAllMyNotifications));

                /*
                 ,{$unwind : '$values'},{$match : {'values.pref_id' : ObjectId("57efa0aa00eba37a3dd379cd")}},
                 {$project : {postId: 1,message : 1,userId : 1,sender : 1,userName : 1,userPic : 1,
                 postName:'$values.options'}},{$unwind : '$postName'}
                 * */

                Utility.AggregateGroup('notification', condToGetAllMyNotifications, function (err, result) {
                    if (err) {
                        callback(err, err);
                    }
                    else if (result.length > 0) {
                        callback(null, result);

                    }
                    else {
                        callback(null, []);
                    }
                });
            }
        },
        function (err, notifications) {
            console.log("notifications.userAndPostData.length : ", notifications.userAndPostData.length)
            console.log("notifications.counts.length : ", notifications.counts.length)
            if (err) {
                res.send({ errCode: 1, errNum: 104, Message: "unknown error occurred." });
                return false;
            }
            else if (notifications.userAndPostData.length > 0 && notifications.counts.length > 0) {
                var notificationData = [];
                for (var notificationCount = 0; notificationCount < notifications.userAndPostData.length; notificationCount++) {
                    var isImageAndNameFilled = 0;
                    var postImage = {};
                    var postName = '';
                    var countOfMessages = '';

                    if (notifications.userAndPostData[notificationCount]['values']) {


                        for (var optionsCount = 0; optionsCount < notifications.userAndPostData[notificationCount]['values'].length; optionsCount++) {
                            if (notifications.userAndPostData[notificationCount]['sender'] == "admin") {
                                if (notifications.userAndPostData[notificationCount]['values'][optionsCount]['pref_id'] == '57f79d6700eba39c7b2f80f1') {
                                    postImage = {}//'';
                                    isImageAndNameFilled++;
                                }

                                if (notifications.userAndPostData[notificationCount]['values'][optionsCount]['pref_id'] == '57efa0aa00eba37a3dd379cd') {
                                    postName = '';
                                    isImageAndNameFilled++;
                                }

                                if (isImageAndNameFilled == 2) {
                                    break;
                                }
                            }
                            else {
                                if (notifications.userAndPostData[notificationCount]['values'][optionsCount]['pref_id'] == '57f79d6700eba39c7b2f80f1') {
                                    postImage = notifications.userAndPostData[notificationCount]['values'][optionsCount]['options'][0];
                                    isImageAndNameFilled++;
                                }

                                if (notifications.userAndPostData[notificationCount]['values'][optionsCount]['pref_id'] == '57efa0aa00eba37a3dd379cd') {
                                    postName = notifications.userAndPostData[notificationCount]['values'][optionsCount]['options'][0];
                                    isImageAndNameFilled++;
                                }

                                if (isImageAndNameFilled == 2) {
                                    break;
                                }
                            }


                        }
                    }
                    for (var messageCouunts = 0; messageCouunts < notifications.counts.length; messageCouunts++) {
                        if (notifications.counts[messageCouunts]['_id'] == notifications.userAndPostData[notificationCount]['sender']) {
                            countOfMessages = notifications.counts[messageCouunts]['count'];
                        }
                    }

                    if (notifications.userAndPostData[notificationCount]['sender'] == "admin") {
                        var isByAdmin = 1;
                        var messageFOrUser = "Admin sent an alert";
                    }
                    else {
                        var isByAdmin = 0;
                        var messageFOrUser = notifications.userAndPostData[notificationCount]['userName'] + " sent " + " a message about " + postName;
                    }


                    notificationData.push({
                        postId: notifications.userAndPostData[notificationCount]['postId'], message: messageFOrUser,
                        mainMessage: notifications.userAndPostData[notificationCount]['message'], isByAdmin: isByAdmin,
                        userId: notifications.userAndPostData[notificationCount]['userId'], sender: notifications.userAndPostData[notificationCount]['sender'],
                        userName: notifications.userAndPostData[notificationCount]['userName'], userPic: notifications.userAndPostData[notificationCount]['userPic'],
                        postImage: postImage, postName: postName, messageCount: countOfMessages
                    });
                }
                console.log("notificationData : ", notificationData);
                res.json({ errCode: 0, Message: "data send succcessfully.", response: notificationData });
            }
            else {
                //var notificationsByAdmin = [];
                //if(notifications.adminNotification.length > 0)
                //{
                //    for(var adminMessCounts=0; adminMessCounts<notifications.adminNotification.length; adminMessCounts++)
                //    {
                //        var messageFOrUser = "admin sent a message";
                //        notificationsByAdmin.push({message : messageFOrUser,mainMessage: notifications.adminNotification[adminMessCounts]['message'],isByAdmin : 1});
                //    }
                //    res.json({errCode: 0,  Message: "data send succcessfully.",response: notificationsByAdmin});
                //}
                //else
                //{
                console.log("notificationData : no have Data ");
                res.send({ errCode: 1, errNum: 214, Message: "currently no notifications found, please try again." });
                return false;
                //}


            }
        })
}

exports.getMyNotifications1 = function (req, res) {
    if (!req.body.userId) {
        res.send({ errCode: 1, errNum: 132, Message: "mandatory user id is missing" });
        return false;
    }

    var condToGetAllMyNotifications = [{ $sort: { _id: 1 } }, { $sort: { _id: 1 } }, { $unwind: '$recievers' },
    { $match: { "recievers.userId": req.body.userId } },
    { $lookup: { from: 'user', localField: 'sender', foreignField: 'userId', as: 'user' } },
    { $unwind: '$user' },
    { $project: { postId: 1, message: 1, userId: '$recievers', sender: 1, userName: '$user.name.fName', userPic: '$user.profilePic' } },
    { $lookup: { from: 'post', localField: 'postId', foreignField: '_id', as: 'post' } }, { $unwind: '$post' },
    {
        $project: {
            postId: 1, message: 1, userId: 1, sender: 1, userName: 1, userPic: 1,
            values: '$post.values'
        }
    }];


    /*
     ,{$unwind : '$values'},{$match : {'values.pref_id' : ObjectId("57efa0aa00eba37a3dd379cd")}},
     {$project : {postId: 1,message : 1,userId : 1,sender : 1,userName : 1,userPic : 1,
     postName:'$values.options'}},{$unwind : '$postName'}
     * */
    Utility.AggregateGroup('notification', condToGetAllMyNotifications, function (err, result) {
        if (err) {
            res.send({ errCode: 1, errNum: 104, Message: "unknown error occurred." });
            return false;
        }
        else if (result.length > 0) {
            callback(null, result);

        }
        else {
            res.send({ errCode: 1, errNum: 214, Message: "currently no notifications found, please try again." });
            return false;
        }
    });
}

function resetNotification(userId) {
    console.log('resetNotification');
    var cond = { userId: userId };
    var dataToUpdate = { totalCount: 0, alertCount: 0 };
    Utility.Update('user', cond, dataToUpdate, function (err, isUpdate) { });
}