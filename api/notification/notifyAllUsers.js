/**
 * Created by embed on 27/1/17.
 */
var notification = require('../notification/sendNotification');
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;
exports.notifyAllUsers = function (req,res)
{
    console.log("notify users:  " + JSON.stringify(req.body));
    var userIds = req.body.userId;
    var userIdss = [];
    var userIdToInsert = [];
    for(var usersCount=0; usersCount<userIds.length; usersCount++)
    {
        userIdss.push(userIds[usersCount]);
    }

    if(userIds.length > 0)
        var conditionToGetUsersNotifications = {userId : {$in : userIdss}, active : '1'};
    else
        var conditionToGetUsersNotifications = {active : '1'};

    console.log("conditionToGetUsersNotifications",conditionToGetUsersNotifications);

    var projectData = {pushToken : 1, userId : 1};
    Utility.SelectMatched('user',conditionToGetUsersNotifications,projectData,function(err,result)
    {
        if (err)
        {
            res.status(520).json({Message: 'Unknown error occurred.'});
            return false;
        }
        else if(result.length > 0)
        {
            var pushTokens = [];
            var userIdss = [];
            for(var j=0; j<result.length; j++)
            {
                if(result[j]['pushToken'])
                {
                    pushTokens.push(result[j]['pushToken']);
                }
                userIdss.push(result[j]['userId']);
                userIdToInsert.push({userId : result[j]['userId']});
            }

            updateBadgeCount(userIdss);


            var dataToInsert = {"message" : req.body.mess,"recievers" : userIdToInsert,
                "sender" : "admin","postId" : ObjectId("589d8c8bc486dd4be78ab335")};
            Utility.Insert('notification',dataToInsert, function (err, isInserted)
            {
            })

            console.log("userIds",userIds);

            if(userIds.length > 0)
            {
                for(var k=0; k<pushTokens.length; k++)
                {
                    var data = {pushTokens: [pushTokens[k]],alert: req.body.mess,payload:{type : "admin alerts"}};

                    notification.sendNotification(data,(function (err, result)
                    {
                        if(err)
                        {
                            console.log("some error occurred:  " + JSON.stringify(err));
                        }
                        else
                        {
                            console.log("checking notifications:  " + JSON.stringify(result));
                        }
                    }));
                }

            }
            else
            {
                console.log("groupp");
                var data1 = {alert: req.body.mess,payload:{type : "admin alerts"}};

                notification.sendGroupNotification(data1,(function (err, result)
                {
                    if(err)
                    {
                        console.log("some error occurred group:  " + JSON.stringify(err));
                    }
                    else
                    {
                        console.log("checking notifications:  " + JSON.stringify(result));
                    }
                }));
            }



            res.status(200).json({Message: 'Notification sent successfully.'});
            return true;
        }
        else
        {
            console.log("no users.");
            res.status(204).json({Message: 'No Content.'});
            return true;
        }
    })
}


function updateBadgeCount(userIds)
{
    console.log("userIdss", userIds);
    console.log("coming update Badge Count");

    var cond = {userId : {$in : userIds}};

    Utility.Select('user',cond, function (err,respo)
    {
        if(err)
        {
            console.log("some error occurred if:  " + JSON.stringify(err));
        }
        else if(respo.length > 0)
        {
            for(var i=0; i<respo.length; i++)
            {
                var alertCount = 0;
                var totalCount = 0;
                if(respo[i]['alertCount'])
                    alertCount = parseInt(respo[i]['alertCount']);

                if(respo[i]['totalCount'])
                    totalCount  = parseInt(respo[i]['totalCount']);

                alertCount = alertCount + 1;
                totalCount  = totalCount  + 1;

                var datoUpdate = {alertCount : alertCount,totalCount : totalCount};

                var condToUpdate = {userId : respo[i]['userId']};
                updateBadge(condToUpdate,datoUpdate);
            }

            console.log("result.length:  " + respo.length)
        }
        else
        {
            console.log("some error occurred else.")
        }
    })
}

function updateBadge(cond,dataToUpdate)
{
    Utility.Update('user',cond,dataToUpdate, function (err,respon)
    {
        if(err)
        {
            console.log("some error occurred if:  " + JSON.stringify(err));
        }
        else if(respon)
        {
            console.log("result.length:  " + respon);
        }
        else
        {
            console.log("some error occurred else.")
        }
    })
}
