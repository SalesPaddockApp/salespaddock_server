/**
 * Created by Niranjan on 8/10/16.
 */

var apn = require('apn');
var path = require("path");
var FCM = require('fcm-push');
var conf = require('../conf');

console.log("conf.SERVER_FCM_KEY",conf.SERVER_FCM_KEY);

var fcm = new FCM(conf.SERVER_FCM_KEY);

exports.sendGroupNotification = function(args, callback)
{
    console.log("sendNotification:  " + JSON.stringify(args));
    //console.log("fcm called");
    //console.log("fcm called");
    var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
        to: '/topics/adminMessage',
        collapse_key: 'your_collapse_key',
        badge : '1',
        priority: "high",

        notification: {
            sound : "sms-received.wav",
            badge : 1,
            title: 'SalesPaddock',
            body:  args.alert
        },

        data: {payload: args.payload}
    };

    try
    {
        fcm.send(message, function(err, response){
            if (err) {
                console.log("Something has gone wrong!", err);
                return callback(err,err);
                //console.log("Something has gone wrong!", err);
                //console.log("Something has gone wrong!", err);
                //console.log("Something has gone wrong!", err);
            } else {
                console.log("Successfully sent with response: ", response);

                return callback(null, {errcode: 0,msg: "push sent successfully"});
                //console.log("Successfully sent with response: ", response);
                //console.log("Successfully sent with response: ", response);
                //console.log("Successfully sent with response: ", response);
                //console.log("Successfully sent with response: ", response);
            }
        });
    }
    catch (e)
    {
        console.log(e);
        console.log(e);
        console.log(e);
    }

};

exports.sendNotification = function(args, callback)
{
    console.log("sendNotification:  " + JSON.stringify(args));
    //console.log("fcm called");
    //console.log("fcm called");
    var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
        to: args.pushTokens[0],
        collapse_key: 'your_collapse_key',
        badge : '1',
        priority: "high",

        notification: {
            sound : "sms-received.wav",
            badge : 1,
            title: 'SalesPaddock',
            body:  args.alert
        },

        data: {payload: args.payload}
    };


    try
    {
        fcm.send(message, function(err, response){
            if (err) {
                console.log("Something has gone wrong!", err);
                return callback(err,err);
                //console.log("Something has gone wrong!", err);
                //console.log("Something has gone wrong!", err);
                //console.log("Something has gone wrong!", err);
            } else {
                console.log("Successfully sent with response: ", response);

                return callback(null, {errcode: 0,msg: "push sent successfully"});
                //console.log("Successfully sent with response: ", response);
                //console.log("Successfully sent with response: ", response);
                //console.log("Successfully sent with response: ", response);
                //console.log("Successfully sent with response: ", response);
            }
        });
    }
    catch (e)
    {
        console.log(e);
        console.log(e);
        console.log(e);
    }

};

exports.sendNotification1 = function(args, callback)
{

    console.log("sendNotification:  " + JSON.stringify(args));
    var production = args.production;
    //var production = false;

    if(production)
    {
        certificates = {cert: "prodCert.pem",key: "prodKey.pem" };
    }
    else
    {
        certificates = {cert: "devCert.pem",key: "devKey.pem" };
    }

    var options = {
        cert: path.join(__dirname, "/" + certificates.cert),
        key: path.join(__dirname, "/" + certificates.key),
        passphrase: "12345",
        production : production
    };

    var apnConnection = new apn.Connection(options);

    if (typeof args.payload != "object")
        args.push_data = {};

    try
    {
        var myDevice;
        if(args.isArr == 1)
        {
            myDevice = args.pushTokens;
        }
        else
        {
            myDevice = new apn.Device(args.pushTokens);
        }

    }
    catch (devErr)
    {
        return callback(devErr);
    }

    var content = 0;
    if(args.content)
        content = args.content;
    else
        content = 0;

    var note = new apn.Notification();

    note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
    note.sound = "sms-received.wav";
    note.badge = '1';
    note.alert = args.alert;
    note.payload = args.payload; // Mush be OBJECT
    note.contentAvailable = content;


    apnConnection.on('connected', function() {
        console.log("Connected");
    });

    apnConnection.on('transmitted', function(notification, device) {
        console.log("Notification transmitted to:" + device.token.toString('hex') + "notification :-" + notification);
    });

    apnConnection.on('transmissionError', function(errCode, notification, device) {
        console.error("Notification caused error: " + errCode + " for device ", device, notification);
    });

    apnConnection.on('timeout', function () {
        console.log("Connection Timeout");
    });

    apnConnection.on('disconnected', function() {
        console.log("Disconnected from APNS");
    });

    apnConnection.on('error', function(error) {
        console.log("getting some error: " + error);
    });

    apnConnection.on('socketError', console.error);

    try
    {
        console.log('Token: ' + JSON.stringify(myDevice));
        apnConnection.pushNotification(note, myDevice);

        return callback(err, {errcode: 0,msg: "push sent successfully"});
    }
    catch (e)
    {
        return callback("catch: " + e.message);
    }

};