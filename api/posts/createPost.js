/**
 * Created by Niranjan on 19/9/16.
 * function name: createPost
 * request: token,userId,wishId
 * response: succuss response if no error else error accordingly
 */

var exports = module.exports = {};
var Utility = require('../UtilityFunc');

var ObjectId = require('mongodb').ObjectID;

exports.createPost = function(req, res)
{
    //console.log("data:  " + JSON.stringify(req.body));

    if(!req.body.userId)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory user id is missing"});
        return false;
    }

    if(!req.body.productType)
    {
        res.send({errCode: 1, errNum: 138, Message: "mandatory product Type name is missing"});
        return false;
    }

    var  values = req.body.values;
    var lat = 0.0;
    var long = 0.0;

    //console.log("values:  ", JSON.stringify(values));
    for(var j=0;j<values.length;j++)
    {
        if(values[j].pref_id == "58ad97cdc288860c2844571d")
        {
            //console.log("values[j]: ", JSON.stringify(values[j]['options'][0]['long']));
            //console.log("values[j]: ", JSON.stringify(values[j]['options'][0]['lat']));
            lat =  parseFloat(values[j]['options'][0]['lat']);
            long = parseFloat(values[j]['options'][0]['long']);
        }

        values[j].pref_id = ObjectId(values[j].pref_id);
    }

    //console.log("Values : "+JSON.stringify(values));
    //req.body.values=values;

    //var lat = 0.0;
    //if(req.body.lat)
    //    lat = parseFloat(req.body.lat);
    //
    //var long = 0.0;
    //if(req.body.long)
    //    long = parseFloat(req.body.long);

    var dataToInsert = {userId: req.body.userId,"activeStatus" : "0",
        location : {longitude : long,latitude : lat},
        created: Date.now(),values: values,productType: req.body.productType, productId: ObjectId(req.body._id)};

    //console.log("data to insert: " + JSON.stringify(dataToInsert));
    Utility.Insert('post',dataToInsert,function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result)
        {
            var condToGetPrefenceCount = {productType : "Horse"};
            Utility.Select('Preferences',condToGetPrefenceCount,function(err,preferenceCount)
            {
                if (err)
                {
                    res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
                    return false;
                }
                else if(preferenceCount.length > 0)
                {
                    res.send({errCode: 0,  Message: "you have created one post successfully.",response:
                    {postId: result['ops'][0]['_id'], totalCount : preferenceCount.length}});
                    return true;
                }
                else
                {
                    res.send({errCode: 0,  Message: "you have created one post successfully.",response:
                    {postId: result['ops'][0]['_id'], totalCount : 0}});
                    return true;
                }
            });
        }
        else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no product is there, please try after some time."});
            return true;
        }
    });
}






