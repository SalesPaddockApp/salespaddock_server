/**
 * Created by Niranjan on 27/01/2017.
 * function name: deletePost
 * request: postId
 * response: succuss response if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;


exports.deletePost = function (req, res)
{
    if(!req.body.postId)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory post id  is missing"});
        return false;
    }


    var cond = {_id: ObjectId(req.body.postId)};
    Utility.Delete('post', cond, function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if (result)
        {
            var deleteMessageCondition = {postId : req.body.postId};
            Utility.Delete('Messages', deleteMessageCondition, function (err, isMessageDeleted)
            {
                if(err)
                {
                    console.log("some error occurred while deleting messages:  " + JSON.stringify(err));
                }
            })

            res.send({errCode: 0, Message: "Your post deleted successfully.", response: {}});
            return true;


        }
        else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no post is there, please try after some time."});
            return true;
        }
    });
}

