/**
 * Created by Niranjan on 30/12/16.
 */

var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var async = require('async');

var ObjectId = require('mongodb').ObjectID;

exports.getAllMyListedPost   = function(req, res)
{
    console.log("getAllMyListedPost:  " + JSON.stringify(req.body));
    if(!req.body.userId)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory user id is missing"});
        return false;
    }

    if(!req.body.showId)
    {
        res.send({errCode: 1, errNum: 145, Message: "mandatory show id is missing"});
        return false;
    }

    async.parallel(
        {
            popular : function(callback)
            {
                var cond = {userId : req.body.userId};

                Utility.Select('post',cond,function (err, popularDetail)
                {
                    if (err)
                    {
                        callback(null,[]);
                    }
                    else if(popularDetail)
                    {
                        callback(null,popularDetail);
                    }
                    else
                    {
                        console.log(2);
                        callback(null,[]);
                    }
                });
            },
            preferences : function(callback)
            {
                var condForPref = {"productType" : "Horse"};
                Utility.Select('Preferences',condForPref,function (err, prefDetails)
                {
                    if (err)
                    {
                        callback(null,0);
                    }
                    else if(prefDetails.length > 0)
                    {
                        callback(null,prefDetails);
                    }
                    else
                    {
                        callback(null,0);
                    }
                });
            },
            show : function(callback)
            {
                Utility.Select('shows',{},function (err, showDetail)
                {
                    if (err)
                    {
                        callback(null,[]);
                    }
                    else if(showDetail)
                    {
                        callback(null,showDetail);
                    }
                    else
                    {
                        console.log(2);
                        callback(null,[]);
                    }
                });
            }
        },
        function(err,results)
        {
            var dataToSends = [];
            var lengthOfValuesOfPost = results.preferences.length;
            for(var j=0; j<results.popular.length; j++)
            {

                // console.log(results.popular[j]['_id'] + "  popularDetail length:  " + JSON.stringify(results.popular[j]['values'].length));

                if(lengthOfValuesOfPost != results.popular[j]['values'].length)
                {
                    continue;
                }
                var dataToSend = [];
                var name = "";
                var image = {};
                var imageId = "";
                for(var k=0; k<results.preferences.length; k++)
                {
                    for(var l=0; l<results.popular[j]['values'].length; l++)
                    {
                        if(results.preferences[k]['_id'].toString() == results.popular[j]['values'][l]['pref_id'].toString())
                        {
                            if(results.preferences[k]['preferenceTitle'] == 'Horse Name')
                            {
                                name = results.popular[j]['values'][l]['options'][0]
                            }

                            if(results.preferences[k]['preferenceTitle'] == 'Image')
                            {
                                image['title'] = results.preferences[k]['preferenceTitle'];
                                image['options'] =  results.popular[j]['values'][l]['options'];
                                image['prirority'] = results.preferences[k]['prirority'];
                                image['onCreateScreen'] = results.preferences[k]['onCreateScreen'];
                                image['unit'] = results.preferences[k]['unit'];
                                image['values'] = results.preferences[k]['optionsValue'];
                                image['id'] = results.preferences[k]['_id'];
                            }
                            else
                            {
                                dataToSend.push({title: results.preferences[k]['preferenceTitle'],
                                    options : results.popular[j]['values'][l]['options'],values : results.preferences[k]['optionsValue'],
                                    type: results.preferences[k]['typeOfPreference'],prirority: results.preferences[k]['prirority'],
                                    onCreateScreen: results.preferences[k]['onCreateScreen'],unit: results.preferences[k]['unit'],
                                    id: results.preferences[k]['_id']});
                            }
                            break;
                        }
                    }

                    if(k == (results.popular[j]['values'].length-1))
                    {
                        //if isPending == 0 means it is approved and if it is == 1 than not approved
                        var isPending = 1;
                        if(results.popular[j]['activeStatus'] == '1')
                            isPending = 0;
                        // console.log("results.popular:  " + results.popular[j]['_id']);
                        dataToSends.push({name: name,data:dataToSend,image: image,postId: results.popular[j]['_id'], isAddedOnShow : 0, isPending : isPending});
                    }
                }
            }

            // console.log("dataToSends length:   " + JSON.stringify(dataToSends.length));

            var indexToRemoveDataFromMainData = [];
            for(var showCount=0; showCount<results.show.length; showCount++)
            {
                for(var dataCount=0; dataCount<dataToSends.length; dataCount++)
                {
                    for(var postIndexOnShow=0; postIndexOnShow<results.show[showCount]['posts'].length; postIndexOnShow++)
                    {
                        if(dataToSends[dataCount]['postId'].toString() == results.show[showCount]['posts'][postIndexOnShow]['postId'].toString())
                        {
                            if(req.body.showId !=  results.show[showCount]['_id'].toString())
                            {
                                // console.log("test: " + dataCount);
                                //dataToSends.splice(0, 1);
                                indexToRemoveDataFromMainData.push(dataCount);
                            }
                            else
                            {
                                dataToSends[dataCount]['isAddedOnShow'] = 1;
                                break;
                            }
                        }
                    }
                }
            }

            for(var countOfRemovingData= (indexToRemoveDataFromMainData.length -1); countOfRemovingData >= 0;countOfRemovingData--)
            {
                dataToSends.splice(indexToRemoveDataFromMainData[countOfRemovingData], 1);
            }

            console.log("indexToRemoveDataFromMainData: " + JSON.stringify(dataToSends));

            //console.dir("dataToSends:  " +JSON.stringify(dataToSends));
            res.send({errCode: 0,  Message: "post data send successfully.",response: {result: dataToSends}});
        });
}

