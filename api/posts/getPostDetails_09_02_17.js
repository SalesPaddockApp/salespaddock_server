/**
 * Created by embed on 9/2/17.
 */
/**
 * Created by Niranjan on 30/9/16.
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var async = require('async');

var ObjectId = require('mongodb').ObjectID;

exports.getPostDetails = function(req, res)
{
    //if(!req.body.userId)
    //{
    //    res.send({errCode: 1, errNum: 132, Message: "mandatory user id is missing"});
    //    return false;
    //}

    if(!req.body.postId)
    {
        res.send({errCode: 1, errNum: 145, Message: "mandatory post id is missing"});
        return false;
    }

    async.parallel(
        {
            popular : function(callback)
            {
                var cond = {_id : ObjectId(req.body.postId)};

                Utility.SelectOne('post',cond,function (err, popularDetail)
                {
                    if (err)
                    {
                        callback(null,[]);
                    }
                    else if(popularDetail)
                    {
                        callback(null,popularDetail);
                    }
                    else
                    {
                        console.log(2);
                        callback(null,[]);
                    }
                });
            },
            isWish : function(callback)
            {
                var cond1 = {postId : ObjectId(req.body.postId),userId: req.body.userId};

                Utility.SelectOne('wishList',cond1,function (err, isWished)
                {
                    if (err)
                    {
                        callback(null, "0");
                    }
                    else if(isWished)
                    {
                        var dataToSend = {isWish: 1};
                        callback(null,"1");
                    }
                    else
                    {
                        console.log(2);
                        callback(null, "0");
                    }
                });
            },
            preferences : function(callback)
            {
                var condForPref = {"productType" : "Horse"};
                Utility.Select('Preferences',condForPref,function (err, prefDetails)
                {
                    if (err)
                    {
                        callback(null,0);
                    }
                    else if(prefDetails.length > 0)
                    {
                        callback(null,prefDetails);
                    }
                    else
                    {
                        callback(null,0);
                    }
                });
            },
            ownerDetails : function(callback)
            {
                var condForPref = [{$lookup:{from:"user",localField :"userId",
                    foreignField:"userId",as:"users"}},{$match : {_id: ObjectId(req.body.postId)}}];
                Utility.AggregateGroup('post',condForPref,function (err,oDetails)
                {
                    if (err)
                    {
                        callback(null,0);
                    }
                    else if(oDetails.length > 0)
                    {
                        callback(null,oDetails);
                    }
                    else
                    {
                        callback(null,0);
                    }
                });
            }
        },
        function(err,results)
        {
            console.log(1);
            //for(var j=0; j<results.popular.length; j++)
            //{
            var dataToSend = [];
            for(var k=0; k<results.preferences.length; k++)
            {
                for(var l=0; l<results.popular['values'].length; l++)
                {
                    if(results.preferences[k]['_id'].toString() == results.popular['values'][l]['pref_id'].toString())
                    {
                        if(results.preferences[k]['preferenceTitle'] != 'Horse Name' && results.preferences[k]['preferenceTitle'] != 'Breed' &&
                            results.preferences[k]['preferenceTitle'] != 'Image'  && results.preferences[k]['preferenceTitle'] != 'Description' )
                        {
                            dataToSend.push({title: results.preferences[k]['preferenceTitle'],selected : results.popular['values'][l]['options']});
                            break;
                        }

                        if(results.preferences[k]['preferenceTitle']  == 'Horse Name')
                        {
                            name = userPostsAndDetails.posts[postCount]['values'][postValuesCount]['options'][0]
                            console.log("name:  "+ name);
                        }

                        if(userPostsAndDetails.preference[preferenceCount]['preferenceTitle'] == 'Price Details')
                        {
                            console.log("it is coming to price detail list");
                            priceDetails['title'] = userPostsAndDetails.preference[preferenceCount]['preferenceTitle'];
                            priceDetails['options'] =  userPostsAndDetails.posts[postCount]['values'][postValuesCount]['options'][0];
                        }

                        if(userPostsAndDetails.preference[preferenceCount]['preferenceTitle'] == 'Image')
                        {
                            image['title'] = userPostsAndDetails.preference[preferenceCount]['preferenceTitle'];
                            image['options'] =  userPostsAndDetails.posts[postCount]['values'][postValuesCount]['options'];
                        }
                        else
                        {
                            dataToSend.push({title: userPostsAndDetails.preference[preferenceCount]['preferenceTitle'],
                                options : userPostsAndDetails.posts[postCount]['values'][postValuesCount]['options']
                            });
                        }
                        break;

                    }
                }

                //if(k == (popularDetail[j]['values'].length-1))
                //{
                //    console.log("Sdfsfsdf");
                //    dataToSends.push(dataToSend);
                //}

            }
            //}

            dataToSends.push({name: name,data:dataToSend,image: image,priceDetails: priceDetails,
                postId: userPostsAndDetails.posts[postCount]['_id']})

            var profilePic = '';
            if(results.ownerDetails[0]['users'][0]['profilePic'])
                profilePic = results.ownerDetails[0]['users'][0]['profilePic'];

            var about = '';
            if(results.ownerDetails[0]['users'][0]['about'])
                about = results.ownerDetails[0]['users'][0]['about'];

            var fName = '';
            if(results.ownerDetails[0]['users'][0]['name']['fName'])
                fName = results.ownerDetails[0]['users'][0]['name']['fName'];

            var ownerDetails = {userId: results.ownerDetails[0]['users'][0]['userId'],name: fName,
                profilePic: profilePic,about: about}
            console.log(results.ownerDetails[0]['users'][0]);

            res.send({errCode: 0,  Message: "post data send successfully.",response: {postDetails: dataToSend, ownerDetails: ownerDetails,isWish: results.isWish}});


        });
}

