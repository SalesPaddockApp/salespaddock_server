/**
 * Created by embed on 10/1/17.
 */

var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var async = require('async');

var ObjectId = require('mongodb').ObjectID;

exports.getValidHorsesInShows   = function(req, res)
{
    if(!req.body.userId)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory user id is missing"});
        return false;
    }

    if(!req.body.postId)
    {
        res.send({errCode: 1, errNum: 145, Message: "mandatory post id is missing"});
        return false;
    }

    async.parallel(
        {
            popular : function(callback)
            {
                var cond = {userId : req.body.userId};

                Utility.Select('post',cond,function (err, popularDetail)
                {
                    if (err)
                    {
                        callback(null,[]);
                    }
                    else if(popularDetail)
                    {
                        callback(null,popularDetail);
                    }
                    else
                    {
                        console.log(2);
                        callback(null,[]);
                    }
                });
            },
            preferences : function(callback)
            {
                var condForPref = {"productType" : "Horse"};
                Utility.Select('Preferences',condForPref,function (err, prefDetails)
                {
                    if (err)
                    {
                        callback(null,0);
                    }
                    else if(prefDetails.length > 0)
                    {
                        callback(null,prefDetails);
                    }
                    else
                    {
                        callback(null,0);
                    }
                });
            },
            show : function(callback)
            {
                Utility.Select('shows',{},function (err, showDetail)
                {
                    if (err)
                    {
                        callback(null,[]);
                    }
                    else if(showDetail)
                    {
                        callback(null,showDetail);
                    }
                    else
                    {
                        console.log(2);
                        callback(null,[]);
                    }
                });
            }
        },
        function(err,results)
        {
            console.log(1);
            var dataToSends = [];
            var lengthOfValuesOfPost = results.preferences.length;
            for(var j=0; j<results.popular.length; j++)
            {
                if(lengthOfValuesOfPost != results.popular[j]['values'].length)
                {
                    break;
                }
                var dataToSend = [];
                var name = "";
                var image = {};
                var imageId = "";
                for(var k=0; k<results.preferences.length; k++)
                {
                    for(var l=0; l<results.popular[j]['values'].length; l++)
                    {
                        if(results.preferences[k]['_id'].toString() == results.popular[j]['values'][l]['pref_id'].toString())
                        {
                            if(results.preferences[k]['preferenceTitle'] == 'Horse Name')
                            {
                                name = results.popular[j]['values'][l]['options'][0]
                            }

                            if(results.preferences[k]['preferenceTitle'] == 'Image')
                            {
                                image['title'] = results.preferences[k]['preferenceTitle'];
                                image['options'] =  results.popular[j]['values'][l]['options'];
                                image['prirority'] = results.preferences[k]['prirority'];
                                image['onCreateScreen'] = results.preferences[k]['onCreateScreen'];
                                image['unit'] = results.preferences[k]['unit'];
                                image['values'] = results.preferences[k]['optionsValue'];
                                image['id'] = results.preferences[k]['_id'];
                            }
                            else
                            {
                                dataToSend.push({title: results.preferences[k]['preferenceTitle'],
                                    options : results.popular[j]['values'][l]['options'],values : results.preferences[k]['optionsValue'],
                                    type: results.preferences[k]['typeOfPreference'],prirority: results.preferences[k]['prirority'],
                                    onCreateScreen: results.preferences[k]['onCreateScreen'],unit: results.preferences[k]['unit'],
                                    id: results.preferences[k]['_id']});
                            }
                            break;
                        }
                    }

                    if(k == (results.popular[j]['values'].length-1))
                    {
                        dataToSends.push({name: name,data:dataToSend,image: image,postId: results.popular[j]['_id'], isAddedOnShow : 0});
                    }
                }
            }

            for(var showCount=0; showCount<results.show.length; showCount++)
            {
                for(var dataCount=0; dataCount<dataToSends.length; dataCount++)
                {
                    for(var postIndexOnShow=0; postIndexOnShow<results.show[showCount]['posts'].length; postIndexOnShow++)
                    {
                        if(dataToSends[dataCount]['postId'].toString() == results.show[showCount]['posts'][postIndexOnShow]['postId'].toString())
                        {
                            dataToSends[dataCount]['isAddedOnShow'] = 1;
                            break;
                        }
                    }
                }
            }

            //console.dir("dataToSends:  " +JSON.stringify(dataToSends));
            res.send({errCode: 0,  Message: "post data send successfully.",response: {result: dataToSends}});
        });
}

