/**
 * Created by Niranjan on 2/3/17.
 * function name:  searchHorse
 * request: token
 * response: all seller details.
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;
var async = require('async');

exports.searchHorse = function (req, res) {
    console.log("req.body.searchParam ", req.body.searchParam)
    if (!req.body.searchParam) {

        var sellerData = [{ "name": { "fName": "Sales Paddock", "lName": "App" }, "userId": "UId8887222" },
        { "name": { "fName": "Linda", "lName": "Radigan" }, "userId": "UId5877747" },
        { "name": { "fName": "Joe", "lName": "Goodnow" }, "userId": "UId6042790" }];

        var postData = [{ name: "Sales Paddock Facts", postId: "58fd48a6a54d4f5c1b22a62f" },
        { name: "Did you know?", postId: "5947dec893cc67272789adaf" },
        { name: "Sellers Update Your Bio", postId: "5970c6f35487f420ef60f1d2" }];

        var showData = [];

        res.json({
            err: 0, response: {
                'horse': postData, "Shows": showData,
                "users": sellerData
            }
        });
        return;


    }

    var curDate = Date.now();
    var likeCn = req.body.searchParam;

    async.parallel(
        {
            horses: function (callback) {
                var cond = [{
                    $match: {
                        activeStatus: "1", values: {
                            $elemMatch:
                                {
                                    $or: [{ $and: [{ pref_id: ObjectId("57ef6d2600eba3231ed379cc") }, { options: { $regex: new RegExp("^" + likeCn, "gi") } }] },
                                    { $and: [{ pref_id: ObjectId("57efa0aa00eba37a3dd379cd") }, { options: { $regex: new RegExp("^" + likeCn, "gi") } }] },
                                    { $and: [{ pref_id: ObjectId("57f8d59100eba3676b2f80f1") }, { options: { $regex: new RegExp("^" + likeCn, "gi") } }] }]
                                }
                        }
                    }
                },
                { $limit: 3 }]
                Utility.AggregateGroup('post', cond, function (err, result) {
                    if (err) {
                        callback(err, err);
                    }
                    else if (result.length > 0) {
                        var dataToSend = [];
                        for (var i = 0; i < result.length; i++) {
                            for (var j = 0; j < result[i]['values'].length; j++) {
                                if (result[i]['values'][j]['pref_id'].toString() == "57efa0aa00eba37a3dd379cd") {
                                    dataToSend.push({ name: result[i]['values'][j]['options'][0], postId: result[i]["_id"] });
                                    break;
                                }
                            }
                        }
                        callback(null, dataToSend);
                    }
                    else {
                        callback(null, []);
                    }
                });
            },
            users: function (callback) {
                var cond1 = [
                    { "$project": { "fullName": { "$concat": ["$name.fName"," ", "$name.lName"] }, userId: 1, name: 1 } },
                    {
                        $match: {
                            $or: [{ "name.fName": { $regex: new RegExp("^" + likeCn, "gi") } },
                            { "name.lName": { $regex: new RegExp("^" + likeCn, "gi") } },
                            { "fullName": { $regex: new RegExp("^" + likeCn, "gi") } }
                            ]
                        }
                    },
                    {
                        $lookup: {
                            from: "post", localField: "userId", foreignField: "userId",
                            as: "post"
                        }
                    },
                    { $match: { "post.activeStatus": "1" } }, { $limit: 3 }]

                console.log("cond1", JSON.stringify(cond1));
                Utility.AggregateGroup('user', cond1, function (err, result) {
                    if (err) {
                        callback(err, err);
                    }
                    else if (result.length > 0) {
                        var dataToSend = [];
                        for (var i = 0; i < result.length; i++) {
                            dataToSend.push({ name: result[i]['name'], userId: result[i]["userId"] });
                        }
                        callback(null, dataToSend);
                    }
                    else {
                        callback(null, []);
                    }
                });
            },
            shows: function (callback) {
                var cond1 = { "showName": { $regex: new RegExp("^" + likeCn, "gi") } };

                var data1 = { posts: 0 };

                Utility.SelectWithPagination('shows', cond1, data1, 0, 3, function (err, result) {
                    if (err) {
                        callback(err, err);
                    }
                    else if (result.length > 0) {
                        var dataToSend = [];
                        for (var i = 0; i < result.length; i++) {
                            var sdate = '';
                            if (result[i]['startDate']) {
                                var mainDate1 = new Date(parseFloat(result[i]['startDate']));

                                var datee1 = mainDate1.getDate();
                                var month1 = mainDate1.getMonth() + 1;
                                var year1 = mainDate1.getFullYear();

                                if (datee1 < 10)
                                    datee1 = "0" + datee1;

                                if (month1 < 10)
                                    month1 = "0" + month1;

                                sdate = month1 + "/" + datee1 + "/" + year1;

                            }

                            var edate = '';
                            if (result[i]['endDate']) {
                                var mainDate = new Date(parseFloat(result[i]['endDate']));

                                var datee = mainDate.getDate();
                                var month = mainDate.getMonth() + 1;
                                var year = mainDate.getFullYear();

                                if (datee < 10)
                                    datee = "0" + datee;

                                if (month < 10)
                                    month = "0" + month;

                                edate = month + "/" + datee + "/" + year;

                            }
                            dataToSend.push({
                                showName: result[i]['showName'], showId: result[i]["_id"],
                                startDate: edate, endDate: sdate,
                                venue: result[i]['venue'], lat: result[i]['lat'],
                                lng: result[i]['lng'], image: result[i]['imgPath'],
                                Description: result[i]['Description'], productType: result[i]['productType']
                            });
                        }
                        callback(null, dataToSend);
                    }
                    else {
                        callback(null, []);
                    }
                });
            }
        },
        function (err, result1) {
            if (err) {
                res.json({ err: 1 });
                return;
            }

            res.json({
                err: 0, response: {
                    'horse': result1.horses, "Shows": result1.shows,
                    "users": result1.users
                }
            })
        }
    )
}