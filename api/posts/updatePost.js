/**
 * Created by Niranjan on 19/9/16.
 * function name: updatePost
 * request: token,userId,wishId
 * response: succuss response if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;

exports.updatePost = function(req, res)
{
    //console.log("updatePost: " + JSON.stringify(req.body));
    if(!req.body.userId)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory user id is missing"});
        return false;
    }

    if(!req.body.postId)
    {
        res.send({errCode: 1, errNum: 139, Message: "mandatory post id is missing"});
        return false;
    }

    var  values = req.body.values;

    for(var j=0;j<values.length;j++)
    {
        if(values[j].pref_id == "58ad97cdc288860c2844571d")
        {
            lat =  parseFloat(values[j]['options'][0]['lat']);
            long = parseFloat(values[j]['options'][0]['long']);
        }
        console.log("Start...");
        values[j].pref_id = ObjectId(values[j].pref_id);
        console.log("End...");
    }


    //var lat = 0.0;
    //if(req.body.lat)
    //    lat = parseFloat(req.body.lat);
    //
    //var long = 0.0;
    //if(req.body.long)
    //    long = parseFloat(req.body.long);


    var dataToInsert = {modified: Date.now(),values: values,
        location : {longitude : long,latitude : lat}};

    console.log("dataToInsert: " + JSON.stringify(dataToInsert));

    var cond = {_id: ObjectId(req.body.postId)};

    console.log("cond: " + JSON.stringify(cond));
    Utility.Update('post',cond,dataToInsert,function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result)
        {
            var conditionToGetCountOfPrefernces = {productType : "Horse"};
            Utility.Select('Preferences', conditionToGetCountOfPrefernces, function(err,countOfPreference)
            {
                if (err)
                {
                    res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
                    return false;
                }
                else if(countOfPreference.length > 0)
                {
                    var image = {};
                    for(var countOfPostDetails=0; countOfPostDetails<req.body.values.length; countOfPostDetails++)
                    {
                        console.log("in for loop: " + countOfPostDetails);
                        if(req.body.values[countOfPostDetails]['pref_id'] == '57f79d6700eba39c7b2f80f1')
                        {
                            console.log("in for loop: " + req.body.values[countOfPostDetails]['pref_id']);
                            console.log("in for loop: " + req.body.values[countOfPostDetails]['options']);
                            image['title'] = 'image';
                            image['options'] = req.body.values[countOfPostDetails]['options'];
                            break;
                        }
                    }

                    var isPostCompleted = 0;
                    if(countOfPreference.length == values.length)
                    {
                        isPostCompleted = 1;
                    }
                    res.send({errCode: 0,  Message: "you have updated your post successfully.",response:
                    {image : image,totalCount : countOfPreference.length, steps : req.body.values.length, isPostCompleted : isPostCompleted}});
                    return true;
                }
                else
                {
                    var image = {};
                    for(var countOfPostDetails=0; countOfPostDetails<req.body.values.length; countOfPostDetails++)
                    {
                        console.log("in for loop: " + countOfPostDetails);
                        if(req.body.values[countOfPostDetails]['pref_id'] == '57f79d6700eba39c7b2f80f1')
                        {
                            console.log("in for loop: " + req.body.values[countOfPostDetails]['pref_id']);
                            console.log("in for loop: " + req.body.values[countOfPostDetails]['options']);
                            image['title'] = 'image';
                            image['options'] = req.body.values[countOfPostDetails]['options'];
                            break;
                        }
                    }
                    res.send({errCode: 0,  Message: "you have updated your post successfully.",response: {image : image,
                        totalCount : 0, steps : req.body.values.length,isPostCompleted : 1}});
                    return true;
                }
            })

        }
        else
        {
            res.send({errCode: 1, errNum: 135, Message: "Currently no product is there, please try after some time."});
            return true;
        }
    });
}






