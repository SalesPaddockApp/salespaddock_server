/**
 * Created by Niranjan on 1/10/16.
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var async = require('async');

var ObjectId = require('mongodb').ObjectID;

exports.getSummary = function(req, res)
{
    if(!params.body.name)
    {
        response.send({errCode: 1, errNum: 142, Message: "Mandatory product type is missing."});
        return false;
    }
    var cond = {productType: params.body.name};

    Utility.Select('Preferences',cond,function(err,result)
    {
        if(err)
        {
            console.log(err);
            response.send({errCode: 1, errNum: 104, Message: "unknown error occurred."});
        }
        else if(result.length > 0)
        {
            var dataToSend = [];
            for(var i=0; i<result.length; i++)
            {
                dataToSend.push({title: result[i]['preferenceTitle'],type: result[i]['typeOfPreference'],
                    prirority: result[i]['prirority'],mandatory: result[i]['mandatory'],onCreateScreen : result[i]['onCreateScreen'],
                    unit: result[i]['unit'],values: result[i]['optionsValue'],id: result[i]['_id']});
            }
            response.send({errCode: 0, Message: "Data sent successfully.", response:{data: dataToSend}});
        }
        else
        {
            response.send({errCode: 1, errNum: 105, Message: "Unknown error occurred"});
            return false;
        }
    })
}

