
module.exports = function(app,express)
{
    var router = express.Router();
    var Utility = require('../UtilityFunc');
    var jsonwebtoken = require('jsonwebtoken');

    var conf = require('../conf');
    var secretKey = conf.secretKey;

    /**
     * Set the middleware 
     * next routes accessible only for authenticated users
     * @Author : Niranjan Kumar
     */
    //app.use(function (req, res, next) {
    //   // console.log("Someone came to our app!");
    //
    //    var token = req.body.token || req.params.token || req.query.token || req.headers['x-access-token'];
    //
    //    if (token) {
    //        jsonwebtoken.verify(token, secretKey, function (err, decoded) {
    //            if (err) {
    //                res.status(403).send({ errCode: 1, errNum: 133, Message: "failed to authenticate" });
    //            } else {
    //                req.decoded = decoded;
    //                next();
    //            }
    //        });
    //    } else {
    //        res.status(403).send({ errCode: 1, errNum: 133,Message: "No token provided" });
    //    }
    //});

    router.post('/getPreference',function(params,response)
    {
        console.log("getPreference:   ",JSON.stringify(params.body));

        if(!params.body.name)
        {
            response.send({errCode: 1, errNum: 142, Message: "Mandatory product type is missing."});
            return false;
        }
        var cond = {productType: params.body.name};
        
        Utility.SelectWithSorting('Preferences',cond,{prirority: 1},function(err,result)
        {
            if(err)
            {
                console.log(err);
                response.send({errCode: 1, errNum: 104, Message: "unknown error occurred."});
            }
            else if(result.length > 0)
            {
                var dataToSend = [];
                var dataToSend1 = [];
                var idImg = '';
                for(var i=0; i<result.length; i++)
                {
                    if(result[i]['onCreateScreen'])
                    {
                        dataToSend1.push({title: result[i]['preferenceTitle'],type: result[i]['typeOfPreference'],
                            prirority: result[i]['prirority'],mandatory: result[i]['mandatory'],onCreateScreen : result[i]['onCreateScreen'],
                            unit: result[i]['unit'],values: result[i]['optionsValue'].sort(),id: result[i]['_id']});
                    }

                    if(result[i]['typeOfPreference'] != 6 && result[i]['preferenceTitle'] != "Price Details" && result[i]['preferenceTitle'] != "Secondary Breed"
                        && result[i]['preferenceTitle'] != "Breed" && result[i]['preferenceTitle'] != "Discipline")
                    {
                        dataToSend.push({title: result[i]['preferenceTitle'],type: result[i]['typeOfPreference'],
                            prirority: result[i]['prirority'],mandatory: result[i]['mandatory'],onCreateScreen : result[i]['onCreateScreen'],
                            unit: result[i]['unit'],values: result[i]['optionsValue'],id: result[i]['_id']});
                    }

                    if(result[i]['typeOfPreference'] == 6 && result[i]['preferenceTitle'] != "Price Details" && result[i]['preferenceTitle'] != "Secondary Breed"
                        && result[i]['preferenceTitle'] != "Breed" && result[i]['preferenceTitle'] != "Discipline")
                    {
                        idImg = result[i]['_id'];
                    }

                    if(result[i]['preferenceTitle'] == "Price Details")
                    {
                        var values = [];
                        values.push("Contact for price");

                        var values1 = result[i]['optionsValue'];
                        //values1.sort()
                        for(var k=0; k<values1.length; k++)
                        {
                            values.push(values1[k]);
                        }
                        dataToSend.push({title: result[i]['preferenceTitle'],type: result[i]['typeOfPreference'],
                            prirority: result[i]['prirority'],mandatory: result[i]['mandatory'],onCreateScreen : result[i]['onCreateScreen'],
                            unit: result[i]['unit'],values: values,id: result[i]['_id']});
                    }

                    if(result[i]['preferenceTitle'] == "Secondary Breed")
                    {
                        var values2 = [];
                        values2.push("Not applicable");

                        var values3 = result[i]['optionsValue'];
                        values3.sort()
                        for(var k=0; k<values3.length; k++)
                        {
                            if(values3[k] == "Not applicable")
                                continue;

                            values2.push(values3[k]);
                        }
                        dataToSend.push({title: result[i]['preferenceTitle'],type: result[i]['typeOfPreference'],
                            prirority: result[i]['prirority'],mandatory: result[i]['mandatory'],onCreateScreen : result[i]['onCreateScreen'],
                            unit: result[i]['unit'],values: values2,id: result[i]['_id']});
                    }

                    if(result[i]['preferenceTitle'] == "Breed")
                    {
                        var values4 = [];

                        var values5 = result[i]['optionsValue'];
                        values5.sort()
                        for(var k=0; k<values5.length; k++)
                        {
                            values4.push(values5[k]);
                        }
                        dataToSend.push({title: result[i]['preferenceTitle'],type: result[i]['typeOfPreference'],
                            prirority: result[i]['prirority'],mandatory: result[i]['mandatory'],onCreateScreen : result[i]['onCreateScreen'],
                            unit: result[i]['unit'],values: values4,id: result[i]['_id']});
                    }

                    if(result[i]['preferenceTitle'] == "Discipline")
                    {
                        var values6 = [];

                        var values7 = result[i]['optionsValue'];
                        values7.sort()
                        for(var k=0; k<values7.length; k++)
                        {
                            values6.push(values7[k]);
                        }
                        dataToSend.push({title: result[i]['preferenceTitle'],type: result[i]['typeOfPreference'],
                            prirority: result[i]['prirority'],mandatory: result[i]['mandatory'],onCreateScreen : result[i]['onCreateScreen'],
                            unit: result[i]['unit'],values: values6,id: result[i]['_id']});
                    }

                }

                var imageData = {title: "Image",type: "6",id: idImg,
                    prirority: 12,mandatory: 1,onCreateScreen : 0,
                    unit: 0,values: []};

                 response.send({errCode: 0, Message: "Data sent successfully.", response:{onCreate: dataToSend1,data: dataToSend,image: imageData}});
            }
            else
            {
                response.send({errCode: 1, errNum: 105, Message: "Unknown error occurred"});
                return false;
            }
        })

        
    })

    // console.log(secretKey.secretKey);
    return router;

}