
module.exports = function(app,express)
{
    var router = express.Router();
    var Utility = require('../UtilityFunc');
    var jsonwebtoken = require('jsonwebtoken');

    var conf = require('../conf');
    var secretKey = conf.secretKey;

    /**
     * Set the middleware 
     * next routes accessible only for authenticated users
     * @Author : Niranjan Kumar
     */
    app.use(function (req, res, next) {
       // console.log("Someone came to our app!");

        var token = req.body.token || req.params.token || req.query.token || req.headers['x-access-token'];

        if (token) {
            jsonwebtoken.verify(token, secretKey, function (err, decoded) {
                if (err) {
                    res.status(403).send({ errCode: 1, errNum: 133, Message: "failed to authenticate" });
                } else {
                    req.decoded = decoded;
                    next();
                }
            });
        } else {
            res.status(403).send({ errCode: 1, errNum: 133,Message: "No token provided" });
        }
    });

    router.post('/getPreference',function(params,response)
    {
        console.log(JSON.stringify(params.body));

        if(!params.body.name)
        {
            response.send({errCode: 1, errNum: 142, Message: "Mandatory product type is missing."});
            return false;
        }
        var cond = {productType: params.body.name};
        
        Utility.Select('Preferences',cond,function(err,result)
        {
            if(err)
            {
                console.log(err);
                response.send({errCode: 1, errNum: 104, Message: "unknown error occurred."});
            }
            else if(result.length > 0)
            {
                var dataToSend = [];
                var dataToSend1 = [];
                for(var i=0; i<result.length; i++)
                {
                    if(result[i]['onCreateScreen'])
                    {
                        dataToSend1.push({title: result[i]['preferenceTitle'],type: result[i]['typeOfPreference'],
                            prirority: result[i]['prirority'],mandatory: result[i]['mandatory'],onCreateScreen : result[i]['onCreateScreen'],
                            unit: result[i]['unit'],values: result[i]['optionsValue'],id: result[i]['_id']});
                    }
                    if(result[i]['typeOfPreference'] != 6)
                    {
                        dataToSend.push({title: result[i]['preferenceTitle'],type: result[i]['typeOfPreference'],
                            prirority: result[i]['prirority'],mandatory: result[i]['mandatory'],onCreateScreen : result[i]['onCreateScreen'],
                            unit: result[i]['unit'],values: result[i]['optionsValue'],id: result[i]['_id']});
                    }
                }
                var imageData = {title: "Image",type: "6",
                    prirority: 12,mandatory: 1,onCreateScreen : 0,
                    unit: 0,values: []};

                console.log("response: " + JSON.stringify({errCode: 0, Message: "Data sent successfully.", response:{onCreate: dataToSend1,data: dataToSend,image: imageData}}));

                 response.send({errCode: 0, Message: "Data sent successfully.", response:{onCreate: dataToSend1,data: dataToSend,image: imageData}});
            }
            else
            {
                response.send({errCode: 1, errNum: 105, Message: "Unknown error occurred"});
                return false;
            }
        })

        
    })

    // console.log(secretKey.secretKey);
    return router;

}