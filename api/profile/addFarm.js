/**
 * Created by embed on 20/2/17.
 * function name: getAllFarms
 * request: token,userId, farm data
 * response: all data of specific user but only related data if  no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var async = require('async');
const limitNumber = 10;

exports.addOrUpdateFarm = function(req, res)
{
    console.log("addFarm: " + JSON.stringify(req.body));
    if(!req.body.userId)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory user id is missing"});
        return false;
    }

    if(!req.body.lat)
    {
        res.send({errCode: 1, errNum: 211, Message: "mandatory latitude is missing"});
        return false;
    }

    if(!req.body.long)
    {
        res.send({errCode: 1, errNum: 212, Message: "mandatory longitude is missing"});
        return false;
    }

    //if(!req.body.street)
    //{
    //    res.send({errCode: 1, errNum: 211, Message: "mandatory street is missing"});
    //    return false;
    //}

    //if(!req.body.apt)
    //{
    //    res.send({errCode: 1, errNum: 213, Message: "mandatory apt is missing"});
    //    return false;
    //}

    if(!req.body.city)
    {
        res.send({errCode: 1, errNum: 215, Message: "mandatory city: is missing"});
        return false;
    }

    if(!req.body.state)
    {
        res.send({errCode: 1, errNum: 216, Message: "mandatory state is missing"});
        return false;
    }

    if(!req.body.zipCode)
    {
        res.send({errCode: 1, errNum: 217, Message: "mandatory zipCode is missing"});
        return false;
    }

    if(!req.body.country)
    {
        res.send({errCode: 1, errNum: 218, Message: "mandatory country is missing"});
        return false;
    }

    if(!req.body.farmName)
    {
        res.send({errCode: 1, errNum: 220, Message: "mandatory farmName is missing"});
        return false;
    }

    if(!req.body.farmID)
    {
        res.send({errCode: 1, errNum: 221, Message: "mandatory farmID is missing"});
        return false;
    }


    if(req.body.isUpdated == 1)
    {
        var dataToPush = {"farmDetail.$.lat" :req.body.lat, "farmDetail.$.long" :req.body.long,
            "farmDetail.$.street" : req.body.street, "farmDetail.$.apt" : req.body.apt,
            "farmDetail.$.city" : req.body.city,"farmDetail.$.state" : req.body.state,
            "farmDetail.$.zipCode" : req.body.zipCode,"farmDetail.$.country" : req.body.country,
            "farmDetail.$.farmName" : req.body.farmName,"farmDetail.$.farmID" : req.body.farmID};

        var cond1 = {$and : [{userId: req.body.userId},{farmDetail : {$elemMatch : {farmID : req.body.farmID}}}]};

        console.log("cond1",JSON.stringify(cond1));

        Utility.Update('user',cond1,dataToPush,function (err, isFarmAdded)
        {
            if (err)
            {
                res.send({errCode: 1, errNum: 211,  Message: "some error occurred, please try again."});
                return false;
            }
            else if(isFarmAdded)
            {
                res.send({errCode : 0, response : {}, Message : "farm added successfully."});
                return true;
            }
            else
            {
                res.send({errCode: 1, errNum: 211,  Message: "some error occurred, please try again."});
                return true;
            }
        });
    }
    else
    {
        var dataToPush = {farmDetail : {lat:req.body.lat,long:req.body.long,street:req.body.street, apt: req.body.apt,
        city: req.body.city,state:req.body.state,zipCode: req.body.zipCode,country:req.body.country,
        farmName:req.body.farmName,farmID:req.body.farmID}};

        var cond2 = {userId: req.body.userId};
        Utility.AddIntoArray('user',cond2,dataToPush,function (err, isFarmAdded)
        {
            if (err)
            {
                res.send({errCode: 1, errNum: 211,  Message: "some error occurred, please try again."});
                return false;
            }
            else if(isFarmAdded)
            {
                res.send({errCode : 0, response : {}, Message : "farm added successfully."});
                return true;
            }
            else
            {
                res.send({errCode: 1, errNum: 211,  Message: "some error occurred, please try again."});
                return true;
            }
        });
    }



}


