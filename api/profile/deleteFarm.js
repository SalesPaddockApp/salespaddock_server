/**
 * Created by embed on 20/2/17.
 * function name: getAllFarms
 * request: token,userId, farm data
 * response: all data of specific user but only related data if  no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var async = require('async');
const limitNumber = 10;

exports.deleteFarm = function(req, res)
{
    console.log("addFarm: " + JSON.stringify(req.body));
    if(!req.body.userId)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory user id is missing"});
        return false;
    }

    if(!req.body.farmID)
    {
        res.send({errCode: 1, errNum: 221, Message: "mandatory farmID is missing"});
        return false;
    }


    var dataToPull = {farmDetail : {farmID:req.body.farmID}};

    var cond2 = {userId: req.body.userId};
    Utility.UpdateField('user',cond2,dataToPull,function (err, isFarmdeleted)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 211,  Message: "some error occurred, please try again."});
            return false;
        }
        else if(isFarmdeleted)
        {
            res.send({errCode : 0, response : {}, Message : "farm deleted successfully."});
            return true;
        }
        else
        {
            res.send({errCode: 1, errNum: 211,  Message: "some error occurred, please try again."});
            return true;
        }
    });



}


