
module.exports = function(app,express)
{
    var router = express.Router();
    var Utility = require('../UtilityFunc');
    var jsonwebtoken = require('jsonwebtoken');
    var updateProfile = require('./updateProfile');
    var sendMessage = require('../message/sendMessage');

    var conf = require('../conf');
    var secretKey = conf.secretKey;

    /**
     * Set the middleware 
     * next routes accessible only for authenticated users
     * @Author : Niranjan Kumar
     */
    app.use(function (req, res, next) {
       // console.log("Someone came to our app!");

        var token = req.body.token || req.params.token || req.query.token || req.headers['x-access-token'];

        if (token) {
            jsonwebtoken.verify(token, secretKey, function (err, decoded) {
                if (err) {
                    res.status(403).send({ errCode: 1, errNum: 133, Message: "failed to authenticate" });
                } else {
                    req.decoded = decoded;
                    next();
                }
            });
        } else {
            res.status(403).send({ errCode: 1, errNum: 133,Message: "No token provided" });
        }
    });

    router.post('/editProfile',function(params,response)
    {
        //console.log("editProfile:  " + JSON.stringify(params.body));
        var dataToUpdate = {};
        if(!params.body.userId)
        {
            response.send({errCode: 1, errNum: 132, Message: "mandatory user id is missing"});
            return false;
        }   
        
        if(params.body.dob)
        {
            dataToUpdate['dob'] = new Date(params.body.dob).getTime();//mm/dd/yyyy
        } 
        
        if(params.body.email && params.body.wEmail)
        {
            dataToUpdate['email'] =
            {
                primaryEmail: params.body.email,
                isPrimaryVerified:0,
                workEmail: params.body.wEmail,
                isWorkVerified:0
            };
        }
        else if(params.body.email)
        {
            dataToUpdate['email'] = {primaryEmail: params.body.email,isPrimaryVerified:0};
        }
        else if(params.body.wEmail)
        {
            dataToUpdate['email'] = {workEmail: params.body.wEmail,isWorkVerified:0};
        }

        if(params.body.fName)
        {
            dataToUpdate['name'] = {fName: params.body.fName,lName: params.body.lName};
        }  

        if(params.body.gender)
        {
            dataToUpdate['gender'] = params.body.gender;
        }  

        if(params.body.about)
        {
            dataToUpdate['about'] = params.body.about;
        }  

        if(params.body.language)
        {
            dataToUpdate['language'] = params.body.language;
        } 

        if(params.body.school)
        {
            dataToUpdate['school'] = params.body.school;
        }  

        if(params.body.location)
        {
            dataToUpdate['location'] = params.body.location;
        }

        //
        //if(params.body.wEmail)
        //{
        //    dataToUpdate['email'] = {workEmail: params.body.wEmail,isWorkVerified:0};
        //}
        
        if(params.body.company)
        {
            dataToUpdate['company'] = params.body.company;
        }

        if(params.body.address)
        {
            dataToUpdate['address'] = params.body.address;
        }

        if(params.body.farmName)
        {
            dataToUpdate['farmName'] = params.body.farmName;
        }

        if(params.body.phone)
        {
            dataToUpdate['phone'] = params.body.phone;

            var randomNumber = Math.floor(Math.random()*9000)+1000;
            var messageForOTP = 'Your registration code on Salespaddock is: ' + randomNumber;
            sendMessage.sendMessage(params.body.phone,messageForOTP,params.body.userId,randomNumber);
        }

        if(params.body.profilePic)
        {
            dataToUpdate['profilePic'] = params.body.profilePic;
        }

        var userId = params.body.userId;
        var cond = {userId : userId};

        console.log("cond: " + JSON.stringify(cond));
        console.log("dataToUpdate: " + JSON.stringify(dataToUpdate));

        updateProfile.updateData(cond,dataToUpdate,userId,function(err,result)
        {
            if(err)
            {
                console.log(err);
                response.send({errCode: 1, errNum: 104, Message: "unknown error occurred."});
            }
            else
            {
                //console.log("edit profile result:  " + JSON.stringify(result));
                response.send(result);
            }
        })

        
    })

    // console.log(secretKey.secretKey);
    return router;

}