/**
 * Created by Niranjan on 20/2/17.
 * function name: getAllFarms
 * request: token,userId, farm data
 * response: all data of specific user but only related data if  no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var async = require('async');

exports.getAllFarms = function(req, res)
{
    console.log("getAllFarms: " + JSON.stringify(req.body));
    if(!req.body.userId)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory user id is missing"});
        return false;
    }

    var condToGetUserDetails = {userId: req.body.userId};
    var dataToProject = {farmDetail : 1};
    Utility.SelectMatch('user',condToGetUserDetails,dataToProject,function (err, farmDetails)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 211,  Message: "some error occurred, please try again."});
            return false;
        }
        else if(farmDetails)
        {
            if(!farmDetails.farmDetail)
            {
                res.send({errCode: 1, errNum: 211,  Message: "no farm found."});
                return;
            }

            var dataToSend = [];

            for(var i=0; i<farmDetails.farmDetail.length; i++)
            {
                var street = "";
                if(farmDetails.farmDetail[i].street)
                    street = farmDetails.farmDetail[i].street;

                var apt = "";
                if(farmDetails.farmDetail[i].apt)
                    apt = farmDetails.farmDetail[i].apt;

                dataToSend.push({lat : farmDetails.farmDetail[i].lat,long : farmDetails.farmDetail[i].long,
                    street : street, apt : apt, city : farmDetails.farmDetail[i].city,state : farmDetails.farmDetail[i].state,
                    zipCode : farmDetails.farmDetail[i].zipCode,country : farmDetails.farmDetail[i].country,farmName : farmDetails.farmDetail[i].farmName,
                    farmDetail : farmDetails.farmDetail[i].farmDetail,farmID : farmDetails.farmDetail[i].farmID});
            }
            res.send({errCode : 0, response : dataToSend, Message : "data sent successfully."})
        }
        else
        {
            res.send({errCode: 1, errNum: 211,  Message: "no farm found."});
        }
    });

}



