/**
 * Created by Niranjan on 8/10/16.
 */

var apn = require('apn');
var path = require("path");
var sendMessage = require('../message/sendMessage');

exports.sendOTP = function(args, callback)
{

    console.log(JSON.stringify(args.body));


    if(!args.body.userId)
    {
        response.send({errCode: 1, errNum: 132, Message: "mandatory user id is missing"});
        return false;
    }

    if(!args.body.phone)
    {
        response.send({errCode: 1, errNum: 132, Message: "mandatory phone number is missing"});
        return false;
    }



    var randomNumber = Math.floor(Math.random()*9000)+1000;
    var messageForOTP = 'Your registration code on Salespaddock is: ' + randomNumber;
    sendMessage.sendMessage(args.body.phone,messageForOTP,args.body.userId,randomNumber);

    callback.json({errCode : 0, Message : "An otp has been sent to your number, please verify your number."})
}