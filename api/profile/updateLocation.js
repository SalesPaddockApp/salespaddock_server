/**
 * Created by Niranjan on 19/9/16.
 * function name: updateLocaton
 * request: token,userId,location
 * response: succuss response if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;

exports.updateLocation = function(req, res)
{
    console.log("updateLocaton: " + JSON.stringify(req.body));
    if(!req.body.userId)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory user id is missing"});
        return false;
    }

    if(!req.body.lat)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory latitude is missing"});
        return false;
    }

    if(!req.body.long)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory longitude is missing"});
        return false;
    }

    var cond = {userId: req.body.userId};
    console.log("cond: " + JSON.stringify(cond));
    if(req.body.lat == 0)
    {
        Utility.SelectOne('user',cond,function(err,counts)
        {
            if (err)
            {
                res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
                return false;
            }
            else if(counts)
            {
                if(counts.deviceId != req.body.deviceId)
                {
                   res.status(403).send({ errCode: 1, errNum: 133, Message: "You have logged in with different device." });
                   return;
                }
                var totalCount = 0;

                if(counts.totalCount)
                    totalCount = counts.totalCount;

                console.log("totalCount: " + totalCount);
                console.log("totalCount: " + totalCount);
                console.log("totalCount: " + totalCount);
                res.send({errCode: 0, Message: "location updated successfully.",totalCount : totalCount});
                return true;

            }
            else
            {
                console.log("totalCount: " + 0);

                res.send({errCode: 1, errNum: 135, Message: "no user found."});
                return true;
            }
        });
    }
    else
    {
        var dataToInsert = {location1 : {longitude: parseFloat(req.body.long),latitude: parseFloat(req.body.lat)}};
        console.log("dataToInsert: " + JSON.stringify(dataToInsert));

        Utility.Update('user',cond,dataToInsert,function (err, result)
        {
            if (err)
            {
                res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
                return false;
            }
            else if(result)
            {

                Utility.SelectOne('user',cond,function(err,counts)
                {
                    if (err)
                    {
                        res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
                        return false;
                    }
                    else if(counts)
                    {
                        if(counts.deviceId != req.body.deviceId)
                        {
                           res.status(403).send({ errCode: 1, errNum: 133, Message: "You have logged in with different device." });
                           return;
                        }
                        var totalCount = 0;

                        if(counts.totalCount)
                            totalCount = counts.totalCount;

                        console.log("totalCount: " + totalCount);
                        console.log("totalCount: " + totalCount);
                        console.log("totalCount: " + totalCount);
                        res.send({errCode: 0, Message: "location updated successfully.",totalCount : totalCount});
                        return true;

                    }
                    else
                    {
                        console.log("totalCount: " + 0);

                        res.send({errCode: 1, errNum: 135, Message: "no user found."});
                        return true;
                    }
                });
            }
            else
            {
                res.send({errCode: 1, errNum: 135, Message: "no user found."});
                return true;
            }
        });
    }
}






