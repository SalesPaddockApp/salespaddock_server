/**
 * Created by Niranjan on 8/10/16.
 */
var exports = module.exports = {};
var notification = require('../notification/sendNotification');
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;
var updateProfile = require('./updateProfile');

exports.validateNumber = function(req,res)
{
    //console.log("validateNumber: " +JSON.stringify(req.body));
    if(!req.body.userId)
    {
        res.send({errCode: 1, errNum: 108, Message: "mandatory user id is missing"});
        return false;
    }

    if(!req.body.phone)
    {
        res.send({errCode: 1, errNum: 108, Message: "mandatory phone number is missing"});
        return false;
    }

    if(!req.body.otp)
    {
        res.send({errCode: 1, errNum: 108, Message: "mandatory otp is missing"});
        return false;
    }

    var cond= {userId : req.body.userId};

    console.log("cond: " +JSON.stringify(cond));
    Utility.SelectOne('user',cond,function(err,result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result)
        {
            if(result.otp == req.body.otp)
            {
                var dataToUpdate = {phone : req.body.phone};
                updateProfile.updateData(cond,dataToUpdate,req.body.userId,function(err,result)
                {
                    if(err)
                    {
                        console.log(err);
                        res.json({errCode: 1, errNum: 104, Message: "unknown error occurred."});
                    }
                    else
                    {
                        //response.send(result);
                        res.json({errCode: 0, Message: "Your number is successfully verified."});
                        return true;
                    }
                })

            }
            else
            {
                res.send({errCode: 1, errNum: 155, Message: "wrong verification code."});
                return true;
            }

        }
        else
        {
            res.send({errCode: 1, errNum: 155, Message: "Invalid user id, please try again."});
            return true;
        }
    })
};