/**
 * Created by Niranjan on 19/9/16.
 * function name: viewProfile
 * request: token,userId
 * response: all data of specific user but only related data if  no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var async = require('async');
const limitNumber = 10;

exports.viewProfile = function(req, res)
{
    console.log("viewProfile: " + JSON.stringify(req.body));
    if(!req.body.userId)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory email is missing"});
        return false;
    }

    if(!req.body.isMyProfile && req.body.isMyProfile != 0)
    {
        res.send({errCode: 1, errNum: 213, Message: "mandatory profile type is missing"});
        return false;
    }


    async.parallel(
    {
        posts : function(callback)
        {
            if(req.body.isMyProfile == 1)
            {
                callback(null,[]);
            }
            else
            {
                var conitionToGetPosts = {"productType" : "Horse","activeStatus" : "1",userId: req.body.userId};
                Utility.Select('post',conitionToGetPosts,function(err,postData)
                {
                    if(err)
                    {
                        console.log("some mongo error occurred: " + JSON.stringify(err));
                        callback(err,[]);
                    }
                    else if(postData.length > 0)
                    {
                        callback(null,postData);
                    }
                    else
                    {
                        console.log("no data found");
                        callback(null,[]);
                    }
                })
            }
        },
        preference : function(callback)
        {
            if(req.body.isMyProfile == 1)
            {
                callback(null,[]);
            }
            else
            {
                var conitionToGetPreference = {"productType" : "Horse"};
                Utility.Select('Preferences',conitionToGetPreference,function(err,preferenceResult)
                {
                    if(err)
                    {
                        console.log("some mongo error occurred: " + JSON.stringify(err));
                        callback(err,[]);
                    }
                    else if(preferenceResult.length > 0)
                    {
                        callback(null,preferenceResult);
                    }
                    else
                    {
                        console.log("no data found");
                        callback(null,[]);
                    }
                })
            }
        },
        userDetail : function(callback)
        {
            var condToGetUserDetails = {userId: req.body.userId};

            Utility.SelectOne('user',condToGetUserDetails,function (err, userDetails)
            {
                if (err)
                {
                    callback(err,err);
                }
                else if(userDetails)
                {
                    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June",
                        "July", "Aug", "Sep", "Oct", "Nov", "Dec"
                    ];
                    var lName = '';
                    if(userDetails.name.lName)
                        lName = userDetails.name.lName;

                    var dob = '';
                    if(userDetails.dob)
                    {
                        var mainDate = new Date(userDetails.dob);

                        var datee = mainDate.getDate();
                        var month = mainDate.getMonth() + 1;
                        var year = mainDate.getFullYear();

                        if(datee < 10)
                            datee = "0" + datee;

                        if(month < 10)
                            month = "0" + month;

                        dob = month + "/" + datee + "/" + year;

                    }

                    var memberSince = '';
                    if(userDetails.memberSince)
                    {
                        var mainDate1 = new Date(userDetails.memberSince);

                        memberSince = monthNames[mainDate1.getMonth()] + " " + mainDate1.getFullYear();

                    }

                    var profilePic = '';
                    if(userDetails.profilePic)
                        profilePic = userDetails.profilePic;

                    var about = '';
                    if(userDetails.about)
                        about = userDetails.about;

                    var address = '';
                    if(userDetails.address)
                        address = userDetails.address;

                    var gender = '';
                    if(userDetails.gender)
                        gender = userDetails.gender.toString();

                    var dataToSend = {fName: userDetails.name.fName,lName: lName,memberSince: memberSince,
                        dob: dob, email: userDetails.email.primaryEmail,profilePic: profilePic,phone: userDetails.phone,
                        gender: gender,about : about,address : address,location : userDetails.location,
                        farmName : userDetails.farmName,company : userDetails.company,school : userDetails.school,wEmail : userDetails.email.workEmail};

                   callback(null,dataToSend);
                }
                else
                {
                    callback(null,[]);
                }
            });
        }
    },
    function(err,userPostsAndDetails)
    {
        if(err)
        {
            console.log("some error occurred while getting user post detail. " + JSON.stringify(err));
            res.send({errCode: 1, errNum: 211,  Message: "some error occurred, please try again."});
        }
        else if(userPostsAndDetails.posts.length > 0 && userPostsAndDetails.preference.length > 0)
        {
            var dataToSends = [];
            for(var postCount=0; postCount<userPostsAndDetails.posts.length; postCount++)
            {
                console.log("postCount:  " + postCount);
                var dataToSend = [];
                var name = "";
                var image = {};
                //var priceDetails = {};
                var priceDetails = "";
                var breed = "";
                var imageId = "";
                for(var preferenceCount=0; preferenceCount<userPostsAndDetails.preference.length; preferenceCount++)
                {
                    for(var postValuesCount=0; postValuesCount<userPostsAndDetails.posts[postCount]['values'].length; postValuesCount++)
                    {
                        if(userPostsAndDetails.preference[preferenceCount]['_id'].toString() == userPostsAndDetails.posts[postCount]['values'][postValuesCount]['pref_id'].toString())
                        {
                            if(userPostsAndDetails.preference[preferenceCount]['preferenceTitle'] == 'Horse Name')
                            {
                                name = userPostsAndDetails.posts[postCount]['values'][postValuesCount]['options'][0]
                                console.log("name:  "+ name);
                            }

                            if(userPostsAndDetails.preference[preferenceCount]['preferenceTitle'] == 'Price Details')
                            {
                                //priceDetails['title'] = userPostsAndDetails.preference[preferenceCount]['preferenceTitle'];
                                priceDetails =  userPostsAndDetails.posts[postCount]['values'][postValuesCount]['options'];
                            }

                            if(userPostsAndDetails.preference[preferenceCount]['preferenceTitle'] == 'Breed')
                            {
                                breed =  userPostsAndDetails.posts[postCount]['values'][postValuesCount]['options'];
                            }

                            if(userPostsAndDetails.preference[preferenceCount]['preferenceTitle'] == 'Image')
                            {
                                image['title'] = userPostsAndDetails.preference[preferenceCount]['preferenceTitle'];
                                image['options'] =  userPostsAndDetails.posts[postCount]['values'][postValuesCount]['options'];
                            }
                            else
                            {
                                dataToSend.push({title: userPostsAndDetails.preference[preferenceCount]['preferenceTitle'],
                                    options : userPostsAndDetails.posts[postCount]['values'][postValuesCount]['options']
                                });
                            }
                            break;
                        }
                    }
                }
                dataToSends.push({name: name,breed : breed,data:dataToSend,image: image,priceDetails: priceDetails,
                    postId: userPostsAndDetails.posts[postCount]['_id']})
            }

            //console.log("view profile result:  " + JSON.stringify({err: 0,userPostDetails : dataToSends,userProfile: userPostsAndDetails.userDetail}));

            res.json({errCode: 0,userPostDetails : dataToSends,userProfile: userPostsAndDetails.userDetail})
        }
        else
        {
            //console.log("view profile data: " + JSON.stringify({err: 0,userProfile: userPostsAndDetails.userDetail}));
            res.json({errCode: 0,userProfile: userPostsAndDetails.userDetail})
        }
    })


}



