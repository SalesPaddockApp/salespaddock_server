/**
 * Created by Niranjan on 29/12/16.
 */

var exports = module.exports = {};
var Utility = require('../UtilityFunc');
const limitNumber = 10;

var async = require('async');

var ObjectId = require('mongodb').ObjectID;

exports.viewProfileForChat = function(req, res)
{
    console.log("viewProfileForChat: " + JSON.stringify(req.body));
    if(!req.body.userId  && req.body.userId != 0)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory email is missing"});
        return false;
    }

    if(!req.body.postId && req.body.postId != 0)
    {
        res.send({errCode: 1, errNum: 214, Message: "mandatory post id is missing"});
        return false;
    }
    async.parallel(
    {
        postDetail : function(callback)
        {
            async.parallel(
                {
                    post : function(callback1)
                    {
                        var conditionToGetPost = {_id : ObjectId(req.body.postId)};
                        Utility.SelectOne('post',conditionToGetPost,function(err,postDetail)
                        {
                            if(err)
                            {
                                console.log("some mongo error occurred:  " + JSON.stringify(err));
                                callback1(err,[]);
                            }
                            else if(postDetail)
                            {
                                callback1(null,postDetail);
                            }
                            else
                            {
                                callback1(null,[]);
                            }
                        });
                    },
                    preference : function (callback1)
                    {
                        var conditionToGetPreference = {"productType" : "Horse"};
                        Utility.Select('Preferences',conditionToGetPreference,function(err,preferenceDetail)
                        {
                            if(err)
                            {
                                console.log("some mongo error occurred:  " + JSON.stringify(err));
                                callback1(err,[]);
                            }
                            else if(preferenceDetail.length > 0)
                            {
                                callback1(null,preferenceDetail);
                            }
                            else
                            {
                                callback1(null,[]);
                            }
                        })
                    }
                },
            function(err,postAndPreferenceDetail)
            {
                if(err)
                {
                    callback(err,err);
                }
                else if(postAndPreferenceDetail.post && postAndPreferenceDetail.preference.length > 0)
                {
                    var dataToSend = [];
                    var image = {};
                    var priceDetails = {};
                    var description = {};
                    var name = '';
                    for(var preferenceCount=0; preferenceCount<postAndPreferenceDetail.preference.length; preferenceCount++)
                    {
                        if(postAndPreferenceDetail.post.values)
                            for(var postValueCount=0; postValueCount<postAndPreferenceDetail.post.values.length; postValueCount++)
                            {
                                if(postAndPreferenceDetail.preference[preferenceCount]['_id'].toString() == postAndPreferenceDetail.post.values[postValueCount]['pref_id'].toString())
                                {

                                    if(postAndPreferenceDetail.preference[preferenceCount]['preferenceTitle'] == 'Image')
                                    {
                                        image['title'] = postAndPreferenceDetail.preference[preferenceCount]['preferenceTitle'];
                                        image['options'] =  postAndPreferenceDetail.post.values[postValueCount]['options'];
                                        break;
                                    }
                                    else if(postAndPreferenceDetail.preference[preferenceCount]['preferenceTitle'] == 'Horse Name')
                                    {
                                        name = postAndPreferenceDetail.post.values[postValueCount]['options'][0];
                                        break;
                                    }
                                    else if(postAndPreferenceDetail.preference[preferenceCount]['preferenceTitle'] == 'Price Details')
                                    {
                                        priceDetails['title'] = postAndPreferenceDetail.preference[preferenceCount]['preferenceTitle'];
                                        priceDetails['options'] = postAndPreferenceDetail.post.values[postValueCount]['options'][0];
                                        //priceDetails['leasePrice'] = postAndPreferenceDetail.post.values[postValueCount]['options'][1];
                                        break;
                                    }
                                    else if(postAndPreferenceDetail.preference[preferenceCount]['preferenceTitle'] == 'Description')
                                    {
                                        description['title'] = postAndPreferenceDetail.preference[preferenceCount]['preferenceTitle'];
                                        description['options'] = postAndPreferenceDetail.post.values[postValueCount]['options'][0];
                                        //priceDetails['leasePrice'] = postAndPreferenceDetail.post.values[postValueCount]['options'][1];
                                        break;
                                    }
                                    else
                                    {
                                        dataToSend.push({title: postAndPreferenceDetail.preference[preferenceCount]['preferenceTitle'],selected : postAndPreferenceDetail.post.values[postValueCount]['options']});
                                        break;
                                    }
                                }

                            }
                    }
                    callback(null,{dataToSend : dataToSend, image: image, name : name,priceDetails : priceDetails,description : description});
                }
                else
                {
                    callback(null,[]);
                }
            })
        },
        userDetail : function(callback)
        {
            var condToGetUserDetails = {userId: req.body.userId};

            Utility.SelectOne('user',condToGetUserDetails,function (err, userDetails)
            {
                if (err)
                {
                    callback(err,err);
                }
                else if(userDetails)
                {
                    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June",
                        "July", "Aug", "Sep", "Oct", "Nov", "Dec"
                    ];
                    var lName = '';
                    if(userDetails.name.lName)
                        lName = userDetails.name.lName;

                    var dob = '';
                    if(userDetails.dob)
                    {
                        var mainDate = new Date(userDetails.dob);

                        var datee = mainDate.getDate();
                        var month = mainDate.getMonth() + 1;
                        var year = mainDate.getYear();

                        if(datee < 10)
                            datee = "0" + datee;

                        if(month < 10)
                            month = "0" + month;

                        dob = month + "/" + datee + "/" + year;

                    }

                    var memberSince = '';
                    if(userDetails.memberSince)
                    {
                        var mainDate1 = new Date(userDetails.memberSince);

                        memberSince = monthNames[mainDate1.getMonth()] + " " + mainDate1.getFullYear();

                    }

                    var profilePic = '';
                    if(userDetails.profilePic)
                        profilePic = userDetails.profilePic;

                    var about = '';
                    if(userDetails.about)
                        about = userDetails.about;

                    var address = '';
                    if(userDetails.address)
                        address = userDetails.address;

                    var gender = '';
                    if(userDetails.gender)
                        gender = userDetails.gender.toString();

                    var dataToSend = {fName: userDetails.name.fName,lName: lName,memberSince: memberSince,
                        dob: dob, email: userDetails.email.primaryEmail,profilePic: profilePic,phone: userDetails.phone,
                        gender: gender,about : about,address : address,location : userDetails.location,
                        work : userDetails.work,company : userDetails.company,school : userDetails.school,wEmail : userDetails.email.workEmail};

                    callback(null,dataToSend);
                }
                else
                {
                    callback(null,[]);
                }
            });
        }
    },
    function(err,userAnsPostDetail)
    {
        console.log("userAnsPostDetail.postDetail.length : " + userAnsPostDetail.postDetail.dataToSend.length );
        if(err)
        {
            res.send({errCode: 1, errNum: 211,  Message: "some error occurred, please try again."});
        }
        else if(userAnsPostDetail.postDetail.dataToSend.length > 0)
        {
            userAnsPostDetail.userDetail['post'] = userAnsPostDetail.postDetail.dataToSend;
            userAnsPostDetail.userDetail['postName'] = userAnsPostDetail.postDetail.name;
            userAnsPostDetail.userDetail['postImage'] = userAnsPostDetail.postDetail.image;
            userAnsPostDetail.userDetail['priceDetails'] = userAnsPostDetail.postDetail.priceDetails;
            userAnsPostDetail.userDetail['description'] = userAnsPostDetail.postDetail.description;
            res.send({errCode: 0, Message: "data send successfully.", data : userAnsPostDetail.userDetail});
        }
        else
        {
            res.send({errCode: 1, errNum: 204, Message: "No data found, for this horse."});
            return false;
        }
    });
}