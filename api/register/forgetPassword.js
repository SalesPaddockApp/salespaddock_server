/**
 * Created by Niranjan on 19/9/16.
 * function name: forgetPassword
 * request: EmailId
 * response: all product type if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var sendMail = require('../email/sendForgetPasswordLink');

exports.forgetPassword = function (req, res)
{
    console.log("forgetPassword: " + JSON.stringify(req.body));

    if (!req.body.email)
    {
        res.send({errCode: 1, errNum: 108, Message: "mandatory email is missing"});
        return false;
    }


    var cond = {"email.primaryEmail": req.body.email.toUpperCase()};

    console.log("cond:  " + JSON.stringify(cond));

    Utility.SelectOne('user', cond, (function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if (result)
        {
            var resetLink = result.userId;

            var dataForMail = {to: req.body.email, subject: 'forgetPassword', name: req.body.email, resetLink: resetLink};

            sendMail.sendForgetPasswordLink(dataForMail, function (err, ress)
            {
                if (err) {
                    console.log("Error is occure. mail not send");
                } else if (ress) {

                    console.log("ress:  " + JSON.stringify(ress));
                    var condss = {"email.primaryEmail":req.body.email.toUpperCase()};

                    var toUpdate = {"changePassword": 1};

                    Utility.Update('user', condss, toUpdate, (function (err1, result1) {
                        if (err1) {
                            console.log("Error is occure. mail sent data not update errnum : 105");
                            res.send({errCode: 1, errNum: 105, Message: "Error is occure. mail sent data not update"});
                            return false;
                        } else if (result1) {
                            console.log(" mail sent data data update Successfully errnum : 106");
                            res.send({errCode: 0, errNum: 106, Message: "Mail sent data update Successfully"});
                            return true;
                        } else {
                            console.log("someting else errnum : 107");
                            res.send({errCode: 1, errNum: 107, Message: "Mail sent, someting else errnum "});
                            return false;
                        }

                    }));

                }
            });


        }
        else
        {
            res.send({errCode: 1, errNum: 112, Message: "User does not exists"});
            return false;
        }
    }));
}


