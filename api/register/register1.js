var exports = module.exports = {};

var Utility = require('../UtilityFunc');

var keyForErrFlag = "errFlag";

exports.registerUser = function(req, res)
{
    console.log("registerUser");
    console.log(req.body);
    if(!req.body.fName)
    {
        res.send({keyForErrFlag: 1, errNum: 107, Message: "mandatory name is missing"});
        return false;
    }

    if(!req.body.email)
    {
        res.send({keyForErrFlag: 1, errNum: 108, Message: "mandatory email is missing"});
        return false;
    }

    if(!req.body.password)
    {
        res.send({keyForErrFlag: 1, errNum: 109, Message: "mandatory password is missing"});
        return false;
    }

    if(!req.body.dob)
    {
       res.send({erkeyForErrFlagrCode: 1, errNum: 110, Message: "mandatory date of birth is missing"});
       return false;
    }

    var cond = {email : req.body.email.toUpperCase()};

    Utility.SelectOne('user',cond, (function (err, result)
    {
        if (err)
        {
            res.send({keyForErrFlag: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result)
        {
            res.send({keyForErrFlag: 1, errNum: 112, Message: "Email already exists"});
            return false;
        }
        else
        {
            var dob = new Date(req.body.dob).getTime()
            var userId = 'UId' + (Math.floor(Math.random()*9000000)+1000000);
            var data = {lName: req.body.lName,dob: dob,password: req.body.password,
                email: req.body.email.toUpperCase(),fName: req.body.fName,userId : userId};

            Utility.Insert('user',data, function (err, isInserted)
            {
                if(err)
                {
                    console.log(err);
                    res.send({keyForErrFlag: 1, errNum: 104, Message: "unknown error occurred."});
                }
                else if(isInserted)
                {
                    res.send({keyForErrFlag: 0, Message: "Sign up successfull.", response:{userId: userId}});
                }
                else
                {
                    res.send({keyForErrFlag: 1, errNum: 105, Message: "Unknown error occurred"});
                return false;
                }

            });
        }
    }));
};