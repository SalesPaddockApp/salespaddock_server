<style>
    .table tbody tr td {

        padding: 10px;
    }
</style>



<script>

    function delete1(val) {
//        alert(val.value);
//        alert(1);
        $('#deletePreference').modal("show");
        $('#idoftaxtodelete').val(val.value);
    }

</script>
<script>
    $(document).ready(function () {
        var id_delete;
              $('.preference_settings').addClass('active');
        $('.preference_settings_thumbnail').attr('src', "<?php echo base_url(); ?>/Img/admin%20assets/icons/froference-settings-on.png");        
 
        $('.table').removeClass('no-footer');


    });
</script>


<script type="text/javascript">

    function getPreferenceType(type, id)
    {
        if (type == 1)
        {
            $("#typeOfPreference" + id).html("Radio Button");
        } else if (type == 2)
        {
            $("#typeOfPreference" + id).html("Check Box");
        } else if (type == 3)
        {
            $("#typeOfPreference" + id).html("Numeric range");
        } else if (type == 4)
        {
            $("#typeOfPreference" + id).html("Text input");
        } else if (type == 5)
        {
            $("#typeOfPreference" + id).html("Calendar");
        } else if (type == 6)
        {
            $("#typeOfPreference" + id).html("Image");
        } else
        {
            $("#typeOfPreference" + id).html(type);
        }
    }


    $(document).ready(function ()
    {

        function ChangeActivationStatus(val)
        {
            var chkid = val.id;

            var ActivationStatus = 0;

            if ($("#" + chkid).is(':checked'))
            {
                ActivationStatus = 1;
            }


            $.ajax({
                url: "<?php echo base_url(); ?>index.php/superadmin/ChangeActivationStatus",
                type: "POST",
                data: {ActiveStatus: ActivationStatus, Recordid: $("#" + chkid).val()},
                success: function (result) {

                    //                    alert("intercange done" + result);

                },
                error: function (res) {
                    alert("Error" + res.message);
                }
            });

        }
    });
    function setIncreasePriority(id) {

        $.ajax({
            url: '<?php echo base_url() . "index.php/preferences/increasePriority/"; ?>' + id,
            type: 'POST',
            success: function (data, textStatus, jqXHR) {

                getAll_ajax();
            },
            error: function (data) {
                alert("error");
            }
        });


    }
    function setDecreasePriority(id)
    {
        $.ajax({
            url: '<?php echo base_url() . "index.php/preferences/decreasePriority/"; ?>' + id,
            type: 'POST',
            success: function (data, textStatus, jqXHR) {

                getAll_ajax();
            },
            error: function (data) {
                alert("error");
            }
        });


    }
    function  getAll_ajax() {
        $.ajax({
            url: '<?php echo base_url() . "index.php/preferences/getAll_ajax"; ?>',
            type: 'POST',
            contentType: 'JSON',
            success: function (data, textStatus, jqXHR) {
                data = jQuery.parseJSON(data);

                if (textStatus)
                {
                    // console.log(123);
                    var table = $("#tableWithSearch").DataTable();
                    var j = 1;
                    var _id, pType = " ";
                    table.clear().draw(false);



                    for (var i = 0; i < data.length; i++)
                    {
                        _id = data[i]["_id"]["$id"];

                        if (data[i]['typeOfPreference'] == 1) {
                            pType = "Radio Button";
                        } else if (data[i]['typeOfPreference'] == 2) {
                            pType = "Check Box";
                        } else if (data[i]['typeOfPreference'] == 3) {
                            pType = "Numeric range";
                        } else if (data[i]['typeOfPreference'] == 4) {
                            pType = "Text input";
                        } else {
                            pType = data[i]['typeOfPreference'];
                        }

                        table.row.add([
                            j++,
                            data[i]['preferenceTitle'],
                            data[i]['productType'],
                            pType
                                    ,
                            data[i]['typeOfPreference'].length,
                            '<a href="<?php echo base_url(); ?>index.php/preferences/editPreferences/' + _id + '"><button class="btn btn-primary id="' + _id + '"><i class="fa fa-edit"></i>&nbsp;Edit&nbsp;&nbsp;</button></a>\n\
                            <button class="btn btn-danger"  onclick=' + 'showDeleteModal("' + _id + '");   id="delete_' + _id + '"><i class="fa fa-trash"></i>&nbsp;Delete</button>'
                                    ,
                            "<button id='row_up_" + j + "' onclick=" + "setIncreasePriority('" + _id + "')" + " class='btn btn-gray btn-circle moveUp'>\n\
                     <i class='fa fa-fw fa-chevron-up'></i></button>\n\
                     <button id='row_down_" + j + "' onclick=" + "setDecreasePriority('" + _id + "')" + " class='btn btn-gray btn-circle moveDown'>\n\
                     <i class='fa fa-fw fa-chevron-down'></i></button>"
                                    ,
                            data[i]['mandatory'] ? "yes" : "No",
                            data[i]['onCreateScreen'] ? "yes" : "No"
                        ]).draw(false);

                    }
                    $('#row_up_2').hide();
                    $('#row_down_' + j).hide();
                    $("td").attr('align', "center");

                }


                console.log("data drw succesfully.");

            },
            error: function (data) {
                alert("error");
            }
        });

    }

    function showDeleteModal(id) {
        this.id_delete = id;
        $("#modal_delete").modal("show");

    }

    function deleteThis() {
        var myid = this.id_delete;
        $.ajax({
            url: '<?php echo base_url() . "index.php/preferences/deletePreference/"; ?>' + this.id_delete,
            type: 'POST',
            success: function (data, textStatus, jqXHR) {
                var t = $("#tableWithSearch").DataTable();

                t.row($('#delete_' + myid).parents('tr')).remove().draw();
            },
            error: function (data) {
                alert("error data not Deleted.");
            }
        });
    }




</script>


<!-- START PAGE CONTENT -->
<div class="content">
    <!-- START JUMBOTRON -->
    <div class="jumbotron bg-white" data-pages="parallax">
        <div class="inner">
            <!-- START BREADCRUMB -->
            <ul class="breadcrumb" style="margin-left: 20px;">
                <li>
                    <a href="<?php echo base_url() ?>index.php/Admin/dishCategory/categoryList">MENU</a>
                </li>
                <li><a href="#" class="active">CATEGORY</a>
                </li>
            </ul>
            <!-- END BREADCRUMB -->
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <div class="row" ><br></div>
                        <div class="row" ><br></div>
                        <div class="row" >

                            <div class="col-md-12">

                                <div class="col-md-3">
                                    <h3><b class="box-title" >Set Up Preferences:</b></h3>
                                </div>    
                                <div class="col-md-9 pull-right">
                                    <div class="col-md-3">
<!--                                        <select name="productType"  class="form-control">
                                            <option value="all" >All</option>
                                            <option value="Breed" >Breed</option>
                                            <option value="Horse" >Horse</option>
                                        </select>-->
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" id="search-table" class="form-control" placeholder="Search">    
                                    </div>
                                    <div class="col-md-2 pull-right">
                                        <a href="<?php echo base_url(); ?>index.php/preferences/addNewPreferences"><input type="button" class="btn btn-success" value="Create New" ></a>    
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="responsive">
                            <div class="table-responsive panel-collapse"  style="margin-left: 1%;margin-right: 1%;">

                                <table class="table table-bordered table-hover" id="tableWithSearch" align="center">
                                    <thead>
                                        <tr>
                                            <th align="center">Sr. no</th>
                                            <th align="center">Preference Title</th>
                                            <th align="center">Product Type</th>
                                            <th align="center">Preference Type</th>
                                            <th align="center">No of options</th>
                                            <th align="center">Action</th>
                                            <th align="center">Display Order</th>
                                            <th align="center">Mandatory</th>
                                            <th align="center">Minimum Requirement</th>
                                           <!--<th width="20%"></th>-->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 1;
                                        foreach ($dashboard as $data) {
                                            ?>
                                            <tr>
                                                <td align="center"><?php echo $i++; ?></td>
                                                <td align="center"><?php echo $data['preferenceTitle']; ?></td>
                                                <td align="center"><?php echo $data['productType']; ?></td>
                                                <td align="center"><span id="typeOfPreference<?php echo $data['_id']; ?>"></span></td>
                                        <script>
                                            getPreferenceType('<?php echo $data['typeOfPreference']; ?>', '<?php echo $data['_id']; ?>');
                                        </script>


                                        <td align="center"><?php echo sizeof($data['optionsValue']); ?></td>

                                        <td align="center">
<!--                                            <a  href="--><?php //echo base_url(); ?><!--index.php/preferences/editPreferences/--><?php //echo $data['_id']; ?><!--">-->
                                                <button class="btn btn-primary" id='<?php echo $data['_id']; ?>' ><i class="fa fa-edit"></i>&nbsp;Edit&nbsp;&nbsp;</button>
<!--                                            </a>-->
                                            <button class="btn btn-danger" ><!--onclick="showDeleteModal('<?php echo $data['_id']; ?>');" id='delete_<?php echo $data['_id']; ?>'-->
                                                <i class="fa fa-trash"></i>&nbsp;Delete</button></td>

                                        <td align="center"> 
                                            <button id="row_up_<?php echo $i; ?>" onclick="setDecreasePriority('<?php echo $data["_id"]; ?>')" class="btn btn-gray btn-circle moveDown">
                                                <i class="fa fa-fw fa-chevron-up"></i>
                                            </button>
                                            <button id="row_down_<?php echo $i; ?>" onclick="setIncreasePriority('<?php echo $data["_id"]; ?>')" class="btn btn-gray btn-circle moveDown">
                                                <i class="fa fa-fw fa-chevron-down"></i>
                                            </button>
                                        </td>

                                        <td align="center"><?php echo $data['mandatory'] ? "yes" : "No"; ?></td>
                                        <td align="center"><?php echo $data['onCreateScreen'] ? "yes" : "No"; ?></td>
                                        </tr>
                                        <?php
                                    }
                                    echo "<script>$(document).ready(function (){ $('#row_up_2').hide();$('#row_down_$i').hide();});</script>";
                                    ?>
                                    </tbody>
                                </table>

                                <p id="saveOrder"></p>
                            </div>

                        </div>
                    </div>





                </div>
            </div>
        </div>

        <!-- END PANEL -->
    </div>
</div>
<!-- END JUMBOTRON -->

<!-- Modal -->
<div class="modal fade" id="modal_delete" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete Category</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure to delete ?</p>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="deleteThis();" class="btn btn-primary" data-dismiss="modal">Yes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            </div>
        </div>

    </div>
</div>


