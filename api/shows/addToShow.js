/**
 * Created by Niranjan on 26/9/16.
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');

var ObjectId = require('mongodb').ObjectID;

/*Including async to make function asynchronous*/
var async=require('async');
var sendMail = require('../email/sendMailForPostCreation');

exports.addToShow = function(req, res)
{
    if(!req.body.userId)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory user id is missing"});
        return false;
    }

    if(!req.body.postId)
    {
        res.send({errCode: 1, errNum: 145, Message: "mandatory post id is missing"});
        return false;
    }


    if(!req.body.showId)
    {
        res.send({errCode: 1, errNum: 146, Message: "mandatory show id is missing"});
        return false;
    }

    var condToCheckShowAddedOnce = {posts : {$elemMatch : {postId : ObjectId(req.body.postId)}}};

    Utility.SelectOne('shows',condToCheckShowAddedOnce,function(err,isALreadyAdded)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 144, Message: "some error occurred, please try again."});
            return true;
        }
        else if(isALreadyAdded)
        {
            res.send({errCode: 1, errNum: 144, Message: "your horse already added in " + isALreadyAdded.showName});
            return true;
        }
        else
        {
            async.parallel(
                {
                    posts : function(callback1)
                    {
                        var cond = {_id: ObjectId(req.body.postId)};

                        Utility.SelectOne('post',cond,function (err, postData)
                        {
                            if (err)
                            {
                                callback1(null,[]);
                            }
                            else if(postData)
                            {
                                callback1(null,postData);
                            }
                            else
                            {
                                callback1(null,[]);
                            }
                        });
                    },
                    preferences : function(callback1)
                    {
                        Utility.SelectOne('Preferences',{"preferenceTitle" : "Horse Name"},function(err,prefResult)
                        {
                            if (err)
                            {
                                callback1(null,[]);
                            }
                            else if(prefResult)
                            {
                                callback1(null,prefResult);
                            }
                            else
                            {
                                callback1(null,[]);
                            }
                        })
                    }
                },
                function(err,results2)
                {
                    var postName = '';
                    var isActive = '0';
                    if(results2.posts.activeStatus)
                    {
                        isActive = results2.posts.activeStatus;
                    }

                    if(results2.posts && results2.preferences)
                    {
                        for(var i=0; i<results2.posts.values.length; i++)
                        {
                            if(results2.preferences._id.toString() == results2.posts.values[i]['pref_id'])
                            {
                                postName = results2.posts.values[i]['options'][0];
                            }


                        }
                    }

                    var cond = {_id: ObjectId(req.body.showId)};
                    var dataToInsert= {posts: {postId: ObjectId(req.body.postId), userId: req.body.userId,postName : postName}};

                    Utility.AddIntoArray('shows',cond,dataToInsert,function (err, result)
                    {
                        if (err)
                        {
                            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
                            return false;
                        }
                        else if(result)
                        {
                            if(isActive == 0)
                            {
                                var dataForMail = {};

                                sendMail.sendMailForPostCreation(dataForMail, function (err, ress)
                                {
                                    if (err)
                                    {
                                        console.log("Error is occure. mail not send");
                                    }
                                    else if (ress)
                                    {

                                    }
                                });
                                res.send({errCode: 0,  Message: "Thank you for your submission, your listing will be reviewed by our review team before going live. Contact our support for any further queries.",response: {}});
                            }
                            else
                                res.send({errCode: 0,  Message: "You have added this horse successfully.",response: {}});

                        }
                        else
                        {
                            res.send({errCode: 1, errNum: 144, Message: "You have selected a wrong show."});
                            return true;
                        }
                    });

                });
        }
    });



}


