/**
 * Created by Niranjan on 30/12/16.
 * function name: updatePost
 * request: token,userId,wishId
 * response: succuss response if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;

exports.deletePostFromShow = function(req, res)
{
    console.log("updatePost: " + JSON.stringify(req.body));

    if(!req.body.postId)
    {
        res.send({errCode: 1, errNum: 139, Message: "mandatory post id is missing"});
        return false;
    }

    if(!req.body.showId)
    {
        res.send({errCode: 1, errNum: 146, Message: "mandatory show id is missing"});
        return false;
    }

    var dataToDeleteFromShow = {posts : {postId : ObjectId(req.body.postId)}};
    var condToDeletePostFromSHow = {_id: ObjectId(req.body.showId)};
    Utility.UpdateField('shows',condToDeletePostFromSHow,dataToDeleteFromShow,function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result)
        {
            res.send({errCode: 0,  Message: "you have successfully removed your post from the show.",response: {}});
            return true;
        }
        else
        {
            res.send({errCode: 1, errNum: 135, Message: "no shows found."});
            return true;
        }
    });
}


