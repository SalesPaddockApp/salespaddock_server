/**
 * Created by Niranjan on 23/9/16.
 * function name:  getShows
 * request: token,userId
 * response: all upcoming shows, recelntly viewed shows, and popular shows.
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');

var ObjectId = require('mongodb').ObjectID;

exports.getAroundYou = function (req, res) {
    console.log("getAroundYou:  " + JSON.stringify(req.body));
    if (!req.body.userId) {
        res.send({ errCode: 1, errNum: 132, Message: "mandatory user id is missing" });
        return false;
    }

    var pageNum = 0;
    if (req.body.pageNum)
        pageNum = req.body.pageNum;

    var limit = 10;
    var skip = 10 * pageNum;

    var cond = { userId: req.body.userId };

    Utility.SelectOne('user', cond, function (err, popularDetail) {
        if (err) {
            res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
            return false;
        }
        else if (popularDetail) {

            var lat = 0.0;
            if (popularDetail.location1 && popularDetail.location1.latitude)
                lat = parseFloat(popularDetail.location1.latitude);


            var long = 0.0;
            if (popularDetail.location1 && popularDetail.location1.longitude)
                long = parseFloat(popularDetail.location1.longitude);

            var filters = req.body.filters;
            var filters1 = req.body.filters;
            var horseFilters = [];
            var desciplineData = [];


            var distance = 50000;
            if (filters && filters.length > 0) {
                for (var countOfFilters = 0; countOfFilters < filters.length; countOfFilters++) {
                    if (filters[countOfFilters]['name'] == 'Height') {

                        var selectedHeigth = []
                        for (var countOfFilterForselectedHeigth = parseInt(filters[countOfFilters]['values'][0]); countOfFilterForselectedHeigth <= parseInt(filters[countOfFilters]['values'][1]); countOfFilterForselectedHeigth++) {
                            for (var j = 0; j < 5; j++) {
                                console.log("countOfFilterForselectedHeigth",countOfFilterForselectedHeigth);
                                selectedHeigth.push(countOfFilterForselectedHeigth + "." + j);
                            }
                        }
                        horseFilters.push({
                            'values': {
                                $elemMatch: {
                                    pref_id: ObjectId(filters[countOfFilters]['pref_id']),
                                    options: { $in: selectedHeigth }
                                }
                            }
                        });

                        //console.log("selectedHeigth",selectedHeigth);

                    }
                    else if (filters[countOfFilters]['name'] == 'Price Details') {
                        var selectedPrice = []
                        for (var countOfFilterForPrice = 0; countOfFilterForPrice < filters[countOfFilters]['values'].length; countOfFilterForPrice++) {
                            selectedPrice.push(filters[countOfFilters]['values'][countOfFilterForPrice])
                        }
                        horseFilters.push({
                            'values': {
                                $elemMatch: {
                                    pref_id: ObjectId(filters[countOfFilters]['pref_id']),
                                    options: { $in: selectedPrice }
                                }
                            }
                        });

                        console.log("selectedPrice", selectedPrice)
                    }
                    else if (filters[countOfFilters]['name'] == 'Gender') {
                        var selectedGender = []
                        for (var countOfFilterForGender = 0; countOfFilterForGender < filters[countOfFilters]['values'].length; countOfFilterForGender++) {
                            selectedGender.push(filters[countOfFilters]['values'][countOfFilterForGender])
                        }
                        horseFilters.push({
                            'values': {
                                $elemMatch: {
                                    pref_id: ObjectId(filters[countOfFilters]['pref_id']),
                                    options: { $in: selectedGender }
                                }
                            }
                        });
                    }
                    else if (filters[countOfFilters]['name'] == 'Breed') {
                        var selectedBreed = []
                        for (var countOfFilterForselectedBreed = 0; countOfFilterForselectedBreed < filters[countOfFilters]['values'].length; countOfFilterForselectedBreed++) {
                            selectedBreed.push(filters[countOfFilters]['values'][countOfFilterForselectedBreed])
                        }
                        horseFilters.push({
                            'values': {
                                $elemMatch: {
                                    pref_id: ObjectId(filters[countOfFilters]['pref_id']),
                                    options: { $in: selectedBreed }
                                }
                            }
                        });
                    }
                    else if (filters[countOfFilters]['name'] == 'Discipline' || filters[countOfFilters]["pref_id"] == '57ef6d2600eba3231ed379cc') {
                        var desciplineData = []
                        for (var countOfFilterForselectedBreed = 0; countOfFilterForselectedBreed < filters[countOfFilters]['values'].length; countOfFilterForselectedBreed++) {
                            desciplineData.push(filters[countOfFilters]['values'][countOfFilterForselectedBreed])
                        }
                        horseFilters.push({
                            'values': {
                                $elemMatch: {
                                    pref_id: ObjectId(filters[countOfFilters]['pref_id']),
                                    options: { $in: desciplineData }
                                }
                            }
                        });
                    }
                    else if (filters[countOfFilters]['name'] == 'Lease' || filters[countOfFilters]["pref_id"] == '599573b67046ff742b53b2d0') {
                        var leaseData = []
                        for (var countOfFilterForselectedBreed = 0; countOfFilterForselectedBreed < filters[countOfFilters]['values'].length; countOfFilterForselectedBreed++) {
                            leaseData.push(filters[countOfFilters]['values'][countOfFilterForselectedBreed])
                        }
                        horseFilters.push({
                            'values': {
                                $elemMatch: {
                                    pref_id: ObjectId(filters[countOfFilters]['pref_id']),
                                    options: { $in: leaseData }
                                }
                            }
                        });
                    }
                    else if (filters[countOfFilters]['name'] == 'Location') {
                        distance = parseInt(filters[countOfFilters]['values'][0]);
                    }

                }
            }
            else {

                if (popularDetail.descipline)
                    desciplineData = [popularDetail.descipline];

                filters = desciplineData;
                filters1 = { "pref_id": "57ef6d2600eba3231ed379cc", "values": [popularDetail.descipline] };
                filters1 = [];
            }

            // console.log("distance: " + distance);

            if (horseFilters.length > 0)
                var cond1 = [
                    {
                        $geoNear: {
                            near: { longitude: long, latitude: lat }, spherical: true,
                            distanceMultiplier: 6378.1, distanceField: "dist", num: 100000
                        }
                    },
                    { $match: { "activeStatus": "1" } },
                    { $match: { $and: horseFilters } },
                    { $match: { dist: { $lte: distance } } },
                    { $sort: { _id: -1 } }, { $skip: skip }, { $limit: 10 }];
            // else if(desciplineData.length > 0)
            //     var cond1 = [
            //          {$geoNear : {near : {longitude : long, latitude : lat},spherical: true,
            //          distanceMultiplier:6378.1,distanceField : "dist",num : 100000}},
            //         {$match : {"activeStatus" : "1",'values.pref_id' : ObjectId("57ef6d2600eba3231ed379cc"),
            //             'values.options' : { $in :  desciplineData}}},
            //              {$match : {dist : {$lte : distance}}},
            //         {$sort : {_id : -1}},{$skip : skip},{$limit : 10}];
            else
                var cond1 = [
                    {
                        $geoNear: {
                            near: { longitude: long, latitude: lat }, spherical: true,
                            distanceMultiplier: 6378.1, distanceField: "dist", num: 100000
                        }
                    },
                    { $match: { "activeStatus": "1" } },
                    { $match: { dist: { $lte: distance } } },
                    { $sort: { _id: -1 } }, { $skip: skip }, { $limit: 10 }];

            console.log("\n\n\ncond1 for get around you", JSON.stringify(cond1));

            Utility.AggregateGroup("post", cond1, function (err, postData) {
                if (err) {
                    res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
                    return false;
                }
                else if (postData.length > 0) {
                    var postDataToSend = [];
                    var conditionToGetUsers = [];


                    for (var j = 0; j < postData.length; j++) {
                        var name = "";
                        var priceDetails = "";
                        var image = [];
                        conditionToGetUsers.push({ userId: postData[j].userId });
                        for (var i = 0; i < postData[j].values.length; i++) {
                            switch (postData[j].values[i]['pref_id'].toString()) {
                                case "57efa0aa00eba37a3dd379cd":
                                    name = postData[j]['values'][i]['options'][0];
                                    break;

                                case "57efa28700eba35a3ed379ce":
                                    priceDetails = postData[j]['values'][i]['options'][0];
                                    break;

                                case "57f79d6700eba39c7b2f80f1":
                                    image = postData[j]['values'][i]['options'];
                                    break;
                            }
                        }
                        postDataToSend.push({
                            name: name, priceDetails: priceDetails, image: image, distance: postData[j]['dist'],
                            postId: postData[j]["_id"], userId: postData[j].userId
                        });
                    }



                    var conditionToGetUserss = { $or: conditionToGetUsers };
                    var projectData = { name: 1, profilePic: 1, userId: 1 };
                    Utility.SelectMatched('user', conditionToGetUserss, projectData, function (err, userData) {
                        if (err) {
                            res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
                            return false;
                        }
                        else if (userData.length > 0) {
                            for (var j = 0; j < userData.length; j++) {
                                var image = "";
                                for (var i = 0; i < postDataToSend.length; i++) {
                                    if (userData[j]['userId'] == postDataToSend[i]['userId']) {
                                        // console.log("userData[j]['userId']: " + userData[j]['userId']);
                                        postDataToSend[i]['userName'] = userData[j]['name'];

                                        if (userData[j]['profilePic'])
                                            image = userData[j]['profilePic'];

                                        postDataToSend[i]['userImage'] = image;
                                    }
                                }
                            }

                            res.send({ errCode: 0, Message: "Data sent successfully.", response: postDataToSend, filter: filters1 });
                        }
                        else {
                            res.send({ errCode: 1, errNum: 104, Message: "No data found." });
                            return false;
                        }
                    })

                }
                else {
                    res.send({ errCode: 1, errNum: 104, Message: "No data found.", filter: filters1 });
                    return false;
                }
            })
        }
        else {
            res.send({ errCode: 1, errNum: 104, Message: "No data found." });
            return false;
        }
    });
}
