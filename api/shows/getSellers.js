/**
 * Created by Niranjan on 2/3/17.
 * function name:  getSellers
 * request: token
 * response: all seller details.
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var async = require("async");
var ObjectId = require('mongodb').ObjectID;
var NoneLocation = {};
exports.getSellers = function (req, res) {

    if (!req.body.userId) {
        res.send({ errCode: 1, errNum: 132, Message: "mandatory user id is missing" });
        return false;
    }

    var pageNum = 0;
    if (req.body.pageNum)
        pageNum = req.body.pageNum;

    var limit = 10;
    var skip = 10 * pageNum;
    console.log("page Number " + pageNum);

    var curDate = Date.now();

    Utility.SelectOne("user", { userId: req.body.userId }, function (err, result) {
        if (err) {
            res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
            return false;
        } else if (result.userId) {

            // if (result.location1)
            //     var cond = [
            //         {
            //             $geoNear: {
            //                 near: {
            //                     longitude: result.location1.longitude,
            //                     latitude: result.location1.latitude
            //                 },
            //                 spherical: true,
            //                 num: 100000,
            //                 'distanceField': 'dist',
            //                 "distanceMultiplier": 6371
            //             }
            //         },
            //         {
            //             $lookup:
            //             {
            //                 from: "post",
            //                 localField: "userId",
            //                 foreignField: "userId",
            //                 as: "Posts"
            //             }
            //         }, { $match: { "Posts.activeStatus": "1", userId: { $ne: req.body.userId } } },
            //         { $project: { name: 1, profilePic: 1, location: 1, userId: 1, dist: 1 } },
            //         { $sort: { "name.fName": 1 } },
            //         { $skip: skip }, { $limit: limit }
            //     ];
            // else
            var cond = [
                {
                    $lookup:
                    {
                        from: "post",
                        localField: "userId",
                        foreignField: "userId",
                        as: "Posts"
                    }
                }, { $match: { "Posts.activeStatus": "1"} },
                { $project: { name: 1, profilePic: 1, location: 1, userId: 1 } },
                { $sort: { "name.fName": 1 } },
                { $skip: skip }, { $limit: limit }
            ];


            console.log("cond:  " + JSON.stringify(cond));
            //Utility.SelectWithPagination('shows',cond,projection,skip,limit,function (err, result)
            Utility.AggregateGroup('user', cond, function (err, result) {
                if (err) {
                    res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
                    return false;
                }
                else if (result.length > 0) {
                    console.log("all user detail", JSON.stringify(result[0]));
                    res.send({ errCode: 0, Message: "data send successfully.", response: result });
                }
                else {
                    res.send({ errCode: 1, errNum: 105, Message: "No data found." });
                    return false;
                }
            });
        }
        else {
            res.send({ errCode: 1, errNum: 105, Message: "No data found." });
            return false;
        }

    });





}

