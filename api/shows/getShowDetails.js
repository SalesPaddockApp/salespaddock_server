/**
 * Created by Niranjan on 1/10/16.
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var async = require('async');

var ObjectId = require('mongodb').ObjectID;

exports.getShowDetails = function(req, res)
{
    //console.log("getShowDetails:  " + JSON.stringify(req.body));
    //if(!req.body.userId)
    //{
    //    res.send({errCode: 1, errNum: 132, Message: "mandatory user id is missing"});
    //    return false;
    //}

    if(!req.body.showId)
    {
        res.send({errCode: 1, errNum: 145, Message: "mandatory show id is missing"});
        return false;
    }

    async.parallel(
        {
            postIds : function(callback)
            {
                var cond = {_id : ObjectId(req.body.showId)};

                Utility.SelectOne('shows',cond,function (err, popularDetail)
                {
                    if (err)
                    {
                        callback(null,[]);
                    }
                    else if(popularDetail)
                    {
                        var dataToSend = []
                        for(var i=0; i<popularDetail.posts.length; i++)
                        {
                            dataToSend.push({_id: ObjectId(popularDetail.posts[i]['postId'])});
                        }
                        callback(null,dataToSend);
                    }
                    else
                    {
                        console.log(2);
                        callback(null,[]);
                    }
                });
            },
            preferences : function(callback)
            {
                var condForPref = {"productType" : "Horse"};
                Utility.Select('Preferences',condForPref,function (err, prefDetails)
                {
                    if (err)
                    {
                        callback(null,0);
                    }
                    else     if(prefDetails.length > 0)
                    {
                        callback(null,prefDetails);
                    }
                    else
                    {
                        callback(null,0);
                    }
                });
            }
        },
        function(err,results2)
        {
            var filters = req.body.filters;
            var horseFilters = [];

            console.log("filters:  " + JSON.stringify(filters));

            if(filters)
            {
                for(var countOfFilters=0; countOfFilters<filters.length; countOfFilters++)
                {
                    if(filters[countOfFilters]['name'] == 'Height')
                    {
                        //if()
                        //horseFilters.push({'values' :{$elemMatch: {pref_id : ObjectId(filters[countOfFilters]['pref_id']),
                        //    options : { $gte :  filters[countOfFilters]['values'][0]}}}});
                        //,options : { $lte :  filters[countOfFilters]['values'][1]}

                        var selectedHeigth = []
                        for(var countOfFilterForselectedHeigth=0; countOfFilterForselectedHeigth<filters[countOfFilters]['values'].length; countOfFilterForselectedHeigth++)
                        {
                            selectedHeigth.push(filters[countOfFilters]['values'][countOfFilterForselectedHeigth])
                        }
                        horseFilters.push({'values' :{$elemMatch: {pref_id : ObjectId(filters[countOfFilters]['pref_id']),
                            options : { $in :  selectedHeigth}}}});
                    }
                    else if(filters[countOfFilters]['name'] == 'Price Details')
                    {
                        var mimAmount = (parseInt(filters[countOfFilters]['values'][0]) * 1000).toString();
                        var maxAmount = (parseInt(filters[countOfFilters]['values'][1]) * 1000).toString();

                        horseFilters.push({'values' :{$elemMatch: {pref_id : ObjectId(filters[countOfFilters]['pref_id']),
                            options : { $gte :  mimAmount},options : { $lte :  maxAmount}}}});
                    }
                    if(filters[countOfFilters]['name'] == 'Gender')
                    {
                        var selectedGender = []
                        for(var countOfFilterForGender=0; countOfFilterForGender<filters[countOfFilters]['values'].length; countOfFilterForGender++)
                        {
                            selectedGender.push(filters[countOfFilters]['values'][countOfFilterForGender])
                        }
                        horseFilters.push({'values' :{$elemMatch: {pref_id : ObjectId(filters[countOfFilters]['pref_id']),
                            options : { $in :  selectedGender}}}});
                    }
                    else if(filters[countOfFilters]['name'] == 'Breed')
                    {
                        var selectedBreed = []
                        for(var countOfFilterForselectedBreed=0; countOfFilterForselectedBreed<filters[countOfFilters]['values'].length; countOfFilterForselectedBreed++)
                        {
                            selectedBreed.push(filters[countOfFilters]['values'][countOfFilterForselectedBreed])
                        }
                        horseFilters.push({'values' :{$elemMatch: {pref_id : ObjectId(filters[countOfFilters]['pref_id']),
                            options : { $in :  selectedBreed}}}});
                    }
                    else if(filters[countOfFilters]['name'] == 'Discipline')
                    {
                        var selectedDiscipline = []
                        for(var countOfFilterForselectedDiscipline=0; countOfFilterForselectedDiscipline<filters[countOfFilters]['values'].length; countOfFilterForselectedDiscipline++)
                        {
                            selectedDiscipline.push(filters[countOfFilters]['values'][countOfFilterForselectedDiscipline])
                        }
                        horseFilters.push({'values' :{$elemMatch: {pref_id : ObjectId(filters[countOfFilters]['pref_id']),
                            options : { $in :  selectedDiscipline}}}});
                    }

                }
            }

            //console.log("horseFilters:  " + JSON.stringify(horseFilters));

            if(horseFilters.length < 1)
            {
                //console.log("horseFilters:  " + JSON.stringify(horseFilters));

                console.log(1);
                var condForPref = [{$match:{$or:results2.postIds}},{$sort : {_id : -1}},
                    {$lookup:{from:'user',localField:'userId',
                        foreignField:'userId',as:'user'}},{$unwind:'$user'},
                    {$project:{values:1,activeStatus : 1,userId:1,about:'$user.about',name:'$user.name',profilePic:'$user.profilePic'}}];
                horseFilters.push({"activeStatus" : "1"});
            }
            else
            {
                console.log(2);
                var condForPref = [{$match:{$or:results2.postIds}},
                    {$match : {$and : horseFilters}},{$sort : {_id : -1}},
                    {$lookup:{from:'user',localField:'userId',
                        foreignField:'userId',as:'user'}},{$unwind:'$user'},
                    {$project:{values:1,activeStatus : 1,userId:1,about:'$user.about',name:'$user.name',profilePic:'$user.profilePic'}}];
                horseFilters.push({"activeStatus" : "1"});
            }

            //console.log("condForPref: " + JSON.stringify(condForPref));

            if(results2.postIds.length > 0)
            {
                Utility.AggregateGroup('post',condForPref,function (err, wishDetails)
                {
                    if (err)
                    {
                        res.send({errCode: 1, errNum: 135, Message: "Currently no product is there, please try after some time."});
                    }
                    else if(wishDetails.length > 0)
                    {
                        console.log("all horses of a particular show: " + wishDetails.length);

                        var dataToSends = [];
                        for(var j=0; j<wishDetails.length; j++)
                        {
                            var dataToSend = [];

                            var name = "";
                            var image = [];
                            var priceDetails = "";
                            var description = "";
                            var breed = "";

                            //dataToSend.push({postId: wishDetails[j]['_id']});
                            for(var k=0; k<results2.preferences.length; k++)
                            {
                                //if(wishDetails[j]['values'].length == results2.preferences.length)
                                if(wishDetails[j]['activeStatus'] == 1)
                                {
                                    for(var l=0; l<wishDetails[j]['values'].length; l++)
                                    {
                                        //console.log("j: " + j + "  k:  " + k + "  l:  " + l);
                                        if(results2.preferences[k]['_id'].toString() == wishDetails[j]['values'][l]['pref_id'].toString())
                                        {
                                            if(results2.preferences[k]['preferenceTitle'] == 'Horse Name')
                                                name = wishDetails[j]['values'][l]['options'];

                                            if(results2.preferences[k]['preferenceTitle'] == 'Image')
                                                image = wishDetails[j]['values'][l]['options'];

                                            if(results2.preferences[k]['preferenceTitle'] == 'Price Details')
                                                priceDetails = wishDetails[j]['values'][l]['options'];

                                            if(results2.preferences[k]['preferenceTitle'] == 'Description')
                                                description = wishDetails[j]['values'][l]['options'];

                                            if(results2.preferences[k]['preferenceTitle'] == 'Breed')
                                                breed = wishDetails[j]['values'][l]['options'];

                                            //dataToSend.push({title: results2.preferences[k]['preferenceTitle'],selected : wishDetails[j]['values'][l]['options']});
                                            break;
                                        }
                                    }

                                    if(k == (wishDetails[j]['values'].length-1))
                                    {
                                        var profilePic = "";
                                        if(wishDetails[j]['profilePic'])
                                            profilePic = wishDetails[j]['profilePic'];

                                        var about = "";
                                        if(wishDetails[j]['about'])
                                            about = wishDetails[j]['about'];

                                        ownerDetail= {name: wishDetails[j]['name']['fName'],profilePic: profilePic,
                                            about: about,userId: wishDetails[j]['userId']};
                                        dataInFormat = {name:name,image:image,priceDetails:priceDetails,
                                            description:description,breed:breed,postId:wishDetails[j]['_id'],
                                            ownerDetail:ownerDetail};
                                        dataToSends.push(dataInFormat);
                                    }
                                }
                                else
                                {
                                    console.log("else part get show detail");
                                    continue;
                                }


                            }
                        }
                        var filters = '';
                        if(req.body.filters)
                            filters = req.body.filters;

                        var countOfFilters = 0;
                        if(req.body.filters)
                            countOfFilters = req.body.filters.length;

                        res.send({errCode: 0,  Message: "show data send successfully.",
                            response : dataToSends, filters : filters, countOfFilters : countOfFilters});
                    }
                    else
                    {
                        var filters = '';
                        if(req.body.filters)
                            filters = req.body.filters;

                        var countOfFilters = 0;
                        if(req.body.filters)
                            countOfFilters = req.body.filters.length;
                        res.send({errCode: 1, errNum: 135, Message: "Currently no product is there, please try after some time.",
                            filters : filters, countOfFilters : countOfFilters});
                    }
                });
            }
            else
            {

                var filters = '';
                if(req.body.filters)
                    filters = req.body.filters;

                var countOfFilters = 0;
                if(req.body.filters)
                    countOfFilters = req.body.filters.length;
                res.send({errCode: 1, errNum: 135, Message: "Currently no product is there, please try after some time.",
                    filters : filters, countOfFilters : countOfFilters});
            }

        });
}

