/**
 * Created by Niranjan on 23/9/16.
 * function name:  getShows
 * request: token,userId
 * response: all upcoming shows, recelntly viewed shows, and popular shows.
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');

var ObjectId = require('mongodb').ObjectID;
var async = require('async');
exports.getShowForUsers1 = function (req, res) {
    console.log("get Show For Users: test 11  " + JSON.stringify(req.body));
    if (!req.body.userId) {
        res.send({ errCode: 1, errNum: 132, Message: "mandatory user id is missing" });
        return false;
    }

    var curDate = Date.now();

    async.parallel(
        {
            activeShows: function (callback) {
                var filters = req.body.filters;
                var filters1 = req.body.filters;
                var horseFilters = [];
                var desciplineData = [];
                if (filters && filters.length > 0) {
                    // console.log(1,JSON.stringify(filters));
                    for (var countOfFilters = 0; countOfFilters < filters.length; countOfFilters++) {

                        if (filters[countOfFilters]['name'] == 'Breed') {
                            var selectedBreed = []
                            for (var countOfFilterForselectedBreed = 0; countOfFilterForselectedBreed < filters[countOfFilters]['values'].length; countOfFilterForselectedBreed++) {
                                selectedBreed.push(filters[countOfFilters]['values'][countOfFilterForselectedBreed])
                            }
                            horseFilters.push({
                                'values': {
                                    $elemMatch: {
                                        pref_id: ObjectId(filters[countOfFilters]['pref_id']),
                                        options: { $in: selectedBreed }
                                    }
                                }
                            });
                        }
                        else if (filters[countOfFilters]['name'] == 'Discipline') {
                            var desciplineData = []
                            for (var countOfFilterForselectedBreed = 0; countOfFilterForselectedBreed < filters[countOfFilters]['values'].length; countOfFilterForselectedBreed++) {
                                desciplineData.push(filters[countOfFilters]['values'][countOfFilterForselectedBreed])
                            }
                            horseFilters.push({
                                'values': {
                                    $elemMatch: {
                                        pref_id: ObjectId(filters[countOfFilters]['pref_id']),
                                        options: { $in: desciplineData }
                                    }
                                }
                            });
                        }
                    }


                    if (horseFilters.length > 0)
                        var cond = [
                            { $match: { "active": "1" } },
                            // { $match: { $and: [{ startDate: { $lte: curDate } }, { endDate: { $gte: curDate } }] } },
                            { $match: { $or: [{ startDate: { $lte: curDate }, endDate: { $gte: curDate } }, { startDate: { $gte: curDate } }] } },
                            { $unwind: "$posts" },
                            { $lookup: { from: "post", localField: "posts.postId", foreignField: "_id", as: "posts" } },
                            { $unwind: "$posts" },
                            { "$match": { "posts.activeStatus": "1" } },
                            {
                                $project: {
                                    _id: 1, showName: 1, startDate: 1, endDate: 1, venue: 1, productType: 1, lat: 1, lng: 1,
                                    imgPath: 1, Description: 1, values: "$posts.values"
                                }
                            },
                            { "$match": { "$and": horseFilters } }, {
                                $group: {
                                    _id: "$_id", showName: { $addToSet: "$showName" },
                                    startDate: { $addToSet: "$startDate" }, endDate: { $addToSet: "$endDate" }, venue: { $addToSet: "$venue" },
                                    productType: { $addToSet: "$productType" },
                                    imgPath: { $addToSet: "$imgPath" }, Description: { $addToSet: "$Description" }
                                }
                            },
                            { $unwind: "$showName" }, { $unwind: "$startDate" }, { $unwind: "$endDate" }, { $unwind: "$venue" },
                            { $unwind: "$productType" }, { $unwind: "$imgPath" }, { $unwind: "$endDate" }, { $unwind: "$Description" },
                            { $sort: { _id: -1 } }, { $skip: 0 }, { $limit: 10 }];
                    else if (desciplineData.length > 0)
                        var cond = [
                            { $match: { "active": "1" } },
                            // { $match: { $and: [{ startDate: { $lte: curDate } }, { endDate: { $gte: curDate } }] } },
                            { $match: { $or: [{ startDate: { $lte: curDate }, endDate: { $gte: curDate } }, { startDate: { $gte: curDate } }] } },
                            { $unwind: "$posts" },
                            { $lookup: { from: "post", localField: "posts.postId", foreignField: "_id", as: "posts" } },
                            { $unwind: "$posts" },
                            { "$match": { "posts.activeStatus": "1" } },
                            {
                                $project: {
                                    _id: 1, showName: 1, startDate: 1, endDate: 1, venue: 1, productType: 1, lat: 1, lng: 1,
                                    imgPath: 1, Description: 1, values: "$posts.values"
                                }
                            },
                            {
                                $match: {
                                    "activeStatus": "1", 'values.pref_id': ObjectId("57ef6d2600eba3231ed379cc"),
                                    'values.options': { $in: desciplineData }
                                }
                            }, {
                                $group: {
                                    _id: "$_id", showName: { $addToSet: "$showName" },
                                    startDate: { $addToSet: "$startDate" }, endDate: { $addToSet: "$endDate" }, venue: { $addToSet: "$venue" },
                                    productType: { $addToSet: "$productType" },
                                    imgPath: { $addToSet: "$imgPath" }, Description: { $addToSet: "$Description" }
                                }
                            },
                            { $unwind: "$showName" }, { $unwind: "$startDate" }, { $unwind: "$endDate" }, { $unwind: "$venue" },
                            { $unwind: "$productType" }, { $unwind: "$imgPath" }, { $unwind: "$endDate" }, { $unwind: "$Description" },
                            { $sort: { _id: -1 } }, { $skip: 0 }, { $limit: 10 }];
                    else
                        var cond = [
                            { $match: { "active": "1" } },
                            // { $match: { $and: [{ startDate: { $lte: curDate } }, { endDate: { $gte: curDate } }] } },
                            { $match: { $or: [{ startDate: { $lte: curDate }, endDate: { $gte: curDate } }, { startDate: { $gte: curDate } }] } },
                            { $unwind: "$posts" },
                            { $lookup: { from: "post", localField: "posts.postId", foreignField: "_id", as: "posts" } },
                            { $unwind: "$posts" },
                            { "$match": { "posts.activeStatus": "1" } },
                            {
                                $project: {
                                    _id: 1, showName: 1, startDate: 1, endDate: 1, venue: 1, productType: 1, lat: 1, lng: 1,
                                    imgPath: 1, Description: 1, values: "$posts.values"
                                }
                            }, {
                                $group: {
                                    _id: "$_id", showName: { $addToSet: "$showName" },
                                    startDate: { $addToSet: "$startDate" }, endDate: { $addToSet: "$endDate" }, venue: { $addToSet: "$venue" },
                                    productType: { $addToSet: "$productType" },
                                    imgPath: { $addToSet: "$imgPath" }, Description: { $addToSet: "$Description" }
                                }
                            },
                            { $unwind: "$showName" }, { $unwind: "$startDate" }, { $unwind: "$endDate" }, { $unwind: "$venue" },
                            { $unwind: "$productType" }, { $unwind: "$imgPath" }, { $unwind: "$endDate" }, { $unwind: "$Description" },
                            { $sort: { _id: -1 } }, { $skip: 0 }, { $limit: 10 }];;

                    // console.log("cond:  " + JSON.stringify(cond));

                }
                else {
                    var cond = [{ $match: { "active": "1" } }, {
                        $project: {
                            showName: 1, startDate: 1, endDate: 1, imgPath: 1, Description: 1, productType: 1, venue: 1
                        }
                    },
                    // { $match: { $and: [{ startDate: { $lte: curDate } }, { endDate: { $gte: curDate } }] } },
                    { $match: { $or: [{ startDate: { $lte: curDate }, endDate: { $gte: curDate } }, { startDate: { $gte: curDate } }] } },
                    { $sort: { _id: -1 } }, { $skip: 0 }, { $limit: 10 }];
                }

                Utility.AggregateGroup('shows', cond, function (err, result) {
                    if (err) {
                        callback(err, err);
                    }
                    else if (result.length > 0) {
                        var onGoingShows = [];

                        for (var i = 0; i < result.length; i++) {
                            var sdate = '';
                            if (result[i]['startDate']) {
                                var mainDate1 = new Date(parseFloat(result[i]['startDate']));

                                var datee1 = mainDate1.getDate();
                                var month1 = mainDate1.getMonth() + 1;
                                var year1 = mainDate1.getFullYear();

                                if (datee1 < 10)
                                    datee1 = "0" + datee1;

                                if (month1 < 10)
                                    month1 = "0" + month1;

                                sdate = month1 + "/" + datee1 + "/" + year1;

                            }

                            var edate = '';
                            if (result[i]['endDate']) {
                                var mainDate = new Date(parseFloat(result[i]['endDate']));

                                var datee = mainDate.getDate();
                                var month = mainDate.getMonth() + 1;
                                var year = mainDate.getFullYear();

                                if (datee < 10)
                                    datee = "0" + datee;

                                if (month < 10)
                                    month = "0" + month;

                                edate = month + "/" + datee + "/" + year;

                            }
                            isUpcommingShow = false;
                            if (result[i]['startDate'] > curDate) {
                                isUpcommingShow = true;
                            }

                            onGoingShows.push({
                                showName: result[i]['showName'],
                                startDate: sdate, endDate: edate, image: result[i]['imgPath'],
                                isUpcommingShow: isUpcommingShow,
                                Description: result[i]['Description'],
                                venue: result[i]['venue'], showId: result[i]['_id']
                            });
                        }


                        callback(null, onGoingShows);

                    }
                    else {
                        callback(null, []);
                    }
                });
            },
            upcomingShows: function (callback) {
                var filters = req.body.filters;
                var filters1 = req.body.filters;
                var horseFilters = [];
                var desciplineData = [];
                var curDate = Date.now();
                if (filters && filters.length > 0) {
                    // console.log(1,JSON.stringify(filters));
                    for (var countOfFilters = 0; countOfFilters < filters.length; countOfFilters++) {

                        if (filters[countOfFilters]['name'] == 'Breed') {
                            var selectedBreed = []
                            for (var countOfFilterForselectedBreed = 0; countOfFilterForselectedBreed < filters[countOfFilters]['values'].length; countOfFilterForselectedBreed++) {
                                selectedBreed.push(filters[countOfFilters]['values'][countOfFilterForselectedBreed])
                            }
                            horseFilters.push({
                                'values': {
                                    $elemMatch: {
                                        pref_id: ObjectId(filters[countOfFilters]['pref_id']),
                                        options: { $in: selectedBreed }
                                    }
                                }
                            });
                        }
                        else if (filters[countOfFilters]['name'] == 'Discipline') {
                            var desciplineData = []
                            for (var countOfFilterForselectedBreed = 0; countOfFilterForselectedBreed < filters[countOfFilters]['values'].length; countOfFilterForselectedBreed++) {
                                desciplineData.push(filters[countOfFilters]['values'][countOfFilterForselectedBreed])
                            }
                            horseFilters.push({
                                'values': {
                                    $elemMatch: {
                                        pref_id: ObjectId(filters[countOfFilters]['pref_id']),
                                        options: { $in: desciplineData }
                                    }
                                }
                            });
                        }
                    }


                    var curDate = Date.now();

                    if (horseFilters.length > 0)
                        var cond = [
                            { $match: { "active": "1" } },
                            // { $match: { $and: [{ startDate: { $gte: curDate } }] } },
                            { $match: { $or: [{ startDate: { $lte: curDate }, endDate: { $gte: curDate } }, { startDate: { $gte: curDate } }] } },
                            { $unwind: "$posts" },
                            { $lookup: { from: "post", localField: "posts.postId", foreignField: "_id", as: "posts" } },
                            { $unwind: "$posts" },
                            { "$match": { "posts.activeStatus": "1" } },
                            {
                                $project: {
                                    _id: 1, showName: 1, startDate: 1, endDate: 1, venue: 1, productType: 1, lat: 1, lng: 1,
                                    imgPath: 1, Description: 1, values: "$posts.values"
                                }
                            },
                            { "$match": { "$and": horseFilters } }, {
                                $group: {
                                    _id: "$_id", showName: { $addToSet: "$showName" },
                                    startDate: { $addToSet: "$startDate" }, endDate: { $addToSet: "$endDate" }, venue: { $addToSet: "$venue" },
                                    productType: { $addToSet: "$productType" },
                                    imgPath: { $addToSet: "$imgPath" }, Description: { $addToSet: "$Description" }
                                }
                            },
                            { $unwind: "$showName" }, { $unwind: "$startDate" }, { $unwind: "$endDate" }, { $unwind: "$venue" },
                            { $unwind: "$productType" }, { $unwind: "$imgPath" }, { $unwind: "$endDate" }, { $unwind: "$Description" },
                            { $sort: { _id: -1 } }, { $skip: 0 }, { $limit: 10 }];
                    else if (desciplineData.length > 0)
                        var cond = [
                            { $match: { "active": "1" } },
                            // { $match: { $and: [{ startDate: { $gte: curDate } }] } },
                            { $match: { $or: [{ startDate: { $lte: curDate }, endDate: { $gte: curDate } }, { startDate: { $gte: curDate } }] } },
                            { $unwind: "$posts" },
                            { $lookup: { from: "post", localField: "posts.postId", foreignField: "_id", as: "posts" } },
                            { $unwind: "$posts" },
                            { "$match": { "posts.activeStatus": "1" } },
                            {
                                $project: {
                                    _id: 1, showName: 1, startDate: 1, endDate: 1, venue: 1, productType: 1, lat: 1, lng: 1,
                                    imgPath: 1, Description: 1, values: "$posts.values"
                                }
                            },
                            {
                                $match: {
                                    "activeStatus": "1", 'values.pref_id': ObjectId("57ef6d2600eba3231ed379cc"),
                                    'values.options': { $in: desciplineData }
                                }
                            }, {
                                $group: {
                                    _id: "$_id", showName: { $addToSet: "$showName" },
                                    startDate: { $addToSet: "$startDate" }, endDate: { $addToSet: "$endDate" }, venue: { $addToSet: "$venue" },
                                    productType: { $addToSet: "$productType" },
                                    imgPath: { $addToSet: "$imgPath" }, Description: { $addToSet: "$Description" }
                                }
                            },
                            { $unwind: "$showName" }, { $unwind: "$startDate" }, { $unwind: "$endDate" }, { $unwind: "$venue" },
                            { $unwind: "$productType" }, { $unwind: "$imgPath" }, { $unwind: "$endDate" }, { $unwind: "$Description" },
                            { $sort: { _id: -1 } }, { $skip: 0 }, { $limit: 10 }];
                    else
                        var cond = [
                            { $match: { "active": "1" } },
                            // { $match: { $and: [{ startDate: { $gte: curDate } }] } },
                            { $match: { $or: [{ startDate: { $lte: curDate }, endDate: { $gte: curDate } }, { startDate: { $gte: curDate } }] } },
                            { $unwind: "$posts" },
                            { $lookup: { from: "post", localField: "posts.postId", foreignField: "_id", as: "posts" } },
                            { $unwind: "$posts" },
                            { "$match": { "posts.activeStatus": "1" } },
                            {
                                $project: {
                                    _id: 1, showName: 1, startDate: 1, endDate: 1, venue: 1, productType: 1, lat: 1, lng: 1,
                                    imgPath: 1, Description: 1, values: "$posts.values"
                                }
                            }, {
                                $group: {
                                    _id: "$_id", showName: { $addToSet: "$showName" },
                                    startDate: { $addToSet: "$startDate" }, endDate: { $addToSet: "$endDate" }, venue: { $addToSet: "$venue" },
                                    productType: { $addToSet: "$productType" },
                                    imgPath: { $addToSet: "$imgPath" }, Description: { $addToSet: "$Description" }
                                }
                            },
                            { $unwind: "$showName" }, { $unwind: "$startDate" }, { $unwind: "$endDate" }, { $unwind: "$venue" },
                            { $unwind: "$productType" }, { $unwind: "$imgPath" }, { $unwind: "$endDate" }, { $unwind: "$Description" },
                            { $sort: { _id: -1 } }, { $skip: 0 }, { $limit: 10 }];;

                    //console.log("cond:  " + JSON.stringify(cond));

                }
                else {
                    var cond = [{ $match: { "active": "1" } }, {
                        $project: {
                            showName: 1, startDate: 1, endDate: 1, imgPath: 1, Description: 1, productType: 1, venue: 1
                        }
                    },
                    // { $match: { $and: [{ startDate: { $gte: curDate } }] } },
                    { $match: { $or: [{ startDate: { $lte: curDate }, endDate: { $gte: curDate } }, { startDate: { $gte: curDate } }] } },
                    { $sort: { _id: -1 } }, { $skip: 0 }, { $limit: 10 }];
                }


                //Utility.SelectWithPagination('shows',cond,projection,skip,limit,function (err, result)
                Utility.AggregateGroup('shows', cond, function (err, result) {
                    if (err) {
                        callback(err, err);
                    }
                    else if (result.length > 0) {
                        var archievedShows = [];

                        //console.log("result:  " + JSON.stringify(result));

                        for (var i = 0; i < result.length; i++) {
                            var sdate = '';
                            if (result[i]['startDate']) {
                                var mainDate1 = new Date(parseFloat(result[i]['startDate']));

                                var datee1 = mainDate1.getDate();
                                var month1 = mainDate1.getMonth() + 1;
                                var year1 = mainDate1.getFullYear();

                                if (datee1 < 10)
                                    datee1 = "0" + datee1;

                                if (month1 < 10)
                                    month1 = "0" + month1;

                                sdate = month1 + "/" + datee1 + "/" + year1;

                            }

                            var edate = '';
                            if (result[i]['endDate']) {
                                var mainDate = new Date(parseFloat(result[i]['endDate']));

                                var datee = mainDate.getDate();
                                var month = mainDate.getMonth() + 1;
                                var year = mainDate.getFullYear();

                                if (datee < 10)
                                    datee = "0" + datee;

                                if (month < 10)
                                    month = "0" + month;

                                edate = month + "/" + datee + "/" + year;

                            }
                            isUpcommingShow = false;
                            if (result[i]['startDate'] > curDate) {
                                isUpcommingShow = true;
                            }

                            archievedShows.push({
                                showName: result[i]['showName'],
                                startDate: sdate, endDate: edate, image: result[i]['imgPath'],
                                isUpcommingShow: isUpcommingShow,
                                Description: result[i]['Description'],
                                venue: result[i]['venue'], showId: result[i]['_id']
                            });
                        }

                        callback(null, archievedShows);

                    }
                    else {
                        callback(null, []);
                    }
                });
            },
            aroundYou: function (callback) {
                var cond = { userId: req.body.userId };

                Utility.SelectOne('user', cond, function (err, popularDetail) {
                    if (err) {
                        callback(null, []);
                    }
                    else if (popularDetail) {

                        var lat = 0.0;
                        if (popularDetail.location1 && popularDetail.location1.latitude)
                            lat = parseFloat(popularDetail.location1.latitude);

                        var long = 0.0;
                        if (popularDetail.location1 && popularDetail.location1.longitude)
                            long = parseFloat(popularDetail.location1.longitude);


                        var distance = 50000;

                        var filters = req.body.filters;
                        var filters1 = req.body.filters;
                        var horseFilters = [];
                        var desciplineData = [];
                        // console.log(2,JSON.stringify(filters));
                        if (filters && filters.length > 0) {
                            // console.log(1,JSON.stringify(filters));
                            for (var countOfFilters = 0; countOfFilters < filters.length; countOfFilters++) {
                                if (filters[countOfFilters]['name'] == 'Height') {

                                    var selectedHeigth = []
                                    for (var countOfFilterForselectedHeigth = parseInt(filters[countOfFilters]['values'][0]); countOfFilterForselectedHeigth <= parseInt(filters[countOfFilters]['values'][1]); countOfFilterForselectedHeigth++) {
                                        for (var j = 1; j < 5; j++) {
                                            //console.log("countOfFilterForselectedHeigth",countOfFilterForselectedHeigth);
                                            selectedHeigth.push(countOfFilterForselectedHeigth + "." + j);
                                        }
                                    }
                                    horseFilters.push({
                                        'values': {
                                            $elemMatch: {
                                                pref_id: ObjectId(filters[countOfFilters]['pref_id']),
                                                options: { $in: selectedHeigth }
                                            }
                                        }
                                    });

                                    //console.log("selectedHeigth",selectedHeigth);

                                }
                                else if (filters[countOfFilters]['name'] == 'Price Details') {
                                    var selectedPrice = []
                                    for (var countOfFilterForPrice = 0; countOfFilterForPrice < filters[countOfFilters]['values'].length; countOfFilterForPrice++) {
                                        selectedPrice.push(filters[countOfFilters]['values'][countOfFilterForPrice])
                                    }
                                    horseFilters.push({
                                        'values': {
                                            $elemMatch: {
                                                pref_id: ObjectId(filters[countOfFilters]['pref_id']),
                                                options: { $in: selectedPrice }
                                            }
                                        }
                                    });

                                    //console.log("selectedPrice",selectedPrice)

                                }
                                else if (filters[countOfFilters]['name'] == 'Gender') {
                                    // console.log("Gender part");
                                    var selectedGender = []
                                    for (var countOfFilterForGender = 0; countOfFilterForGender < filters[countOfFilters]['values'].length; countOfFilterForGender++) {
                                        selectedGender.push(filters[countOfFilters]['values'][countOfFilterForGender])
                                    }
                                    horseFilters.push({
                                        'values': {
                                            $elemMatch: {
                                                pref_id: ObjectId(filters[countOfFilters]['pref_id']),
                                                options: { $in: selectedGender }
                                            }
                                        }
                                    });
                                }
                                else if (filters[countOfFilters]['name'] == 'Breed') {
                                    // console.log("Breed part");
                                    var selectedBreed = []
                                    for (var countOfFilterForselectedBreed = 0; countOfFilterForselectedBreed < filters[countOfFilters]['values'].length; countOfFilterForselectedBreed++) {
                                        selectedBreed.push(filters[countOfFilters]['values'][countOfFilterForselectedBreed])
                                    }
                                    horseFilters.push({
                                        'values': {
                                            $elemMatch: {
                                                pref_id: ObjectId(filters[countOfFilters]['pref_id']),
                                                options: { $in: selectedBreed }
                                            }
                                        }
                                    });
                                }
                                else if (filters[countOfFilters]['name'] == 'Discipline' || filters[countOfFilters]["pref_id"] == '57ef6d2600eba3231ed379cc') {
                                    var desciplineData = []
                                    for (var countOfFilterForselectedBreed = 0; countOfFilterForselectedBreed < filters[countOfFilters]['values'].length; countOfFilterForselectedBreed++) {
                                        desciplineData.push(filters[countOfFilters]['values'][countOfFilterForselectedBreed])
                                    }
                                    horseFilters.push({
                                        'values': {
                                            $elemMatch: {
                                                pref_id: ObjectId(filters[countOfFilters]['pref_id']),
                                                options: { $in: desciplineData }
                                            }
                                        }
                                    });
                                }
                                else if (filters[countOfFilters]['name'] == 'Lease' || filters[countOfFilters]["pref_id"] == '599573b67046ff742b53b2d0') {
                                    var leaseData = []
                                    for (var countOfFilterForselectedBreed = 0; countOfFilterForselectedBreed < filters[countOfFilters]['values'].length; countOfFilterForselectedBreed++) {
                                        leaseData.push(filters[countOfFilters]['values'][countOfFilterForselectedBreed])
                                    }
                                    horseFilters.push({
                                        'values': {
                                            $elemMatch: {
                                                pref_id: ObjectId(filters[countOfFilters]['pref_id']),
                                                options: { $in: leaseData }
                                            }
                                        }
                                    });
                                }
                                else if (filters[countOfFilters]['name'] == 'Location') {
                                    distance = parseInt(filters[countOfFilters]['values'][0]);
                                }

                            }
                        }
                        else {

                            if (popularDetail.descipline)
                                desciplineData = popularDetail.descipline;

                            filters = desciplineData;
                            //filters1 = {"pref_id" : "57ef6d2600eba3231ed379cc","values" : [popularDetail.descipline]};
                            filters1 = [];
                        }


                        // console.log("distance: " + distance);

                        if (horseFilters.length > 0)
                            var cond1 = [
                                {
                                    $geoNear: {
                                        near: { longitude: long, latitude: lat }, spherical: true,
                                        distanceMultiplier: 6378.1, distanceField: "dist", num: 100000
                                    }
                                },
                                { $match: { "activeStatus": "1" } },
                                { $match: { $and: horseFilters } },
                                { $match: { dist: { $lte: distance } } },
                                { $sort: { dateOfActivation: -1, _id: -1 } }, { $skip: 0 }, { $limit: 10 }];
                        // else if(desciplineData.length > 0)
                        //     var cond1 = [
                        //          {$geoNear : {near : {longitude : long, latitude : lat},spherical: true,
                        //          distanceMultiplier:6378.1,distanceField : "dist",num : 100000}},
                        //         {$match : {"activeStatus" : "1",'values.pref_id' : ObjectId("57ef6d2600eba3231ed379cc"),
                        //             'values.options' : { $in :  desciplineData}}},
                        //          {$match : {dist : {$lte : distance}}},
                        //         {$sort : {dateOfActivation : -1,_id : -1}},{$skip : 0},{$limit : 10}];
                        else
                            var cond1 = [
                                {
                                    $geoNear: {
                                        near: { longitude: long, latitude: lat }, spherical: true,
                                        distanceMultiplier: 6378.1, distanceField: "dist", num: 100000
                                    }
                                },
                                { $match: { "activeStatus": "1" } },
                                { $match: { dist: { $lte: distance } } },
                                { $sort: { dateOfActivation: -1, _id: -1 } }, { $skip: 0 }, { $limit: 10 }];

                        console.log('\n\n');
                        console.log("cond1 to get posts:  " + JSON.stringify(cond1));
                        console.log('\n\n');

                        Utility.AggregateGroup("post", cond1, function (err, postData) {
                            if (err) {
                                callback(null, []);
                            }
                            else if (postData.length > 0) {
                                var postDataToSend = [];
                                var conditionToGetUsers = [];


                                for (var j = 0; j < postData.length; j++) {
                                    var name = "";
                                    var priceDetails = "";
                                    var image = [];
                                    conditionToGetUsers.push({ userId: postData[j].userId });
                                    for (var i = 0; i < postData[j].values.length; i++) {
                                        switch (postData[j].values[i]['pref_id'].toString()) {
                                            case "57efa0aa00eba37a3dd379cd": {
                                                if (postData[j]['values'][i]['options']) {
                                                    name = postData[j]['values'][i]['options'][0];
                                                }
                                                break;
                                            }
                                            case "57efa28700eba35a3ed379ce":
                                            {
                                                if(postData[j]['values'][i]['options']){
                                                    priceDetails = postData[j]['values'][i]['options'][0];
                                                }
                                                break;
                                            }
                                              
                                             

                                            case "57f79d6700eba39c7b2f80f1":
                                            {
                                                image = postData[j]['values'][i]['options'];
                                                break;
                                            }
                                               
                                        }
                                    }
                                    postDataToSend.push({
                                        name: name, priceDetails: priceDetails, image: image, distance: postData[j]['dist'],
                                        postId: postData[j]["_id"], userId: postData[j].userId
                                    });
                                }

                                var conditionToGetUserss = { $or: conditionToGetUsers };
                                var projectData = { name: 1, profilePic: 1, userId: 1 };
                                Utility.SelectMatched('user', conditionToGetUserss, projectData, function (err, userData) {
                                    if (err) {
                                        callback(null, null);
                                    }
                                    else if (userData.length > 0) {
                                        for (var j = 0; j < userData.length; j++) {
                                            var image = "";
                                            for (var i = 0; i < postDataToSend.length; i++) {
                                                if (userData[j]['userId'] == postDataToSend[i]['userId']) {
                                                    // console.log("userData[j]['userId']: " + userData[j]['userId']);
                                                    postDataToSend[i]['userName'] = userData[j]['name'];

                                                    if (userData[j]['profilePic'])
                                                        image = userData[j]['profilePic'];

                                                    postDataToSend[i]['userImage'] = image;
                                                }
                                            }
                                        }

                                        // console.log("postDataToSend",postDataToSend);
                                        // console.log("filters1",filters1);

                                        //callback(null,dataToSend);
                                        callback(null, { response: postDataToSend, filters: filters1 });
                                    }
                                    else {
                                        callback(null, []);
                                    }
                                })

                            }
                            else {
                                // console.log(2);
                                callback(null, { filters: filters1 });
                            }
                        });
                    }
                    else {
                        // console.log(2);
                        callback(null, []);
                    }
                });
            },
            sellerProfile: function (callback) {
                Utility.SelectOne("user", { userId: req.body.userId }, function (err, result) {
                    if (err) {
                        console.log("Error : ", err)
                        callback(err, "error in DB ");
                    }
                    else if (result.userId) {
                        // if (result.location1)
                        //     var cond = [
                        //         {
                        //             $geoNear: {
                        //                 near: {
                        //                     longitude: result.location1.longitude,
                        //                     latitude: result.location1.latitude
                        //                 },
                        //                 spherical: true,
                        //                 'distanceField': 'dist'
                        //             }
                        //         },
                        //         {
                        //             $lookup:
                        //             {
                        //                 from: "post",
                        //                 localField: "userId",
                        //                 foreignField: "userId",
                        //                 as: "Posts"
                        //             }
                        //         }, { $match: { "Posts.activeStatus": "1", userId: { $ne: req.body.userId } } },
                        //         { $project: { name: 1, profilePic: 1, location: 1, userId: 1, dist: 1 } },
                        //         { $sort: { "name.fName": 1 } },
                        //         { $skip: 0 }, { $limit: 25 }];
                        // else
                        var cond = [
                            {
                                $lookup:
                                    {
                                        from: "post",
                                        localField: "userId",
                                        foreignField: "userId",
                                        as: "Posts"
                                    }
                            }, { $match: { "Posts.activeStatus": "1" } },
                            { $project: { name: 1, profilePic: 1, location: 1, userId: 1, dist: 1 } },
                            { $sort: { "name.fName": 1 } },
                            { $skip: 0 }, { $limit: 10 }];


                        console.log("cond:  " + JSON.stringify(cond));
                        //Utility.SelectWithPagination('shows',cond,projection,skip,limit,function (err, result)
                        Utility.AggregateGroup('user', cond, function (err, result) {
                            if (err) {
                                callback(err, err);
                            }
                            else if (result.length > 0) {
                                console.log("all user detail", JSON.stringify(result));
                                callback(null, result);
                            }
                            else {
                                callback(null, []);
                            }
                        });
                    } else {
                        callback("No data Found ", "No data Found ");
                    }

                });


            }
        },
        function (err, showResult) {
            if (err) {
                res.json({ err: 1 });
                return;
            }

            res.json({
                err: 0, response: {
                    'Active Shows': showResult.activeShows, "Upcoming Shows": showResult.upcomingShows,
                    "Around You": showResult.aroundYou, "Seller": showResult.sellerProfile
                }
            })
        }
    )
}


