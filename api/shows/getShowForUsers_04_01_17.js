/**
 * Created by Niranjan on 29/9/16.
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');

var ObjectId = require('mongodb').ObjectID;

/*Including async to make function asynchronous*/
var async=require('async');

exports.getShowForUsers = function(req, res)
{
    if(!req.body.userId)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory user id is missing"});
        return false;
    }

    var recShowData = [];
    var popShowData = [];

    async.parallel(
    {
        shows : function(callback)
        {
            var cond = {$or: [{active : 1},{active : '1'}]};
            var projection= {showName: 1,startDate:1, endDate:1,venue:1,_id:1,imgPath:1,Description: 1};
            var skip =0;
            var limit = 10;
            Utility.SelectWithPagination('shows',cond,projection,skip,limit,function (err, result)
            {
                if (err)
                {
                    callback(null,[]);
                }
                else if(result.length > 0)
                {
                    var showToSend = [];
                    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June",
                        "July", "Aug", "Sep", "Oct", "Nov", "Dec"
                    ];

                    for(var i=0; i<result.length; i++)
                    {
                        var mainDate1 = new Date(parseFloat(result[i]['startDate']));

                        var datee1 = mainDate1.getDate();
                        var month1 = mainDate1.getMonth() + 1;
                        var year1 = mainDate1.getFullYear();

                        if(datee1 < 10)
                            datee1 = "0" + datee1;

                        if(month1 < 10)
                            month1 = "0" + month1;

                        sdate = datee1 + " " + monthNames[mainDate1.getMonth()] + " " + year1;

                        var mainDate = new Date(parseFloat(result[i]['endDate']));

                        var datee = mainDate.getDate();
                        var month = mainDate.getMonth() + 1;
                        var year = mainDate.getFullYear();

                        if(datee < 10)
                            datee = "0" + datee;

                        if(month < 10)
                            month = "0" + month;

                        edate = datee + " " + monthNames[mainDate.getMonth()] + " " + year;

                        showToSend.push({showName: result[i]['showName'],_id: result[i]['_id'],venue: result[i]['venue'],Description: result[i]['Description'],
                            imgPath: result[i]['imgPath'],startDate: sdate,endDate: edate});
                    }
                    callback(null,showToSend);
                }
                else
                {
                    callback(null,[]);
                }
            });
        },
        recentList : function(callback)
        {
            async.parallel(
            {
                posts : function(callback1)
                {
                    var cond = {userId: req.body.userId};
                    var projectD = {showId:1,postId:1};

                    Utility.SelectMatched('recentVisitors',cond,projectD,function (err, wishIds)
                    {
                        if (err)
                        {
                            callback1(null,[]);
                        }
                        else if(wishIds)
                        {
                            var idOfRecentVisits = [];
                            for(var m=0; m<wishIds.length; m++)
                            {
                                idOfRecentVisits.push({_id: ObjectId(wishIds[m]['postId'])});
                                recShowData.push({postId: wishIds[m]['postId'],showId: wishIds[m]['showId']});
                            }
                            callback1(null,idOfRecentVisits);
                        }
                        else
                        {
                            callback1(null,[]);
                        }
                    });
                },
                preferences : function(callback1)
                {
                    var condForPref = {"productType" : "Horse"};
                    Utility.Select('Preferences',condForPref,function (err, prefDetails)
                    {
                        if (err)
                        {
                            callback1(null,[]);
                        }
                        else if(prefDetails.length > 0)
                        {
                            callback1(null,prefDetails);
                        }
                        else
                        {
                            callback1(null,[]);
                        }
                    });
                }
            },
            function(err,results2)
            {
                var postIds = results2.posts;
                //if(results2.posts.wishId)
                //    for(var i=0; i<results2.posts.wishId.length; i++)
                //    {
                //        postIds.push({_id: ObjectId(results2.posts.wishId[i])});
                //    }

                if(postIds.length > 0)
                {
                    var condForPref = [{$match:{$and: [{$or:postIds},{"activeStatus" : "1"}]}},
                        {$lookup:{from:'user',localField:'userId',
                            foreignField:'userId',as:'user'}},{$unwind:'$user'},
                        {$project:{values:1,userId:1,about:'$user.about',name:'$user.name',profilePic:'$user.profilePic'}}];
                    //{"$or" : postIds};

                    console.log("condForPref:   " + JSON.stringify(condForPref));


                    Utility.AggregateGroup('post',condForPref,function (err, wishDetails)
                    {
                        if (err)
                        {
                            callback(null,[]);
                        }
                        else if(wishDetails.length > 0)
                        {
                            console.log(1);
                            var dataToSends = [];
                            var name = "";
                            var image = [];
                            var priceDetails = "";
                            var description = "";
                            var breed = "";

                            for(var j=0; j<wishDetails.length; j++)
                            {
                                var dataToSend = [];

                                for(var k=0; k<results2.preferences.length; k++)
                                {
                                    for(var l=0; l<wishDetails[j]['values'].length; l++)
                                    {
                                        if(results2.preferences[k]['_id'].toString() == wishDetails[j]['values'][l]['pref_id'].toString())
                                        {

                                            if(results2.preferences[k]['preferenceTitle'] == 'Horse Name')
                                                name = wishDetails[j]['values'][l]['options'];

                                            if(results2.preferences[k]['preferenceTitle'] == 'Image')
                                                image = wishDetails[j]['values'][l]['options'];

                                            if(results2.preferences[k]['preferenceTitle'] == 'Price Details')
                                                priceDetails = wishDetails[j]['values'][l]['options'];

                                            if(results2.preferences[k]['preferenceTitle'] == 'Description')
                                                description = wishDetails[j]['values'][l]['options'];

                                            if(results2.preferences[k]['preferenceTitle'] == 'Breed')
                                                breed = wishDetails[j]['values'][l]['options'];

                                            //dataToSend.push({title: results2.preferences[k]['preferenceTitle'],selected : wishDetails[j]['values'][l]['options']});
                                            break;
                                        }
                                    }



                                    var dataInFormat ={};
                                    if(k == (wishDetails[j]['values'].length-1))
                                    {
                                        for(var m=0; m<recShowData.length; m++)
                                        {
                                            if(wishDetails[j]['_id'] == recShowData[m]['postId'])
                                            {
                                                ownerDetail= {name: wishDetails[j]['name'],profilePic: wishDetails[j]['profilePic'],
                                                    about: wishDetails[j]['about'],userId: wishDetails[j]['userId']};
                                                dataInFormat = {name:name,image:image,priceDetails:priceDetails,
                                                    description:description,breed:breed,postId:wishDetails[j]['_id'],
                                                    showId: recShowData[m]['showId'],ownerDetail:ownerDetail};
                                            }
                                        }
                                        dataToSends.push(dataInFormat);
                                    }

                                }
                            }

                            callback(null,dataToSends);
                        }
                        else
                        {
                            console.log(2);
                            callback(null,[]);
                        }
                    });
                }
                else
                {
                    callback(null,[]);
                }


            });
        },
        populars : function(callback)
        {
            console.log("popular");
            async.parallel(
            {
                popular : function(callback1)
                {
                    var cond = [{$group : {_id : "$postId",count: { $sum: 1 },
                        postId:{$addToSet:"$postId"},showId:{$addToSet:"$showId"}}},{$sort:{count:-1}},{$limit:5}];

                    Utility.AggregateGroup('recentVisitors',cond,function (err, popularIds)
                    {
                        if (err)
                        {
                            callback1(null,[]);
                        }
                        else if(popularIds.length > 0)
                        {
                            var idOfPopulars = [];
                            for(var n=0; n<popularIds.length; n++)
                            {
                                idOfPopulars.push({_id: ObjectId(popularIds[n]['postId'][0])});
                                popShowData.push({postId: popularIds[n]['postId'][0],showId: popularIds[n]['showId'][0]});
                            }
                            callback1(null,idOfPopulars);
                        }
                        else
                        {
                            callback1(null,[]);
                        }
                    });
                },
                preferences : function(callback1)
                {
                    var condForPref = {"productType" : "Horse"};
                    Utility.Select('Preferences',condForPref,function (err, prefDetails)
                    {
                        if (err)
                        {
                            callback1(null,[]);
                        }
                        else if(prefDetails.length > [])
                        {
                            callback1(null,prefDetails);
                        }
                        else
                        {
                            callback1(null,[]);
                        }
                    });
                }
            },
            function(err,results2)
            {
                console.log("popular function:  " + JSON.stringify(results2.popular));
                var postIds = results2.popular;
                //if(results2.posts.wishId)
                //    for(var i=0; i<results2.posts.wishId.length; i++)
                //    {
                //        postIds.push({_id: ObjectId(results2.posts.wishId[i])});
                //    }

                if(postIds.length > 0)
                {
                    var condForPref = {"$or" : postIds};

                    var conddd = [{$match:{$or:postIds}},
                        {$lookup:{from:'user',localField:'userId',
                            foreignField:'userId',as:'user'}},{$unwind:'$user'},
                        {$project:{values:1,userId:1,about:'$user.about',name:'$user.name',profilePic:'$user.profilePic'}}];

                    console.dir("conddd:  " + JSON.stringify(conddd));

                    //{$and : [{'values.options.0': {$gt: "17604"}},{'values.options.1':{$lt :"767"}}]}}

                    console.log("condForPref:   " + JSON.stringify(condForPref));
                    Utility.Select('post',condForPref,function (err, popularDetail)
                    {
                        if (err)
                        {
                            callback(null,[]);
                        }
                        else if(popularDetail.length > 0)
                        {
                            console.log(1);
                            var dataToSends = [];
                            for(var j=0; j<popularDetail.length; j++)
                            {
                                var dataToSend = [];
                                var name = "";
                                var image = [];
                                var priceDetails = "";
                                var description = "";

                                //dataToSend.push({postId: popularDetail[j]['_id']});
                                for(var k=0; k<results2.preferences.length; k++)
                                {
                                    for(var l=0; l<popularDetail[j]['values'].length; l++)
                                    {
                                        if(results2.preferences[k]['_id'].toString() == popularDetail[j]['values'][l]['pref_id'].toString())
                                        {
                                            if(results2.preferences[k]['preferenceTitle'] == 'Horse Name')
                                                name = popularDetail[j]['values'][l]['options'];

                                            if(results2.preferences[k]['preferenceTitle'] == 'Image')
                                                image = popularDetail[j]['values'][l]['options'];

                                            if(results2.preferences[k]['preferenceTitle'] == 'Price Details')
                                                priceDetails = popularDetail[j]['values'][l]['options'];

                                            if(results2.preferences[k]['preferenceTitle'] == 'Description')
                                                description = popularDetail[j]['values'][l]['options'];


                                            //dataToSend.push({title: results2.preferences[k]['preferenceTitle'],
                                            //    selected : popularDetail[j]['values'][l]['options']});
                                            break;
                                        }
                                    }

                                    var dataFormatToSend = {};
                                    if(k == (popularDetail[j]['values'].length-1))
                                    {
                                        for(var m=0; m<popShowData.length; m++)
                                        {
                                            if(popularDetail[j]['_id'] == popShowData[m]['postId'])
                                            {
                                                dataFormatToSend = {name:name,image:image,priceDetails:priceDetails,
                                                    description:description,showId:popShowData[m]['showId'],
                                                    postId: popularDetail[j]['_id'],data: dataToSend};
                                            }
                                        }
                                        //dataFormatToSend = {name:name,image:image,priceDetails:priceDetails,
                                        //    description:description,
                                        //    postId: popularDetail[j]['_id'],data: dataToSend};
                                        console.log("Sdfsfsdf");
                                        dataToSends.push(dataFormatToSend);
                                    }

                                }
                            }
                            callback(null,dataToSends);
                        }
                        else
                        {
                            console.log(2);
                            callback(null,[]);
                        }
                    });
                }
                else
                {
                    callback(null,[]);
                }


            });
        }
    },
    function(err,results)
    {
        console.log("executed successfully.");
        res.send({errCode: 0,  Message: "All shows send successfully.",response: {result: results}});
    });
}

