/**
 * Created by Niranjan on 23/9/16.
 * function name:  getShows
 * request: token,userId
 * response: all upcoming shows, recelntly viewed shows, and popular shows.
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');

var ObjectId = require('mongodb').ObjectID;
var async = require('async');
exports.getShowForUsers = function(req, res)
{

    async.parallel(
        {
            postIds : function(callback)
            {
                var cond = {_id : ObjectId(req.body.showId)};

                Utility.SelectOne('shows',cond,function (err, popularDetail)
                {
                    if (err)
                    {
                        callback(null,[]);
                    }
                    else if(popularDetail)
                    {
                        var dataToSend = []
                        for(var i=0; i<popularDetail.posts.length; i++)
                        {
                            dataToSend.push({_id: ObjectId(popularDetail.posts[i]['postId'])});
                        }
                        callback(null,dataToSend);
                    }
                    else
                    {
                        console.log(2);
                        callback(null,[]);
                    }
                });
            }
        },
        function (err, showResult)
        {

        }
    )

    var cond = [{$match:{"active" : "1"}},{$project: {
        showName: 1,startDate : 1,endDate : 1,imgPath:1,Description:1,productType:1,venue:1}}];

    //Utility.SelectWithPagination('shows',cond,projection,skip,limit,function (err, result)
    Utility.AggregateGroup('shows',cond,function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result.length > 0)
        {
            var curDate = Date.now();
            var onGoingShows = [];
            var futureShows = [];
            var archievedShows = [];

            //console.log("result:  " + JSON.stringify(result));

            for(var i=0; i<result.length; i++)
            {
                var sdate = '';
                if(result[i]['startDate'])
                {
                    var mainDate1 = new Date(parseFloat(result[i]['startDate']));

                    var datee1 = mainDate1.getDate();
                    var month1 = mainDate1.getMonth() + 1;
                    var year1 = mainDate1.getFullYear();

                    if(datee1 < 10)
                        datee1 = "0" + datee1;

                    if(month1 < 10)Archived
                        month1 = "0" + month1;

                    sdate = month1 + "/" + datee1 + "/" + year1;

                }

                var edate = '';
                if(result[i]['endDate'])
                {
                    var mainDate = new Date(parseFloat(result[i]['endDate']));

                    var datee = mainDate.getDate();
                    var month = mainDate.getMonth() + 1;
                    var year = mainDate.getFullYear();

                    if(datee < 10)
                        datee = "0" + datee;

                    if(month < 10)
                        month = "0" + month;

                    edate = month + "/" + datee + "/" + year;

                }

                //console.log("result[i]['startDate']:  " + result[i]['startDate']);
                //console.log("curDate:  " + curDate);
                //console.log("result[i]['endDate']:  " + result[i]['endDate']);


                if(parseFloat(result[i]['startDate']) < parseFloat(curDate) &&  parseFloat(curDate) < parseFloat(result[i]['endDate']))
                {
                    onGoingShows.push({showName: result[i]['showName'],
                        startDate: sdate,endDate: edate,image: result[i]['imgPath'],
                        Description: result[i]['Description'],
                        venue: result[i]['venue'],showId: result[i]['_id']});

                }
                else if(parseFloat(result[i]['startDate']) > parseFloat(curDate))
                {
                    futureShows.push({showName: result[i]['showName'],
                        startDate: sdate,endDate: edate,image: result[i]['imgPath'],
                        Description: result[i]['Description'],
                        venue: result[i]['venue'],showId: result[i]['_id']});
                }
                else
                {
                    archievedShows.push({showName: result[i]['showName'],
                        startDate: sdate,endDate: edate,image: result[i]['imgPath'],
                        Description: result[i]['Description'],
                        venue: result[i]['venue'],showId: result[i]['_id']});
                }
            }

            var dataToSend = [];
            if(onGoingShows.length > 0)
            {
                dataToSend.push({'key': 'Active Shows','data': onGoingShows});
            }

            if(futureShows.length > 0)
            {
                dataToSend.push({'key': 'Upcoming Shows','data': futureShows});
            }

            if(archievedShows.length > 0)
            {
                dataToSend.push({'key': 'Archived Shows','data': archievedShows});
            }

            res.send({errCode: 0,  Message: "All shows send successfully.",response:dataToSend});

        }
        else
        {
            res.send({errCode: 1,  errNum: 135, Message: "Currently no product is there, please try after some time."});
        }
    });
}


