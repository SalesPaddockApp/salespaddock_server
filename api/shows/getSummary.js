/**
 * Created by Niranjan on 12/1/17.
 */

var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var async = require('async');

var ObjectId = require('mongodb').ObjectID;

exports.getSummary = function(req, res)
{
    if(!req.body.userId)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory user id is missing"});
        return false;
    }

    //if(!req.body.showId)
    //{
    //    res.send({errCode: 1, errNum: 145, Message: "mandatory post id is missing"});
    //    return false;
    //}

    var showData = [];

    async.parallel(
        {
            preferences : function(callback)
            {
                var condForPref = {"productType" : "Horse"};
                Utility.Select('Preferences',condForPref,function (err, prefDetails)
                {
                    if (err)
                    {
                        callback(null,0);
                    }
                    else if(prefDetails.length > 0)
                    {
                        callback(null,prefDetails);
                    }
                    else
                    {
                        callback(null,0);
                    }
                });
            },
            wishCount : function(callback1)
            {
                var condForWishCount = [{$match:{"hostUserId": req.body.userId}},{$group : {_id : "$postId",count: { $sum: 1 },
                    postId:{$addToSet:"$postId"}}}];
                Utility.AggregateGroup('wishList',condForWishCount,function (err, prefDetails)
                {
                    if (err)
                    {
                        callback1(null,[]);
                    }
                    else if(prefDetails.length > 0)
                    {
                        callback1(null,prefDetails);
                    }
                    else
                    {
                        console.log("3");
                        callback1(null,[]);
                    }
                });
            },
            views : function(callback1)
            {
                var condForView = [{$match:{"hostUserId": req.body.userId}},
                    {"$group":{"_id":"$postId","userId":{"$addToSet":"$userId"}}},
                    {"$project":{"userId":{"$size":"$userId"},"postId":"$_id"}}];
                Utility.AggregateGroup('recentVisitors',condForView,function (err, prefDetails)
                {
                    if (err)
                    {
                        callback1(null,[]);
                    }
                    else if(prefDetails.length > 0)
                    {
                        callback1(null,prefDetails);
                    }
                    else
                    {
                        callback1(null,[]);
                    }
                });
            }
        },
        function(err,results2)
        {
            console.log(1);
            var condForPref = {userId : req.body.userId,activeStatus : "1"};
            Utility.Select('post',condForPref,function (err, wishDetails)
            {
                    if (err)
                    {
                        callback1(null,[]);
                    }
                    else if(wishDetails.length > 0)
                    {
                        var dataToSends = [];
                        for(var j=0; j<wishDetails.length; j++)
                        {

                            //var dataToSend = [];
                            //dataToSend.push({postId: wishDetails[j]['_id']});
                            for(var k=0; k<results2.preferences.length; k++)
                            {

                                for(var l=0; l<wishDetails[j]['values'].length; l++)
                                {
                                    if(results2.preferences[k]['_id'].toString() == wishDetails[j]['values'][l]['pref_id'].toString())
                                    {
                                        if(results2.preferences[k]['preferenceTitle'] == 'Horse Name')
                                        {
                                            if(dataToSends[(dataToSends.length-1)])
                                            {
                                                if(Object.keys(dataToSends[(dataToSends.length-1)]).length == 1)
                                                {
                                                    dataToSends[dataToSends.length -1]['name']= wishDetails[j]['values'][l]['options'][0];
                                                    dataToSends[dataToSends.length -1]['postId']= wishDetails[j]['_id'];
                                                    break;

                                                }
                                                else
                                                {
                                                    dataToSends.push({name: wishDetails[j]['values'][l]['options'][0],postId: wishDetails[j]['_id']});
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                dataToSends.push({name: wishDetails[j]['values'][l]['options'][0],postId: wishDetails[j]['_id']});
                                                break;
                                            }

                                        }

                                        if(results2.preferences[k]['preferenceTitle'] == 'Image')
                                        {

                                            if(dataToSends[(dataToSends.length-1)])
                                            {
                                                if(Object.keys(dataToSends[(dataToSends.length-1)]).length == 2)
                                                {
                                                    if(wishDetails[j]['values'][l]['options'][0]['type'] == 0)
                                                    {
                                                        dataToSends[dataToSends.length -1]['image']= wishDetails[j]['values'][l]['options'][0]['imageUrl'];
                                                        break;
                                                    }
                                                    else if(wishDetails[j]['values'][l]['options'][0]['type'] == 1)
                                                    {
                                                        dataToSends[dataToSends.length -1]['image']= wishDetails[j]['values'][l]['options'][0]['thumbnailUrl'];
                                                        break;
                                                    }
                                                }
                                                else
                                                {
                                                    if(wishDetails[j]['values'][l]['options'][0]['type'] == 0)
                                                    {
                                                        dataToSends.push({'image' : wishDetails[j]['values'][l]['options'][0]['imageUrl']});
                                                        break;
                                                    }
                                                    else if(wishDetails[j]['values'][l]['options'][0]['type'] == 1)
                                                    {
                                                        dataToSends.push({'image' : wishDetails[j]['values'][l]['options'][0]['thumbnailUrl']});
                                                        break;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if(wishDetails[j]['values'][l]['options'][0]['type'] == 0)
                                                {
                                                    dataToSends.push({'image' : wishDetails[j]['values'][l]['options'][0]['imageUrl']});
                                                    break;
                                                }
                                                else if(wishDetails[j]['values'][l]['options'][0]['type'] == 1)
                                                {
                                                    dataToSends.push({'image' : wishDetails[j]['values'][l]['options'][0]['thumbnailUrl']});
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }

                                //if(k == (wishDetails[j]['values'].length-1))
                                //{
                                //    console.log("Sdfsfsdf");
                                //    dataToSends.push(dataToSend);
                                //}
                            }
                        }

                        console.log("dataToSends: " + JSON.stringify(dataToSends));

                        for(var m=0; m<showData.length; m++)
                        {
                            for(var n=0; n<dataToSends.length; n++)
                            {
                                if(dataToSends[n]['postId'].toString() == showData[m]['_id'].toString())
                                {
                                    dataToSends[n]['showName'] = showData[m]['showName'];
                                    dataToSends[n]['showId'] = showData[m]['showId'];
                                    dataToSends[n]['showImage'] = showData[m]['imgPath'];
                                }
                            }
                        }

                        //console.log("views: " + JSON.stringify(results2.views));
                        //console.log("dataToSends: " + JSON.stringify(dataToSends));

                        for(var m=0; m<results2.wishCount.length; m++)
                        {
                            for(var n=0; n<dataToSends.length; n++)
                            {
                                if(dataToSends[n]['postId'].toString() == results2.wishCount[m]['postId'][0].toString())
                                {
                                    dataToSends[n]['wishCount'] =  results2.wishCount[m]['count']
                                }
                            }
                        }

                        for(var m=0; m<results2.views.length; m++)
                        {
                            for(var n=0; n<dataToSends.length; n++)
                            {
                                if(dataToSends[n]['postId'].toString() == results2.views[m]['postId'].toString())
                                {
                                    dataToSends[n]['views'] =  results2.views[m]['userId']
                                }
                            }
                        }

                        console.log("dataToSends FOR GET SUMMARY",req.body.userId,JSON.stringify(dataToSends));
                        res.send({errCode: 0,  Message: "show data send successfully.",response: dataToSends});

                    }
                    else
                    {
                        res.send({errCode: 1, errNum: 153, Message: "no data found."});
                    }
                });

        });
}

