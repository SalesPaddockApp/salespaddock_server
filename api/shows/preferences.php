﻿<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Preferences extends CI_Controller {

    public function __construct() {
        parent::__construct();
      
        
        $this->load->helper('url');
        $this->load->model("preferences_model");
        $this->load->library('session');
//        $this->load->library('excel');
        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
    }

    public function index() {
        $data['dashboard'] = $this->preferences_model->SelectAll();
        $data['pagename'] = "ViewPrefrenceList";
        $this->load->view("V_Dashbord", $data);
    }

    public function addNewPreferences() {
        $this->load->model("producttypes_model");
        $data['dashboard'] = $this->producttypes_model->SelectAll();
        $data['pagename'] = "AddNewPrefrences";
        $this->load->view("V_Dashbord", $data);
    }

    public function editPreferences($id) {
        $data['_id'] = $id;
        $data['dashboard'] = $this->get_onece($id);
        $data['pagename'] = "editPrefrences";
        $this->load->view("V_Dashbord", $data);
    }

    public function InsertPreferences($arg = 0) {
        $this->preferences_model->InsertPreferences();
    }

    public function updatePreferences($_id) {
        $this->preferences_model->updatePreferences($_id);
    }

    public function deletePreference($_id) {
        $this->preferences_model->deletePreferences($_id);
    }

    public function increasePriority($id) {
        $this->preferences_model->increasePriority($id);
    }

    public function decreasePriority($id) {
        $this->preferences_model->decreasePriority($id);
    }

    public function getAll_ajax() {
        $data = $this->preferences_model->getAll_ajax();
    }

    public function get_onece($id) {
        return $this->preferences_model->get_onece($id);
    }

}
