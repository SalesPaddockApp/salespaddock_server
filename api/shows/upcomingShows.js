/**
 * Created by Niranjan on 2/3/17.
 * function name:  upcomingShows
 * request: token,userId
 * response: all upcoming shows, recelntly viewed shows, and popular shows.
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');

var ObjectId = require('mongodb').ObjectID;

exports.upcomingShows = function(req, res)
{
    console.log("upcomingShows:  " + JSON.stringify(req.body));
    if(!req.body.userId)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory user id is missing"});
        return false;
    }

    var pageNum = 0;
    if(req.body.pageNum)
        pageNum = req.body.pageNum;

    var limit = 10;
    var skip = 10 * pageNum;

    var curDate = Date.now();
    var cond = [{$match:{"active" : "1"}},{$project: {
        showName: 1,startDate : 1,endDate : 1,imgPath:1,Description:1,productType:1,venue:1}},
        {$match : {startDate : {$gte : curDate}}},{$skip : skip},{$limit : limit}];

    //Utility.SelectWithPagination('shows',cond,projection,skip,limit,function (err, result)
    Utility.AggregateGroup('shows',cond,function (err, result)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result.length > 0)
        {
            var upcomingShows = [];

            for(var i=0; i<result.length; i++)
            {
                var sdate = '';
                if(result[i]['startDate'])
                {
                    var mainDate1 = new Date(parseFloat(result[i]['startDate']));

                    var datee1 = mainDate1.getDate();
                    var month1 = mainDate1.getMonth() + 1;
                    var year1 = mainDate1.getFullYear();

                    if(datee1 < 10)
                        datee1 = "0" + datee1;

                    if(month1 < 10)
                        month1 = "0" + month1;

                    sdate = month1 + "/" + datee1 + "/" + year1;

                }

                var edate = '';
                if(result[i]['endDate'])
                {
                    var mainDate = new Date(parseFloat(result[i]['endDate']));

                    var datee = mainDate.getDate();
                    var month = mainDate.getMonth() + 1;
                    var year = mainDate.getFullYear();

                    if(datee < 10)
                        datee = "0" + datee;

                    if(month < 10)
                        month = "0" + month;

                    edate = month + "/" + datee + "/" + year;

                }

                upcomingShows.push({showName: result[i]['showName'],
                    startDate: sdate,endDate: edate,image: result[i]['imgPath'],
                    Description: result[i]['Description'],
                    venue: result[i]['venue'],showId: result[i]['_id']});
            }

            res.send({errCode: 0, Message : "data send successfully.", response : upcomingShows});

        }
        else
        {
            res.send({errCode: 1, errNum: 105, Message: "No data found."});
            return false;
        }
    });
}

