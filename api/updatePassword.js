/**
 * Created by Niranjan on 7/3/2017.
 */
const express = require("express"),
    bodyParser = require("body-parser"),
    cors = require("cors");

const app = express();

// Set View Engine
app.engine('html', require('hogan-express'));

// By default, Express will use a generic HTML wrapper (a layout) to render all your pages. If you don't need that, turn it off.
app.set('view options', {layout: true});

// Set the layout page. Layout page needs {{{ yield }}}  where page content will be injected
app.set('layout', 'container');

app.use(bodyParser.json());

app.use(cors({credentials: true, origin: true}));
app.set('views', __dirname + '/');

app.set('view engine', 'html');

var Utility = require('./UtilityFunc');

app.get('/resetPassword/:token',function(req,res)
{
    console.log("resetPassword",JSON.stringify(req.params));
    var condition = {userId : req.params.token};
    Utility.SelectOne('user',condition,function(err,resp)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred, please try again in few hours."});
            return false;
        }
        else if (resp)
        {
            if(resp.changePassword == 1)
                res.render("index", { token : req.params.token });
            else
                res.json({ Error : "Your session has been expired"});
        }
        else
        {
            res.json({ Error : "Your session has been expired"});
            return false;
        }
    })
})


app.post('/resetPassword',function(req,res)
{
    console.log("req.body[1]['value']",req.body[1]['value']);
    console.log("req.body[2]['value']",req.body[2]['value']);
    var condition = {userId : req.body[0]['value']};
    Utility.SelectOne('user',condition,function(err,resp)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred, please try again in few hours."});
            return false;
        }
        else if (resp)
        {
            var dataToUpdate = {password : req.body[1]['value'],changePassword : 0}
            Utility.Update('user',condition,dataToUpdate, function (err, isUpdated) {
                if (err)
                {
                    res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred, please try again in few hours."});
                    return false;
                }
                else
                {
                    res.send({errCode: 0, errNum: 104, Message: "your password is updated successfully."});
                }
            })
        }
        else
        {
            res.send({errCode: 1, errNum: 104, Message: "No user found, please contact to admin"});
            return false;
        }
    })
})

console.log("its started now:  " + 5007);
app.listen(5007)