/**
 * Created by Niranjan on 23/12/16.
 */
var express = require('express');

var bodyParser = require('body-parser');
cors = require('cors');

var multer = require('multer');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'videos/')
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
    }
})

var upload = multer({storage: storage });


var app = express();

app.use(cors());


app.post('/upload', upload.single('photo'), function (req, res, next)
{
    if (req)
        console.log("req.file: " + JSON.stringify(req.file));

    if (res)
    {
        console.log("res.file: " + req.file.path);
        return res.json({ code: 200,
            url:  req.file.path});
    }

});



app.listen(8009, function (err) {
    if (err) {
        console.log(err);
    } else {
        console.log('App listening on port: ' + 8009);
    }
});