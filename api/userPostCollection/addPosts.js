
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;


exports.addPosts = function (req, res) {

    var isNewRecored = false;
    console.log("add posts ", req.body);

    if (!req.body.postIds || !req.body.postIds[0]) {
        res.send({ errCode: 1, errNum: 113, Message: "mandatory postIds  is missing" });
        return false;
    }
    if (!req.body.userId) {
        res.send({ errCode: 1, errNum: 113, Message: "mandatory userId  is missing" });
        return false;
    }
    if (!req.body.userId) {
        res.send({ errCode: 1, errNum: 113, Message: "mandatory userId  is missing" });
        return false;
    }

    if (!req.body.coverPost) {
        res.send({ errCode: 1, errNum: 113, Message: "mandatory coverPost  is missing" });
        return false;
    }

    if (!req.body.name) {
        // res.send({ errCode: 1, errNum: 113, Message: "mandatory name is missing" });
        // return false;
    }

    if (!req.body.collectionId) {
        isNewRecored = true;
        if (!req.body.image) {
            res.send({ errCode: 1, errNum: 113, Message: "mandatory image  is missing" });
            return false;
        }
    }

    var _id = '';

    if (isNewRecored) {


        for (index = 0; index < req.body.postIds.length; index++) {
            req.body.postIds[index] = ObjectId(req.body.postIds[index]);
        }
        let _id = new ObjectId();
        var data = {
            _id: _id,
            name: req.body.name || "",
            image: req.body.image,
            userId: req.body.userId,
            creation: new Date().getTime(),
            coverPost: req.body.postIds[0],
            posts: req.body.postIds
        };


        Utility.Insert('userPostCollection', data, function (err, result) {
            if (err) {
                res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
                return false;
            }
            else {
                res.send({ errCode: 0, Message: "Collection created with posts sucessfully.", "CollectionId": _id });
                return true;
            }
        });
    } else {
        condition = { _id: ObjectId(req.body.collectionId) };

        Utility.Select('userPostCollection', condition, function (err, result) {
            if (err) {
                res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
                return false;
            } else if (result[0]) {

                var posts = [];
                if (result[0].posts) {
                    for (index = 0; index < result[0].posts.length; index++) {
                        posts.push(result[0].posts[index]);
                    }
                }

                for (index = 0; index < req.body.postIds.length; index++) {

                    posts.push(ObjectId(req.body.postIds[index]));


                }

                let coverPost = ObjectId(req.body.coverPost);
                if (posts.length == 1) {
                    coverPost = posts[0];
                }

                condition = { _id: ObjectId(req.body.collectionId) };
                data = { "posts": posts, image: req.body.image, coverPost: coverPost };

                Utility.Update('userPostCollection', condition, data, function (err, result) {
                    if (err) {
                        res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
                        return false;
                    }
                    else {
                        res.send({ errCode: 0, Message: "Post Added sucessfully." });
                        return true;
                    }
                });

            } else {
                res.send({ errCode: 0, Message: "No Collection find." });
                return true;
            }
        })



    }


}