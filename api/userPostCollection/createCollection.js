/**
 * Created by Niranjan on 27/01/2017.
 * function name: deletePost
 * request: postId
 * response: succuss response if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;


exports.createCollection = function (req, res) {
    if (!req.body.name) {
        res.send({ errCode: 1, errNum: 132, Message: "mandatory name is missing" });
        return false;
    }
    if (!req.body.userId) {
        res.send({ errCode: 1, errNum: 113, Message: "mandatory userId  is missing" });
        return false;
    }
    if (!req.body.image) {
        res.send({ errCode: 1, errNum: 113, Message: "mandatory image  is missing" });
        return false;
    }



    var dataToInsert = {
        name: req.body.name,
        userId: req.body.userId,
        image: req.body.image,
        creation: new Date().getTime()
    };
    
    Utility.Insert('userPostCollection', dataToInsert, function (err, result) {
        if (err) {
            res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
            return false;
        }
        else {

            res.send({ errCode: 0, Message: "Your collection created successfully." });
            return true;
        }
    });
}