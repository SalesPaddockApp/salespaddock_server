
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;


exports.deleteCollection = function (req, res) {

    if (!req.body.collectionId) {
        res.send({ errCode: 1, errNum: 113, Message: "mandatory collectionId  is missing" });
        return false;
    }

    var condition = { _id: ObjectId(req.body.collectionId) };

    Utility.Delete('userPostCollection', condition, function (err, result) {
        if (err) {
            res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
            return false;
        }
        else {
            res.send({ errCode: 0, Message: "Collection Deleted sucessfully." });
            return true;
        }
    });
}