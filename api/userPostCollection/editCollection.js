
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;


exports.editCollection = function (req, res) {

    console.log("add posts ", req.body);

    if (!req.body.collectionId) {
        res.send({ errCode: 1, errNum: 113, Message: "mandatory collectionId  is missing" });
        return false;
    }
    if (!req.body.name) {
        res.send({ errCode: 1, errNum: 113, Message: "mandatory name  is missing" });
        return false;
    }
    if (!req.body.coverPost) {
        res.send({ errCode: 1, errNum: 113, Message: "mandatory coverPost  is missing" });
        return false;
    }
    // if (!req.body.coverPostId) {
    //     res.send({ errCode: 1, errNum: 113, Message: "mandatory coverPostId  is missing" });
    //     return false;
    // }
    if (!req.body.image) {
        res.send({ errCode: 1, errNum: 113, Message: "mandatory image  is missing" });
        return false;
    }


    var condition = { _id: ObjectId(req.body.collectionId) };

    // var data = { "name": req.body.name, "coverPost": ObjectId(req.body.coverPostId), image: req.body.image };
    var data = { "name": req.body.name, image: req.body.image, coverPost: ObjectId(req.body.coverPost) };

    Utility.Update('userPostCollection', condition, data, function (err, result) {
        if (err) {
            res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
            return false;
        }
        else {
            res.send({ errCode: 0, Message: "Data Update sucessfully." });
            return true;
        }
    });
}