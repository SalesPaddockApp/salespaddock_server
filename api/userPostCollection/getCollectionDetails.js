/**
 * Created by Niranjan on 27/01/2017.
 * function name: deletePost
 * request: postId
 * response: succuss response if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;


exports.getCollectionDetails = function (req, res) {


    console.log("getCollectionDetails ", req.body);
    if (!req.body.collectionId) {
        res.send({ errCode: 1, errNum: 113, Message: "mandatory collectionId  is missing" });
        return false;
    }
    if (!req.body.userId) {
        res.send({ errCode: 1, errNum: 113, Message: "mandatory userId  is missing" });
        return false;
    }


    var condition =
        [
            { "$match": { _id: ObjectId(req.body.collectionId), userId: req.body.userId } },
            { "$project": { "collectionId": "$_id", "name": 1, "coverPost": 1, "_id": 0, "image": 1, "posts": 1 } },
            {
                "$unwind": {
                    "path": "$posts",
                    "preserveNullAndEmptyArrays": true
                }
            },
            { "$lookup": { "from": "post", "localField": "posts", "foreignField": "_id", "as": "Data" } },
            {
                "$unwind": {
                    "path": "$Data",
                    "preserveNullAndEmptyArrays": true
                }
            }
        ];

    Utility.AggregateGroup('userPostCollection', condition, function (err, result) {
        if (err) {
            res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
            return false;
        }
        else if (result[0]) {

            let postDetails = [];

            if (result[0]["Data"]) {

                for (indexData = result.length - 1; indexData >= 0; indexData--) {
                    let obj = { "_id": result[indexData]["Data"]["_id"] };

                    for (index = 0; index < result[indexData]["Data"]["values"].length; index++) {


                        if (result[indexData]["Data"]["values"][index]["pref_id"] == "57efa0aa00eba37a3dd379cd") {
                            if (result[indexData]["Data"]["values"][index]["options"][0]) {
                                obj["name"] = result[indexData]["Data"]["values"][index]["options"][0];
                            }
                        } else if (result[indexData]["Data"]["values"][index]["pref_id"] == "57f79d6700eba39c7b2f80f1") {
                            if (result[indexData]["Data"]["values"][index]["options"][0]) {
                                obj["image"] = result[indexData]["Data"]["values"][index]["options"][0];
                            }
                        }

                    }
                    postDetails.push(obj);
                }



                let dataToSend = {
                    "name": result[0]["name"],
                    "image": result[0]["image"],
                    "coverPost": result[0]["coverPost"],
                    "collectionId": result[0]["collectionId"],
                    "postDetails": postDetails
                };

                res.send({ errCode: 0, Message: "Collection found successfully", responce: dataToSend });
                return true;

            } else {
                result[0]["image"] = "";
                result[0]["coverPost"] = "";

                res.send({ errCode: 0, Message: "Collection found successfully", responce: result[0] });
                return true;
            }



        }
        else {

            res.send({ errCode: 0, Message: "No have Collection for this User" });
            return true;
        }
    });
}