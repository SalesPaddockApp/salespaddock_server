/**
 * Created by Niranjan on 27/01/2017.
 * function name: deletePost
 * request: postId
 * response: succuss response if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;
var async = require('async');


exports.getCollections = function (req, res) {

    if (!req.body.userId) {
        res.send({ errCode: 1, errNum: 113, Message: "mandatory userId  is missing" });
        return false;
    }

    var condition =
        [
            { "$match": { userId: req.body.userId } },
            { "$project": { "collectionId": "$_id", "name": 1, "coverPost": 1, "image": 1, "posts": 1 } },
            { "$sort": { "_id": -1 } }
        ];

    Utility.AggregateGroup('userPostCollection', condition, function (err, CollectionDetails) {
        if (err) {
            res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
            return false;
        }

        for (index = 0; index < CollectionDetails.length; index++) {
            if (CollectionDetails[index]["posts"] && !CollectionDetails[index]["posts"][0]) {
                CollectionDetails[index]["image"] = "";
            }

        }

        let loopindex = 0;
        async.each(CollectionDetails, (data, loopCallback) => {
            if (data["posts"] && data["posts"][0]) {
                let matchId = true;
                for (index = 0; index < data["posts"].length; index++) {

                    if (data["posts"][index].toString() == data["coverPost"].toString()) {
                        matchId = false;
                    }
                }
                if (matchId) {
                    let postsize = data["posts"].length - 1;
                    Utility.Select("post", { _id: ObjectId(data["posts"][postsize]) }, (err, result) => {

                        if (result && result[0]) {
                            for (index = 0; index < result[0].values.length; index++) {
                                if (result[0].values[index]["pref_id"].toString() == "57f79d6700eba39c7b2f80f1") {

                                    if (result[0].values[index]["options"] && result[0].values[index]["options"][0]) {
                                        CollectionDetails[loopindex]["image"] = (result[0].values[index]["options"][0]["type"] == "0") ? result[0].values[index]["options"][0]["imageUrl"] : result[0].values[index]["options"][0]["thumbnailUrl"];
                                        Utility.Update("userPostCollection", { _id: ObjectId(data["_id"]) }, {
                                            image: CollectionDetails[loopindex]["image"],
                                            coverPost: ObjectId(result[0]["_id"])
                                        },
                                            () => { })
                                    }
                                    loopindex++;
                                    loopCallback(null, 1);
                                }

                            }
                        } else {
                            loopindex++;
                            loopCallback(null, 1);
                        }



                    })


                } else {
                    loopindex++;
                    loopCallback(null, 1);
                }
            } else {
                CollectionDetails[loopindex]["image"] = "";
                loopindex++;
                loopCallback(null, 1);
            }
        }, function (err, result) {

            async.parallel({
                popular: function (callback) {
                    var cond = [{ $match: { "userId": req.body.userId } },
                    { $lookup: { from: "post", localField: "postId", foreignField: "_id", as: "post" } },
                    { $unwind: "$post" }, { $project: { postId: "$postId", values: "$post.values" } }];

                    Utility.AggregateGroup('wishList', cond, function (err, popularDetail) {
                        if (err) {
                            callback(null, []);
                        }
                        else if (popularDetail) {
                            callback(null, popularDetail);
                        }
                        else {
                            console.log(2);
                            callback(null, []);
                        }
                    });
                },
                preferences: function (callback) {
                    var condForPref = { "productType": "Horse" };
                    Utility.Select('Preferences', condForPref, function (err, prefDetails) {
                        if (err) {
                            callback(null, 0);
                        }
                        else if (prefDetails.length > 0) {
                            callback(null, prefDetails);
                        }
                        else {
                            callback(null, 0);
                        }
                    });
                }
            },
                function (err, results) {
                    if (err) {
                        res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
                        return false;
                    }

                    var dataToSends = [];
                    for (var j = 0; j < results.popular.length; j++) {
                        var name = "";
                        var priceDetail = "";
                        var image = "";
                        for (var k = 0; k < results.preferences.length; k++) {
                            for (var l = 0; l < results.popular[j]['values'].length; l++) {
                                if (results.preferences[k]['_id'].toString() == results.popular[j]['values'][l]['pref_id'].toString()) {
                                    switch (results.preferences[k]['preferenceTitle']) {
                                        case 'Horse Name':
                                            name = results.popular[j]['values'][l]['options'][0];
                                            break;

                                        case 'Image':
                                            image = results.popular[j]['values'][l]['options'];
                                            break;

                                        case "Price Details":
                                            priceDetail = results.popular[j]['values'][l]['options'][0];
                                            break;
                                    }


                                    break;
                                }
                                else if (l == (results.popular[j]['values'].length - 1)) {
                                    switch (results.preferences[k]['preferenceTitle']) {
                                        case 'Horse Name':
                                            name = "";
                                            break;

                                        case 'Image':
                                            image = [];
                                            break;

                                        case "Price Details":
                                            priceDetail = "";
                                            break;
                                    }


                                }
                            }

                            if (k == (results.popular[j]['values'].length - 1)) {

                                dataToSends.push({ name: name, image: image, priceDetail: priceDetail, postId: results.popular[j]['postId'] })

                            }
                        }
                    }

                    console.log("==>>CollectionDetails ", CollectionDetails);
                    res.send({ errCode: 0, Message: "post data send successfully.", response: { wishListDetails: dataToSends, CollectionDetails: CollectionDetails } });
                });
        });
    });
}