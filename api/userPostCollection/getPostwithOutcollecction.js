/**
 * Created by Niranjan on 27/01/2017.
 * function name: deletePost
 * request: postId
 * response: succuss response if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;


exports.getPostwithOutcollecction = function (req, res) {

    if (!req.body.userId) {
        res.send({ errCode: 1, errNum: 113, Message: "mandatory userId  is missing" });
        return false;
    }
    if (!req.body.collectionId) {
        res.send({ errCode: 1, errNum: 113, Message: "mandatory collectionId  is missing" });
        return false;
    }

    let condition = { "_id": ObjectId(req.body.collectionId) };

    Utility.SelectOne("userPostCollection", condition, (err, dataA) => {
        if (err) {
            res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
            return false;
        }

        let postIds = [];
        if (dataA && dataA["posts"]) {
            postIds = dataA["posts"];
        }

        condition = [
            { "$match": { "userId": req.body.userId, "postId": { $nin: postIds } } },
            { "$lookup": { "from": "post", "localField": "postId", "foreignField": "_id", "as": "Data" } },
            {"$unwind":"$Data"},
            { "$project": { "postId": 1, "hostUserId": 1, "userId": 1, "Data": 1 } },
            { "$sort": { "_id": -1 } }
        ];

        console.log("condition ", JSON.stringify(condition));
        Utility.AggregateGroup('wishList', condition, function (err, result) {
            if (err) {
                res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
                return false;
            }
            else if (result[0]) {

                let postDetails = [];

                if (result[0]["Data"]) {

                    for (indexData = 0; indexData < result.length; indexData++) {
                        let obj = { "_id": result[indexData]["Data"]["_id"], userId: result[indexData]["userId"] };

                        for (index = 0; index < result[indexData]["Data"]["values"].length; index++) {
                            if (result[indexData]["Data"]["values"][index]["pref_id"] == "57efa0aa00eba37a3dd379cd") {
                                if (result[indexData]["Data"]["values"][index]["options"][0]) {
                                    obj["name"] = result[indexData]["Data"]["values"][index]["options"][0];
                                }
                            } else if (result[indexData]["Data"]["values"][index]["pref_id"] == "57f79d6700eba39c7b2f80f1") {
                                if (result[indexData]["Data"]["values"][index]["options"][0]) {
                                    obj["image"] = result[indexData]["Data"]["values"][index]["options"][0];
                                }
                            }
                        }
                        postDetails.push(obj);
                    }




                }
                res.send({ errCode: 0, Message: "data found successfully", responce: postDetails });
                return true;
            }
            else {

                res.send({ errCode: 0, Message: "No have data for this User" });
                return true;
            }
        });
    })




}