
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;


exports.removePostFromCollection = function (req, res) {

    if (!req.body.postId) {
        res.send({ errCode: 1, errNum: 113, Message: "mandatory postId  is missing" });
        return false;
    }
    if (!req.body.collectionId) {
        res.send({ errCode: 1, errNum: 113, Message: "mandatory collectionId  is missing" });
        return false;
    }

    var condition = { _id: ObjectId(req.body.collectionId) };
    var data = { "posts": ObjectId(req.body.postId) }

    Utility.UpdateField('userPostCollection', condition, data, function (err, result) {
        if (err) {
            res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
            return false;
        }
        else {
            res.send({ errCode: 0, Message: "Post Deleted sucessfully." });
            return true;
        }
    });
}