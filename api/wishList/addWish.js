/**
 * Created by Niranjan on 19/9/16.
 * function name: addWish
 * request: token,userId,wishId
 * response: succuss response if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var ObjectId = require('mongodb').ObjectID;

exports.addWish = function(req, res)
{
    console.log("addWish: " + JSON.stringify(req.body));
    if(!req.body.userId)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory user id is missing"});
        return false;
    }

    if(!req.body.postId)
    {
        res.send({errCode: 1, errNum: 136, Message: "mandatory wish is missing"});
        return false;
    }

    var timestamp = Date.now();
    var dataToPush = {postId: ObjectId(req.body.postId),timestamp: timestamp,
        userId: req.body.userId,hostUserId : req.body.hostUserId};

    var cond = {postId: ObjectId(req.body.postId),userId: req.body.userId};
    Utility.SelectOne('wishList',cond,function (err, result1)
    {
        if (err)
        {
            res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
            return false;
        }
        else if(result1)
        {

            res.send({errCode: 0,  Message: "your wish saved successfully.",response: {}});
            return true;
        }
        else
        {
            Utility.Insert('wishList',dataToPush,function (err, result)
            {
                if (err)
                {
                    res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
                    return false;
                }
                else if(result)
                {

                    res.send({errCode: 0,  Message: "your wish saved successfully.",response: {}});
                    return true;
                }
                else
                {
                    res.send({errCode: 1, errNum: 135, Message: "Currently no product is there, please try after some time."});
                    return true;
                }
            });
        }
    })

}






