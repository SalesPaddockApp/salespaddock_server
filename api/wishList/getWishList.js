/**
 * Created by Niranjan on 30/9/16.
 */

var exports = module.exports = {};
var Utility = require('../UtilityFunc');

var ObjectId = require('mongodb').ObjectID;

/*Including async to make function asynchronous*/
var async=require('async');

exports.getWishList = function(req, res)
{
    if(!req.body.userId)
    {
        res.send({errCode: 1, errNum: 132, Message: "mandatory user id is missing"});
        return false;
    }

    //if(!req.body.postId)
    //{
    //    res.send({errCode: 1, errNum: 145, Message: "mandatory post id is missing"});
    //    return false;
    //}

    async.parallel(
        {
            popular : function(callback)
            {
                var cond = [{$match: {"userId" : req.body.userId}},
                    {$lookup : {from : "post", localField : "postId", foreignField : "_id", as : "post"}},
                    {$unwind : "$post"},{$project : {postId : "$postId",values : "$post.values"}}];

                Utility.AggregateGroup('wishList',cond,function (err, popularDetail)
                {
                    if (err)
                    {
                        callback(null,[]);
                    }
                    else if(popularDetail)
                    {
                        callback(null,popularDetail);
                    }
                    else
                    {
                        console.log(2);
                        callback(null,[]);
                    }
                });
            },
            preferences : function(callback)
            {
                var condForPref = {"productType" : "Horse"};
                Utility.Select('Preferences',condForPref,function (err, prefDetails)
                {
                    if (err)
                    {
                        callback(null,0);
                    }
                    else if(prefDetails.length > 0)
                    {
                        callback(null,prefDetails);
                    }
                    else
                    {
                        callback(null,0);
                    }
                });
            }
        },
        function(err,results)
        {
            if (err)
            {
                res.send({errCode: 1, errNum: 104, Message: "Unknown error occurred"});
                return false;
            }

            console.log(1);
            var dataToSends = [];
            for(var j=0; j<results.popular.length; j++)
            {
                var name = "";
                var priceDetail = "";
                var image = "";
                for(var k=0; k<results.preferences.length; k++)
                {
                    for(var l=0; l<results.popular[j]['values'].length; l++)
                    {
                        if(results.preferences[k]['_id'].toString() == results.popular[j]['values'][l]['pref_id'].toString())
                        {
                            switch(results.preferences[k]['preferenceTitle'])
                            {
                                case 'Horse Name' :
                                    name = results.popular[j]['values'][l]['options'][0];
                                    break;

                                case 'Image' :
                                    image =  results.popular[j]['values'][l]['options'];
                                    break;

                                case "Price Details" :
                                    priceDetail =  results.popular[j]['values'][l]['options'][0];
                                    break;
                            }

                            //if(results.preferences[k]['preferenceTitle'] == 'Horse Name')
                            //{
                            //    name = results.popular[j]['values'][l]['options'][0]
                            //}
                            //
                            //if(results.preferences[k]['preferenceTitle'] == 'Image')
                            //{
                            //    image['title'] = results.preferences[k]['preferenceTitle'];
                            //    image['options'] =  results.popular[j]['values'][l]['options'];
                            //    image['prirority'] = results.preferences[k]['prirority'];
                            //    image['onCreateScreen'] = results.preferences[k]['onCreateScreen'];
                            //    image['unit'] = results.preferences[k]['unit'];
                            //    image['values'] = results.preferences[k]['optionsValue'];
                            //    image['id'] = results.preferences[k]['_id'];
                            //}
                            //
                            //if(results.preferences[k]['preferenceTitle'] == "Price Details")
                            //{
                            //    priceDetail =  results.popular[j]['values'][l]['options'][0];
                            //}
                            //else
                            //{
                            //    dataToSend.push({title: results.preferences[k]['preferenceTitle'],
                            //        options : results.popular[j]['values'][l]['options'],values : results.preferences[k]['optionsValue'],
                            //        type: results.preferences[k]['typeOfPreference'],prirority: results.preferences[k]['prirority'],
                            //        onCreateScreen: results.preferences[k]['onCreateScreen'],unit: results.preferences[k]['unit'],
                            //        id: results.preferences[k]['_id']});
                            //}
                            break;
                        }
                        else if(l == (results.popular[j]['values'].length-1))
                        {
                            switch(results.preferences[k]['preferenceTitle'])
                            {
                                case 'Horse Name' :
                                    name = "";
                                    break;

                                case 'Image' :
                                    image =  [];
                                    break;

                                case "Price Details" :
                                    priceDetail =  "";
                                    break;
                            }

                            //if(results.preferences[k]['preferenceTitle'] == 'Horse Name')
                            //{
                            //    name = "";
                            //}
                            //
                            //if(results.preferences[k]['preferenceTitle'] == 'Image')
                            //{
                            //    image['title'] = results.preferences[k]['preferenceTitle'];
                            //    image['options'] = [];
                            //    image['prirority'] = results.preferences[k]['prirority'];
                            //    image['onCreateScreen'] = results.preferences[k]['onCreateScreen'];
                            //    image['unit'] = results.preferences[k]['unit'];
                            //    image['values'] = results.preferences[k]['optionsValue'];
                            //    image['id'] = results.preferences[k]['_id'];
                            //}
                            //else
                            //{
                            //    dataToSend.push({title: results.preferences[k]['preferenceTitle'],
                            //        options : [],values : results.preferences[k]['optionsValue'],
                            //        type: results.preferences[k]['typeOfPreference'],prirority: results.preferences[k]['prirority'],
                            //        onCreateScreen: results.preferences[k]['onCreateScreen'],unit: results.preferences[k]['unit'],
                            //        id: results.preferences[k]['_id']});
                            //}
                        }
                    }

                    if(k == (results.popular[j]['values'].length-1))
                    {

                        dataToSends.push({name: name,image: image,priceDetail : priceDetail, postId: results.popular[j]['postId']})

                    }
                }
            }


            console.dir("dataToSends:  " +JSON.stringify(dataToSends));
            res.send({errCode: 0,  Message: "post data send successfully.",response: {result: dataToSends}});
        });
}
