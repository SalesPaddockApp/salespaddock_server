/**
 * Created by Dipen on 19/9/16.
 * function name: removeWish
 * request: token,userId,wishId
 * response: succuss response if no error else error accordingly
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');

var ObjectId = require('mongodb').ObjectID;

exports.removeWish = function (req, res) {
    if (!req.body.userId) {
        res.send({ errCode: 1, errNum: 132, Message: "mandatory user id is missing" });
        return false;
    }

    if (!req.body.postId) {
        res.send({ errCode: 1, errNum: 136, Message: "mandatory wish is missing" });
        return false;
    }


    let condition = { userId: req.body.userId };
    let dataToPull = { "posts": ObjectId(req.body.postId) };
    console.log("condition ", condition)

    Utility.UpdateField("userPostCollection", condition, dataToPull, (err, result) => {
        if (err) {
            res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
            return false;
        }
    });

    var cond = { postId: ObjectId(req.body.postId), userId: req.body.userId };
    //var dataToDelete = {wishId: req.body.wishId};
    Utility.Delete('wishList', cond, function (err, result) {
        if (err) {
            res.send({ errCode: 1, errNum: 104, Message: "Unknown error occurred" });
            return false;
        }
        else if (result) {

            res.send({ errCode: 0, Message: "your wish removed successfully.", response: {} });
            return true;
        }
        else {
            res.send({ errCode: 1, errNum: 135, Message: "Currently no product is there, please try after some time." });
            return true;
        }
    });
}






