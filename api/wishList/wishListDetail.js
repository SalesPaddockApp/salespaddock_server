/**
 * Created by Niranjan on 12/10/16.
 */
var exports = module.exports = {};
var Utility = require('../UtilityFunc');
var async = require('async');

var ObjectId = require('mongodb').ObjectID;

exports.wishListDetail = function(req, res)
{
    //if(!req.body.userId)
    //{
    //    res.send({errCode: 1, errNum: 132, Message: "mandatory user id is missing"});
    //    return false;
    //}

    if(!req.body.postIds)
    {
        res.send({errCode: 1, errNum: 145, Message: "mandatory show id is missing"});
        return false;
    }

    async.parallel(
        {
            postIds : function(callback)
            {
                var dataToSend = []
                for(var p=0; p<req.body.postIds.length; p++)
                {
                    dataToSend.push({_id: ObjectId(req.body.postIds[p])});
                }
                callback(null,dataToSend);
            },
            preferences : function(callback)
            {
                var condForPref = {"productType" : "Horse"};
                Utility.Select('Preferences',condForPref,function (err, prefDetails)
                {
                    if (err)
                    {
                        callback(null,0);
                    }
                    else if(prefDetails.length > 0)
                    {
                        callback(null,prefDetails);
                    }
                    else
                    {
                        callback(null,0);
                    }
                });
            }
        },
        function(err,results2)
        {
            console.log(1);
            //var condForPref = {"$or" : results2.postIds};
            var condForPref = [{$match:{$or:results2.postIds}},
                {$lookup:{from:'user',localField:'userId',
                    foreignField:'userId',as:'user'}},{$unwind:'$user'},
                {$project:{values:1,userId:1,about:'$user.about',name:'$user.name',profilePic:'$user.profilePic'}}];

            console.log("condForPref: " + JSON.stringify(condForPref));

            if(results2.postIds.length > 0)
            {
                Utility.AggregateGroup('post',condForPref,function (err, wishDetails)
                {
                    if (err)
                    {
                        res.send({errCode: 1, errNum: 135, Message: "Currently no product is there, please try after some time."});
                    }
                    else if(wishDetails.length > 0)
                    {
                        var dataToSends = [];
                        for(var j=0; j<wishDetails.length; j++)
                        {
                            var dataToSend = [];

                            var name = "";
                            var image = [];
                            var priceDetails = "";
                            var description = "";
                            var breed = "";

                            //dataToSend.push({postId: wishDetails[j]['_id']});
                            for(var k=0; k<results2.preferences.length; k++)
                            {
                                if(wishDetails[j]['values'].length == results2.preferences.length)
                                {
                                    for(var l=0; l<wishDetails[j]['values'].length; l++)
                                    {
                                        console.log("j: " + j + "  k:  " + k + "  l:  " + l);
                                        if(results2.preferences[k]['_id'].toString() == wishDetails[j]['values'][l]['pref_id'].toString())
                                        {
                                            if(results2.preferences[k]['preferenceTitle'] == 'Horse Name')
                                                name = wishDetails[j]['values'][l]['options'];

                                            if(results2.preferences[k]['preferenceTitle'] == 'Image')
                                                image = wishDetails[j]['values'][l]['options'];

                                            if(results2.preferences[k]['preferenceTitle'] == 'Price Details')
                                                priceDetails = wishDetails[j]['values'][l]['options'];

                                            if(results2.preferences[k]['preferenceTitle'] == 'Description')
                                                description = wishDetails[j]['values'][l]['options'];

                                            if(results2.preferences[k]['preferenceTitle'] == 'Breed')
                                                breed = wishDetails[j]['values'][l]['options'];

                                            //dataToSend.push({title: results2.preferences[k]['preferenceTitle'],selected : wishDetails[j]['values'][l]['options']});
                                            break;
                                        }
                                    }

                                    if(k == (wishDetails[j]['values'].length-1))
                                    {
                                        var profilePic = "";
                                        if(wishDetails[j]['profilePic'])
                                            profilePic = wishDetails[j]['profilePic'];

                                        var about = "";
                                        if(wishDetails[j]['about'])
                                            about = wishDetails[j]['about'];

                                        ownerDetail= {name: wishDetails[j]['name']['fName'],profilePic: profilePic,
                                            about: about,userId: wishDetails[j]['userId']};
                                        dataInFormat = {name:name,image:image,priceDetails:priceDetails,
                                            description:description,breed: breed,postId:wishDetails[j]['_id'],
                                            ownerDetail:ownerDetail};
                                        dataToSends.push(dataInFormat);
                                    }
                                }
                                else
                                {
                                    console.log("else part get show detail");
                                    continue;
                                }


                            }
                        }
                        res.send({errCode: 0,  Message: "show data send successfully.",response: dataToSends});
                    }
                    else
                    {
                        res.send({errCode: 1, errNum: 135, Message: "Currently no product is there, please try after some time."});
                    }
                });
            }
            else
            {
                res.send({errCode: 1, errNum: 135, Message: "Currently no product is there, please try after some time."});
            }

        });
}

